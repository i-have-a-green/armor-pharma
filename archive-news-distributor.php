<?php
$user = get_current_user_id();
if($user == 0){
  wp_redirect( home_url(), 302 );
}
if(!in_array('administrator', $current_user->roles) && !in_array('distributor', $current_user->roles)){
  wp_redirect( home_url(), 302 );
}
get_header();
?>
<div id="content-header">
  <div class="wrapper">
    <?php the_breadcrumb();?>
  </div>
</div>
<main id="main" role="main" class="wrapper">
  <div>
    <header class="wrapper" id="header-page">
      <?php $p = get_page_by_path("quality/distributor-area");  ?>
      <h1 class="page-title degrade" id="h1distributor"><?php echo get_the_title( $p );?></h1>
    </header>
    <div id="header-quality">
      <div id="header-quality-content">
        <div>
          <?php echo $p->post_content; ?>
        </div>
      </div>
      <ul id="menu-menu-quality" class="main-menu">
        <li id="menu-item-574" class="menu-item menu-documentation current_page_item active"><a href="#h1distributor"><?php _e("Documentation","armor-pharma");?></a></li>
        <li id="menu-item-574" class="menu-item menu-news current_page_item active"><a href="#h2news"><?php _e("News","armor-pharma");?></a></li>
      </ul>
    </div>


    <section class="entry-content wrapper" itemprop="articleBody">
      <h2><?php _e("Documentation","armor-pharma");?></h2>
      <div id="">
        <div>
          <strong><?php _e("Get here all documentations you might need !","armor-pharma");?></strong>
        </div>
      </div>
      <div id="files">
      <?php
      $categorys = get_terms( 'category-doc-distributor' );
      foreach ($categorys as $category) :
        $args = array(
          "posts_per_page"=> -1,
          'post_type' => 'doc-distributor',
          'tax_query' => array(
            array(
              'taxonomy' => 'category-doc-distributor',
              'field'    => 'id',
              'terms'    => $category->term_id,
            )
          ),
        );


        $query = new WP_Query($args);
        if ( $query->have_posts() ) :
          echo '<div class="listing-product form"><h2>'.$category->name.'</h2>';
          echo '<hr class="hrTriangle" />';
          while ( $query->have_posts() ) :
            $query->the_post();
            echo '<p><a href="'.get_field("fichier").'" download="'.basename(str_replace(" ","-",get_the_title())).'.pdf"><span class="picto picto-download"></span>'.get_the_title().'</a></p>';
          endwhile;
          echo '</div>';
          wp_reset_postdata();
        endif;
      endforeach;
      ?>
      </div>
    </section>
    <hr>
    <section id="news">
      <h2 id="h2news"><?php _e("News","armor-pharma");?></h2>
			<?php if (have_posts()) :?>
				<div id="listing-article">
					<?php while ( have_posts() ) : the_post();?>
						<div class="article">
              <h2><?php the_title();?></h2>
              <div class="thumbnail">
                <?php the_post_thumbnail('wpgreen-400'); ?>
              </div>
              <div class="content">
                <p>
                  <b><?php echo get_the_date();?></b>
                </p>
                <?php the_content();?>
              </div>
            </div>
            <hr />
					<?php endwhile;?>
					<div id="pagination">
						<?php wpgreen_page_navi();?>
					</div>
				</div>
			<?php endif; ?>
		</section>
	</div>
</main>
<?php get_footer(); ?>
