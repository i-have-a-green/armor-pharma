<?php
/*
Template Name: Cart
*/
get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post();?>
  <div id="content-header">
    <div class="wrapper">
      <?php the_breadcrumb();?>
    </div>
  </div>
	<main id="main" role="main" class="wrapper">
		<article>
			<header class="wrapper" id="header-page">
				<h1 class="page-title degrade"><?php the_title();?></h1>
        <p>
          <?php _e("Our samples consists of 500g boxes of lactose. All our samples are delivered with MSDS, specification, and CoA.","armor-pharma");?>
        </p>
			</header>
			<div id="tabCart" class="form">
			    <div class="id_lactose head"><?php _e("Products","armor-pharma");?></div>
          <!--<div class="quality head"><?php _e("Quality statement","armor-pharma");?></div>-->
          <div class="quantity head"><?php _e("Quantity","armor-pharma");?></div>
          <div class="sample head"><?php _e("Sample(s) destination","armor-pharma");?></div>
          <div class="specifics head"><?php _e("Specific needs","armor-pharma");?></div>
          <div class="trash head">&nbsp;</div>
          <?php
          $metaCart = get_user_meta(get_current_user_id(), 'cart', true);
          $metaCart = json_decode($metaCart);
          $tabId = [];
          if(empty($metaCart) && sizeof($metaCart) == 0):?>
            <p class="text-center" id="bagEmpty">
              <?php _e("Your bag is empty. Don't hesitate to get free sample or order your CoA","armor-pharma");?><br />
              <a href="<?php echo home_url();?>" class="button">
                <?php _e("Back Home","armor-pharma");?>
              </a>
            </p>
          <?php
          else:
            $i = 0;
            foreach ($metaCart as $lactose):
              $post = get_post( $lactose->id_lactose );
              setup_postdata( $post );
              $tabId[] = get_the_id();
              ?>
              <div class="id_lactose <?php echo ($i%2==0)?'bg':'';?>" data-lactose="<?php the_id();?>">
                <?php echo get_the_lactose();?>
              </div>
              <!--<div class="quality grid2 <?php echo ($i%2==0)?'bg':'';?>"> data-lactose="<?php the_id();?>"
                <div class="onlyMobile">
                  <?php _e("Quality statement","armor-pharma");?>
                </div>
                <div>
                  <?php the_field("quality"); ?>
                </div>

              </div>-->
              <div class="quantity grid2 <?php echo ($i%2==0)?'bg':'';?>" data-lactose="<?php the_id();?>">
                <div class="onlyMobile"><?php _e("Quantity","armor-pharma");?></div>
                <div>
                  <select id="select_quantity_<?php the_id();?>" value="<?php echo $lactose->quantity;?>">
                    <option value="0.5" <?php echo ($lactose->quantity == "0.5")? 'selected':'';?>>0,5 Kg</option>
                    <option value="1" <?php echo ($lactose->quantity == "1")? 'selected':'';?>>1 Kg</option>
                    <option value="1.5" <?php echo ($lactose->quantity == "1.5")? 'selected':'';?>>1,5 Kg</option>
                    <option value="2" <?php echo ($lactose->quantity == "2")? 'selected':'';?>>2 Kg</option>
                    <option value="2.5" <?php echo ($lactose->quantity == "2.5")? 'selected':'';?>>2,5 Kg</option>
                    <option value="3" <?php echo ($lactose->quantity == "3")? 'selected':'';?>>3 Kg</option>
                  </select>
                </div>
              </div>
              <div class="sample grid2 <?php echo ($i%2==0)?'bg':'';?>" data-lactose="<?php the_id();?>">
                <div class="onlyMobile"><?php _e("Sample destination","armor-pharma");?></div>
                <div>
                  <select id="select_formulation_<?php the_id();?>" value="<?php echo $lactose->formulation;?>">
                    <option value="Formulation test" <?php echo ($lactose->formulation == "Formulation test")? 'selected':'';?>><?php _e("Formulation test","armor-pharma");?></option>
                    <option value="product approval" <?php echo ($lactose->formulation == "product approval")? 'selected':'';?>><?php _e("product approval","armor-pharma");?></option>
                    <option value="Other" <?php echo ($lactose->formulation == "Other")? 'selected':'';?>><?php _e("Other","armor-pharma");?></option>
                  </select>
                </div>
                <div class="onlyMobile"><?php _e("Dosage form process :","armor-pharma");?></div>
                <div>
                  <select id="select_tablet_<?php the_id();?>" value="<?php echo $lactose->tablet;?>">
                    <option value="Tablet (wet granulation)" <?php echo ($lactose->tablet == "Tablet (wet granulation)")? 'selected':'';?>><?php _e("Tablet (wet granulation)","armor-pharma");?></option>
                    <option value="Tablet (dry granulation)" <?php echo ($lactose->tablet == "Tablet (dry granulation)")? 'selected':'';?>><?php _e("Tablet (dry granulation)","armor-pharma");?></option>
                    <option value="Tablet (direct compression)" <?php echo ($lactose->tablet == "Tablet (direct compression)")? 'selected':'';?>><?php _e("Tablet (direct compression)","armor-pharma");?></option>
                    <option value="Sachet" <?php echo ($lactose->tablet == "Sachet")? 'selected':'';?>><?php _e("Sachet","armor-pharma");?></option>
                    <option value="Capsule" <?php echo ($lactose->tablet == "Capsule")? 'selected':'';?>><?php _e("Capsule","armor-pharma");?></option>
                    <option value="DPI" <?php echo ($lactose->tablet == "DPI")? 'selected':'';?>><?php _e("DPI","armor-pharma");?></option>
                    <option value="Other" <?php echo ($lactose->tablet == "Other")? 'selected':'';?>><?php _e("Other","armor-pharma");?></option>
                  </select>
                </div>
              </div>
              <div class="specifics grid2 <?php echo ($i%2==0)?'bg':'';?>" data-lactose="<?php the_id();?>">
                <div class="onlyMobile"><?php _e("Specific needs","armor-pharma");?></div>
                <div>
                  <textarea id="specifics_<?php the_id();?>" placeholder="<?php _e("Please precise your specific needs (samples from different batches, documents to be added with your delivery...)","armor-pharma");?>"><?php echo $lactose->specifics;?></textarea>
                </div>
              </div>
              <div class="trash <?php echo ($i%2==0)?'bg':'';?>" data-lactose="<?php the_id();?>">
                <span class="picto picto-trash" data-value="<?php the_id();?>"></span>
              </div>
            <?php
            $i++;
            endforeach;
            wp_reset_postdata();
            ?>
            <div class="id_lactose <?php echo ($i%2==0)?'bg':'';?>" data-lactose="new">
            <select id="newLactose" class="new" data-lactose-id="">
              <option value="" selected><?php _e("Select a lactose","armor-pharma");?></option>
            <?php $posts = get_posts(array(
              "posts_per_page"=>-1,
              "post_type"=>"lactose",
              "post__not_in" => $tabId,
              /*'tax_query'      => array(
                  array(
                      'taxonomy' => 'category-lactose',
                      'terms' => array('excipure'),
                      'field' => 'slug',
                      'operator' => 'NOT IN',
                  )
              ),*/
            ));
            foreach ($posts as $post) : setup_postdata( $post );
              echo '<option value="'.get_the_id().'">'.get_the_lactose().'</option>';
            endforeach;
            wp_reset_postdata();
            ?>
          </select>
            </div>
            <div class="quantity grid2 <?php echo ($i%2==0)?'bg':'';?>" data-lactose="new">
              <div class="onlyMobile"><?php _e("Quantity","armor-pharma");?></div>
              <div>
                <select id="select_quantity_new" value="" class="new">
                  <option value="0.5" >0,5 Kg</option>
                  <option value="1" >1 Kg</option>
                  <option value="1.5" >1,5 Kg</option>
                  <option value="2" >2 Kg</option>
                  <option value="2.5" >2,5 Kg</option>
                  <option value="3" >3 Kg</option>
                </select>
              </div>
            </div>
            <div class="sample grid2 <?php echo ($i%2==0)?'bg':'';?>" data-lactose="new">
              <div class="onlyMobile"><?php _e("Sample destination","armor-pharma");?></div>
              <div>
                <select id="select_formulation_new" value="" class="new">
                  <option value="Formulation test" ><?php _e("Formulation test","armor-pharma");?></option>
                  <option value="product approval" ><?php _e("product approval","armor-pharma");?></option>
                  <option value="Other" ><?php _e("Other","armor-pharma");?></option>
                </select>
              </div>
              <div class="onlyMobile"><?php _e("Dosage form process :","armor-pharma");?></div>
              <div>
                <select id="select_tablet_new" value="" class="new">
                  <option value="Tablet (wet granulation)" ><?php _e("Tablet (wet granulation)","armor-pharma");?></option>
                  <option value="Tablet (dry granulation)" ><?php _e("Tablet (dry granulation)","armor-pharma");?></option>
                  <option value="Tablet (direct compression)" ><?php _e("Tablet (direct compression)","armor-pharma");?></option>
                  <option value="Sachet" ><?php _e("Sachet","armor-pharma");?></option>
                  <option value="Capsule" ><?php _e("Capsule","armor-pharma");?></option>
                  <option value="DPI" ><?php _e("DPI","armor-pharma");?></option>
                  <option value="Other" ><?php _e("Other","armor-pharma");?></option>
                </select>
              </div>
            </div>
            <div class="specifics grid2 <?php echo ($i%2==0)?'bg':'';?>" data-lactose="new">
              <div class="onlyMobile"><?php _e("Specific needs","armor-pharma");?></div>
              <div>
                <textarea id="specifics_new" class="new" placeholder="<?php _e("Please precise your specific needs (samples from different batches, documents to be added with your delivery...)","armor-pharma");?>"></textarea>
              </div>
            </div>
            <div class="add <?php echo ($i%2==0)?'bg':'';?>" data-lactose="new">
              <span class="picto picto-add" data-value="new"></span>
            </div>
        </div>
        <div id="contButton">
          <em>* We offer FREE samples, up to 3 kg/product. For higher quantities, we will revert to you in the shortest delay</em>
          <a href="<?php echo get_the_permalink(get_page_by_path("my-cart-shipment"));?>" class="button" id="getMySample"><?php _e("GET MY Samples","armor-pharma");?></a>
        </div>
        <?php
        endif;?>
      </div>
      <div id="cartIllustrationFooter" class="wrapper">
        <div>
          <?php

            $image = get_field('image1');
            $size = 'large'; // (thumbnail, medium, large, full or custom size)
            if( $image ) {
              echo wp_get_attachment_image( $image, $size );
            }
          ?>
          <!--<img src="<?php echo get_stylesheet_directory_uri() . '/assets/css/images/process5.png';?>">-->
        </div>
        <div>
          <?php
            $image = get_field('image2');
            $size = 'large'; // (thumbnail, medium, large, full or custom size)
            if( $image ) {
              echo wp_get_attachment_image( $image, $size );
            }
          ?>
          <!--<img src="<?php echo get_stylesheet_directory_uri() . '/assets/css/images/process6.png';?>">-->
        </div>
        <div>
          <?php
            $image = get_field('image3');
            $size = 'large'; // (thumbnail, medium, large, full or custom size)
            if( $image ) {
              echo wp_get_attachment_image( $image, $size );
            }
          ?>
          <!--<img src="<?php echo get_stylesheet_directory_uri() . '/assets/css/images/process6.png';?>">-->
        </div>
        <!--<div id="illuText">
          <div>
            <?php the_content();?>
          </div>
        </div>
      </div>-->
		</article>
	</main>
<?php endwhile; endif; ?>
<?php get_footer(); ?>
