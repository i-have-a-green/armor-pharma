<?php
/*
Template Name: Design video
*/
get_header();
if (have_posts()) :  while ( have_posts() ) : the_post();
?>
<div id="content-header">
  <div class="wrapper">
    <?php the_breadcrumb();?>
  </div>
</div>
<div class="text-center">
  <h1 class="degrade"><?php _e("Design the lactose <b>you</b> like","armor-pharma");?></h1>
</div>
<?php
//set_query_var( 'item', 0 );
//get_template_part( 'template-parts/arianne', 'design' );?>
<div class="wrapper" id="mainTemplateDesign">
  <div class="help">
    <a href="<?php echo get_the_permalink(get_page_by_path( "contact" ) );?>">
      <span><?php _e("For more complicated requests (several product, global sourcing...) Contact us directly!","armor-pharma");?></span>
    </a>
    <a class="picto-info" data-slug="help-design">
      <img src="<?php echo get_stylesheet_directory_uri() . '/assets/css/images/help.png';?>">
    </a>
  </div>
  <main>
    <?php the_content();?>
    <div class="text-center">
    <p>
        <a href="<?php echo get_the_permalink(get_page_by_path( "design-the-lactose-you-like" ) );?>" class="button">
            <?php _e("continue", "armor-pharma");?>
        </a>
    </p>
    </div>
  </main>
</div>
<?php
endwhile; endif;
get_footer(); ?>
