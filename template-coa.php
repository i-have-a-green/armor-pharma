<?php
/*
Template Name: CoA
*/
get_header();
?>
<?php if (have_posts()) : while (have_posts()) : the_post();?>
  <div id="content-header">
    <div class="wrapper">
      <?php the_breadcrumb();?>
    </div>
  </div>
	<main id="main" role="main" class="wrapper my-account">
		<article>
			<header class="wrapper" id="header-page">
				<h1 class="page-title degrade"><?php _e("My account","armor-pharma");?></h1>
			</header>

			<section class="entry-content wrapper" itemprop="articleBody">
        <div id="menu-account">
          <?php set_query_var( 'active', 2 );?>
          <?php get_template_part( 'template-parts/menu', 'account' );?>
        </div>
        <div id="my-account-content">
          <h1 class="degrade semiCercle small"><?php the_title( );?></h1>
          <?php
            $coa = get_user_meta(get_current_user_id(), 'coa', true);
            $coa = json_decode(stripslashes($coa));
            if(sizeof($coa) > 0){
              foreach ($coa as $val) {
               // echo '<p><a href="'.$val->url.'" download="CoA.pdf">'.$val->label.'</a></p>';
              }
            }
            else{
              ?>
              <p>Searching for CoA of lactose samples/commercial batches ?</p>
              <p>Check out our <a href="<?php $p = get_page_by_path( "quality" ); echo get_the_permalink( $p);?>">Quality & Regulatory Corner</a> and download all your CoAs in our <a href="<?php $p = get_page_by_path( "find-your-coa" ); echo get_the_permalink( $p);?>">Find your CoA</a> section</p>
              <?php
            }
          ?>
        </div>
			</section>
		</article>
	</main>
<?php endwhile; endif; ?>
<?php get_footer(); ?>
