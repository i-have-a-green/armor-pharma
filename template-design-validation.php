<?php
/*
Template Name: Design validation
*/

$user = get_current_user_id();
if($user == 0){
  wp_redirect( home_url(), 302 );
}
get_header();
$design = get_user_meta(get_current_user_id(), 'design', true);
$JSONdesign = $design;
$design = json_decode(stripslashes($design));
?>
<script>//var design = <?php echo $JSONdesign;?>;</script>
<?php
$user = wp_get_current_user();
$subject = "Lactose customization  ".$user->display_name." ".$design->lactose->name;

$body = $user->civilite.' '.$user->user_firstname. " ".$user->user_lastname."\r\n";
$body .= 'country : '.$user->country."\r\n";
$body .= 'phone : '.$user->phone."\r\n";
$body .= 'profil : '.$user->profil."\r\n";
$body .= 'company : '.$user->company."\r\n";
$body .= 'email : '.$user->user_email."\r\n\r\n";

if($design->pharmacopoeial){
  $body .= "Pharmacopoeial conformance : \r\n";
  foreach ($design->pharmacopoeial as $val) {
    $body .= $val->label."\r\n";
  }
}

if($design->microbial){
  $body .= "Microbial limits : \r\n";
  foreach ($design->microbial as $val) {
    $body .= $val->label." : ".$val->value." ".$val->unit."\r\n";
  }
}

if(isset($design->anotherTest)){
  $body .= "Another test : \r\n";
  foreach ($design->anotherTest as $val) {
      $body .= $val->label." : ".$val->limits." ".$val->method."\r\n";
  }
}

if($design->particule){
  $body .= "Particule size distribution (Air jet sieve) : \r\n";
  foreach ($design->particule as $val) {
    $body .= $val->label." ".$val->value."\r\n";
  }
}

if(isset($design->fileSpecification)){
  $body .= "Specification file : \r\n";
  $body .= '<a href="'.$design->fileSpecification->link.'" target="_blank">'.$design->fileSpecification->file.'</a>';
  $body .= "\r\n";
}
if(isset($design->targetSpecification) && !empty($design->targetSpecification)){
  $body .= "Specification target : \r\n";
  $body .= $design->targetSpecification;
  $body .= "\r\n";
}

$body .= "\r\n\r\n Packaging : \r\n\r\n";

if($design->packaging->packagingOptions){
  $body .= $design->packaging->label."\r\n";
  foreach ($design->packaging->packagingOptions as $val){
    $body .= $val->valueText."\r\n";
  }
}
if(isset($design->fileSpecification)){
  $body .= "Packaging file : \r\n";
  $body .= '<a href="'.$design->filePackaging->link.'" target="_blank">'.$design->filePackaging->file.'</a>';
  $body .= "\r\n";
}

$body .= "\r\n\r\n Palletization : \r\n\r\n";

if($design->palet->typePalet){
  $body .= "Adjust my palet \r\n";
  foreach ($design->palet->typePalet as $val){
    $body .= $val->valueText."\r\n";
  }
}
if($design->palet->adjustPalet){
  $body .= "The type of pallet \r\n";
  foreach ($design->palet->adjustPalet as $val){
    $body .= $val->valueText."\r\n";
  }
}

if(isset($design->filePalet)){
  $body .= "Palletization file : \r\n";
  $body .= '<a href="'.$design->filePalet->link.'" target="_blank">'.$design->filePalet->file.'</a>';
  $body .= "\r\n";
}
$body .= $design->targetPalet."\r\n";
$body .= "Adjust my palet\r\n";
$body .= $design->delivery->needQuantity->valueText."\r\n";
$body .= $design->delivery->needText."\r\n";
if(isset($design->delivery->company)){
  $body .= $design->delivery->company."\r\n";
  $body .= $design->delivery->address."\r\n";
  $body .= $design->delivery->zipCode.' ';
  $body .= $design->delivery->city."\r\n";
  $body .= $design->delivery->stateProvince."\r\n";
  $body .= $design->delivery->country."\r\n";
}


$headers[] = 'From: '.get_bloginfo('name').' <'. __('no-reply@', 'wpgreen') . str_replace('www.', '', $_SERVER['SERVER_NAME'] ) .'>';
wp_mail( get_option( 'admin_email'), $subject, $body, $headers);
$postDesign = [];
$postDesign['post_type']   = 'design';
$postDesign['post_status'] = 'publish';
$postDesign['post_title'] = $user->display_name." - ".$design->lactose->name;
$postDesign['post_content'] = $body;
wp_insert_post( $postDesign, true );

$body = __('<b>Thank you for contacting ARMOR PHARMA !</b>
<p>
Your request has been successfully submitted and we will revert to you in the shortest delay
</p>
<p>
Kind regards<br />
ARMOR PHARMA’s team
</p>','wpgreen');
$headers[] = 'From: '.get_bloginfo('name').' <'. WPGREEN_EMAIL_FROM .'>';
wp_mail( sanitize_text_field($user->user_email), __("Armor Pharma - Lactose customization ",'wpreen'), $body, $headers);

delete_user_meta( get_current_user_id(), 'design');

get_header();
if (have_posts()) :  while ( have_posts() ) : the_post();
?>
<div id="content-header">
  <div class="wrapper">
    <?php the_breadcrumb();?>
  </div>
</div>
<div class="text-center">
  <h1 class="degrade"><?php _e("Design the lactose <b>you</b> like","armor-pharma");?></h1><br />
  <p class="degrade" id="titreProduct">
    <?php _e("How would you like your","armor-pharma");?> « <?php echo $design->lactose->name;?> »
  </p>
</div>
<?php
set_query_var( 'item', 7 );
get_template_part( 'template-parts/arianne', 'design' );?>
<div class="wrapper" id="contentDesign">
  <main>

    <div id="validation" class="text-center">
      <h2 class="degrade semiCercle small"><?php the_title();?></h2><br />
      <img src="<?php echo get_stylesheet_directory_uri() . '/assets/css/images/validation.png';?>" class="pictoSummary">
      <p>
        <b><?php _e("A copy of your request has been emailed to you. The content has been received and will be reviewed by our technical sales and engineering services resources.","armor-pharma");?></b>
      </p>
      <p>
        <?php _e("We aim to revert to you within 3 days of receipt of your request.If you have any queries please don’t hesitate to contact us.","armor-pharma");?>
      </p>
      <hr>
      <div id="vote">
        <h3><?php _e("Did you like it ?","armor-pharma");?></h3>
        <button class="button small" id="voteYes">
          <span class="picto picto-yes"></span>
          <?php _e("YES","armor-pharma");?>          
        </button>
        <button class="button small" id="voteNo">
          <span class="picto picto-no"></span>
          <?php _e("NO","armor-pharma");?>          
        </button>
      </div>
      <p class="text-center">
        <a id="" href="<?php echo get_the_permalink(get_page_by_path( 'design-the-lactose-you-like/' ) );?>" class="button small another"><?php _e("CUSTOMIZE ANOTHER LACTOSE","armor-pharma");?></a>
        <a id="" href="<?php echo home_url();?>" class="button small home"><span class="picto picto-home"></span><?php _e("BACK HOME","armor-pharma");?></a>
      </p>
    </div>
  </main>
</div>
<?php
endwhile; endif;
get_footer(); ?>
