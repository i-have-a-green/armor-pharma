<?php
get_header();
$term = get_queried_object();
?>
<div id="content-header">
  <div class="wrapper">
    <?php the_breadcrumb();?>
  </div>
</div>
<div id="archive">
	<header class="wrapper" id="header-page">
		<h1 class="page-title" style="color:<?php the_field('color', $term);?>"><?php the_field('title',$term);?></h1>
    <?php // echo term_description();?>
    <?php the_field('descriptionTaxo', $term);?>
    <p>
      <strong><?php _e("Select the products you would like to compare and click on [Enlarge] to discover their main characteristics.","armor-pharma");?></strong>
    </p>
	</header>
	<div id="archive-product" class="wrapper">
    <script>
      var datasets = [];
    </script>
    <div id="listing-product">
      <?php
      $process = get_terms( 'process-lactose' );
      foreach ($process as $catProcess) :
        $args = array(
        	'post_type' => 'lactose',
        	'tax_query' => array(
        		'relation' => 'AND',
        		array(
        			'taxonomy' => 'process-lactose',
        			'field'    => 'id',
        			'terms'    => $catProcess->term_id,
        		),
            array(
        			'taxonomy' => 'category-lactose',
        			'field'    => 'id',
        			'terms'    => $term->term_id,
        		)
        	),
        );
        $query = new WP_Query($args);
        if ( $query->have_posts() ) :
          echo '<h2>'.$catProcess->name.'</h2>';
        	echo '<div class="listing-product-process">';
        	while ( $query->have_posts() ) :
        		$query->the_post();
            set_query_var( 'color', get_field('color', $term) );
            get_template_part( 'template-parts/loop', 'lactose-product' );
        	endwhile;
        	echo '</div>';
        	wp_reset_postdata();
        endif;
        endforeach;
        ?>
    </div>
    <div id="archive-graphe">
      <img id="demiCercle" src="<?php echo get_stylesheet_directory_uri();?>/assets/css/images/illustrationCircle.png" />
      <div id="cont-h2-degarde">
        <h2 class="degrade"><?php _e("YOUR SELECTION","armor-pharma");?></h2>
      </div>
      <canvas id="chartjsCol" class="chartjs" width="" height="400"></canvas>
      <p class="text-center">
        <a href="#" class="button small" id="openModalGraph" onclick="openModalChart()" style="color:<?php the_field('color', $term);?>;border-color:<?php the_field('color', $term);?>"><?php _e("ENLARGE","armor-pharma");?></a>
        <?php info("enlarge");?>
      </p>
    </div>
	</div>
  <div id="" class="wrapper">
    <h2><?php _e("Find the suitable lactose for your application","armor-pharma");?></h2>
  </div>
  <?php get_template_part( 'template-parts/configurateur', '' );?>
</div>
<?php
get_footer();
?>
<!-- MODAL -->
<div class="modal" id="modal-chart">
  <div class="modal-header">
    <h2 id="modal-title" class="degrade"><?php _e("Compare Products");?></h2>
    <p class="text-center">
      <?php _e("We have selected the main characterisics & functionnalities of our products to enable you understanding they differentiate from one another.<br />
Easily create your personnalized product comparison graph and display your selection","armor-pharma");?>
    </p>
  </div>
  <div id="modal-chart-content">
    <canvas id="chartjs-modal" class="chartjs" width="" height=""></canvas>
    <?php
    $post = get_page_by_path("enlarge", OBJECT, 'info');
    echo $post->post_content;
    ?>
    <div id="modal-listing">
      <?php if (have_posts()) : $i=0;?>
        <?php while ( have_posts() ) : the_post();
          set_query_var( 'color', get_field('color', $term) );
          get_template_part( 'template-parts/loop', 'lactose-product-modal' );?>
        <?php endwhile;?>
      <?php endif;?>
    </div>

  </div>
  <div class="modal-footer">
    <p>
      <button class="button" onclick="closeModal()">
        &times; <?php _e('Close','wpgreen');?>
      </button>
    </p>
  </div>
</div>
