<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post();?>
  <div id="content-header">
    <div class="wrapper">
      <?php the_breadcrumb();?>
    </div>
  </div>
	<main id="main" role="main" class="wrapper">
		<article>
			<header id="header-page">
				<h1 class="page-title degrade"><?php the_title();?></h1>
			</header>
      <div id="sub-header">
        <div id="thumbnail">
  				<?php the_post_thumbnail( 'wpgreen-medium'); ?>
  			</div>
        <div id="excerpt">
          <div id="header-content">
            <date class="polyDate"><?php the_field("label-date");?></date>
            <div id="name-event">
              <?php the_field("name-event");?>
            </div>
            <div id="event-place">
              <?php the_field("event-place");?>
            </div>
          </div>
          <h2><?php the_field('sub-title');?></h2>
  				<?php the_excerpt();?>
  			</div>
        <div id="place">
          <h3 class="degrade semiCercle small"><?php _e("Get there", "armor-pharma");?> <?php //echo get_the_date();?></h3>
          <?php
            $location = get_field('map');
          ?>
          <a href="http://maps.google.fr/maps?f=q&hl=fr&q=<?php echo $location['lat'].",".$location['lng'];?>" target="_blank">
            <img src="https://maps.googleapis.com/maps/api/staticmap?center=<?php echo $location['lat'].",".$location['lng'];?>&zoom=<?php the_field('zoom');?>&markers=color:red%7C<?php echo $location['lat'].",".$location['lng'];?>&size=400x300&key=AIzaSyAcsaZSk-QezAqacx-tUhSaHiTI7W3RnK4">
          </a>
          <?php the_field("address");?>
          <div class="text-center" id="cont-btn-calendar">
            <?php if(empty(get_field("date-finish"))):?>
            <a href="https://www.google.com/calendar/render?action=TEMPLATE&text=<?php echo str_replace(" ", "+", get_the_title());?>&date=<?php the_field("date-begin");?>T000000Z&details=For+details,+link+here:+<?php echo get_the_permalink();?>&sf=true&output=xml" class="button" target="_blank" id="btn-calendar">
            <?php else:?>
            <a href="https://www.google.com/calendar/render?action=TEMPLATE&text=<?php echo str_replace(" ", "+", get_the_title());?>&dates=<?php the_field("date-begin");?>T000000Z/<?php the_field("date-finish");?>T000000Z&details=For+details,+link+here:+<?php echo get_the_permalink();?>&sf=true&output=xml" class="button" target="_blank" id="btn-calendar">
            <?php endif;?>
              <?php _e("Add to my agenda", "armor-pharma");?>
            </a>
          </div>

        </div>
        <div class="entry-content" id="entry-content" itemprop="articleBody">
  				<?php the_content(); ?>
  			</div>
      </div>
      <div id="find-us">
        <h3><?php _e("Find us in the event");?></h3>
        <?php the_field('find-us');?>
        <?php
        $images = get_field('images');
        $size = 'medium'; // (thumbnail, medium, large, full or custom size)

        if( $images ): ?>
            <div class="flexslider" id="sliderEvent">
              <ul class="slides">
                <?php foreach( $images as $image ): ?>
                    <li>
                    	<?php echo wp_get_attachment_image( $image['ID'], $size ); ?>
                    </li>
                <?php endforeach; ?>
              </ul>
            </div>
        <?php endif; ?>
      </div>
      <div id="event-more">
        <div id="more-info">
          <h3><?php _e("More information", "armor-pharma");?></h3>
          <?php
          if( have_rows('more_information') ):
            echo "<ul>";
            while ( have_rows('more_information') ) : the_row();
              echo "<li>";
                the_sub_field('content');
              echo "</li>";
            endwhile;
            echo "</ul>";
          endif;

          if(!empty(get_field('pdf'))):
          ?>
          <a href="<?php the_field('pdf');?>" download class="button download"><?php _e("Download the program","armor-pharma");?></a>
        <?php endif;?>
        </div>
        <div id="form-request-event">
          <h3><?php _e("Request for a meeting", "armor-pharma");?></h3>
          <form id="form-event" class="form" name="form-event" action="<?php the_permalink();?>" method="post">
            <div>
              <label for="date">
                <?php _e("availabilities*", "armor-pharma");?>
                <input type="date" name="date" id="date" required>
              </label>
              <label for="comments">
                <?php _e("Subject you would like to discuss*", "armor-pharma");?>
                <textarea name="comments" id="comments" required></textarea>
              </label>
            </div>
            <div>
              <b><?php _e("Who would you like to meet ?", "armor-pharma");?></b>
              <input type="checkbox" name="service[]" id="cat_1" value="<?php _e("Sales", "armor-pharma");?>" class="" >
              <label for="cat_1"><?php _e("Sales", "armor-pharma");?></label>

              <input type="checkbox" name="service[]" id="cat_2" value="<?php _e("R&D and Technical support", "armor-pharma");?>" class="" >
              <label for="cat_2"><?php _e("R&D and Technical support", "armor-pharma");?></label>

              <input type="checkbox" name="service[]" id="cat_3" value="<?php _e("Marketing", "armor-pharma");?>" class="" >
              <label for="cat_3"><?php _e("Marketing", "armor-pharma");?></label>

              <input type="checkbox" name="service[]" id="cat_4" value="<?php _e("Quality", "armor-pharma");?>" class="" >
              <label for="cat_4"><?php _e("Quality", "armor-pharma");?></label>

              <input type="checkbox" name="service[]" id="cat_5" value="<?php _e("Other", "armor-pharma");?>" class="" >
              <label for="cat_5"><?php _e("Other", "armor-pharma");?></label>

              <input type="submit" id="submitBtn" value="<?php _e("Send a request", "armor-pharma");?>" />
            </div>
          </form>
        </div>
      </div>

  	  <hr class="hrTriangle">
      <div id="footer-nav">
        <div>
           <?php previous_post_link('<span class="picto angle-left"></span> %link', __("PREVIOUS EVENT","armor-pharma"));?>
        </div>
        <div>
          <?php $p = get_page_by_path( "events-news" );
           echo '<a href="'.get_the_permalink($p).'" id="link-all-events">'. __('Back to all events', "armor-pharma").'</a>';?>
        </div>
        <div>
           <?php next_post_link('%link <span class="picto angle-right"></span>', __("NEXT EVENT","armor-pharma"));?>
        </div>
      </div>
    </article>
  </main>
  <footer class="article-footer">
    <div class="wrapper">
      <div id="same-category">
        <div class="text-center">
          <h2 class="degrade semiCercle small"><?php _e("Next events", "armor-pharma");?></h2>
        </div>

        <div id="archive-event">
          <?php
          $dateBegin = date('Ym');
          $args = array (
            'posts_per_page'  => 4,
            'post_type' => 'event',
            'status' => 'publish',
            'meta_query' => array(
              array(
                    'key'		=> 'date-begin',
                    'compare'	=> '>=',
                    'value'		=> $dateBegin,
              )
            )
          );
          $args['cat'] = wpgreen_get_the_category_id();
          $posts = get_posts($args);
          if( $posts ) {
            echo '<div class="archive-news" id="archive-event-pSingle"><ul class="slides">';
            foreach( $posts as $post ) {
              setup_postdata( $post );
              echo '<li>';
              get_template_part( 'template-parts/loop', 'event' );
              echo '</li>';
            }
            echo '</ul></div>';
            wp_reset_postdata();
          }
          ?>
        </div>
      </div>
    </div>
	</footer>
  <!--<div class="modal" id="modalMeeting">
    <?php
      $user = wp_get_current_user();
      $current_user_exists = $user->exists();
      $genre = ($current_user_exists)?$user->civilite:'';
      $firstname = ($current_user_exists)?$user->user_firstname:'';
      $lastname = ($current_user_exists)?$user->user_lastname:'';
      $country = ($current_user_exists)?$user->country:'';
      $phone = ($current_user_exists)?$user->phone:'';
      $profil = ($current_user_exists)?$user->profil:'';
      $company = ($current_user_exists)?$user->company:'';
      $email = ($current_user_exists)?$user->user_email:'';
    ?>
    <form id="form-meeting" name="form-meeting" action="<?php the_permalink();?>" method="post" enctype="multipart/form-data">
      <input type="hidden" name="action" value="formmeeting">
      <?php wp_nonce_field('nonceformmeeting', 'nonceformmeeting'); ?>
      <input type="hidden" name="event" value="<?php the_title();?>">
      <div class="modal-content form">
        <div>
          <label for="meeting_Mr" class="notDisplayBlock">Mr</label><input type="radio" <?php echo ($genre == "Mr")?'checked':'';?> id="meeting_Mr" name="genre" value="Mr" />
          <label for="meeting_Mrs" class="notDisplayBlock">Mrs</label><input type="radio" <?php echo ($genre == "Mrs")?'checked':'';?> id="meeting_Mrs" name="genre" value="Mrs" />
        </div>
        <div class="grid2">
          <div>
            <label for="meeting_firstname"><?php _e("First name","armor-pharma");?>*</label>
            <input type="text" name="firstname" value="<?php echo $firstname;?>" id="meeting_firstname" required />
          </div>
          <div>
            <label for="meeting_lastname"><?php _e("Last name","armor-pharma");?>*</label>
            <input type="text" name="lastname" value="<?php echo $lastname;?>" id="meeting_lastname" required />
          </div>
        </div>
        <div class="grid2">
          <div>
            <label for="meeting_country"><?php _e("Country","armor-pharma");?>*</label>
            <input type="text" name="country" value="<?php echo $country;?>" id="meeting_country" required />
          </div>
          <div>
            <label for="meeting_phone"><?php _e("Phone","armor-pharma");?></label>
            <input type="tel" name="phone" value="<?php echo $phone;?>" id="meeting_phone"  />
          </div>
        </div>
        <div>
          <label id="meeting_profil">
            <?php _e("Profile","armor-pharma");?>*
          </label>
          <select name="profil" id="meeting_profil" required>
            <option <?php echo ($profil == "Responsible for excipients purchases")?'selected':'';?>>
              Responsible for excipients purchases
            </option>
            <option <?php echo ($profil == "Working on tablets/sachets/capsules formulation")?'selected':'';?>>
              Working on tablets/sachets/capsules formulation
            </option>
            <option <?php echo ($profil == "Working on Dry Powder Inhalers development")?'selected':'';?>>
              Working on Dry Powder Inhalers development
            </option>
            <option <?php echo ($profil == "Student in pharmaceutical formulation")?'selected':'';?>>
              Student in pharmaceutical formulation
            </option>
            <option <?php echo ($profil == "Professor/associate professor at University")?'selected':'';?>>
              Professor/associate professor at University
            </option>
            <option <?php echo ($profil == "PhD student")?'selected':'';?>>
              PhD student
            </option>
            <option <?php echo ($profil == "other")?'selected':'';?>>
              other
            </option>
          </select>
        </div>
        <div class="grid2">
          <div>
            <label for="meeting_company"><?php _e("Company","armor-pharma");?>*</label>
            <input type="text" name="company" value="<?php echo $company;?>" id="meeting_company" required />
          </div>
          <div>
            <label for="vemail"><?php _e("Email","armor-pharma");?>*</label>
            <input type="email" name="email" value="<?php echo $email;?>" id="meeting_email" required />
          </div>
        </div>
        <div class="border"></div>
        <div>
          <b><?php _e("Subject","armor-pharma");?></b><br /><?php _e("Request for a meeting","armor-pharma");?>
          <input type="hidden" value="<?php _e("Request for a meeting","armor-pharma");?>" name="subject" id="meeting_subject" readonly />
        </div>
        <div>
          <b><?php _e("Who would you like to meet ?","armor-pharma");?></b><br />
          <input type="hidden" value="" name="meeting_who" id="meeting_who" readonly />
          <span id="meeting_who_span"></span>
        </div>
        <div>
          <b><?php _e("Availabilities","armor-pharma");?></b><br />
          <input type="hidden" value="" name="meeting_date" id="meeting_date" readonly />
          <span id="meeting_date_span"></span>
        </div>
        <div>
          <b><?php _e("Comments","armor-pharma");?></b><br />
          <input type="hidden" value="" name="meeting_comments" id="meeting_comments" readonly />
          <span id="meeting_comments_span"></span>
        </div>
        <input type="checkbox" name="check" id="check" required />
        <label for="check">
          <?php _e("By submitting your data through this form, you confirm that you are above the age of 18, that you have read and understood the Privacy Policy, and that you agree to the collection, use and processing of your Personal Information by ARMOR PHARMA in accordance with said Policy","armor-pharma");?>*
        </label>
        <div class="text-center">
          <input type="submit" class="button submitBtn" id="meeting_send" value="<?php _e("SEND","armor-pharma");?>" />
        </div>
      </div>
    </form>
    <div class="modal-footer text-center">
      <p>
        <button class="button" onclick="closeModal()">
          &times; <?php _e('Close','wpgreen');?>
        </button>
      </p>
    </div>
  </div>-->
<?php endwhile; endif; ?>
<?php get_footer(); ?>
