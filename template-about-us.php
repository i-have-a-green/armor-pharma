<?php
/*
Template Name: About us
*/
get_header();
?>
<?php if (have_posts()) : while (have_posts()) : the_post();?>
  <div id="content-header">
    <div class="wrapper">
      <?php the_breadcrumb();?>
    </div>
  </div>
	<main id="main" role="main">
		<article>
			<header class="wrapper" id="header-page">
				<h1 class="page-title degrade"><?php the_field("title");?></h1>
			</header>
			<section class="entry-content wrapper" itemprop="articleBody">
				<?php the_content(); ?>
			</section>
      <div id="video">
        <div class="wrapper">
          <?php the_field("video");?>
        </div>
      </div>
      <div id="savencia" class="wrapper">
        <div>
          <a href="http://www.savencia-fromagedairy.com/" target="_blank" class="attachment-wpgreen-logo size-wpgreen-logo">
            <img src="<?php echo get_stylesheet_directory_uri() . '/assets/css/images/savencia.png';?>" />
          </a>
        </div>
        <div>
          <div>
            <img src="<?php echo get_stylesheet_directory_uri() . '/assets/css/images/team.png';?>" />
          </div>
          <hr class='hrTriangle' />
          <p>
            <?php the_field("team");?>
          </p>
        </div>
        <div>
          <div>
            <img src="<?php echo get_stylesheet_directory_uri() . '/assets/css/images/ca.png';?>" />
          </div>
          <hr class='hrTriangle' />
          <p>
            <?php the_field("ca");?>
          </p>
        </div>
        <div>
          <div>
            <img src="<?php echo get_stylesheet_directory_uri() . '/assets/css/images/leader.png';?>" />
          </div>
          <hr class='hrTriangle' />
          <p>
            <?php the_field("leader");?>
          </p>
        </div>
      </div>
      <div id="capabilities" class="wrapper">
        <h2 class="degrade"><?php the_field("title_capabilities");?></h2>
        <?php the_field("content_capabilities");?>
      </div>
      <div id="video2">
        <div class="wrapper">
          <?php the_field("video2");?>
        </div>
      </div>
      <div id="text" class="wrapper">
        <?php the_field("text");?>
      </div>
      <div id="btns" class="wrapper">
        <div>
          <a href="<?php echo get_the_permalink(get_page_by_path("quality"));?>">
            <img src="<?php echo get_stylesheet_directory_uri() . '/assets/css/images/btnQuality.png';?>" />
          </a>
        </div>
        <div>
          <a href="<?php echo get_the_permalink(get_page_by_path("technical-library"));?>">
            <img src="<?php echo get_stylesheet_directory_uri() . '/assets/css/images/btnTechnical.png';?>" />
          </a>
        </div>
      </div>
      <div id="distributor">
        <div class="wrapper">
          <h2 class="degrade"><?php _e("Availability","armor-pharma");?></h2>

          <p>
            <?php _e("All Armor Pharma products are available world-wide. Where necessary Armor Pharma is supported by a network of specialist distributors that can provide local knowledge, local stock and can liaise between users and Armor Pharma. Whatever your need is, please contact us directly or contact any local representative.","armor-pharma");?>
          </p>

          <h2 class="degrade"><?php _e("Get in touch...","armor-pharma");?></h2><br>
          <h3 class="degrade"><?php _e("... with Armor Pharma team","armor-pharma");?></h3>

          <?php
        $user = wp_get_current_user();
        $current_user_exists = $user->exists();
        $genre = ($current_user_exists)?$user->civilite:'';
        $firstname = ($current_user_exists)?$user->user_firstname:'';
        $lastname = ($current_user_exists)?$user->user_lastname:'';
        $country = ($current_user_exists)?$user->country:'';
        $phone = ($current_user_exists)?$user->phone:'';
        $profil = ($current_user_exists)?$user->profil:'';
        $company = ($current_user_exists)?$user->company:'';
        $email = ($current_user_exists)?$user->user_email:'';
      ?>
      <div class="wrapper" id="formContact">
        <form id="form-contact" name="form-contact" action="<?php the_permalink();?>" method="post" enctype="multipart/form-data">
          <input type="hidden" name="action" value="formExpert">
          <input type="hidden" name="honey" value="">
          <?php wp_nonce_field('nonceformContact', 'nonceformContact'); ?>
          <div class="form">
            <div class="colLeft">

              <div class="label">
                <label for="contact_Mr" class="notDisplayBlock">Mr</label><input type="radio" <?php echo ($genre == "Mr")?'checked':'';?> id="contact_Mr" name="genre" value="Mr" />
                <label for="contact_Mrs" class="notDisplayBlock">Mrs</label><input type="radio" <?php echo ($genre == "Mrs")?'checked':'';?> id="contact_Mrs" name="genre" value="Mrs" />
              </div>
              <div class="grid2">
                <div>
                  <label for="contact_firstname"><?php _e("First name","armor-pharma");?>*</label>
                  <input type="text" value="<?php echo $firstname;?>" name="firstname" id="contact_firstname" required />
                </div>
                <div>
                  <label for="contact_lastname"><?php _e("Last name","armor-pharma");?>*</label>
                  <input type="text" value="<?php echo $lastname;?>" name="lastname" id="contact_lastname" required />
                </div>
              </div>
              <div class="grid2">
                <div>
                  <label for="contact_country"><?php _e("Country","armor-pharma");?>*</label>
                  <input type="text" value="<?php echo $country;?>" name="country" id="contact_country" required />
                </div>
                <div>
                  <label for="contact_phone"><?php _e("Phone","armor-pharma");?></label>
                  <input type="tel"  name="phone" value="<?php echo $phone;?>" id="contact_phone"  />
                </div>
              </div>
              <div>
                <label id="contact_profil">
                  <?php _e("Profile","armor-pharma");?>*
                </label>
                <select name="profil" id="contact_profil" required>
                  <option <?php echo ($profil == "Responsible for excipients purchases")?'selected':'';?>>
                    Responsible for excipients purchases
                  </option>
                  <option <?php echo ($profil == "Working on tablets/sachets/capsules formulation")?'selected':'';?>>
                    Working on tablets/sachets/capsules formulation
                  </option>
                  <option <?php echo ($profil == "Working on Dry Powder Inhalers development")?'selected':'';?>>
                    Working on Dry Powder Inhalers development
                  </option>
                  <option <?php echo ($profil == "Student in pharmaceutical formulation")?'selected':'';?>>
                    Student in pharmaceutical formulation
                  </option>
                  <option <?php echo ($profil == "Professor/associate professor at University")?'selected':'';?>>
                    Professor/associate professor at University
                  </option>
                  <option <?php echo ($profil == "PhD student")?'selected':'';?>>
                    PhD student
                  </option>
                  <option <?php echo ($profil == "other")?'selected':'';?>>
                    other
                  </option>
                </select>
              </div>
              <div class="grid2">
                <div>
                  <label for="contact_company"><?php _e("Company","armor-pharma");?>*</label>
                  <input type="text" name="company" value="<?php echo $company;?>" id="contact_company" required />
                </div>
                <div>
                  <label for="contact_email"><?php _e("Email","armor-pharma");?>*</label>
                  <input type="email" name="email" value="<?php echo $email;?>" id="contact_email" required />
                </div>
              </div>
            </div>
            <div class="colRight">
              <div>
                <label for="contact_subject"><?php _e("Subject","armor-pharma");?></label>
                <input type="text" name="subject" id="contact_subject" required />
              </div>
              <div>
                <label for="contact_comments"><?php _e("Comments","armor-pharma");?></label>
                <textarea name="comments"  id="contact_comments" required ></textarea>
              </div>
            </div>
            <div class="colLeft"></div>
            <div class="colRight">
              <div>
                <input type="checkbox" name="check" id="contact_check" required />
                <label for="contact_check">
                <?php $p = get_page_by_path( "privacy-policy-cookie-policy" );?>
                <?php _e("By submitting your data through this form, you confirm that you are above the age of 18, that you have read and understood the <a href='".get_the_permalink( $p)."' target='_blank'>Privacy Policy</a>, and that you agree to the collection, use and processing of your Personal Information by ARMOR PHARMA in accordance with said Policy*","armor-pharma");?>
                </label>
              </div>
            </div>
            <div class="col2 text-center">
              <button class="button" id="contact_sendAskExpert"><?php _e("SEND","armor-pharma");?></button>
            </div>
          </div>
        </form>
      </div>
      <h3 class="degrade"><?php _e("... or with our local partners","armor-pharma");?></h3>

          <div id="countries">

          <?php
          $terms = get_terms( array( 'taxonomy' => 'category-distributor'));
          foreach ($terms as $term) {
            echo '<div class="country">
              <h3>'.$term->name.'</h3>';
              $args = array(
                'post_type' => 'distributor',
                'posts_per_page'  => -1,
                'orderby'   => 'title',
                'order'     => 'ASC',
                'tax_query' => array(
                  array(
                    'taxonomy' => 'category-distributor',
                    'field'    => 'id',
                    'terms'    => $term->term_id,
                  ),
                ),
              );
              $query = new WP_Query( $args );
              if ( $query->have_posts() ) :
                while ( $query->have_posts() ) : $query->the_post(); ?>
                  <div data-id="<?php the_id();?>" class="openDistributor">
                    <div>
                      <?php
                      $image = get_field('flag');
                      $size = 'flag'; // (thumbnail, medium, large, full or custom size)
                      if( $image ) {
                        echo wp_get_attachment_image( $image, $size );
                      }
                      ?>
                    </div>
                    <div>
                      <span><?php the_title(); ?></span>
                    </div>
                  </div>
              <?php endwhile; ?>
            <?php endif;?>
        <?php wp_reset_postdata();

            echo '</div>';
          }
          ?>
        </div>

      <?php
        $user = wp_get_current_user();
        $current_user_exists = $user->exists();
        $genre = ($current_user_exists)?$user->civilite:'';
        $firstname = ($current_user_exists)?$user->user_firstname:'';
        $lastname = ($current_user_exists)?$user->user_lastname:'';
        $country = ($current_user_exists)?$user->country:'';
        $phone = ($current_user_exists)?$user->phone:'';
        $profil = ($current_user_exists)?$user->profil:'';
        $company = ($current_user_exists)?$user->company:'';
        $email = ($current_user_exists)?$user->user_email:'';
      ?>
      <!-- MODAL -->
      <div class="modal" id="modalAskExpert">
        <form id="form-expert" name="form-expert" action="<?php the_permalink();?>" method="post" enctype="multipart/form-data">
          <input type="hidden" name="action" value="formExpert">
          <input type="hidden" name="distributorId" id="distributorId" value="">
          <input type="hidden" name="honey" value="">
          <?php wp_nonce_field('nonceformExpert', 'nonceformExpert'); ?>
          <div class="modal-content form">
            <div>
              <label for="Expert_Mr" class="notDisplayBlock">Mr</label><input type="radio" <?php echo ($genre == "Mr")?'checked':'';?> id="Expert_Mr" name="genre" value="Mr" />
              <label for="Expert_Mrs" class="notDisplayBlock">Mrs</label><input type="radio" <?php echo ($genre == "Mrs")?'checked':'';?> id="Expert_Mrs" name="genre" value="Mrs" />
            </div>
            <div class="grid2">
              <div>
                <label for="firstname"><?php _e("First name","armor-pharma");?>*</label>
                <input type="text" value="<?php echo $firstname;?>" name="firstname" id="firstname" required />
              </div>
              <div>
                <label for="lastname"><?php _e("Last name","armor-pharma");?>*</label>
                <input type="text" value="<?php echo $lastname;?>" name="lastname" id="lastname" required />
              </div>
            </div>
            <div class="grid2">
              <div>
                <label for="Expert_country"><?php _e("Country","armor-pharma");?>*</label>
                <input type="text" value="<?php echo $country;?>" name="country" id="Expert_country" required />
              </div>
              <div>
                <label for="Expert_phone"><?php _e("Phone","armor-pharma");?></label>
                <input type="tel" placeholder="+33 0 00 00 00 00" name="phone" value="<?php echo $phone;?>" id="Expert_phone"  />
              </div>
            </div>
            <div>
              <label id="profil">
                <?php _e("Profile","armor-pharma");?>*
              </label>
              <select name="profil" id="profil" required>
                <option <?php echo ($profil == "Responsible for excipients purchases")?'selected':'';?>>
                  Responsible for excipients purchases
                </option>
                <option <?php echo ($profil == "Working on tablets/sachets/capsules formulation")?'selected':'';?>>
                  Working on tablets/sachets/capsules formulation
                </option>
                <option <?php echo ($profil == "Working on Dry Powder Inhalers development")?'selected':'';?>>
                  Working on Dry Powder Inhalers development
                </option>
                <option <?php echo ($profil == "Student in pharmaceutical formulation")?'selected':'';?>>
                  Student in pharmaceutical formulation
                </option>
                <option <?php echo ($profil == "Professor/associate professor at University")?'selected':'';?>>
                  Professor/associate professor at University
                </option>
                <option <?php echo ($profil == "PhD student")?'selected':'';?>>
                  PhD student
                </option>
                <option <?php echo ($profil == "other")?'selected':'';?>>
                  other
                </option>
              </select>
            </div>
            <div class="grid2">
              <div>
                <label for="Expert_company"><?php _e("Company","armor-pharma");?>*</label>
                <input type="text" name="company" value="<?php echo $company;?>" id="Expert_company" required />
              </div>
              <div>
                <label for="email"><?php _e("Email","armor-pharma");?>*</label>
                <input type="email" name="email" value="<?php echo $email;?>" id="email" required />
              </div>
            </div>
            <div class="border"></div>
            <div>
              <label for="subject"><?php _e("Subject","armor-pharma");?></label>
              <input type="text" value="<?php _e("Help me finding the suitable lactose","armor-pharma");?>" name="subject" id="subject" required readonly />
            </div>
            <div>
              <label for="comments"><?php _e("Comments","armor-pharma");?></label>
              <textarea name="comments" id="comments" required ></textarea>
            </div>
            <div>
              <input type="checkbox" name="check" id="check" required />
              <label for="check">
                <?php $p = get_page_by_path( "privacy-policy-cookie-policy" );?>
                <?php _e("By submitting your data through this form, you confirm that you are above the age of 18, that you have read and understood the <a href='".get_the_permalink( $p)."' target='_blank'>Privacy Policy</a>, and that you agree to the collection, use and processing of your Personal Information by ARMOR PHARMA in accordance with said Policy","armor-pharma");?>*
              </label>
            </div>
            <div class="text-center">
              <button type="submit" class="button submitBtn" id="sendAskExpert"><?php _e("SEND","armor-pharma");?></button>
            </div>
          </div>
        </form>
      </div>

      
		</article>
	</main>

<?php endwhile; endif; ?>
<?php get_footer(); ?>
