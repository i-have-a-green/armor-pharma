<?php
/*
Template Name: Orders
*/
get_header();
?>
<?php if (have_posts()) : while (have_posts()) : the_post();?>
  <div id="content-header">
    <div class="wrapper">
      <?php the_breadcrumb();?>
    </div>
  </div>
	<main id="main" role="main" class="wrapper my-account">
		<article>
			<header class="wrapper" id="header-page">
				<h1 class="page-title degrade"><?php _e("My account","armor-pharma");?></h1>
			</header>

			<section class="entry-content wrapper" itemprop="articleBody">
        <div id="menu-account">
          <?php set_query_var( 'active', 1 );?>
          <?php get_template_part( 'template-parts/menu', 'account' );?>
        </div>
        <div id="my-account-content">
          <h1 class="degrade semiCercle small"><?php the_title( );?></h1>
          <div id="orders">
          <?php
            $nbOrder = 1;
            $orders = get_user_meta(get_current_user_id(), 'orders', true);
            $orders = json_decode($orders);
            if($orders):
            $orders = array_reverse($orders);
            foreach ($orders as $order):?>
              <div class="order">
                <div class="numberOrder">
                  <div>
                    <?php echo $nbOrder;?>
                  </div>
                </div>
                <div class="articlesOrders">
                  <h2><?php _e("products","armor-pharma");?></h2>
                  <?php
                    if($order->cart):
                    foreach ($order->cart as $lactose):
                      $post = get_post( $lactose->id_lactose );
                      setup_postdata( $post );
                    ?>
                    <p>
                      <?php echo get_the_lactose();?>
                      (<?php echo $lactose->quantity;?> Kg)
                    </p>
                    <?php
                    endforeach;
                    wp_reset_postdata();
                  endif;
                  ?>
                </div>
                <div class="dateOrders">
                  <h2><?php _e("Order date","armor-pharma");?></h2>
                  <p>
                    <b><?php echo $order->date;?></b>
                  </p>
                </div>
              </div>
            <?php
            $nbOrder++;
            endforeach;
          else:
            _e("You haven't requested any samples yet. Feel free to add free samples into your cart from our lactose portfolio page.","armor-pharma");
          endif;
            ?>
          </div>
        </div>
			</section>
		</article>
	</main>
<?php endwhile; endif; ?>
<?php get_footer(); ?>
