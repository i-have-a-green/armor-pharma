<?php
get_header();
$content_news = get_field('content_news');
?>
    <div id="content-header">
      <div class="wrapper">
        <?php the_breadcrumb();?>
      </div>
    </div>
    <div id="content-body" class="wrapper">
      <section id="event">
        <h1 class="degrade"><?php _e("Events", 'armar-pharma');?></h1>
        <?php the_field('content_event');?>
        <div class="flexslider" id="carouselEvents">
          <div class="custom-navigation">
            <a href="#" class="flex-prev"><</a>
            <a href="#" class="flex-next">></a>
          </div>
          <ul class="slides">
            <?php
            $m = array(__("Jan"),__("Feb"),__("Mar"),__("Apr"),__("May"),__("Jun"),__("Jul"),__("Agu"),__("Sep"),__("Oct"),__("Nov"),__("Dec"));
            for($i=-1; $i < 2; $i++): ?>
              <?php for($j=1; $j <= 12; $j++): ?>
                <?php
                if(isset($_GET["m"])){
                  $class = ( date('Ym', mktime(0, 0, 0, $_GET["m"], 1, $_GET["y"]) ) == date('Ym', mktime(0, 0, 0, $j, 1, date("Y") + $i ) ) ) ? "active":"";
                }
                else{
                  $class = ( date('Ym') == date('Ym', mktime(0, 0, 0, $j, 1, date("Y") + $i ) ) ) ? "active":"";
                }
                $nbMois = (($j - 1) + (($i+1)*12));
                ?>
              <li>
                <div
                  class="<?php echo $class; ?>"
                  data-value12="<?php echo floor($nbMois / 12);?>"
                  data-value6="<?php echo floor($nbMois / 6);?>"
                  data-value3="<?php echo floor($nbMois / 3);?>">
                  <a href="<?php echo get_the_permalink() .'?m='.$j.'&y='. (date("Y") + $i );?>">
                    <?php echo $m[$j-1];?> <?php echo (date("Y") + $i );?>
                  </a>
                </div>
              </li>
              <?php endfor;?>
            <?php endfor;?>
          </ul>

        </div>
  		  <div id="archive-event" class="flexslider"><ul class="slides">
          <?php
          if(isset($_GET["m"]) && isset($_GET["y"]) && !empty($_GET["m"]) && !empty($_GET["y"])){
              $dateBegin = date('Ym', mktime(0, 0, 0, $_GET["m"], 1, $_GET["y"]));
              if(date('m')>9){
                $dateFinish = date('Ym', mktime(0, 0, 0, $_GET["m"] - 9, 1, $_GET["y"] + 1) );
              }
              else{
                $dateFinish = date('Ym', mktime(0, 0, 0, $_GET["m"] + 3, 1, $_GET["y"]));
              }
          }else{
            $dateBegin = date('Ym');
            if(date('m')>9){
              $dateFinish = date('Ym', mktime(0, 0, 0, date('m') -9, 1, date('Y') + 1) );
            }
            else{
              $dateFinish = date('Ym', mktime(0, 0, 0, date('m') + 3, 1, date('Y')) );
            }
          }
          $args = array (
            'posts_per_page'  => 4,
            'post_type' => 'event',
            'status' => 'publish',
            'meta_key'	=> 'date-begin',
          	'orderby'	=> 'meta_value_num',
          	'order'		=> 'ASC',
            'meta_query' => array(
              'relation' => 'AND',
              array(
                    'key'		=> 'date-begin',
                    'compare'	=> '>=',
                    'value'		=> $dateBegin,
              ),
              array(
                    'key'		=> 'date-begin',
                    'compare'	=> '<=',
                    'value'		=> $dateFinish,
                )
            ),
          );
          $posts = get_posts($args);
          if( $posts ) {
          	foreach( $posts as $post ) {
          		setup_postdata( $post );
              echo '<li>';
              get_template_part( 'template-parts/loop', 'event' );
              echo '</li>';

          	}
        	  wp_reset_postdata();
          }
          ?>
    		</div>
      </section>
      <section id="news">
        <h1 class="degrade"><?php _e("News & Articles", 'armar-pharma');?></h1>
        <div id="header-news">
          <div>
              <?php echo $content_news;?>
          </div>
          <div>
            <h2 class="degrade"><?php _e("Register your email & be informed", "armor-pharma");?></h2>
            <form class="form">
              <input type="email" required name="emailNewsletter" placeholder="<?php _e("Your email", "armor-pharma");?>" />
              <input type="submit" value="OK" />
            </form>
          </div>
          <div id="follow-linkedin">
            <div>
              <a href="">Follow<br />us on</a>
              <img src="<?php echo get_stylesheet_directory_uri() . '/assets/css/images/linkedin.png';?>" id="picto-linkedin" />
            </div>
          </div>
        </div>

        <div id="news-cat">
          <?php _e("CATEGORY", "armor-pharma");?> :
          <?php $terms = get_terms('category', array('hide_empty' => true));?>
          <form class="form">
          <?php foreach ($terms as $term): ?>
<input type="checkbox" id="cat_<?php echo $term->term_id;?>" onclick="checkboxCat()"  value="<?php echo $term->term_id;?>" class="checkboxCat" >
<label for="cat_<?php echo $term->term_id;?>" onclick="checkboxCat()"><?php echo $term->name;?></label>

          <?php endforeach;?>
          </form>
        </div>
  		  <div id="archive-news">
            <?php get_archive_news(); ?>
    		</div>
        <div class="text-center">
          <hr class="hrTriangle">
            <a href="#" id="more-news" class="">More news</a>
        </div>

      </section>

  	</div>
<?php get_footer(); ?>
