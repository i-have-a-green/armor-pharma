<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post();?>
  <div id="content-header">
    <div class="wrapper">
      <?php the_breadcrumb();?>
    </div>
  </div>
	<main id="main" role="main" class="wrapper">
		<article>
			<header class="wrapper" id="header-page">
				<h1 class="page-title degrade"><?php the_title();?></h1>
			</header>

			<!--<div class="wrapper" id="excerpt">
				<?php the_excerpt();?>
			</div>-->

			<div class="wrapper" id="thumbnail">
				<?php the_post_thumbnail( 'wpgreen-full'); ?>
			</div>

			<section class="entry-content wrapper" itemprop="articleBody">
				<?php the_content(); ?>
			</section>
		</article>
	</main>
<?php endwhile; endif; ?>
<?php get_footer(); ?>
