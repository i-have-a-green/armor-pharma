<?php
get_header();
/*
Template Name: Quality Risk
*/
?>
<?php if (have_posts()) : while (have_posts()) : the_post();?>
  <div id="content-header">
    <div class="wrapper">
      <?php the_breadcrumb();?>
    </div>
  </div>
	<main id="main" role="main" class="">
		<article>
			<header class="wrapper" id="header-page">
				<h1 class="page-title degrade">
          <?php //the_title();?>
          <?php $p = get_page_by_path("quality"); echo $p->post_title; ?>
        </h1>
			</header>
      <div class="wrapper">
        <?php get_template_part( 'template-parts/header', 'quality' );?>
      </div>
			<section class="entry-content" itemprop="articleBody">
        <div class="wrapper">
            <?php the_content();?>
        </div>
        <div class="wrapper" id="risk">
            <div id="faqRisk">
              <ul>
              <?php
              $terms = get_terms( 'category-risk' );
              $active = true;
              if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
                 foreach ( $terms as $term ) {
                    echo '<li class="category"><h2>'.$term->name.'</h2>';
                    $posts = get_posts(array(
                      "posts_per_page"    => -1,
                      'post_type'        => 'risk',
                      'post_status'      => 'publish',
                      'tax_query' => array(
                        array(
                          'taxonomy' => 'category-risk',
                          'field' => 'id',
                          'terms' => $term->term_id, // Where term_id of Term 1 is "1".
                          'include_children' => false
                        )
                      )
                    ));
                    echo '<ul class="post-risk">';
                    foreach ( $posts as $post ) :
                      setup_postdata( $post ); ?>
                    	<li>
                        <div class="post-risk-title" data-repeat="repeat_<?php echo get_the_id();?>">
                          <?php the_title(); ?>
                        </div>
                        <ul class="repeats">
                        <?php
                        if( have_rows('questions') ):
                          $i=0;
                          while ( have_rows('questions') ) : the_row();
                              if(get_current_user_id() > 0){
                                echo '<li class="repeat repeat_'.get_the_id().'">
                                  <div class="question" data-reponse="reponse_'.get_the_id().'_'.$i.'">
                                    <!--<span>'.__("QUESTION ","armor-pharma").' '.($i+1).' </span><br />-->
                                    '.get_sub_field('question').'
                                  </div>
                                  <div id="reponse_'.get_the_id().'_'.$i.'" class="reponse">
                                    '.get_sub_field('reponse').'
                                  </div>
                                </li>';
                              }
                              else{
                                echo '<li class="repeat repeat_'.get_the_id().'">
                                  <div class="question" data-reponse="reponse_'.get_the_id().'_'.$i.'"  onclick="openModalConnect(0);">
                                    <!--<span>'.__("QUESTION ","armor-pharma").' '.($i+1).' </span><br />-->
                                    '.get_sub_field('question').'
                                  </div>
                                  <div id="reponse_'.get_the_id().'_'.$i.'" class="reponse">

                                  </div>
                                </li>';
                              }
                              $i++;
                          endwhile;
                        endif;
                        ?>
                        </ul>
                      </li>
                    <?php endforeach;
                    wp_reset_postdata();
                    echo '</ul>';
                    echo '</li>';
                 }
              }
              ?>
              </ul>
            </div>
            <div id="downloadRisk">
              <?php if(get_current_user_id() > 0):?>
              <a href="<?php the_field("fichier");?>" download class="download">
              <?php else:?>
              <a href="#"  onclick="openModalConnect(0);" class="download">
              <?php endif;?>
                 <p>
                   <?php _e("Download full exipient information package","armor-pharma");?>
                 </p>
                 <div class="text-center">
                   <span class="picto picto-download"></span>
                 </div>
              </a>
            </div>
        </div>
			</section>
		</article>
	</main>
  <?php //get_template_part( 'template-parts/faq', '' );?>
  <div id="askExpert" class="wrapper">
    <hr>
    <div class="form">
      <label for="specTxt">
        <?php _e("Can't find what you're looking for ...", "arma-pharma");?>
        <input type="text" id="specTxt" name="specTxt" placeholder="<?php _e("Type your question", "armor-pharma");?>" />
        <buton class="button" id="submitAskExpert"><?php _e("Ask the expert","armar-pharma");?></buton>
      </label>
    </div>
  </div>
  <!-- MODAL -->
  <?php
    $user = wp_get_current_user();
    $current_user_exists = $user->exists();
    $genre = ($current_user_exists)?$user->civilite:'';
    $firstname = ($current_user_exists)?$user->user_firstname:'';
    $lastname = ($current_user_exists)?$user->user_lastname:'';
    $country = ($current_user_exists)?$user->country:'';
    $phone = ($current_user_exists)?$user->phone:'';
    $profil = ($current_user_exists)?$user->profil:'';
    $company = ($current_user_exists)?$user->company:'';
    $email = ($current_user_exists)?$user->user_email:'';
  ?>
  <div class="modal" id="modalAskExpert">
    <form id="form-expert" name="form-expert" action="<?php the_permalink();?>" method="post" enctype="multipart/form-data">
      <input type="hidden" name="action" value="formExpert">
      <input type="hidden" name="honey" value="">
      <?php wp_nonce_field('nonceformExpert', 'nonceformExpert'); ?>
      <div class="modal-content form">
        <div>
          <label for="Expert_Mr" class="notDisplayBlock">Mr</label><input type="radio" <?php echo ($genre == "Mr")?'checked':'';?> id="Expert_Mr" name="genre" value="Mr" />
          <label for="Expert_Mrs" class="notDisplayBlock">Mrs</label><input type="radio" <?php echo ($genre == "Mrs")?'checked':'';?> id="Expert_Mrs" name="genre" value="Mrs" />
        </div>
        <div class="grid2">
          <div>
            <label for="firstname"><?php _e("First name","armor-pharma");?>*</label>
            <input type="text" value="<?php echo $firstname;?>" name="firstname" id="firstname" required />
          </div>
          <div>
            <label for="lastname"><?php _e("Last name","armor-pharma");?>*</label>
            <input type="text" value="<?php echo $lastname;?>" name="lastname" id="lastname" required />
          </div>
        </div>
        <div class="grid2">
          <div>
            <label for="Expert_country"><?php _e("Country","armor-pharma");?>*</label>
            <input type="text" value="<?php echo $country;?>" name="country" id="Expert_country" required />
          </div>
          <div>
            <label for="Expert_phone"><?php _e("Phone","armor-pharma");?>*</label>
            <input type="tel" placeholder="+33 0 00 00 00 00" name="phone" value="<?php echo $phone;?>" id="Expert_phone"  />
          </div>
        </div>
        <div>
          <label id="Expert_profil">
            <?php _e("Profile","armor-pharma");?>*
          </label>
          <select name="profil" id="Expert_profil" required>
            <option <?php echo ($profil == "Responsible for excipients purchases")?'selected':'';?>>
              Responsible for excipients purchases
            </option>
            <option <?php echo ($profil == "Working on tablets/sachets/capsules formulation")?'selected':'';?>>
              Working on tablets/sachets/capsules formulation
            </option>
            <option <?php echo ($profil == "Working on Dry Powder Inhalers development")?'selected':'';?>>
              Working on Dry Powder Inhalers development
            </option>
            <option <?php echo ($profil == "Student in pharmaceutical formulation")?'selected':'';?>>
              Student in pharmaceutical formulation
            </option>
            <option <?php echo ($profil == "Professor/associate professor at University")?'selected':'';?>>
              Professor/associate professor at University
            </option>
            <option <?php echo ($profil == "PhD student")?'selected':'';?>>
              PhD student
            </option>
            <option <?php echo ($profil == "other")?'selected':'';?>>
              other
            </option>
          </select>
        </div>
        <div class="grid2">
          <div>
            <label for="Expert_company"><?php _e("Company","armor-pharma");?></label>
            <input type="text" name="company" id="Expert_company" required />
          </div>
          <div>
            <label for="email"><?php _e("Email","armor-pharma");?></label>
            <input type="email" name="email" id="email" required />
          </div>
        </div>
        <div class="border"></div>
        <div>
          <label for="subject"><?php _e("Subject","armor-pharma");?></label>
          <input type="text" readonly name="subject" id="subject" required />
        </div>
        <div>
          <label for="comments"><?php _e("Comments","armor-pharma");?></label>
          <textarea name="comments" id="comments" required ></textarea>
        </div>
        <div>
          <input type="checkbox" name="check" id="check" required />
          <label for="check">
            <?php _e("By submitting your data through this form, you confirm that you are above the age of 18, that you have read and understood the Privacy Policy, and that you agree to the collection, use and processing of your Personal Information by ARMOR PHARMA in accordance with said Policy","armor-pharma");?>
          </label>
        </div>
        <div class="text-center">
          <button class="button" id="sendAskExpert"><?php _e("SEND","armor-pharma");?></button>
        </div>
      </div>
      <div id="Mendatory">
        <?php _e("*Mandatory fields","armor-pharma");?>
      </div>
    </form>
  </div>

<?php endwhile; endif; ?>
<?php get_footer(); ?>
