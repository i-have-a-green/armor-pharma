<div class="wrapper" id="arianeDesign">
  <div>
    <div class="<?php echo ($item >= 'O')? 'active':'';?> <?php echo ($item == 'O')? 'smallActive':'';?>">
        <span>1</span><?php _e("select PRODUCT","armor-pharma");?>
    </div>
  </div>
  <div>
    <div class="<?php echo ($item>='1')? 'active':'';?> <?php echo ($item == '1')? 'smallActive':'';?>">
      <span>2</span><?php _e("register","armor-pharma");?>
    </div>
  </div>
  <div>
    <div class="<?php echo ($item>='2')? 'active':'';?> <?php echo ($item == '2')? 'smallActive':'';?>">
      <span>3</span><?php _e("specification","armor-pharma");?>
    </div>
  </div>
  <div>
    <div class="<?php echo ($item>='3')? 'active':'';?> <?php echo ($item == '3')? 'smallActive':'';?>">
      <span>4</span><?php _e("packaging","armor-pharma");?>
    </div>
  </div>
  <div>
    <div class="<?php echo ($item>='4')? 'active':'';?> <?php echo ($item == '4')? 'smallActive':'';?>">
      <span>5</span><?php _e("palletization","armor-pharma");?>
    </div>
  </div>
  <div>
    <div class="<?php echo ($item>='5')? 'active':'';?> <?php echo ($item == '5')? 'smallActive':'';?>">
      <span>6</span><?php _e("delivery","armor-pharma");?>
    </div>
  </div>
  <div>
    <div class="<?php echo ($item>='6')? 'active':'';?> <?php echo ($item == '6')? 'smallActive':'';?>">
      <span>7</span><?php _e("summary","armor-pharma");?>
    </div>
  </div>
  <div>
    <div class="<?php echo ($item>='7')? 'active':'';?> <?php echo ($item == '7')? 'smallActive':'';?>">
      <span>8</span><?php _e("validation to get a quote","armor-pharma");?>
    </div>
  </div>
  <div class="pictoArianne <?php echo ($item>='O')? 'active':'';?> <?php echo ($item=='O')? 'smallActive':'';?>"></div>
  <div class="pictoArianne <?php echo ($item>='1')? 'active':'';?> <?php echo ($item=='1')? 'smallActive':'';?>"></div>
  <div class="pictoArianne <?php echo ($item>='2')? 'active':'';?> <?php echo ($item=='2')? 'smallActive':'';?>"></div>
  <div class="pictoArianne <?php echo ($item>='3')? 'active':'';?> <?php echo ($item=='3')? 'smallActive':'';?>"></div>
  <div class="pictoArianne <?php echo ($item>='4')? 'active':'';?> <?php echo ($item=='4')? 'smallActive':'';?>"></div>
  <div class="pictoArianne <?php echo ($item>='5')? 'active':'';?> <?php echo ($item=='5')? 'smallActive':'';?>"></div>
  <div class="pictoArianne <?php echo ($item>='6')? 'active':'';?> <?php echo ($item=='6')? 'smallActive':'';?>"></div>
  <div class="pictoArianne <?php echo ($item>='7')? 'active':'';?> <?php echo ($item=='7')? 'smallActive':'';?>"></div>
</div>
