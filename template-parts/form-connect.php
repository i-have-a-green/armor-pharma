<div id="form-connect">
  <p>
    <?php _e("To register, please fill in the form below and we will grant you access to full technical and quality documentation, white papers and latest news !","armor-pharma");?>
  </p>
  <section id="signIn" class="form">
    <img src="<?php echo get_stylesheet_directory_uri() . '/assets/css/images/picto-user-form.png';?>" class="pictoForm">
    <h3><?php _e("Already have an account ? Sign in !","armor-pharma");?></h3>
    <hr />
    <?php
    $permalink = get_the_permalink();
    if(is_page("registration")){
      $permalink = get_the_permalink(get_page_by_path("design-the-lactose-you-like/my-customize-specification"));
      if(isset($_GET['id']) && !empty($_GET['id'])){
        $permalink .= '?id='.$_GET['id'];
      }
    }
    wp_login_form( array(
        'remember' => false,
        'redirect' => $permalink,
        'label_username' => __("Email","armor-pharma"),
        'id_username'    => 'connect_user_login',
	      'id_password'    => 'connect_user_pass',
    ) );
    ?>
    <?php if(isset($_GET["login"]) && $_GET["login"] == "failed" ):?>
      <span class="error">
        <?php _e("Invalid details used. Please check and try again","armor-pharma");?>
      </span>
    <?php endif;?>
    <div id="lostpassword">
      <a href="<?php echo wp_lostpassword_url(home_url()); ?>" title="Lost Password"><?php _e("Forgot your password?","armor-pharma");?></a>
    </div>

  </section>
  <section id="createAccount">
    <img src="<?php echo get_stylesheet_directory_uri() . '/assets/css/images/picto-user-form.png';?>" class="pictoForm">
    <h3><?php _e("Create your account","armor-pharma");?></h3>
    <hr />
    <?php echo wpgreen_form_user(false);?>
    <div id="Mendatory">
      <?php _e("*Mandatory fields","armor-pharma");?>
    </div>
  </section>
</div>
