<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article">
	<div class="image">
		<a href="<?php the_permalink() ?>"><?php the_post_thumbnail('400'); ?></a>
		<date><?php echo get_the_date();?></date> <?php wpgreen_get_the_category();?>
	</div>
  <header class="article-header">
		<h2>
			<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
				<?php the_title(); ?>
			</a>
		</h2>
	</header> <!-- end article header -->
	<section class="entry-content" itemprop="articleBody">
    <div class="excerpt-news">
		  <?php the_excerpt(); ?>
    </div>
    <div class="small-excerpt-news">
		  <?php the_field('small_description'); ?>
    </div>
		<p class="text-center">
			<a href="<?php the_permalink() ?>" class="button readmore small" title="en savoir plus">
				<?php _e('READ MORE', "armor-pharma");?>
			</a>
		</p>
	</section>
</article> <!-- end article -->
