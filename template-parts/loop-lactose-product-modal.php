<article id="modal-post-<?php the_ID(); ?>" <?php post_class('modal-product'); ?> role="article">
	<div class="image">
    <?php the_post_thumbnail('wpgreen-250'); ?>
    <h2 style="background-color:<?php echo $color;?>">
			<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
				<?php the_title(); ?>
			</a>
		</h2>
	</div>
	<section class="entry-content" itemprop="articleBody">
    <h2>
			<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>" style="color:<?php echo $color;?>">
				<?php single_term_title(); ?>
			</a>
		</h2>
    <div class="modal-lactose-description">
      <div class="pictoLactose">
        <?php $process = get_the_terms( $post->ID, 'process-lactose'); ?>
        <?php echo wp_get_attachment_image( get_field('picto',$process[0]), "wpgreen-75-90" );?>
      </div>
      <div class="suitable">
        <div>
          <?php _e("Suitable&nbsp;for&nbsp;:","armor-pharma");?>
        </div>
        <div class="imgApplications">
          <?php
            $applications = get_field("application2");
            foreach( $applications as $application ):
    		    ?>
            <div class="imgApplication">
              <img src="<?php echo get_stylesheet_directory_uri() . '/assets/css/images/'.$application.'.png';?>'" />
            </div>
            <?php
    	      endforeach;
          ?>
          <div class="x50">
            x50 : <?php the_field("psd_50");?> µm
          </div>
        </div>
      </div>
    </div>
	</section>
  <div class="text-center viewProduct">
      <a href="<?php the_permalink();?>"><?php _e("VIEW PRODUCT","armor-pharma");?></a>
  </div>

</article> <!-- end article -->
