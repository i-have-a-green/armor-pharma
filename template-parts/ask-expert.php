<div id="askExpert">
  <h3><?php _e("Help me finding the suitable lactose", "arma-pharma");?></h3>
  <div class="form">
    <label for="specFile">
      <?php _e("Upload your own specification ", "arma-pharma");?>
      <input type="file" id="specFile" name="specFile" onchange="console.log(this.value.replace(/.*[\/\\]/, ''));" />
    </label>
    <label for="specTxt">
      <?php _e("Or enter target PSD specifications :", "arma-pharma");?>
      <input type="text" id="specTxt" name="specTxt" placeholder="<?php _e("(please precise method Airjet or mechnical sieving)", "arma-pharma");?>" />
      <buton class="button" id="submitAskExpert"><?php _e("Ask the expert","armar-pharma");?></buton>
    </label>
  </div>
  <p class="small">
    <em><?php _e("Our experts will revert to you in the shortest delay !","armor-pharma");?></em>
  </p>
</div>
<?php
  $user = wp_get_current_user();
  $current_user_exists = $user->exists();
  $genre = ($current_user_exists)?$user->civilite:'';
  $firstname = ($current_user_exists)?$user->user_firstname:'';
  $lastname = ($current_user_exists)?$user->user_lastname:'';
  $country = ($current_user_exists)?$user->country:'';
  $phone = ($current_user_exists)?$user->phone:'';
  $profil = ($current_user_exists)?$user->profil:'';
  $company = ($current_user_exists)?$user->company:'';
  $email = ($current_user_exists)?$user->user_email:'';
?>
<!-- MODAL -->
<div class="modal" id="modalAskExpert">
  <form id="form-expert" name="form-expert" action="<?php the_permalink();?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="honey" value="">
    <input type="hidden" name="action" value="formExpert">
    <?php wp_nonce_field('nonceformExpert', 'nonceformExpert'); ?>
    <div class="modal-content form">
      <div>
        <label for="Expert_Mr" class="notDisplayBlock">Mr</label><input type="radio" <?php echo ($genre == "Mr")?'checked':'';?> id="Expert_Mr" name="genre" value="Mr" />
        <label for="Expert_Mrs" class="notDisplayBlock">Mrs</label><input type="radio" <?php echo ($genre == "Mrs")?'checked':'';?> id="Expert_Mrs" name="genre" value="Mrs" />
      </div>
      <div class="grid2">
        <div>
          <label for="firstname"><?php _e("First name","armor-pharma");?>*</label>
          <input type="text" value="<?php echo $firstname;?>" name="firstname" id="firstname" required />
        </div>
        <div>
          <label for="lastname"><?php _e("Last name","armor-pharma");?>*</label>
          <input type="text" value="<?php echo $lastname;?>" name="lastname" id="lastname" required />
        </div>
      </div>
      <div class="grid2">
        <div>
          <label for="Expert_country"><?php _e("Country","armor-pharma");?>*</label>
          <input type="text" value="<?php echo $country;?>" name="country" id="Expert_country" required />
        </div>
        <div>
          <label for="Expert_phone"><?php _e("Phone","armor-pharma");?></label>
          <input type="tel" placeholder="+33 0 00 00 00 00" name="phone" value="<?php echo $phone;?>" id="Expert_phone"  />
        </div>
      </div>
      <div>
        <label id="profil">
          <?php _e("Profile","armor-pharma");?>*
        </label>
        <select name="profil" id="profil" required>
          <option <?php echo ($profil == "Responsible for excipients purchases")?'selected':'';?>>
            Responsible for excipients purchases
          </option>
          <option <?php echo ($profil == "Working on tablets/sachets/capsules formulation")?'selected':'';?>>
            Working on tablets/sachets/capsules formulation
          </option>
          <option <?php echo ($profil == "Working on Dry Powder Inhalers development")?'selected':'';?>>
            Working on Dry Powder Inhalers development
          </option>
          <option <?php echo ($profil == "Student in pharmaceutical formulation")?'selected':'';?>>
            Student in pharmaceutical formulation
          </option>
          <option <?php echo ($profil == "Professor/associate professor at University")?'selected':'';?>>
            Professor/associate professor at University
          </option>
          <option <?php echo ($profil == "PhD student")?'selected':'';?>>
            PhD student
          </option>
          <option <?php echo ($profil == "other")?'selected':'';?>>
            other
          </option>
        </select>
      </div>
      <div class="grid2">
        <div>
          <label for="Expert_company"><?php _e("Company","armor-pharma");?>*</label>
          <input type="text" name="company" value="<?php echo $company;?>" id="Expert_company" required />
        </div>
        <div>
          <label for="email"><?php _e("Email","armor-pharma");?>*</label>
          <input type="email" name="email" value="<?php echo $email;?>" id="email" required />
        </div>
      </div>
      <div class="border"></div>
      <div>
        <label for="subject"><?php _e("Subject","armor-pharma");?></label>
        <input type="text" value="<?php _e("Help me finding the suitable lactose","armor-pharma");?>" name="subject" id="subject" required readonly />
      </div>
      <div>
        <label for="comments"><?php _e("Comments","armor-pharma");?></label>
        <textarea name="comments" id="comments" required ></textarea>
      </div>
      <div>
        <input type="checkbox" name="check" id="check" required />
        <label for="check">
          <?php $p = get_page_by_path( "privacy-policy-cookie-policy" );?>
          <?php _e("By submitting your data through this form, you confirm that you are above the age of 18, that you have read and understood the <a href='".get_the_permalink( $p)."' target='_blank'>Privacy Policy</a>, and that you agree to the collection, use and processing of your Personal Information by ARMOR PHARMA in accordance with said Policy","armor-pharma");?>*
        </label>
      </div>
      <div class="text-center">
        <button type="submit" class="button submitBtn" id="sendAskExpert"><?php _e("SEND","armor-pharma");?></button>
      </div>
    </div>
    <div id="Mendatory">
      <?php _e("*Mandatory fields","armor-pharma");?>
    </div>
  </form>
</div>
