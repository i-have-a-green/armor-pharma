<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article">
	<header class="article-header">
    <a href="<?php the_permalink();?>">
      <div class="event-image" style="background-image:url(<?php echo get_the_post_thumbnail_url($post->ID,'medium');?>);">
        <date class="polyDate"><?php the_field("label-date");?></date>
      </div>
    </a>
    <h2><a href="<?php the_permalink();?>"><?php the_title();?></a></h2>
	</header> <!-- end article header -->
	<section class="entry-content" itemprop="articleBody">
    <p class="event-place">
      <?php the_field("event-place");?>
    </p>
		<?php the_excerpt(); ?>
		<p>
			<a href="<?php the_permalink() ?>" class="button readmore small">
				<?php _e("Read more", "armor-pharma");?>
			</a>
		</p>
	</section>
</article> <!-- end article -->
