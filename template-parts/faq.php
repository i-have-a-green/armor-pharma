<div id="FAQ" class="text-center">
  <div class="wrapper">
    <h1 class="degrade semiCercle small"><?php _e("FAQ","armor-pharma");?></h1>
    <h2><?php _e("Many things you would like to know about lactose","armor-pharma");?></h2>
    <p>
      <?php if(get_current_user_id() == 0):?>
      <em onclick="openModalConnect(0);" ><?php _e("You need to be register to get full access !","armor-pharma");?></em>
    <?php endif;?>
    </p>
    <?php $categories = get_terms( 'category-faq' );
    foreach ($categories as $category) :
      $args = array(
        "posts_per_page"=>-1,
        'post_type' => 'faq',
        'tax_query' => array(
          array(
            'taxonomy' => 'category-faq',
            'field'    => 'id',
            'terms'    => $category->term_id,

          )
        ),
      );
      $query = new WP_Query($args);
      if ( $query->have_posts() ) :
        echo "<div class='faq-cat'><h3 data-id='".$category->term_id."'>".$category->name."</h3>";
        echo '<div class="questions" id="question_'.$category->term_id.'">';
        while ( $query->have_posts() ) :
          $query->the_post();
          if(get_current_user_id() > 0){
            echo '<p class="question" data-id="'.get_the_id().'">
              '. get_the_title().'
              <div class="answer" id="answer_'.get_the_id().'">
                '.get_the_content().'
              </div>
              <hr />
            </p>';
          }
          else{
            echo '<p class="question" onclick="openModalConnect(0)">
              '. get_the_title().'
              <div class="answer" id="answer_'.get_the_id().'">
                <em onclick="openModalConnect(0);" >'. __("You need to be register to get full access !","armor-pharma").'</em>
              </div>
              <hr />
            </p>';
          }
        endwhile;
        echo '</div></div>';
        wp_reset_postdata();

      endif;
    endforeach;
    ?>
  </div>
</div>
