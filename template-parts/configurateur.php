<div class="wrapper" id="formSearchLactose">
  <div class="form">
    <div id="application">
      <div id="application-content">
        <?php _e("Select your application&nbsp;<br />and dosage form :", "armor-pharma");?>
      </div>
      <div id="application-values">
        <input type="checkbox" id="application1" value="1" class="checkboxApplication" onchange="updateConfigurateur()" >
        <label for="application1">
          <img src="<?php echo get_stylesheet_directory_uri() . '/assets/css/images/small-gelule.png';?>'" />
          <?php _e("Capsule", "armor-pharma");?>
        </label><br />
        <input type="checkbox" id="application2" value="2" class="checkboxApplication"  onchange="updateConfigurateur()">
        <label for="application2">
          <img src="<?php echo get_stylesheet_directory_uri() . '/assets/css/images/small-sachet.png';?>'" />
          <?php _e("Sachet", "armor-pharma");?>
        </label><br />
        <input type="checkbox" id="application3" value="3" class="checkboxApplication"  onchange="updateConfigurateur()">
        <label for="application3">
          <img src="<?php echo get_stylesheet_directory_uri() . '/assets/css/images/small-comprime.png';?>'" />
          <?php _e("Tablet <small>(wet or dry granulation)</small>", "armor-pharma");?>
        </label><br />
        <input type="checkbox" id="application4" value="4" class="checkboxApplication"  onchange="updateConfigurateur()">
        <label for="application4">
          <img src="<?php echo get_stylesheet_directory_uri() . '/assets/css/images/small-comprime.png';?>'" />
          <?php _e("Tablet <small>(direct compaction)</small>", "armor-pharma");?>
        </label><br />
        <input type="checkbox" id="application5" value="5" class="checkboxApplication"  onchange="updateConfigurateur()">
        <label for="application5">
          <img src="<?php echo get_stylesheet_directory_uri() . '/assets/css/images/small-inhalation.png';?>'" />
          <?php _e("Dry Powder Inhaler", "armor-pharma");?>
        </label>
      </div>
    </div>

    <div id="configurateurGrid">
      <div id="configurateur">
        <h3><?php _e("Particle size distribution", "armor-pharma");?></h3>
        <div class="configurateur-section">
          <div class="configurateur-label">
            <?php info("laser-diffraction");?><h4><?php _e("LASER DIFFRACTION", "armor-pharma");?></h4>
          </div>
          <div class="configurateur-values">
            <?php configateurValeur("psd_10","x10", "0μm", "800μm", 0, 800, 150, 680);?>
            <?php configateurValeur("psd_50","x50", "0μm", "800μm", 0, 800, 220, 680);?>
            <?php configateurValeur("psd_90","x90", "0μm", "800μm", 0, 800, 300, 500);?>
          </div>

          <!--<div class="configurateur-label">
            <?php info("airjet-sieve");?><h4><?php _e("AIRJET SIEVE", "armor-pharma");?></h4>
          </div>
          <div class="configurateur-values">
            <div class="configurateur-value">
              <div class="configurateur-value-label">
                <?php _e("Particle size:","armor-pharma");?>
              </div>
              <div>
              </div>
              <div>
                <select id="airjetSieveParticleSize" onchange="updateConfigurateur()">
                  <option value=""><?php _e("All sizes","armor-pharma");?></option>
                  <option value="45">45 µm</option>
                  <option value="63">63 µm</option>
                  <option value="75">75 µm</option>
                  <option value="100">100 µm</option>
                  <option value="150">150 µm</option>
                  <option value="250">250 µm</option>
                  <option value="315">315 µm</option>
                  <option value="355">355 µm</option>
                  <option value="500">500 µm</option>
                </select>
              </div>
              <div>
              </div>
            </div>
            <?php// configateurValeur("psd_airjet","Limits in %", "0%", "100%", 0, 100, 20, 60);?>
          </div>-->
          
        </div>
        <input type="hidden" name="airjetSieveParticleSize" value="">
        <h3><?php _e("Density", "armor-pharma");?></h3>
        <div class="configurateur-section">
          <div class="configurateur-label">
            <?php info("poured-density");?><h4><?php _e("Poured density", "armor-pharma");?></h4>
          </div>
          <div class="configurateur-values">
            <?php configateurValeur("poured_density","", "0,2g/mL", "1,00g/mL", "0.2", 1, "0.4", "0.9", '0.1');?>
          </div>

          <div class="configurateur-label">
            <?php info("tapped-density");?><h4><?php _e("TAPPED DENSITY", "armor-pharma");?></h4>
          </div>
          <div class="configurateur-values">
            <?php configateurValeur("tapped_density","", "0,3g/mL", "1,10g/mL", "0.3", "1.1", "0.6", "1", '0.1');?>
          </div>
        </div>
        <h3><?php _e("Flowability", "armor-pharma");?></h3>
        <div class="configurateur-section">
          <div class="configurateur-label">
            <?php info("hausner-ratio");?><h4><?php _e("Hausner ratio", "armor-pharma");?></h4>
          </div>
          <div class="configurateur-values">
            <?php configateurValeur("hausner_ratio","", "1", "1,9", "1", "1.9", "1", "1.3", '0.1');?>
          </div>
        </div>
        <div class="configurateur-section">
          <div class="configurateur-label">
            <?php info("carrs-index");?><h4><?php _e("CARR'S INDEX", "armor-pharma");?></h4>
          </div>
          <div class="configurateur-values">
            <?php configateurValeur("carr_s_index","", "0%", "50%", "0", "50", "10", "40");?>
          </div>
        </div>
      </div>
      <div id="configurateurRightCol">
        <div id="configurateurButton">

          <?php _e("Check matching lactose grades","armor-pharma");?>
          <div id="displayNbrResult">&nbsp;</div>
          <div class="text-center picto-more">
            <img src="<?php echo get_stylesheet_directory_uri() . '/assets/css/images/more.svg';?>'" class="more" />
          </div>

        </div>
        <img id="demiCercle" src="<?php echo get_stylesheet_directory_uri();?>/assets/css/images/illustrationCircle.png" />
      </div>
    </div>
  </div>
  <?php get_template_part( 'template-parts/ask', 'expert' );?>
  <div id="cont-res-configurateur">
    <h1 class="degrade"><?php _e("Your search results","armor-pharma");?></h1>
    <p>
      <?php _e("Thanks for trying our product finder.<br />
        The following products are in line with your requirements : add FREE samples to your cart or get them customized!","armor-pharma");?>
    </p>
    <div id="res-configurateur">
    </div>
  </div>
</div>
<!-- MODAL -->
<div class="modal" id="modalInhaler">
  <form id="formInhaler" name="formInhaler" action="<?php the_permalink();?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="action" value="formExpert">
    <input type="hidden" name="honey" value="">
    <?php wp_nonce_field('nonceformExcipure', 'nonceformExcipure'); ?>
    <?php
      $user = wp_get_current_user();
      $current_user_exists = $user->exists();
      $genre = ($current_user_exists)?$user->civilite:'';
      $firstname = ($current_user_exists)?$user->user_firstname:'';
      $lastname = ($current_user_exists)?$user->user_lastname:'';
      $country = ($current_user_exists)?$user->country:'';
      $phone = ($current_user_exists)?$user->phone:'';
      $profil = ($current_user_exists)?$user->profil:'';
      $company = ($current_user_exists)?$user->company:'';
      $email = ($current_user_exists)?$user->user_email:'';
    ?>
    <div class="modal-content form">
      <p><h2>Dry Powder Inhaler</h2></p>
      <p>
        We would be delighted to discuss your requirements further.
      </p>

      <div>
        <label for="excipure_Mr" class="notDisplayBlock">Mr</label><input type="radio" <?php echo ($genre == "Mr")?'checked':'';?> id="excipure_Mr" name="genre" value="Mr" />
        <label for="excipure_Mrs" class="notDisplayBlock">Mrs</label><input type="radio" <?php echo ($genre == "Mrs")?'checked':'';?> id="excipure_Mrs" name="genre" value="Mrs" />
      </div>
      <div class="grid2">
        <div>
          <label for="excipure_firstname"><?php _e("First name","armor-pharma");?>*</label>
          <input type="text" value="<?php echo $firstname;?>" name="firstname" id="excipure_firstname" required />
        </div>
        <div>
          <label for="excipure_lastname"><?php _e("Last name","armor-pharma");?>*</label>
          <input type="text" value="<?php echo $lastname;?>" name="lastname" id="excipure_lastname" required />
        </div>
      </div>
      <div class="grid2">
        <div>
          <label for="excipure_country"><?php _e("Country","armor-pharma");?>*</label>
          <input type="text" value="<?php echo $country;?>" name="country" id="excipure_country" required />
        </div>
        <div>
          <label for="excipure_phone"><?php _e("Phone","armor-pharma");?></label>
          <input type="tel" placeholder="+33 0 00 00 00 00" name="phone" value="<?php echo $phone;?>" id="excipure_phone"  />
        </div>
      </div>
      <div class="grid2">
          <div>
          <label id="excipure_profil">
            <?php _e("Profile","armor-pharma");?>*
          </label>
          <select name="profil" id="excipure_profil" required>
            <option <?php echo ($profil == "Responsible for excipients purchases")?'selected':'';?>>
              Responsible for excipients purchases
            </option>
            <option <?php echo ($profil == "Working on tablets/sachets/capsules formulation")?'selected':'';?>>
              Working on tablets/sachets/capsules formulation
            </option>
            <option <?php echo ($profil == "Working on Dry Powder Inhalers development")?'selected':'';?>>
              Working on Dry Powder Inhalers development
            </option>
            <option <?php echo ($profil == "Student in pharmaceutical formulation")?'selected':'';?>>
              Student in pharmaceutical formulation
            </option>
            <option <?php echo ($profil == "Professor/associate professor at University")?'selected':'';?>>
              Professor/associate professor at University
            </option>
            <option <?php echo ($profil == "PhD student")?'selected':'';?>>
              PhD student
            </option>
            <option <?php echo ($profil == "other")?'selected':'';?>>
              other
            </option>
          </select>
        </div>   
        <div>
          <label for="excipure_company"><?php _e("Company","armor-pharma");?>*</label>
          <input type="text" name="company" value="<?php echo $company;?>" id="excipure_company" required />
        </div>
      </div>
      <div class="grid2">
        <div>
          <label for="excipure_email"><?php _e("Email","armor-pharma");?>*</label>
          <input type="email" name="email" value="<?php echo $email;?>" id="excipure_email" required />
        </div>
      </div>
      <div>
        <label for="excipure_subject"><?php _e("Subject","armor-pharma");?></label>
        <input type="text" name="subject" id="excipure_subject" required readonly value="Question for the INHALATION expert" />
      </div>
      <div>
        <label for="excipure_comments"><?php _e("Comments","armor-pharma");?></label>
        <textarea name="comments" width="100%"  id="excipure_comments" required placeholder="Please precise your need"></textarea>
      </div>

      <div class="border"></div>
      
      <p>Do you want one of our experts to contact you?</p>

      <div>
        <p>
          If so how would you like to be recontacted:
        </p>
        <div>
          <input type="checkbox" id="exipure_by_mail" name="exipure_by[]" value="email" /><label for="exipure_by_mail">By email</label>
          <input type="checkbox" id="exipure_by_tel" name="exipure_by[]" value="phone" /><label for="exipure_by_tel">By phone</label>
        </div>
      </div>
      <div>
        <label for="dateInhaler"><?php _e("Any schedule preference?","armor-pharma");?></label>
        <input type="datetime-local" name="date" value="" id="dateInhaler" />
      </div>
      
      <div>
        <input type="checkbox" name="check" id="checkInhaler" required />
        <label for="checkInhaler">
          <?php $p = get_page_by_path( "privacy-policy-cookie-policy" );?>
          <?php _e("By submitting your data through this form, you confirm that you are above the age of 18, that you have read and understood the <a href='".get_the_permalink( $p)."' target='_blank'>Privacy Policy</a>, and that you agree to the collection, use and processing of your Personal Information by ARMOR PHARMA in accordance with said Policy","armor-pharma");?>*
        </label>
      </div>
      <div class="text-center">
        <button type="submit" class="button submitBtn" id="sendAskInhaler"><?php _e("SEND","armor-pharma");?></button>
      </div>
    </div>
    <div id="Mendatory">
      <?php _e("*Mandatory fields","armor-pharma");?>
    </div>
  </form>
</div>
