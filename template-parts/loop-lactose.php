<div class="categories-lactose">
<?php
$terms = get_terms( 'category-lactose' );
if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
   echo '<div id="category-lactose">';
   foreach ( $terms as $term ) {
     $image = get_field('image', $term);
     echo '<div class="category-lactose">
        <a href="'.get_term_link( $term ).'">' . wp_get_attachment_image( $image , 'wpgreen-400' ) . '</a>
        <div class="category-lactose-title">
          <a href="'.get_term_link( $term ).'"><h3 style="color:'.get_field('color', $term).'">' . get_field('title', $term). '</h3></a>
        </div>
        <hr class="hrColor" style="border-color:'.get_field('color', $term).'" />

        <p>' . $term->description . '</p>

        <hr class="hrTriangle" />

        <a href="'.get_term_link( $term ).'" class="link" style="color:'.get_field('color', $term).'">'.__("Discover", "armor-pharma").'</a>

      </div>';
   }
   echo '</div>';
}
?>

<?php
$terms = get_terms( 'category-lactose' );
if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
   echo '<div id="category-lactose-mobile"class="flexslider"><ul class="slides">';
   foreach ( $terms as $term ) {
     $image = get_field('image', $term);
     echo '<li class="category-lactose">
        <a href="'.get_term_link( $term ).'">' . wp_get_attachment_image( $image , 'wpgreen-400' ) . '</a>
        <div class="category-lactose-title">
          <a href="'.get_term_link( $term ).'"><h3 style="color:'.get_field('color', $term).'">' . get_field('title', $term). '</h3></a>
        </div>
        <hr style="border-color:'.get_field('color', $term).'" />

        <p>' . $term->description . '</p>

        <hr class="hrTriangle" />

        <a href="'.get_term_link( $term ).'" class="link" style="color:'.get_field('color', $term).'">'.__("Discover", "armor-pharma").'</a>

      </li>';
   }
   echo '</ul></div>';
}
?>
</div>
