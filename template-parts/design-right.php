<?php
  $design = get_user_meta(get_current_user_id(), 'design', true);
  $design = json_decode(stripslashes($design));
?>
<div>
  <section id="myProduct">
    <div class="contPictoIllu">
        <img src="<?php echo get_stylesheet_directory_uri() . '/assets/css/images/myProduct.png';?>" class="pictoIllu">
    </div>
    <h3>
      <a href="<?php echo get_the_permalink(get_page_by_path( 'design-the-lactose-you-like' ) );?>"><?php _e("My product","armor-pharma");?><span class="picto picto-update">Modify</span></a>
    </h3>
    <p>
      <?php echo $design->lactose->name;?>
    </p>

  </section>
  <?php if(isset($design->pharmacopoeial)):?>
  <section id="myCustomized">
    <div class="contPictoIllu">
        <img src="<?php echo get_stylesheet_directory_uri() . '/assets/css/images/customization.png';?>" class="pictoIllu">
    </div>
    <h3>
      <a href="<?php echo get_the_permalink(get_page_by_path( 'design-the-lactose-you-like/my-customize-specification/' ) );?>"><?php _e("My customized specification","armor-pharma");?><span class="picto picto-update"></span></a>
    </h3>
    <p>
      <?php if(isset($design->pharmacopoeial)):?>
        <h4><?php _e("Pharmacopoeial conformance","armor-pharma");?></h4>
        <?php foreach ($design->pharmacopoeial as $val) {
          echo '<span class="standard'.$val->standard.'">'.$val->label.'</span><br />';
        }
      endif;?>
      <?php if(isset($design->microbial)):?>
        <h4><?php _e("Microbial limits","armor-pharma");?></h4>
        <?php foreach ($design->microbial as $val) {
          $valMicrobial = (empty($val->value))?$val->valueStandard:$val->value;
          echo '<span class="standard'.$val->standard.'">'.$val->label.' : '.$valMicrobial.' '.$val->unit.'</span><br />';
        }
      endif;?>

      <?php if(isset($design->anotherTest)):?>
        <h4><?php _e("Additional test","armor-pharma");?></h4>
        <?php foreach ($design->anotherTest as $val) {
          echo '<span class="standard">'.$val->label.' : '.$val->limits.' '.$val->method.'</span><br />';
        }
      endif;?>

      <?php if(isset($design->particule)):?>
        <h4><?php _e("Particule size distribution (Air jet sieve) :","armor-pharma");?></h4>
        <?php foreach ($design->particule as $val) {
          echo '<span class="standard'.$val->standard.'">'.$val->label.' '.$val->value.'</span><br />';
        }
      endif;?>

      <?php if(isset($design->fileSpecification)):?>
        <h4><?php _e("Specification file :","armor-pharma");?></h4>
        <?php echo '<span><a href="'.$design->fileSpecification->link.'" target="_blank" class="standard">'.$design->fileSpecification->file.'</a></span><br />';
      endif;?>

      <?php if(isset($design->targetSpecification) && !empty($design->targetSpecification)):?>
        <h4><?php _e("Specification target :","armor-pharma");?></h4>
        <?php echo '<span class="standard">'.$design->targetSpecification.'</span><br />';
      endif;?>

    </p>
  </section>
<?php endif;?>
<?php if(isset($design->packaging)):?>
  <section id="myPackaging">
    <div class="contPictoIllu">
        <img src="<?php echo get_stylesheet_directory_uri() . '/assets/css/images/packaging.png';?>" class="pictoIllu">
    </div>
    <h3>
      <a href="<?php echo get_the_permalink(get_page_by_path( 'design-the-lactose-you-like/packaging/' ) );?>"><?php _e("My packaging","armor-pharma");?><span class="picto picto-update">Modify</span></a>
    </h3>
    <p>
      <h4 class="standard<?php echo ($design->packaging->standard)?'1':'';?>"><?php echo $design->packaging->label;?></h4>
      <span  class="standard<?php echo ($design->packaging->standard)?'1':'';?>">
      <?php foreach ($design->packaging->packagingOptions as $val) :?>
        <?php echo $val->valueText;?><br />
      <?php endforeach;?>
      <?php if(isset($design->filePackaging) && !empty($design->filePackaging->file)):
        echo 'File : <a href="'.$design->filePackaging->link.'" target="_blank" class="standard">'.$design->filePackaging->file.'</a><br />';
      endif;
      echo '<p class="standard">'.$design->targetPackaging.'</p>';
      ?>
    </span>
    </p>
  </section>
<?php endif;?>
<?php if(isset($design->palet)): ?>

  <section id="myPalletization">
    <div class="contPictoIllu">
        <img src="<?php echo get_stylesheet_directory_uri() . '/assets/css/images/pallette.png';?>" class="pictoIllu">
    </div>
    <h3>
      <a href="<?php echo get_the_permalink(get_page_by_path( 'design-the-lactose-you-like/palletization/' ) );?>"><?php _e("My palletization","armor-pharma");?><span class="picto picto-update">Modify</span></a>
    </h3>
    <p>
      <h4><?php _e("Adjust my palet","armor-pharma");?></h4>
      <?php foreach ($design->palet->typePalet as $val){
        if("100 X 120 cm" != $val->valueText){
          echo '<span class="standard">'.$val->valueText.'</span><br />';
        }
        else {
          echo '<span class="">'.$val->valueText.'</span><br />';
        }

      }?>
      <h4><?php _e("The type of pallet","armor-pharma");?></h4>
      <?php foreach ($design->palet->adjustPalet as $val){
        $standard = "standard";
        if($val->valueText == "Adjust maximum pallet height (cm) : 185"){
          $standard = "";
        }
        echo '<span class="'.$standard.'">'.$val->valueText.' cm</span><br />';
      }?>
      <h4><?php _e("Pallet labelling","armor-pharma");?></h4>
      <?php if(isset($design->filePalet) && !empty($design->filePalet->file)):
        echo 'File : <a href="'.$design->filePalet->link.'" target="_blank" class="standard">'.$design->filePalet->file.'</a><br />';
      endif;?>
      <?php if(isset($design->targetPalet) && !empty($design->targetPalet)):
        echo '<span  class="standard">'.$design->targetPalet.'</span><br />';
      endif;?>
    </p>
  </section>
<?php endif;?>
<?php if(isset($design->delivery)): ?>
  <section id="myDelivery">
    <div class="contPictoIllu">
        <img src="<?php echo get_stylesheet_directory_uri() . '/assets/css/images/livraison.png';?>" class="pictoIllu">
    </div>
    <h3>
      <a href="<?php echo get_the_permalink(get_page_by_path( 'design-the-lactose-you-like/delivery/' ) );?>"><?php _e("My delivery","armor-pharma");?><span class="picto picto-update">Modify</span></a>
    </h3>
    <p>
      <h4><?php _e("Adjust my palet","armor-pharma");?></h4>
      <span  class="standard">
        <?php echo $design->delivery->needQuantity->valueText.'<br />';?>
        <?php echo $design->delivery->needText.'<br />';?>
      </span>
      <?php if(isset($design->delivery->company)):?>
        <h4><?php _e("Place of delivery","armor-pharma");?></h4>
        <?php echo $design->delivery->company.'<br />';?>
        <?php echo $design->delivery->address.'<br />';?>
        <?php echo $design->delivery->zipCode.' ';?>
        <?php echo $design->delivery->city.'<br />';?>
        <?php echo $design->delivery->stateProvince.'<br />';?>
        <?php echo $design->delivery->country.'<br />';?>
      <?php endif;?>
    </p>
  </section>
<?php endif;?>
</div>
