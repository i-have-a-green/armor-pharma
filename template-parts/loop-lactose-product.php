<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article">
	<div class="image">
    <?php
    $image = get_field('image_zoom');
    $size = 'full'; // (thumbnail, medium, large, full or custom size)
    if( $image ) {
      $image = wp_get_attachment_image_url( $image, $size );
    }
    the_post_thumbnail('wpgreen-400', array('id'=>'image-'.get_the_id(),'class'=>'imgZoom', 'data-img-full'=>$image , 'data-title'=>get_the_lactose() ));
    //the_post_thumbnail('wpgreen-400', array('id'=>'image-'.get_the_id(),'class'=>'imgZoom', 'data-img-full'=>get_the_post_thumbnail_url(get_the_id(), 'full') )); ?>
    <!--<div id="myresult-<?php the_id();?>" class="img-zoom-result"></div>
    <script>
      window.addEventListener("load", function() {
        imageZoom("image-<?php the_id();?>", "myresult-<?php the_id();?>");
      });
    </script>-->
    <h2 style="background-color:<?php echo $color;?>">
			<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
				<?php the_title(); ?>
			</a>
		</h2>
	</div>
  <div class="form">
    <input type="checkbox" value="<?php the_ID();?>" checked class="compareLactose" id="compare<?php the_ID();?>" data-id="<?php the_ID();?>">
    <label for="compare<?php the_ID();?>"><?php _e("Compare","armor-pharma");?></label>
  </div>
  <header class="article-header">
    <u><?php _e("Key benefits","armor-pharma");?></u> : <?php the_field('key_benefits');?>
	</header> <!-- end article header -->
	<section class="entry-content" itemprop="articleBody">
    <div class="suitable">
      <div>
        <?php _e("Suitable&nbsp;for&nbsp;:","armor-pharma");?>
      </div>
      <div class="imgApplications">
        <?php
          $applications = get_field("application2");
          if($applications):
          foreach( $applications as $application ):
  		    ?>
          <div class="imgApplication">
            <img src="<?php echo get_stylesheet_directory_uri() . '/assets/css/images/'.$application.'.png';?>'" />
          </div>
          <?php
          endforeach;
          endif;
        ?>
      </div>
    </div>
    <div class="x50 text-center" style="color:<?php echo $color;?>">
      x50 : <?php the_field("psd_50");?> µm
    </div>
		<p class="text-center">
			<a href="<?php the_permalink() ?>" class="button readmore small" title="en savoir plus">
				<?php _e('LEARN MORE', "armor-pharma");?>
			</a>
      <a href="<?php echo get_permalink( get_page_by_path( "design-the-lactose-you-like/my-customize-specification" ) ).'?id='.get_the_id();?>" class="button readmore small" title="en savoir plus">
				<?php _e('CUSTOMIZE IT', "armor-pharma");?>
			</a>
		</p>
	</section>
  <hr class="hrTriangle" />
  <div class="contCartLink">
      <a href="#" class="cartLink" data-lactose-id="<?php the_id();?>" style="color:<?php echo $color;?>"><span class="picto"></span><?php _e("GET FREE SAMPLE","armor-pharma");?></a>
  </div>

</article> <!-- end article -->
<script>
  datasets.push({"label":"<?php the_title();?>",
    "data":[<?php the_field("graphe_compressibility");?>,
      <?php the_field("graphe_flowability");?>,
      <?php the_field("graphe_cohesivness");?>,
      <?php the_field("graphe_poured_density");?>,
      <?php the_field("graphe_psd_50");?>,
      <?php the_field("graphe_psd_span");?>,
      <?php the_field("graphe_specific_surface_area");?>],
    "dataLabel":[<?php the_field("carr_s_index");?>,
      <?php the_field("hausner_ratio");?>,
      <?php the_field("tapped_density");?>,
      <?php the_field("poured_density");?>,
      <?php the_field("psd_50");?>,
      <?php the_field("psd_airjet");?>,
      <?php the_field("specific_surface_area");?>],
    "fill":true,
    "backgroundColor":"<?php the_field("color");?>00",
    "borderColor":"<?php the_field("color");?>",
    "pointBackgroundColor":"<?php the_field("color");?>",
    "pointBorderColor":"#fff",
    "pointHoverBackgroundColor":"#fff",
    "pointHoverBorderColor":"<?php the_field("color");?>"});
</script>
