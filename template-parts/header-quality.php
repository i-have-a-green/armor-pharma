<div id="header-quality">
  <div id="header-quality-content">
    <div>
      <?php $p = get_page_by_path("quality"); echo $p->post_content; ?>
    </div>
    <div id="contDiscover">
      <img id="demiCercle" src="<?php echo get_stylesheet_directory_uri();?>/assets/css/images/illustrationCircle.png" />
      <?php $info = get_field("info", $p); ?>
      <a class="picto-info" data-slug="<?php echo $info->post_name;?>">
        <div id="discover">
          <div>
            <?php _e("DISCOVER MORE ABOUT EXCIPACT","armor-pharma");?>
          </div>
          <div class="text-center">
            <span class="picto picto-plus"></span>
          </div>
        </div>
      </a>
    </div>
  </div>
  <?php  wpgreen_top_nav("quality-nav"); ?>
</div>
