<div id="account">
  <img id="illuaAccount" src="<?php echo get_stylesheet_directory_uri();?>/assets/css/images/account.png" />
  <span id="hello"><?php _e("Hello","armor-pharma");?></span><br />
  <?php global $current_user; wp_get_current_user(); ?>
  <h3><?php echo $current_user->display_name;?></h3>
  <hr />
  <div id="signOut">
      <a href="<?php echo wp_logout_url( home_url() ); ?>"><span class="picto picto-logOut"></span><?php _e("Sign out","armor-pharma");?></a>
  </div>

</div>
<div id="menu">
  <?php if(in_array('administrator', $current_user->roles) || in_array('distributor', $current_user->roles)):?>
    <a href="<?php echo get_post_type_archive_link( 'news-distributor' ); ?>"><?php _e("Distributor area","armor-pharma");?></a>
  <?php endif;?>
  <a href="<?php echo get_the_permalink(get_page_by_path("my-samples-orders"));?>" class="<?php echo ($active == 1)?'active':'';?>">
    <?php _e("My samples orders","armor-pharma");?>
  </a>
  <a href="<?php echo get_the_permalink(get_page_by_path("my-coa"));?>" class="<?php echo ($active == 2)?'active':'';?>">
    <?php _e("My CoA","armor-pharma");?>
  </a>
  <a href="<?php echo get_the_permalink(get_page_by_path("my-details"));?>" class="<?php echo ($active == 3)?'active':'';?>">
    <?php _e("My details","armor-pharma");?>
  </a>
  <a href="<?php echo get_the_permalink(get_page_by_path("my-newsletters"));?>" class="<?php echo ($active == 4)?'active':'';?>">
    <?php _e("My subscriptions","armor-pharma");?>
  </a>
  <a href="<?php echo get_the_permalink(get_page_by_path("change-password"));?>" class="<?php echo ($active == 5)?'active':'';?>">
    <?php _e("Change password","armor-pharma");?>
  </a>
  <a href="<?php echo get_the_permalink(get_page_by_path("contact"));?>" class="help">
    <?php _e("Need help ?","armor-pharma");?>
  </a>
</div>
