<?php
get_header();
/*
Template Name: Technical library
*/
?>
<?php if (have_posts()) : while (have_posts()) : the_post();?>
  <div id="content-header">
    <div class="wrapper">
      <?php the_breadcrumb();?>
    </div>
  </div>
	<main id="main" role="main" class="wrapper">
		<article>
			<header class="wrapper" id="header-page">
				<h1 class="page-title degrade"><?php the_title();?></h1>
			</header>
			<section class="entry-content wrapper" itemprop="articleBody">
				<?php the_content(); ?>
        <h2><?php _e("Download all technical documents","armor-pharma");?></h2>

        <div id="contSelectLactose">
          <div>
            <strong><?php _e("Select your product and download full related technical documentation","armor-pharma");?></strong>
          </div>
          <div class="form">
            <select id="selectLactose">
              <option value="0"><?php _e("View all","armor-pharma");?></option>
              <?php
              $posts = get_posts(array(
                "posts_per_page"=> -1,
                "post_type"=>"lactose",
                'tax_query'      => array(
                    array(
                        'taxonomy' => 'category-lactose',
                        'terms' => array('excipure'),
                        'field' => 'slug',
                        'operator' => 'NOT IN',
                    )
                ),
                'status'      => 'publish'
              ));

              foreach ($posts as $post) :   setup_postdata( $post );
                echo '<option value="'.$post->ID.'"">'.get_the_lactose().'</option>';
              endforeach;
              wp_reset_postdata();
              ?>
            </select>
          </div>
        </div>
        <div id="files">
        <?php
        $categorys = get_terms( 'category-library' );
        foreach ($categorys as $category) :
          $args = array(
            'posts_per_page'=> -1,
          	'post_type' => 'library',
          	'tax_query' => array(
              array(
          			'taxonomy' => 'category-library',
          			'field'    => 'id',
          			'terms'    => $category->term_id,
          		)
          	),
          );
          $query = new WP_Query($args);
          if ( $query->have_posts() ) :
            echo '<div class="listing-product form"><h2 class="text-center">'.$category->name.'</h2>';
            echo '<hr class="hrTriangle hrImportant" /><p>
            <input type="checkbox" class="all_category" id="all_category_'.$category->term_id.'" data-category="'.$category->term_id.'" />
            <label for="all_category_'.$category->term_id.'"><b>'.__("Select all documents","armor-pharma").'</b></label>
            </p><br />';
          	while ( $query->have_posts() ) :
          		$query->the_post();
              echo '<p>
                <input type="checkbox" id="ckeckbox_'.get_the_id().'" class="file ';
                //lactose_'.get_field("lactose").
                $lactoses = get_field("lactose");
                if(is_array($lactoses)){
                  foreach( $lactoses as $lactose){
                    echo " lactose_".$lactose;
                  }
                }
                else{
                  echo " lactose_".$lactoses;
                }
              echo ' category_'.$category->term_id.'" data-file="'.get_field('fichier').'" /><label for="ckeckbox_'.get_the_id().'">'.get_the_title().'</label>
              </p>';
          	endwhile;
          	echo '</div>';
          	wp_reset_postdata();
          endif;
        endforeach;
        ?>
        </div>
        <div id="nbFileSelected" class="text-center">
          <span>0</span> <?php _e("Files selected","armor-pharma");?>
        </div>
        <div class="text-center">
            <?php if(get_current_user_id() > 0):?>
            <a href="" download id="downloadZipFile" class="button"><span class="picto picto-download"></span> <?php _e("Download selected items in zip file","armor-pharma");?></a>
            <?php else:?>
            <button onclick="openModalConnect(0);" id="downloadZipFile" class="button"><span class="picto picto-lock"></span> <span class="picto picto-download"></span> <?php _e("Download selected items in zip file","armor-pharma");?></button>
          <?php endif;?>
        </div>
        <?php if( have_rows('videos') ):?>
          <h2><?php _e("Watch technical supports video","armor-pharma");?></h2>
          <p>
            <?php if(get_current_user_id() == 0):?>
              <em onclick="openModalConnect(0);" ><?php _e("You need to be register to get full access !","armor-pharma");?></em>
            <?php endif;?>
          </p>
          <div id="videos">
          <?php
            while ( have_rows('videos') ) : the_row();?>
              <?php if(get_current_user_id() > 0):?>
              <a href="<?php echo get_sub_field('video');?>" download>
              <?php else:?>
              <a href="#" onclick="openModalConnect(0);">
              <?php endif;?>
              <?php echo wp_get_attachment_image( get_sub_field('image'), "large" );?>
            </a>
          <?php endwhile;?>
        </div>
      <?php endif;?>
			</section>
		</article>
	</main>
  <?php get_template_part( 'template-parts/faq', '' );?>
  <div id="askExpert" class="wrapper">
    <hr>
    <div class="form">

      <input type="hidden" id="subjectLactose" name="subjectLactose" value="Question for the technical support" />
      <label for="specTxt">
        <?php _e("Can't find what you're looking for ...", "arma-pharma");?>
        <input type="text" id="specTxt" name="specTxt" placeholder="<?php _e("Type your question", "armor-pharma");?>" />
        <button class="button" id="submitAskExpert"><?php _e("Ask the expert","armar-pharma");?></button>
      </label>
    </div>
  </div>
  <?php
    $user = wp_get_current_user();
    $current_user_exists = $user->exists();
    $genre = ($current_user_exists)?$user->civilite:'';
    $firstname = ($current_user_exists)?$user->user_firstname:'';
    $lastname = ($current_user_exists)?$user->user_lastname:'';
    $country = ($current_user_exists)?$user->country:'';
    $phone = ($current_user_exists)?$user->phone:'';
    $profil = ($current_user_exists)?$user->profil:'';
    $company = ($current_user_exists)?$user->company:'';
    $email = ($current_user_exists)?$user->user_email:'';
  ?>
  <!-- MODAL -->
  <div class="modal" id="modalAskExpert">
    <form id="form-expert" name="form-expert" action="<?php the_permalink();?>" method="post" enctype="multipart/form-data">
      <input type="hidden" name="action" value="formExpert">
      <input type="hidden" name="honey" value="">
      <?php wp_nonce_field('nonceformExpert', 'nonceformExpert'); ?>
      <div class="modal-content form">
        <div>
          <label for="Expert_Mr" class="notDisplayBlock">Mr</label><input type="radio" <?php echo ($genre == "Mr")?'checked':'';?> id="Expert_Mr" name="genre" value="Mr" />
          <label for="Expert_Mrs" class="notDisplayBlock">Mrs</label><input type="radio" <?php echo ($genre == "Mrs")?'checked':'';?> id="Expert_Mrs" name="genre" value="Mrs" />
        </div>
        <div class="grid2">
          <div>
            <label for="firstname"><?php _e("First name","armor-pharma");?>*</label>
            <input type="text" value="<?php echo $firstname;?>" name="firstname" id="firstname" required />
          </div>
          <div>
            <label for="lastname"><?php _e("Last name","armor-pharma");?>*</label>
            <input type="text" value="<?php echo $lastname;?>" name="lastname" id="lastname" required />
          </div>
        </div>
        <div class="grid2">
          <div>
            <label for="Expert_country"><?php _e("Country","armor-pharma");?>*</label>
            <input type="text" value="<?php echo $country;?>" name="country" id="Expert_country" required />
          </div>
          <div>
            <label for="Expert_phone"><?php _e("Phone","armor-pharma");?></label>
            <input type="tel" placeholder="+33000000000" name="phone" value="<?php echo $phone;?>" id="Expert_phone" />
          </div>
        </div>
        <div>
          <label id="profil">
            <?php _e("Profile","armor-pharma");?>*
          </label>
          <select name="profil" id="profil" required>
            <option <?php echo ($profil == "Responsible for excipients purchases")?'selected':'';?>>
              Responsible for excipients purchases
            </option>
            <option <?php echo ($profil == "Working on tablets/sachets/capsules formulation")?'selected':'';?>>
              Working on tablets/sachets/capsules formulation
            </option>
            <option <?php echo ($profil == "Working on Dry Powder Inhalers development")?'selected':'';?>>
              Working on Dry Powder Inhalers development
            </option>
            <option <?php echo ($profil == "Student in pharmaceutical formulation")?'selected':'';?>>
              Student in pharmaceutical formulation
            </option>
            <option <?php echo ($profil == "Professor/associate professor at University")?'selected':'';?>>
              Professor/associate professor at University
            </option>
            <option <?php echo ($profil == "PhD student")?'selected':'';?>>
              PhD student
            </option>
            <option <?php echo ($profil == "other")?'selected':'';?>>
              other
            </option>
          </select>
        </div>
        <div class="grid2">
          <div>
            <label for="Expert_company"><?php _e("Company","armor-pharma");?>*</label>
            <input type="text" name="company" value="<?php echo $company;?>" id="Expert_company" required />
          </div>
          <div>
            <label for="email"><?php _e("Email","armor-pharma");?>*</label>
            <input type="email" name="email" value="<?php echo $email;?>" id="email" required />
          </div>
        </div>
        <div class="border"></div>
        <div>
          <label for="subject"><?php _e("Subject","armor-pharma");?></label>
          <input type="text" readonly name="subject" id="subject" required />
        </div>
        <div>
          <label for="comments"><?php _e("Comments","armor-pharma");?></label>
          <textarea name="comments" id="comments" required ></textarea>
        </div>
        <div>
          <input type="checkbox" name="check" id="check" required />
          <label for="check">
            <?php _e("By submitting your data through this form, you confirm that you are above the age of 18, that you have read and understood the Privacy Policy, and that you agree to the collection, use and processing of your Personal Information by ARMOR PHARMA in accordance with said Policy","armor-pharma");?>
          </label>
        </div>
        <div class="text-center">
          <button class="button" id="sendAskExpert"><?php _e("SEND","armor-pharma");?></button>
        </div>
        <div id="Mendatory">
          <?php _e("*Mandatory fields","armor-pharma");?>
        </div>
      </div>
    </form>
  </div>

<?php endwhile; endif; ?>
<?php get_footer(); ?>
