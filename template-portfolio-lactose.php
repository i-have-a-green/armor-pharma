<?php
/*
Template Name: Lactose Portfolio
*/
get_header();
if (have_posts()) :  while ( have_posts() ) : the_post();
?>
<div id="content-header">
  <div class="wrapper">
    <?php the_breadcrumb();?>
  </div>
</div>
<main id="main" role="main" class="wrapper">
  <header id="header-page">
    <h1 class="page-title degrade"><?php the_title();?></h1>
    <h2><?php the_field('subtitle');?></h2>
  </header>
	<?php get_template_part( 'template-parts/loop', 'lactose' );?>
  <div id="theContent">
    <?php the_content( );?>
  </div>
</main>
<div class="wrapper" id="formSearchLactose">
  <?php get_template_part( 'template-parts/configurateur', '' );?>
</div>
<?php
endwhile; endif;
get_footer(); ?>
