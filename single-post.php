<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post();?>
  <div id="content-header">
    <div class="wrapper">
      <?php the_breadcrumb();?>
    </div>
  </div>
	<main id="main" role="main" class="wrapper">
		<article>
			<header id="header-page">
				<h1 class="page-title degrade"><?php the_title();?></h1>
			</header>
      <div id="sub-header">
        <div id="thumbnail">
  				<?php the_post_thumbnail( 'wpgreen-full'); ?>
  			</div>
        <div id="excerpt">
          <date><?php echo get_the_date();?></date>
          <?php wpgreen_get_the_category();?>
          <h2><?php the_field("sub-title");?></h2>
  				<?php the_excerpt();?>
  			</div>
      </div>
			<section class="entry-content" itemprop="articleBody">
				<?php the_content(); ?>
        <hr class="hrTriangle" />
        <?php $p = get_page_by_path( "events-news" );
        echo '<p class="text-center">
        <a href="'.get_the_permalink($p).'" id="linkBack">'.$p->post_title.'</a>
        </p>';?>
			</section>
      <div id="footer-nav">
        <div>
          <span class="picto angle-left"></span> <?php previous_post_link('%link', __("PREVIOUS ARTICLE","armor-pharma"));?>
        </div>
        <div>
          <span class="picto angle-right"></span> <?php next_post_link('%link', __("NEXT ARTICLE","armor-pharma"));?>
        </div>
      </div>
    </article>
  </main>
	<footer class="article-footer">
    <div id="same-category" class="wrapper">
      <div class="text-center">
        <h2 class="degrade semiCercle small"><?php _e("Same Category", "armor-pharma");?></h2>
      </div>

      <div id="archive-news">
        <?php
        $args = array (
          'posts_per_page'  => 3,
          'post_type' => 'post',
          'status' => 'publish'
        );
        $args['cat'] = wpgreen_get_the_category_id();
        $posts = get_posts($args);
        if( $posts ) {
          echo '<div class="archive-news" id="archive-news-flexslider"><ul class="slides">';
          foreach( $posts as $post ) {
            setup_postdata( $post );
            echo '<li>';
            get_template_part( 'template-parts/loop', 'archive' );
            echo '</li>';
          }
          echo '</ul></div>';
          wp_reset_postdata();
        }
        ?>
      </div>
    </div>
	</footer>
<?php endwhile; endif; ?>
<?php get_footer(); ?>
