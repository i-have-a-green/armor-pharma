<?php
/*
Template Name: Design Register
*/
$user = get_current_user_id();
if($user > 0){
  $url = get_the_permalink(get_page_by_path("design-the-lactose-you-like/my-customize-specification"));
  if(isset($_GET['id']) && !empty($_GET['id'])){
      wp_redirect( add_query_arg( 'id', $_GET['id'], $url) , 302 );
  }
  else{
    wp_redirect( $url , 302 );
  }
}

get_header();
if (have_posts()) :  while ( have_posts() ) : the_post();
?>
<div id="content-header">
  <div class="wrapper">
    <?php the_breadcrumb();?>
  </div>
</div>
<div class="text-center">
  <h1 class="degrade"><?php _e("Design the lactose <b>you</b> like","armor-pharma");?></h1>
</div>
<?php
set_query_var( 'item', 1 );
get_template_part( 'template-parts/arianne', 'design' );?>
<div class="wrapper" id="contentDesign">
  <main>
    <div class="help">
      <a href="<?php echo get_the_permalink(get_page_by_path( "contact" ) );?>">
        <span><?php _e("For more complicated requests (several product, global sourcing...) Contact us directly!","armor-pharma");?></span>
      </a>
      <a class="picto-info" data-slug="help-design">
        <img src="<?php echo get_stylesheet_directory_uri() . '/assets/css/images/help.png';?>">
      </a>
    </div>
    <h2 class="degrade semiCercle small"><?php the_title();?></h2>
    <?php get_template_part( 'template-parts/form', 'connect' );?>
  </main>
  <div id="rightCol">

  </div>
</div>
<?php
endwhile; endif;
get_footer(); ?>
