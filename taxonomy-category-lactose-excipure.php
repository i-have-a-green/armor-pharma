<?php
get_header();
$term = get_queried_object();
?>
<div id="content-header">
  <div class="wrapper">
    <?php the_breadcrumb();?>
  </div>
</div>
<div id="archive">
	<header class="wrapper" id="header-page">
		<h1 class="page-title" style="color:<?php the_field('color', $term);?>"><?php the_field('title',$term);?></h1>
    <?php // echo term_description();?>
    <?php the_field('descriptionTaxo', $term);?>
    <!--<p>
      <strong><?php _e("Select the products you would like to compare and click on [Enlarge] to discover their main characteristics.","armor-pharma");?></strong>
    </p>-->
	</header>
	<div id="archive-product" class="wrapper archive-product-excipure">
    <script>
      var datasets = [];
    </script>
    <div id="listing-product">
      <?php
    	echo '<div class="listing-product-process">';
    	/*if (have_posts()) : while (have_posts()) : the_post();
          set_query_var( 'color', get_field('color', $term) );
          get_template_part( 'template-parts/loop', 'lactose-product-excipure' );
      endwhile;endif;*/
      $args = array(
        'post_type' => 'lactose',
        'tax_query' => array(
          array(
            'taxonomy' => 'category-lactose',
            'field'    => 'id',
            'terms'    => $term->term_id,
          )
        ),
      );
      $query = new WP_Query($args);
      if ( $query->have_posts() ) :
        while ( $query->have_posts() ) :
          $query->the_post();
          set_query_var( 'color', get_field('color', $term) );
          get_template_part( 'template-parts/loop', 'lactose-product-excipure' );
        endwhile;
        wp_reset_postdata();
      endif;
      echo '</div>';
        ?>
    </div>
  </div>
  <div id="content_exipure">
    <div class="wrapper">
      <?php the_field("content_exipure", $term);?>
    </div>
    </div>
  <div id="" class="wrapper">
  <?php //get_template_part( 'template-parts/ask', 'expert' );?>
  <div class="team">
      <div class="teamPicture">
        <?php
        $image = get_field('imageContact', $term);
        $size = 'medium'; // (thumbnail, medium, large, full or custom size)
        if( $image ) {
          echo wp_get_attachment_image( $image, $size );
        }
        ?>
        <img src="<?php echo get_stylesheet_directory_uri() . '/assets/css/images/bg-contact.png';?>" class="mask" />
      </div>
      <div class="teamInfo">
        <div class="name">
          <div>
            <?php the_field("firstnameContact", $term);?> <?php the_field("lastnameContact", $term);?>
          </div>
        </div>
        <hr>
        <div class="function">
          <div><?php the_field("functionContact", $term);?></div>
        </div>
        <p class="tel">
          <?php the_field("telContact", $term);?>
        </p>
        <p>
          <?php 
            $p = get_page_by_path( "contact" ); 
            $link = add_query_arg(
              array( 
                  'askContactExcipure' => 'askContactExcipure',
                  'email' => get_field("emailContact", $term),
                  'prenom' => get_field("firstnameContact", $term)
              ), 
              get_the_permalink( $p));
          ?>
          <button class="button small " onclick="location.href='<?php echo $link;?>';"><?php _e("Contact","armor-pharma", $term);?> <?php the_field("firstnameContact", $term);?></button>
        </p>
      </div>

    </div>
  </div>
  
    
  <!-- MODAL -->
<div class="modal" id="modalInhaler">
  <form id="formInhaler" name="formInhaler" action="<?php the_permalink();?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="honey" value="">
    <input type="hidden" name="action" value="formExpert">
    <?php wp_nonce_field('nonceformExcipure', 'nonceformExcipure'); ?>
    <?php
      $user = wp_get_current_user();
      $current_user_exists = $user->exists();
      $genre = ($current_user_exists)?$user->civilite:'';
      $firstname = ($current_user_exists)?$user->user_firstname:'';
      $lastname = ($current_user_exists)?$user->user_lastname:'';
      $country = ($current_user_exists)?$user->country:'';
      $phone = ($current_user_exists)?$user->phone:'';
      $profil = ($current_user_exists)?$user->profil:'';
      $company = ($current_user_exists)?$user->company:'';
      $email = ($current_user_exists)?$user->user_email:'';
    ?>
    <div class="modal-content form">
      <p><h2>Dry Powder Inhaler</h2></p>
      <p>
        We would be delighted to discuss your requirements further.
      </p>

      <div>
        <label for="excipure_Mr" class="notDisplayBlock">Mr</label><input type="radio" <?php echo ($genre == "Mr")?'checked':'';?> id="excipure_Mr" name="genre" value="Mr" />
        <label for="excipure_Mrs" class="notDisplayBlock">Mrs</label><input type="radio" <?php echo ($genre == "Mrs")?'checked':'';?> id="excipure_Mrs" name="genre" value="Mrs" />
      </div>
      <div class="grid2">
        <div>
          <label for="excipure_firstname"><?php _e("First name","armor-pharma");?>*</label>
          <input type="text" value="<?php echo $firstname;?>" name="firstname" id="excipure_firstname" required />
        </div>
        <div>
          <label for="excipure_lastname"><?php _e("Last name","armor-pharma");?>*</label>
          <input type="text" value="<?php echo $lastname;?>" name="lastname" id="excipure_lastname" required />
        </div>
      </div>
      <div class="grid2">
        <div>
          <label for="excipure_country"><?php _e("Country","armor-pharma");?>*</label>
          <input type="text" value="<?php echo $country;?>" name="country" id="excipure_country" required />
        </div>
        <div>
          <label for="excipure_phone"><?php _e("Phone","armor-pharma");?></label>
          <input type="tel" placeholder="+33 0 00 00 00 00" name="phone" value="<?php echo $phone;?>" id="excipure_phone"  />
        </div>
      </div>
      <div class="grid2">
          <div>
          <label id="excipure_profil">
            <?php _e("Profile","armor-pharma");?>*
          </label>
          <select name="profil" id="excipure_profil" required>
            <option <?php echo ($profil == "Responsible for excipients purchases")?'selected':'';?>>
              Responsible for excipients purchases
            </option>
            <option <?php echo ($profil == "Working on tablets/sachets/capsules formulation")?'selected':'';?>>
              Working on tablets/sachets/capsules formulation
            </option>
            <option <?php echo ($profil == "Working on Dry Powder Inhalers development")?'selected':'';?>>
              Working on Dry Powder Inhalers development
            </option>
            <option <?php echo ($profil == "Student in pharmaceutical formulation")?'selected':'';?>>
              Student in pharmaceutical formulation
            </option>
            <option <?php echo ($profil == "Professor/associate professor at University")?'selected':'';?>>
              Professor/associate professor at University
            </option>
            <option <?php echo ($profil == "PhD student")?'selected':'';?>>
              PhD student
            </option>
            <option <?php echo ($profil == "other")?'selected':'';?>>
              other
            </option>
          </select>
        </div>   
        <div>
          <label for="excipure_company"><?php _e("Company","armor-pharma");?>*</label>
          <input type="text" name="company" value="<?php echo $company;?>" id="excipure_company" required />
        </div>
      </div>
      <div class="grid2">
        <div>
          <label for="excipure_email"><?php _e("Email","armor-pharma");?>*</label>
          <input type="email" name="email" value="<?php echo $email;?>" id="excipure_email" required />
        </div>
      </div>
      <div>
        <label for="excipure_subject"><?php _e("Subject","armor-pharma");?></label>
        <input type="text" name="subject" id="excipure_subject" required readonly value="Question for the INHALATION expert" />
      </div>
      <div>
        <label for="excipure_comments"><?php _e("Comments","armor-pharma");?></label>
        <textarea name="comments" width="100%"  id="excipure_comments" required placeholder="Please precise your need"></textarea>
      </div>

      <div class="border"></div>
      
      <p>Do you want one of our experts to contact you?</p>

      <div>
        <p>
          If so how would you like to be recontacted:
        </p>
        <div>
          <input type="checkbox" id="exipure_by_mail" name="exipure_by[]" value="email" /><label for="exipure_by_mail">By email</label>
          <input type="checkbox" id="exipure_by_tel" name="exipure_by[]" value="phone" /><label for="exipure_by_tel">By phone</label>
        </div>
      </div>
      <div>
        <label for="dateInhaler"><?php _e("Any schedule preference?","armor-pharma");?></label>
        <input type="datetime-local" name="date" value="" id="dateInhaler" />
      </div>
      
      <div>
        <input type="checkbox" name="check" id="checkInhaler" required />
        <label for="checkInhaler">
          <?php $p = get_page_by_path( "privacy-policy-cookie-policy" );?>
          <?php _e("By submitting your data through this form, you confirm that you are above the age of 18, that you have read and understood the <a href='".get_the_permalink( $p)."' target='_blank'>Privacy Policy</a>, and that you agree to the collection, use and processing of your Personal Information by ARMOR PHARMA in accordance with said Policy","armor-pharma");?>*
        </label>
      </div>
      <div class="text-center">
        <button type="submit" class="button submitBtn" id="sendAskInhaler"><?php _e("SEND","armor-pharma");?></button>
      </div>
    </div>
    <div id="Mendatory">
      <?php _e("*Mandatory fields","armor-pharma");?>
    </div>
  </form>
</div>

</div>
<?php
get_footer();
?>
