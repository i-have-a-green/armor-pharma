<?php
get_header();

?>
<?php if (have_posts()) : while (have_posts()) : the_post();
$wpseo_primary_term = new WPSEO_Primary_Term( 'category-lactose', get_the_id() );
$wpseo_primary_term = $wpseo_primary_term->get_primary_term();
$term = get_term( $wpseo_primary_term );
?>
  <div id="content-header">
    <div class="wrapper">
      <?php the_breadcrumb();?>
    </div>
  </div>
	<main id="main" role="main" class="wrapper">
		<article>
			<header id="header-page">
				<h1 class="page-title" style="color:<?php the_field('color', $term);?>">
          <?php the_field('title',$term);?>
        </h1>
        <div>
          <div style="background-color:<?php the_field('color', $term);?>" id="title">
              <?php the_title();?>
          </div>
        </div>
			</header>
      <div id="theContent">
        <div class="image">
          <?php // the_post_thumbnail('wpgreen-full', array('id'=>'image-'.get_the_id()));
          $image = get_field('image_zoom');
          $size = 'full'; // (thumbnail, medium, large, full or custom size)
          if( $image ) {
          	$image = wp_get_attachment_image_url( $image, $size );
          }
          the_post_thumbnail('wpgreen-400', array('id'=>'image-'.get_the_id(),'class'=>'imgZoom', 'data-img-full'=>$image )); ?>
          <!--<div id="myresult-<?php the_id();?>" class="img-zoom-result"></div>
          <script>
            window.addEventListener("load", function() {
              imageZoom("image-<?php the_id();?>", "myresult-<?php the_id();?>");
            });
          </script>-->
      	</div>
        <div id="colRight">
          <div class="pictoLactose">
            <?php $process = get_the_terms( $post->ID, 'process-lactose'); ?>
            <?php echo wp_get_attachment_image( get_field('picto',$process[0]), "full" );?>
          </div>
          <?php the_content();?>
          <hr />
          <div id="benefitsApplications">
            <div id="key_benefits">
              <h3><?php _e("Key benefits","armor-pharma");?> :</h3>
              <?php the_field("key_benefits");?>
            </div>
            <div class="suitable">
              <div>
                <h3><?php _e("Applications :","armor-pharma");?></h3>
              </div>
              <div class="imgApplications">
                <?php
                  $applications = get_field("application2");
                  if($applications):
                  foreach( $applications as $application ):
          		    ?>
                  <div class="imgApplication">
                    <img src="<?php echo get_stylesheet_directory_uri() . '/assets/css/images/'.$application.'.png';?>'" />
                  </div>
                  <?php
                  endforeach;
                  endif;
                ?>
              </div>
            </div>
          </div>
          <div id="allCharacteristics">
            <div id="specification">
              <h3><?php _e("Particle size distribution :","armor-pharma");?></h3>
              <h4><?php _e("Specification (Air Jet Sieve)","armor-pharma");?></h4>
              <?php while ( have_rows('specification_text') ) : the_row(); ?>
                <b><?php the_sub_field("label");?></b> <?php the_sub_field("value");?><br />
              <?php endwhile;?>

            </div>
            <div id="otherCharacteristics">
              <h3><?php _e("Other characteristics :","armor-pharma");?> </h3>
              <p>
                <?php info("poured-density");?><b><?php _e("Poured density","armor-pharma");?></b> : <?php the_field("poured_density");?> g/mL<br />
                <?php info("tapped-density");?><b><?php _e("Tapped density","armor-pharma");?></b> : <?php the_field("tapped_density");?> g/mL<br />
                <?php info("carrs-index");?><b><?php _e("Carr's index","armor-pharma");?></b> : <?php the_field("carr_s_index");?> %<br />
                <?php info("hausner-ratio");?><b><?php _e("Hausner ratio","armor-pharma");?></b> : <?php the_field("hausner_ratio");?><br />
                <!--<?php info("specifique-surface-area");?><b><?php _e("Specific Surface Area","armor-pharma");?></b> : <?php the_field("specific_surface_area");?>-->
              </p>
            </div>
            <div id="chart">
              <h3>&nbsp;</h3>
              <h4><?php _e("Laser diffraction (Malvern, wet) for information :","armor-pharma");?></h4>
                <?php // echo wp_get_attachment_image( get_field('chart'), "full" );?>
                <script>
                dataLabel = [];
                dataProductVolume = [];
                dataProductCombinate = [];
                <?php
                $volume = 0;
                while ( have_rows('chart') ) : the_row(); ?>
              		dataLabel.push(<?php the_sub_field("particle_diameter");?>);
                  dataProductVolume.push(<?php the_sub_field("volume");?>);
                  <?php $volume += get_sub_field("volume");?>
                  dataProductCombinate.push(<?php echo $volume;?>);
              	<?php endwhile; ?>

                 </script>
                <canvas id="chartProduct" class="chartjs" width="400" height="400"></canvas>
                <div id="psd" class="text-center">
                  x10 : <?php the_field("psd_10");?> µm&nbsp;&nbsp;&nbsp;
                  x50 : <?php the_field("psd_50");?> µm&nbsp;&nbsp;&nbsp;
                  x90 : <?php the_field("psd_90");?> µm
                </div>
            </div>
            <div id="packagingSection">
              <h3><?php _e("Packaging section :","armor-pharma");?> </h3>
              <h4>
                <?php the_field("packagingSection");?>
              </h4>
              <div class="grid">
                <?php if(have_rows('packaginginfos')):?>
                  <?php while ( have_rows('packaginginfos') ) : the_row();?>
                    <div>
                      <h5>
                        <?php the_sub_field("label");?>
                        <?php if(get_sub_field("picto")):
                          $info = get_sub_field("picto");
                          info($info->post_name);
                        endif;?>
                      </h5>
                      <ul>
                      <?php while ( have_rows('listinfopackaging') ) : the_row();
                        echo '<li>';
                        the_sub_field("info");
                        if(get_sub_field("picto")):
                          $info = get_sub_field("picto");
                          info($info->post_name);
                        endif;
                        echo '</li>';
                      endwhile;?>
                      </ul>
                  </div>
                  <?php endwhile;?>
                <?php endif;?>
              </div>
          </div>
        </div>
      </div>
      <div id="download">
        <h2><?php _e("Download documentation :","armor-pharma");?></h2>
        <p>
          <?php if(!empty(get_field("technical_information"))):?>
            <?php _e("Technical information","armor-pharma");?><a href="<?php the_field("technical_information");?>" download><span class="picto picto-download"></span></a>
          <?php endif;?>
          <?php if(!empty(get_field("spec"))):?>
            <?php _e("SPEC","armor-pharma");?><a href="<?php the_field("spec");?>" download><span class="picto picto-download"></span></a>
          <?php endif;?>
          <?php if(!empty(get_field("msds"))):?>
            <?php _e("MSDS","armor-pharma");?><a href="<?php the_field("msds");?>" download><span class="picto picto-download"></span></a>
          <?php endif;?>
        </p>
      </div>
    </div>
      <!--<h2><?php _e("Any other technical information needed ?","armor-pharma");?></h2>
      <form class="form">
        <label for="question"><?php _e("Type your question","armor-pharma");?></label>
        <input type="text" id="question" name="question" />
        <button id="askExpert"><?php _e("Ask the expert","armor-pharma");?></button>
      </form>-->
      <?php get_template_part( 'template-parts/ask', 'productInfo' );?>
    </div>
    <?php if(have_rows('questions')):?>                 
      <div class="faqLactose">
        <div class="wrapper">
          <h2 class="degrade semiCercle small"><?php _e("Your question, our solutions","armor-pharma");?></h2>
          <?php $question=0; while ( have_rows('questions') ) : the_row(); ?>
              <hr>
              <div class="question" data-reponse="reponse_<?php echo $question;?>"><?php the_sub_field("question");?></div>
              <div class="reponse" id="reponse_<?php echo $question;?>"><?php the_sub_field("reponse");?></div>
          <?php $question++; endwhile;?>
        </div>
      </div>
    <?php endif;?>
    </div><!-- theContent -->
    <div class="wrapper">
      <hr class="hrTriangle">
      <div class="footer">
        <div>
          <?php


          $category = get_primary_taxonomy_id( get_the_id(), 'category-lactose');
          $category = get_term( $category );
          ?>
          <a href="<?php echo get_category_link( $category->term_id );?>" class="button"><span class="picto picto-back"></span><?php _e("Back to","armor-pharma");?> <?php echo $category->name;?></a>
        </div>
        <div>
          <a href="#"  data-lactose-id="<?php the_id();?>" class="cartLink"><span class="picto picto-cart"></span><?php _e("GET FREE SAMPLE","armor-pharma");?></a>
        </div>
        <div>
          <a href="<?php echo get_permalink( get_page_by_path( "design-the-lactose-you-like/my-customize-specification" ) ).'?id='.get_the_id();?>" class="button small"><?php _e("GET IT CUSTOMIZED","armor-pharma");?></a>
        </div>
      </div>
    </div>
  </article>
</main>
<?php endwhile; endif; ?>
<?php get_footer(); ?>
