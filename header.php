<?php
$nbLactoseInCart = nbLactoseInCart();
?>
<!doctype html>

  <html <?php language_attributes(); ?>>

	<head>
		<meta charset="utf-8">

		<!-- Force IE to use the latest rendering engine available -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<!-- Mobile Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<?php wp_head(); ?>

		<!-- Analytics here -->
    <script>
      var _paq = window._paq = window._paq || [];
      /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
      _paq.push(['trackPageView']);
      _paq.push(['enableLinkTracking']);
      (function() {
        var u="https://analytics.savencia.com/";
        _paq.push(['setTrackerUrl', u+'matomo.php']);
        _paq.push(['setSiteId', '15']);
        var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
        g.async=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
      })();
    </script>
		<!-- end analytics -->

	</head>

	<body <?php body_class(); ?> itemscope="itemscope" itemtype="http://schema.org/WebPage">
		<div class="" id="menuMobile">
      <div id="headerMenuMobile">
        <ul>
          <?php if ( is_active_sidebar( 'language' ) ) : ?>
            <?php dynamic_sidebar( 'language' ); ?>
          <?php endif; ?>
          <?php if(!empty(get_theme_mod( 'wpgreen_share_linkedin' ))): ?>
            <li>
              <a href="<?php echo get_theme_mod( 'wpgreen_share_linkedin' );?>" target="_blank">
                <img src="<?php echo get_stylesheet_directory_uri() . '/assets/css/images/linkedin.png';?>" id="picto-linkedin" />
              </a>
            </li>
          <?php endif;?>
        </ul>
      </div>
			<?php  wpgreen_top_nav(); ?>
      <?php //get_search_form();?>
		</div>
		<div id="burger-menu" class="">&nbsp;</div>
    <a href="<?php echo home_url();?>" class="logo-home-mobile">
      <?php
      $custom_logo_id = get_theme_mod( 'custom_logo' );
      if ( has_custom_logo() ) :
        echo wp_get_attachment_image( $custom_logo_id , 'wpgreen-logo' );
      else:
        echo get_bloginfo( 'name' );
      endif;
      $current_user = wp_get_current_user();
      ?>
    </a>
    <div id="languette-verte" class="languette">
      <a href="<?php echo get_the_permalink(get_page_by_path("quality"));?>">
        <?php _e("Quality & regulatory corner","armor-pharma");?>
      </a>
    </div>
    <div id="languette-bleu" class="languette">
      <a href="<?php echo get_the_permalink(get_page_by_path("technical-library"));?>">
        <?php _e("Technical Library","armor-pharma");?>
      </a>
    </div>
    <div id="languettes-mobile">
      <div id="languette-verte-mobile" class="languette-mobile">
        <div>
            <img src="<?php echo get_stylesheet_directory_uri() . '/assets/css/images/quality-footer.png';?>" />
        </div>
        <div>
          <a href="<?php echo get_the_permalink(get_page_by_path("quality"));?>">
            <?php _e("Quality & regulatory corner","armor-pharma");?>
          </a>
        </div>
      </div>
      <div id="languette-bleu-mobile" class="languette-mobile">
        <div>
          <img src="<?php echo get_stylesheet_directory_uri() . '/assets/css/images/library-footer.png';?>" />
        </div>
        <div>
          <a href="<?php echo get_the_permalink(get_page_by_path("technical-library"));?>">
            <?php _e("Technical Library","armor-pharma");?>
          </a>
        </div>
      </div>
    </div>
    <header id="headerMobile">
      <nav  calss="link-navigation" role="navigation">
        <ul>
          <li class="header-button <?php echo (is_user_logged_in())?'connect':'';?>" id="my-account-mobile" onclick="openModalConnect(<?php echo $current_user->ID ;?>);">
            <?php if(is_user_logged_in()):?>
              <?php
                $current_user = wp_get_current_user();
                echo $current_user->display_name;
              ?>
            <?php else:?>
              <?php _e("my account", "armor-pharma"); ?>
            <?php endif;?>
          </li>
          <li class="header-button" id="my-cart-mobile" onclick="myCart(<?php echo $current_user->ID ;?>)">
            <?php _e("my cart", "armor-pharma"); ?>
            <span class="nbCart">
            <?php echo $nbLactoseInCart;  ?>
            </span>

          </li>
        </ul>
      </nav>
    </header>
		<header role="banner" id="header">
      <div id="headerContent" class="wrapper">
  			<div id="header-logo">
  				<a href="<?php echo home_url();?>">
  					<?php
  					$custom_logo_id = get_theme_mod( 'custom_logo' );
  					if ( has_custom_logo() ) :
  						echo wp_get_attachment_image( $custom_logo_id , 'wpgreen-logo' );
  					else:
  						echo get_bloginfo( 'name' );
  					endif;
  					?>
  				</a>
  			</div>
        <nav id="link-navigation" calss="link-navigation" role="navigation">
          <ul>
            <li class="header-button <?php echo (is_user_logged_in())?'connect':'';?>" id="my-account" onclick="openModalConnect(<?php echo $current_user->ID ;?>);">
            <?php if(is_user_logged_in()):?>
              <?php
                $current_user = wp_get_current_user();
                echo $current_user->display_name;
              ?>
            <?php else:?>
              <?php _e("my account", "armor-pharma"); ?>
            <?php endif;?>
            </li>

            <li class="header-button" id="my-cart"  onclick="myCart(<?php echo $current_user->ID ;?>)">
              <?php _e("my cart", "armor-pharma"); ?>
              <span class="nbCart">
              <?php echo $nbLactoseInCart; ?>
              </span>
            </li>

            <?php if ( is_active_sidebar( 'language' ) ) : ?>
              <?php dynamic_sidebar( 'language' ); ?>
          	<?php endif; ?>

            <?php
            if(!empty(get_theme_mod( 'wpgreen_share_linkedin' ))): ?>
  						<li>
                <a href="<?php echo get_theme_mod( 'wpgreen_share_linkedin' );?>" target="_blank">
                  <img src="<?php echo get_stylesheet_directory_uri() . '/assets/css/images/linkedin.png';?>" id="picto-linkedin" />
                </a>
              </li>
  					<?php endif;?>

            <li>
              <?php get_search_form();?>
            </li>
          </ul>
        </nav>
  			<nav id="site-navigation" class="main-navigation" role="navigation">
  				<?php  wpgreen_top_nav(); ?>
  			</nav>
      </div>
		</header> <!-- end .header -->
		<div id="content">
