<?php
/*
Template Name: Newsletter
*/
$user = get_current_user_id();
if($user == 0){
  wp_redirect( add_query_arg( 'login', '1', home_url() ), 302 );
}
get_header();
?>
<?php if (have_posts()) : while (have_posts()) : the_post();?>
  <div id="content-header">
    <div class="wrapper">
      <?php the_breadcrumb();?>
    </div>
  </div>
	<main id="main" role="main" class="wrapper my-account">
		<article>
			<header class="wrapper" id="header-page">
				<h1 class="page-title degrade"><?php _e("My account","armor-pharma");?></h1>
			</header>

			<section class="entry-content wrapper" itemprop="articleBody">
        <div id="menu-account">
          <?php set_query_var( 'active', 4 );?>
          <?php get_template_part( 'template-parts/menu', 'account' );?>
        </div>
        <div id="my-account-content">
          <h1 class="degrade semiCercle small"><?php the_title( );?></h1>
          <?php echo do_shortcode( '[mailpoet_manage_subscription]' );?>
        </div>
			</section>
		</article>
	</main>
<?php endwhile; endif; ?>
<?php get_footer(); ?>
