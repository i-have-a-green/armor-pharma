<?php
/*
Template Name: Design specification

*/
/*$design = get_user_meta(get_current_user_id(), 'design', true);
$design = json_decode($design);
*/
$user = get_current_user_id();
if($user == 0){
  $pDesign = get_the_permalink(get_page_by_path( "design-the-lactose-you-like/registration" ));
  if(isset($_GET['id']) && !empty($_GET['id'])){
    $pDesign = add_query_arg( 'id', $_GET['id'], $pDesign );
  }
  wp_redirect( $pDesign, 302 );
}
get_header();
if(isset($_GET['id']) && !empty($_GET['id'])){
  $post = get_post($_GET['id']);
  setup_postdata( $post );
  $JSONdesign = '{"lactose":{"id":'.get_the_id().',"name":"'.get_the_lactose().'"}}';
  $design = json_decode(stripslashes($JSONdesign));
  update_user_meta( get_current_user_id(), 'design',  json_encode($design));
  wp_reset_postdata();
}
else{
  $design = get_user_meta(get_current_user_id(), 'design', true);
  $JSONdesign = $design;
  $design = json_decode(stripslashes($design));
}
?>
<script>var design = <?php echo $JSONdesign;?>;</script>
<?php
if (have_posts()) :  while ( have_posts() ) : the_post();
?>
<div id="content-header">
  <div class="wrapper">
    <?php the_breadcrumb();?>
  </div>
</div>
<div class="text-center">
  <h1 class="degrade"><?php _e("Design the lactose <b>you</b> like","armor-pharma");?></h1><br />
  <p class="degrade" id="titreProduct">
    <?php _e("How would you like your","armor-pharma");?> « <?php echo $design->lactose->name;?> »
  </p>
</div>
<?php
set_query_var( 'item', 2 );
get_template_part( 'template-parts/arianne', 'design' );?>
<div class="wrapper" id="contentDesign">
  <main>
    <div class="help">
      <a href="<?php echo get_the_permalink(get_page_by_path( "contact" ) );?>">
        <span><?php _e("For more complicated requests (several product, global sourcing...) Contact us directly!","armor-pharma");?></span>
      </a>
      <a class="picto-info" data-slug="help-design">
        <img src="<?php echo get_stylesheet_directory_uri() . '/assets/css/images/help.png';?>">
      </a>
    </div>
    <a id="btnSpecification" href="<?php echo get_the_permalink(get_page_by_path( 'design-the-lactose-you-like/packaging' ) );?>" class="button small next"><?php _e("NEXT","armor-pharma");?></a>
    <a href="<?php echo get_the_permalink(get_page_by_path( 'design-the-lactose-you-like/' ) );?>" class="button small back"><?php _e("BACK","armor-pharma");?></a>
    <h2 class="degrade semiCercle small"><?php the_title();?></h2><br />
    <p>
      <a href="<?php the_field("spec", $design->lactose->id);?>" download >
        <?php _e("Download", "armor-pharma");?>
        <?php echo $design->lactose->name;?>
        <?php _e("specification to compare with your requirements","armor-pharma");?>
      <span class="picto picto-download"></span> </a>
    </p>
    <div class="formDesign">
      <h3><?php _e("Pharmacopoeial conformance :","armor-pharma");?></h3>
      <div id="pharmacopoeial">
        <?php
        if(isset($design->pharmacopoeial)){
          $pharmacopoeial = [];
          foreach ($design->pharmacopoeial as $val) {
            $pharmacopoeial[] = $val->label;
          }
        }
        if( have_rows('pharmacopoeial_conformance', $design->lactose->id) ): $i=0;
     	    while ( have_rows('pharmacopoeial_conformance', $design->lactose->id) ) : the_row();
            $checked = '';
            if(isset($pharmacopoeial) && in_array(get_sub_field('label'), $pharmacopoeial)){
              $checked = 'checked';
            }
            else{
              $checked = (get_sub_field('default'))?'checked':'';
            }

          ?>
          <input type="checkbox" data-standard="<?php echo (get_sub_field('default'))?'true':'false';?>" id="Pharmacopoeial<?php echo $i;?>" <?php echo $checked;?> value="<?php the_sub_field('label');?>" class="checkboxCusto Pharmacopoeial">
          <label for="Pharmacopoeial<?php echo $i;?>">
            <?php the_sub_field('label');?>
          </label>
          <?php
          $i++;
          endwhile;
        endif;
        ?>
      </div>
      <h3><?php _e("Microbial limits:","armor-pharma");?></h3>
      <table id="microbial">
        <tr>
          <th>
            <?php _e("Microbial limits:","armor-pharma");?>
          </th>
          <th>
            <?php _e("Test limits:","armor-pharma");?>
          </th>
          <th>
            <?php _e("Method limits:","armor-pharma");?>
          </th>
          <th>
            <?php _e("Customized limits:","armor-pharma");?>
          </th>
        </tr>
        <?php

        if( have_rows('microbial_limits', $design->lactose->id) ): $i=0;
     	    while ( have_rows('microbial_limits', $design->lactose->id) ) : the_row();
            $checked = '';
            //$microbialValue = get_sub_field('test_limits');
            $microbialValue = '';
            if(isset($design->microbial)){
              foreach ($design->microbial as $value) {
                if(get_sub_field('label') == $value->label){
                  $checked = 'checked';
                  $microbialValue = $value->value;
                }
              }
            }
            else{
              $checked = (get_sub_field('default'))?'checked':'';
            }

          ?>
          <tr>
            <td>
              <input type="checkbox" data-i="<?php echo $i;?>" data-standard="<?php echo (get_sub_field('default'))?'true':'false';?>" id="microbial<?php echo $i;?>" <?php echo $checked;?> value="<?php the_sub_field('label');?>" class="checkboxCusto microbial">
              <label for="microbial<?php echo $i;?>">
                <?php the_sub_field('label');?>
              </label>
            </td>
            <td>
              <?php the_sub_field('test_limits');?> <?php the_sub_field('unit');?>
            </td>
            <td>
              <?php
              if(get_sub_field('default')){
                info(get_sub_field('method')->post_name); _e("(Ph. Eur)","armor-pharma");
              }?>
            </td>
            <td class="form">
              <input type="text" placeholder="<?php _e("Customized limits","armor-pharma");?>" class="microbial_value" value="<?php echo $microbialValue;?>" id="microbial_value_<?php echo $i;?>" data-unit="<?php the_sub_field('unit');?>" data-value-standard="<?php the_sub_field('test_limits');?>" />
              <?php the_sub_field('unit');?>
            </td>
          </tr>
          <?php
          $i++;
          endwhile;
        endif;
        ?>
      </table>

      <h3><?php _e("Add another test :","armor-pharma");?></h3>
      <table id="anotherTest">
        <tr>
          <th>
            <?php _e("Add test title","armor-pharma");?>
          </th>
          <th>
            <?php _e("Add limits","armor-pharma");?>
          </th>
          <th>
            <?php _e("Precise method","armor-pharma");?>
          </th>
          <th>
          </th>
        </tr>
        <?php if(isset($design->anotherTest)): foreach ($design->anotherTest as $value) : ?>
          <tr>
            <td>
              <?php echo $value->label;?>
            </td>
            <td>
              <?php echo $value->limits;?>
            </td>
            <td>
              <?php echo $value->method;?>
            </td>
            <td>
            </td>
          </tr>
        <?php endforeach;endif;?>
        <tr class="form">
          <td>
            <input type="text" id="anotherTestLabel" placeholder="<?php _e("Add test title","armor-pharma");?>">
          </td>
          <td>
            <input type="text" id="anotherTestLimits" placeholder="<?php _e("Add limits","armor-pharma");?>">
          </td>
          <td>
            <input type="text" id="anotherTestMethod" placeholder="<?php _e("Precise method","armor-pharma");?>">
          </td>
          <td>
            <span class="picto picto-plus" id="addAnotherTest"></span>
          </td>
        </tr>
      </table>


      <h3><?php _e("Particule size distribution (Air jet sieve) :","armor-pharma");?></h3>
      <table id="particule">

      <?php
      if( have_rows('specification_text', $design->lactose->id) ): $i=0;
   	    while ( have_rows('specification_text', $design->lactose->id) ) : the_row();
        ?>
        <tr>
          <td>
            <?php the_sub_field("label");?>
          </td>
          <td>
            <?php the_sub_field("value");?>
          </td>
          <td class="form">
            <input type="text" class="particule"
              value="<?php echo isset($design->particule[$i]->value)?$design->particule[$i]->value:get_sub_field("value");?>"
              id="particule_<?php echo $i;?>"
              data-label="<?php the_sub_field('label');?>"
              data-value-standard="<?php the_sub_field("value");?>">
          </td>
        </tr>
        <?php
        $i++;
        endwhile;
      endif;
      ?>
      </table>

      <h3><?php _e("Need help ?","armor-pharma");?></h3>
      <p>
        <?php _e("Upload your own specification :","armor-pharma");?>
         <input type="file" id="fileSpecification" name="fileSpecification" />
      </p>
      <p class="form">
        <?php _e("Enter your target specification :","armor-pharma");?>
        <input type="text" class="targetSpecification"
          value="<?php echo isset($design->targetSpecification)?$design->targetSpecification:'';?>"
          id="targetSpecification">
      </p>

    </div>
  </main>
  <div id="rightCol">

  </div>
</div>
<?php
endwhile; endif;
get_footer(); ?>
