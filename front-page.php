<?php get_header(); ?>
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <video playsinline autoplay muted loop poster="<?php echo get_stylesheet_directory_uri() . '/assets/css/images/bg-home.png';?>" id="bgvid">
      <source src="<?php echo get_stylesheet_directory_uri() . '/assets/css/images/AP_VideoAcceuil.mp4';?>" type="video/mp4">
    </video>
			<div id="home-header">
        <div id="home-header-content" class="wrapper">

					<h1 class="degrade"><?php the_title();?></h1>
          <p>
            <a href="#" class="button" id="openVideoButton" onclick="openModalVideo()"><?php _e("Learn more about us", "armar-pharma");?></a>
          </p>
          <p id="ourApplication">
            <?php _e("Our applications", "armar-pharma");?>
          </p>
          <div id="imgApplications">
            <div class="imgApplication">
              <img src="<?php echo get_stylesheet_directory_uri() . '/assets/css/images/comprime.png';?>" />
            </div>
            <div class="imgApplication">
              <img src="<?php echo get_stylesheet_directory_uri() . '/assets/css/images/gelules.png';?>" />
            </div>
            <div class="imgApplication">
              <img src="<?php echo get_stylesheet_directory_uri() . '/assets/css/images/sachet.png';?>" />
            </div>
            <div class="imgApplication">
              <img src="<?php echo get_stylesheet_directory_uri() . '/assets/css/images/effervescent.png';?>" />
            </div>
            <div class="imgApplication">
              <img src="<?php echo get_stylesheet_directory_uri() . '/assets/css/images/inhalateur1.png';?>" />
            </div>
            <div class="imgApplication">
              <img src="<?php echo get_stylesheet_directory_uri() . '/assets/css/images/inhalateur2.png';?>" />
            </div>
            <div class="imgApplication">
              <img src="<?php echo get_stylesheet_directory_uri() . '/assets/css/images/inhalateur3.png';?>" />
            </div>
			    </div>
        </div>
      </div>
      <main class="wrapper">
        <div id="home-content">
          <h2 class="degrade"><?php the_field('title');?></h2>
          <?php the_field('content');?>
        </div>
        <div id="home-content-visuel">
          <div id="findYourCOA">
            <a href="<?php echo get_permalink( get_page_by_path( "quality/find-your-coa" ) );?>"><?php _e("FIND YOUR COA", "armor-pharma");?></a>
            <div class="text-center picto-more">
              <a href="<?php echo get_permalink( get_page_by_path( "quality/find-your-coa" ) );?>"><img src="<?php echo get_stylesheet_directory_uri() . '/assets/css/images/more.svg';?>'" class="more" /></a>
            </div>
          </div>
          <div id="designLactose">
            <a href="<?php echo get_permalink( get_page_by_path( "design-the-lactose-you-like" ) );?>"><?php _e("Design the lactose YOU like!<p>from particles to delivery…</p>", "armor-pharma");?></a>
            <div class="text-center">
              <a href="<?php echo get_permalink( get_page_by_path( "design-the-lactose-you-like" ) );?>"><img src="<?php echo get_stylesheet_directory_uri() . '/assets/css/images/more.svg';?>'" class="more" /></a>
            </div>
          </div>
          <div id="illustrationCircle">
          </div>
        </div>
      </main>
      <div id="lactose" class="wrapper">
        <h2 class="degrade semiCercle"><?php _e("Lactose portfolio", "armor-pharma");?></h2>
        <?php get_template_part( 'template-parts/loop', 'lactose' );?>
      </div>
      <div id="newsEvents">
        <div class="wrapper">
          <div id="news">
            <h2 class="degrade semiCercle small"><?php _e("News", 'armor-pharma');?></h2>
            <a href="<?php echo get_permalink( get_page_by_path( "events-news" ) );?>" id="allNews" class="button"><?php _e("SEE ALL NEWS", "armor-pharma");?></a>
            <div id="listing-news">
              <?php
              global $post;
              $args = array(  'posts_per_page'  => 3,
                              'post_status'     => "publish",
                              'post_type'       => "post");

              $myposts = get_posts( $args );
              foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
              <div class="news" style="background-image:url(<?php echo get_the_post_thumbnail_url($post->ID,'large');?>);">
                <div class="news-content">
                  <div class="notHover">
                    <date><?php echo get_the_date();?></date>
                    <?php wpgreen_get_the_category();?>
                    <a href="<?php the_permalink(); ?>" class="title">
                      <h3><?php the_title(); ?></h3>
                      <p>
                        <?php the_field("small_description");?>
                      </p>
                    </a>
                  </div>
                  <div class="hover">
                    <a href="<?php the_permalink(); ?>" class="title">
                      <h3><?php the_title(); ?></h3>
                    </a>
                    <div class="text-center">
                        <img src="<?php echo get_stylesheet_directory_uri() . '/assets/css/images/arrow.svg';?>" class="arrow" />
                    </div>
                    <div class="text-center">
                      <a href="<?php the_permalink(); ?>" class="button small"><?php _e("Read more", "armor-pharma");?></a>
                    </div>
                  </div>
                </div>

              </div>
              <?php endforeach;
              wp_reset_postdata();
              ?>
            </div>
          </div>

          <div id="news-mobile">
            <h2 class="degrade semiCercle small"><?php _e("News", 'armor-pharma');?></h2>
            <a href="<?php echo get_permalink( get_page_by_path( "events-news" ) );?>" id="allNews-mobile" class="button"><?php _e("SEE ALL NEWS", "armor-pharma");?></a>
            <div id="listing-news-mobile" class="flexslider">
              <div class="custom-navigation-news-mobile">
                <a href="#" class="flex-prev"> < </a>
                <a href="#" class="flex-next"> > </a>
              </div>
              <ul class="slides">
              <?php
              global $post;
              $args = array(  'posts_per_page'  => 3,
                              'post_status'     => "publish",
                              'post_type'       => "post");

              $myposts = get_posts( $args );
              foreach ( $myposts as $post ) : setup_postdata( $post );
                echo '<li class="archive-news">';
                get_template_part( 'template-parts/loop', 'archive' );
                echo '</li>';
              endforeach;
              wp_reset_postdata();
              ?>
            </ul></div>
          </div>

          <div id="events">
            <h2 class="degrade semiCercle small"><?php _e("Events", 'armor-pharma');?></h2>
            <a href="<?php echo get_permalink( get_page_by_path( "events-news" ) );?>" id="allEvents" class="button"><?php _e("SEE ALL EVENTS", "armor-pharma");?></a>
            <div class="flexslider" id="listing-events">
              <div class="custom-navigation">
                <a href="#" class="flex-prev"><span class="picto angle-left"></span> <?php _e("Previous events", "armor-pharma");?></a>
                <a href="#" class="flex-next"><?php _e("Next events", "armor-pharma");?> <span class="picto angle-right"></span></a>
              </div>
              <hr>
              <ul class="slides">

              <?php
              $today = date('Ym');
              global $post;
              $args = array (
                'posts_per_page'  => 4,
                'post_type' => 'event',
                'status' => 'publish',
                'meta_key'	=> 'date-begin',
              	'orderby'	=> 'meta_value_num',
              	'order'		=> 'ASC',
                'meta_query' => array(
                array(
                      'key'		=> 'date-begin',
                      'compare'	=> '>=',
                      'value'		=> $today,
                  )
                ),
              );

              $myposts = get_posts( $args );
              $i=0;
              foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
                <?php if($i % 2 == 0): ?>
                <li>
                <?php endif;?>
                  <div class="event">
                    <div class="event-image" style="background-image:url(<?php echo get_the_post_thumbnail_url($post->ID,'medium');?>);">
                      <date class="polyDate"><?php the_field("label-date");?></date>
                    </div>
                    <div class="event-content">
                      <a href="<?php the_permalink(); ?>" class="title">
                        <h3><?php the_title(); ?></h3>
                      </a>
                      <p class="event-place">
                        <?php the_field("event-place");?>
                      </p>
                      <p>
                        <?php the_excerpt();?>
                      </p>
                      <a href="<?php the_permalink(); ?>" class="button small"><?php _e("Read more", "armor-pharma");?></a>
                    </div>
                  </div>
                <?php $i++;?>
                <?php if($i % 2 == 0): ?>
                </li>
                <?php endif;?>

              <?php endforeach;
              wp_reset_postdata();
              if($i % 2 != 0){echo '</li>';}
              ?>
            </ul>
          </div>
        </div>

        <div id="events-mobile">
          <h2 class="degrade semiCercle small"><?php _e("Events", 'armor-pharma');?></h2>
          <a href="<?php echo get_permalink( get_page_by_path( "events-news" ) );?>" id="allEvents-mobile" class="button"><?php _e("SEE ALL EVENTS", "armor-pharma");?></a>
          <div class="flexslider" id="listing-events-mobile">
            <div class="custom-navigation-events-mobile">
              <a href="#" class="flex-prev"> <span class="picto angle-left"></span> </a>
              <a href="#" class="flex-next"> <span class="picto angle-right"></span> </a>
            </div>
            <ul class="slides archive-news">

            <?php
            $today = date('Ym');
            global $post;
            $args = array (
              'posts_per_page'  => 4,
              'post_type' => 'event',
              'status' => 'publish',
              'meta_key'	=> 'date-begin',
              'orderby'	=> 'meta_value_num',
              'order'		=> 'ASC',
              'meta_query' => array(
              array(
                    'key'		=> 'date-begin',
                    'compare'	=> '>=',
                    'value'		=> $today,
                )
              ),
            );

            $myposts = get_posts( $args );
            foreach ( $myposts as $post ) : setup_postdata( $post );
              ?><li><?php
              get_template_part( 'template-parts/loop', 'event' );
              ?></li><?php
            endforeach;
            wp_reset_postdata();
            ?>
          </ul>
        </div>

      </div>
	<?php endwhile; endif; ?>
<?php get_footer(); ?>
    <!-- MODAL -->
		<div class="modal" id="modal-video">
			<div class="modal-header">
				<h2 id="modal-title" class="degrade"><?php _e("Learn more about us");?></h2>
			</div>
			<div id="modal-content">
        <div class="embed-container ">
          <?php the_field("video");?>
        </div>
			</div>
			<div class="modal-footer">
				<p>
					<button class="button" onclick="closeModal()">
						&times; <?php _e('Close','wpgreen');?>
					</button>
				</p>
			</div>
		</div>
