<?php
get_header();
/*
Template Name: Quality Methods
 */
?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
  <div id="content-header">
    <div class="wrapper">
      <?php the_breadcrumb(); ?>
    </div>
  </div>
	<main id="main" role="main" class="">
		<article>
			<header class="wrapper" id="header-page">
				<h1 class="page-title degrade"><?php the_title(); ?></h1>
			</header>
      <div class="wrapper">
        <?php get_template_part('template-parts/header', 'quality'); ?>
      </div>
			<section class="entry-content" itemprop="articleBody">
        <div class="wrapper">
            <?php the_content(); ?>
        </div>
        <div id="methods">
          <div class="wrapper">
            <div id="images-categories">
              <?php
              $terms = get_terms('category-lactose');
              $active = true;
              if (!empty($terms) && !is_wp_error($terms)) {
                foreach ($terms as $term) {
                  $image = get_field('image_methods', $term);
                  ?>
                   <div data-term-id="<?php echo $term->term_id; ?>" class="image-category-wrapper <?php echo ($active) ? 'active' : ''; ?>" style="color:<?php the_field("color", $term); ?>;background-image:url(<?php echo wp_get_attachment_image_url($image, 'wpgreen-400'); ?>);">
                      <div class="image-category <?php echo ($active) ? 'active' : '';
                                                $active = false; ?>">
                        <div>
                          <h2><?php the_field("title", $term); ?></h2>

                        </div>
                        <div>
                          <hr /> 
                        </div>
                        <div>
                            <?php the_field("short_description", $term); ?>
                        </div>
                      </div>
                   </div>
                   <?php

                }
              }
              ?>
            </div>

            <div id="testMethods">
              <?php
              $terms = get_terms('category-lactose');
              $active = true;
              if (!empty($terms) && !is_wp_error($terms)) :
                foreach ($terms as $term) :
                $methods_of_analysis = get_field('methods_of_analysis', $term);
              ?>
                     <div class="testMethod <?php echo ($active) ? 'active' : '';
                                            $active = false; ?>" id="testMethod_<?php echo $term->term_id; ?>">
                       <table>
                         <tr>
                           <th>
                             <?php _e("TEST"); ?>
                           </th>
                           <th>
                             <?php _e("TEST LIMITS"); ?>
                           </th>
                           <th>
                             <?php _e("METHOD OF ANALYSIS"); ?>
                           </th>
                        </tr>
                        <?php
                        if ($methods_of_analysis) :
                          foreach ($methods_of_analysis as $method) :
                          echo '<tr>';
                        echo '<td>' . $method['test'] . '</td>';
                        echo '<td>' . $method['test_limits'] . '</td>';
                        $file = $method['method'];
                        if (get_current_user_id() > 0) {
                          if (empty($file['url'])) {
                            echo '<td></td>';
                          } else {
                            echo '<td><a href="' . $file['url'] . '" download="armorpharma.pdf"><span class="picto picto-pdf"></span>file.pdf</a></td>';
                          }
                        } else {
                          echo '<td><a href="#" onclick="openModalConnect(0);"><span class="picto picto-lock"></span>' . __("Connect to download", "armor-pharma") . '</a></td>';
                        }
                        echo '</tr>';
                        endforeach;
                        endif; ?>
                       </table>
                       <div class="right">
                        <!--<?php if (get_current_user_id() > 0) : ?>
                        <a href="<?php the_field("download", $term); ?>" download class="download">
                        <?php else : ?>
                        <a href="#" onclick="openModalConnect(0);" class="download">
                        <?php endif; ?>
                           <p>
                             <?php _e("Download", "armor-pharma"); ?>
                             <?php echo $term->name; ?>
                           </p>
                           <div class="text-center">
                             <span class="picto picto-download"></span>
                           </div>
                        </a>-->

                        <a href="<?php _e("https://www.edqm.eu/", "armor-pharma"); ?>" target="_blank" class="external-link">
                            <p>
                              <?php _e("Go to the European Pharmacopei a website", "armor-pharma"); ?>
                            </p>
                            <div class="text-center">
                              <span class="picto picto-external"></span>
                            </div>
                        </a>
                       </div>
                     </div>


                     <?php
                    endforeach;
                    endif;

                    ?>
            </div>
          </div>
        </div>
			</section>
		</article>
	</main>
  <?php //get_template_part( 'template-parts/faq', '' );?>
  <div id="askExpert" class="wrapper">
    <hr>
    <div class="form">
      <label for="specTxt">
        <?php _e("Can't find what you're looking for ...", "arma-pharma"); ?>
        <input type="text" id="specTxt" name="specTxt" placeholder="<?php _e("Type your question", "armor-pharma"); ?>" />
        <buton class="button" id="submitAskExpert"><?php _e("Ask the expert", "armar-pharma"); ?></buton>
      </label>
    </div>
  </div>
  <!-- MODAL -->
  <div class="modal" id="modalAskExpert">
    <form id="form-expert" name="form-expert" action="<?php the_permalink(); ?>" method="post" enctype="multipart/form-data">
      <input type="hidden" name="action" value="formExpert">
      <input type="hidden" name="honey" value="">
      <?php wp_nonce_field('nonceformExpert', 'nonceformExpert'); ?>
      <div class="modal-content form">
        <div>
          <label for="Expert_Mr" class="notDisplayBlock">Mr</label><input type="radio" id="Expert_Mr" name="genre" value="Mr" />
          <label for="Expert_Mrs" class="notDisplayBlock">Mrs</label><input type="radio" id="Expert_Mrs" name="genre" value="Mrs" />
        </div>
        <div class="grid2">
          <div>
            <label for="firstname"><?php _e("First name", "armor-pharma"); ?></label>
            <input type="text" name="firstname" id="firstname" required />
          </div>
          <div>
            <label for="lastname"><?php _e("Last name", "armor-pharma"); ?></label>
            <input type="text" name="lastname" id="lastname" required />
          </div>
        </div>
        <div class="grid2">
          <div>
            <label for="Expert_country"><?php _e("Country", "armor-pharma"); ?></label>
            <input type="text" name="country" id="Expert_country" required />
          </div>
          <div>
            <label for="Expert_phone"><?php _e("Phone", "armor-pharma"); ?></label>
            <input type="tel" name="phone" id="Expert_phone"  />
          </div>
        </div>
        <div>
          <label id="profil">
            <?php _e("Profile", "armor-pharma"); ?>
          </label>
          <select name="profil" id="profil" required>
            <option>
              profil 1
            </option>
            <option>
              profil 2
            </option>
            <option>
              profil 3
            </option>
          </select>
        </div>
        <div class="grid2">
          <div>
            <label for="Expert_company"><?php _e("Company", "armor-pharma"); ?></label>
            <input type="text" name="company" id="Expert_company" required />
          </div>
          <div>
            <label for="email"><?php _e("Email", "armor-pharma"); ?></label>
            <input type="email" name="email" id="email" required />
          </div>
        </div>
        <div class="border"></div>
        <div>
          <label for="subject"><?php _e("Subject", "armor-pharma"); ?></label>
          <input type="text" readonly name="subject" id="subject" required />
        </div>
        <div>
          <label for="comments"><?php _e("Comments", "armor-pharma"); ?></label>
          <textarea name="comments" id="comments" required ></textarea>
        </div>
        <div>
        
          <input type="checkbox" name="check" id="check" required />
          <label for="check">
            <?php _e("By submitting your data through this form, you confirm that you are above the age of 18, that you have read and understood the Privacy Policy, and that you agree to the collection, use and processing of your Personal Information by ARMOR PHARMA in accordance with said Policy", "armor-pharma"); ?>
          </label>
        </div>
        <div class="text-center">
          <button class="button" id="sendAskExpert"><?php _e("SEND", "armor-pharma"); ?></button>
        </div>
      </div>
    </form>
  </div>

<?php endwhile;
endif; ?>
<?php get_footer(); ?>
