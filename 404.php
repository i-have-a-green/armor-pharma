<?php
get_header();
?>

 
	<main id="main" role="main" class="wrapper my-account">
		<article>
			

			<section class="" itemprop="articleBody" style="text-align:center">
				<br><br>
				<br><br>
				<p><h1>Page Not Found (404 Error)</h1></p>
				<br>
				<p>The page you are looking for cannot be found or no longer exists.</p>
				<br>
				<p>We invite you to:</p>

				<ul>
				<li>Return to the <a href="<?php echo home_url();?>">homepage</a> to access key information.</li>
				<li>Contact us if you need assistance. Our team is here to help.</li>
				</ul>
				<br>

				<p>Thank you for your understanding.</p>
				<br><br>
				<br><br>
			</section>
		</article>
	</main>

<?php get_footer(); ?>



