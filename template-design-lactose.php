<?php
/*
Template Name: Design lactose
*/
get_header();
if (have_posts()) :  while ( have_posts() ) : the_post();
?>
<div id="content-header">
  <div class="wrapper">
    <?php the_breadcrumb();?>
  </div>
</div>
<div class="text-center">
  <h1 class="degrade"><?php _e("Design the lactose <b>you</b> like","armor-pharma");?></h1>
</div>
<?php
set_query_var( 'item', 0 );
get_template_part( 'template-parts/arianne', 'design' );?>
<div class="wrapper" id="mainTemplateDesign">
  <div class="help">
    <a href="<?php echo get_the_permalink(get_page_by_path( "contact" ) );?>">
      <span><?php _e("For more complicated requests (several product, global sourcing...) Contact us directly!","armor-pharma");?></span>
    </a>
    <a class="picto-info" data-slug="help-design">
      <img src="<?php echo get_stylesheet_directory_uri() . '/assets/css/images/help.png';?>">
    </a>
  </div>
  <main>
    <div class="grid">
      <div>
        <img src="<?php echo get_stylesheet_directory_uri() . '/assets/css/images/process1.png';?>" id="illustrationDesign">
      </div>
      <div class="form">
        <div>
          <h2 class="degrade semiCercle small"><?php _e("I know the product I am looking for :","armor-pharma");?></h2><br />
          <select id="designSelectLactose">
            <?php $posts = get_posts(array(
              "posts_per_page"=>-1,
              "post_type"=>"lactose",
              'tax_query'      => array(
                  array(
                      'taxonomy' => 'category-lactose',
                      'terms' => array('excipure'),
                      'field' => 'slug',
                      'operator' => 'NOT IN',
                  )
              ),
            ));
            foreach ($posts as $post) : setup_postdata( $post );
              if(!isset($idNext)){
                $idNext = get_the_id();
              }
              echo '<option value="'.get_the_id().'">'.get_the_lactose().'</option>';
            endforeach;
            wp_reset_postdata();
            ?>
          </select>
          <a href="<?php echo get_the_permalink(get_page_by_path( 'design-the-lactose-you-like/registration' ) ).'?id='.$idNext;?>" id="buttonNext" class="button small " data-url="<?php echo get_the_permalink(get_page_by_path( 'design-the-lactose-you-like/registration' ) );?>"><?php _e("NEXT","armor-pharma");?></a>
          <p>
            <?php
            $design = get_user_meta(get_current_user_id(), 'design', true);
            $design = json_decode($design);
            if(!empty($design)):?>
              <a href="<?php echo get_the_permalink(get_page_by_path("design-the-lactose-you-like/registration"));?>"><?php _e("Continue your last design","armor-pharma");?></a>
            <?php endif
            ?>
          </p>
          <hr />
          <p>
            <?php _e("I would like to select the suitable product for my project!","armor-pharma");?>
          </p>
          <a href="<?php echo get_the_permalink(get_page_by_path( 'lactose-portfolio' ) );?>" class="button small"><?php _e("SEARCH","armor-pharma");?></a>
        </div>
      </div>
    </div>
  </main>
</div>
<?php
endwhile; endif;
get_footer(); ?>
