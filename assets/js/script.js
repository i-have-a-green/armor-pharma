/********HELPERS**********/
function hasClass(el, className) {
  if (el.classList)
    return el.classList.contains(className)
  else
    return !el.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'))
}

function addClass(el, className) {
  if (el.classList)
    el.classList.add(className)
  else if (!hasClass(el, className)) el.className += " " + className
}

function removeClass(el, className) {
  if (el.classList)
    el.classList.remove(className)
  else if (hasClass(el, className)) {
    var reg = new RegExp('(\\s|^)' + className + '(\\s|$)')
    el.className=el.className.replace(reg, ' ')
  }
}

$( "select" ).wrap("<div class='selectWrapper'></div>");

 
var el = document.getElementsByClassName('no-link');
for(var i = 0; i < el.length; i++){
  el[i].getElementsByTagName('a')[0].href="javascript:void(0)";
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
/********MODAL*******/
function openModal(text, title){
	var text = (typeof text !== 'undefined') ? text : "";
	var title = (typeof title !== 'undefined') ? title : "";
	if(text.length){
		document.getElementById('modal-content').innerHTML = text;
	}
	if(title.length){
		document.getElementById('modal-title').innerHTML = title;
	}
	addClass(document.getElementById('modal'), 'openModal');
	overlay = document.getElementById("overlay");
	addClass(overlay, 'overlayOpen');
}
function closeModal(){
	el = document.getElementsByClassName('modal');
	for(var i = 0; i < el.length; i++){
		removeClass(el[i], 'openModal');
	}
	overlay = document.getElementById("overlay");
	removeClass(overlay, 'overlayOpen');
	el = document.getElementById("menuMobile");
	removeClass(el, 'menuMobileOpen');
}
function openModalVideo(){
	addClass(document.getElementById('modal-video'), 'openModal');
	overlay = document.getElementById("overlay");
	addClass(overlay, 'overlayOpen');
}
document.getElementById("overlay").addEventListener("click", function(){closeModal();});
/*******MENU MOBILE*******/
if(document.getElementById("burger-menu")){
	document.getElementById("burger-menu").addEventListener("click", function(){menuMobile();});
}
function menuMobile() {
	var el = document.getElementById("menuMobile");
	var overlay = document.getElementById("overlay");
  var burger = document.getElementById("burger-menu");
  if(hasClass(el, 'menuMobileOpen')){
		removeClass(el, 'menuMobileOpen');
		removeClass(overlay, 'overlayOpen');
    removeClass(burger, 'open');
		document.getElementById("overlay").addEventListener("click", function(){return false;});
	}
	else{
		addClass(el, 'menuMobileOpen');
		addClass(overlay, 'overlayOpen');
    addClass(burger, 'open');
		document.getElementById("overlay").addEventListener("click", menuMobile);
	}
}
/*MENU sticky*/

isScroll = false;
window.onscroll = function() {
  posScroll = window.pageYOffset;
  if(posScroll > 50 && isScroll == false){
    //ajoute la class move
    addClass(document.getElementById('header'), 'move');
    addClass(document.getElementById('headerMobile'), 'move');

    isScroll = true;
  }
  else if (posScroll < 50 && isScroll){
    removeClass(document.getElementById('header'), 'move');
    removeClass(document.getElementById('headerMobile'), 'move');
    isScroll = false;
  }
}

//contact
function wpgreen_recaptchaCallback() {
	document.getElementById('submitBtn').disabled = false;
};

document.addEventListener('DOMContentLoaded', function(){
/*  if(document.getElementById('form-event')){
    $("#date").on("blur", function(){
      console.log($("#date").val());
    })
    $('#meeting_send').on("click", function(e){
      e.preventDefault();
      var form = document.forms.namedItem("form-meeting");
      testForm = form.reportValidity();
      if(testForm){
        document.getElementById('meeting_send').disabled = true;
  			var formData = new FormData(form);
  		  xhr = new XMLHttpRequest();
  			xhr.open('POST', url.ajaxurl, true);//commentaire
  			xhr.onload = function() {
  			    if (xhr.status === 200) {
            closeModal();
  					openModal("Thank you for your request, we will revert to you with appointment proposals in the shortest delay","Request for a meeting");
  					document.getElementById('submitBtn').disabled = false;
  			    }
  			};
  			xhr.send(formData);
      }
    });

		document.getElementById('form-event').addEventListener('submit', function(e) {
			//document.getElementById('submitBtn').disabled = true;
		  e.preventDefault();


      var value = "";
      var valueTxt = "";
      $("#form-event input[type=checkbox]").each(function(){
        value += $(this).val() + " - ";
        valueTxt += $(this).val() + "<br>";
      });
      $("#meeting_who").val(value);
      $("#meeting_who_span").html(valueTxt);
      $("#subject").val($("#specTxt").val());
      $("#meeting_comments_span").html($("#comments").val());
      $("#meeting_comments").val($("#comments").val());
      $("#meeting_date_span").html($("#date").val());
      $("#meeting_date").val($("#date").val());

      addClass(document.getElementById('modalMeeting'), 'openModal');
    	overlay = document.getElementById("overlay");
    	addClass(overlay, 'overlayOpen');
		});
	}*/

  if(document.forms.namedItem('form-expert')){
		document.forms.namedItem('form-expert').addEventListener('submit', function(e) {
 	    e.preventDefault();
      document.getElementById('sendAskExpert').disabled = true;
			var form = document.forms.namedItem("form-expert");

			var formData = new FormData(form);
      if(document.getElementById('specFile') != null){
        formData.append("specFile", document.getElementById('specFile').files[0]);
      }

      if(document.getElementById('distributorId') != null){
        formData.append("distributorId", document.getElementById('distributorId').value);
      }

      if(document.getElementById('teamEmail') != null){
        formData.append("teamEmail", document.getElementById('teamEmail').value);
      }

		  /*xhr = new XMLHttpRequest();
			xhr.open('POST', url.ajaxurl, true);// à traiter
			xhr.onload = function() {
			    if (xhr.status === 200) {
            closeModal();
  					document.getElementById('sendAskExpert').disabled = false;
  					openModal("Thank you for submitting your request! Our experts will revert to you in the shortest delay.","Ask the expert");
			    }
			};
			xhr.send(formData);*/
      fetch( ihagRest.resturl + 'formExpert', { 
        method: 'POST',
        body: formData,
        headers: {'X-WP-Nonce': ihagRest.nonce},
        cache: 'no-cache',
      } )
      .then(function (data) {
        if (data.status === 200) { // Vérifier le statut comme un nombre
            closeModal();
            document.getElementById('sendAskExpert').disabled = false;
            openModal("Thank you for submitting your request! Our experts will revert to you in the shortest delay.", "Ask the expert");
        }
      })
      .catch( function ( err ) {
          console.log( 'Fetch Error :-S', err );
        } );
		  });
	}
  if(document.forms.namedItem('form-contact')){
		document.forms.namedItem('form-contact').addEventListener('submit', function(e) {
 	    e.preventDefault();
      document.getElementById('contact_sendAskExpert').disabled = true;
			var form = document.forms.namedItem("form-contact");

			var formData = new FormData(form);

		 /* xhr = new XMLHttpRequest();
			xhr.open('POST', url.ajaxurl, true);//à traiter
			xhr.onload = function() {
			    if (xhr.status === 200) {
            closeModal();
  					document.getElementById('contact_sendAskExpert').disabled = false;
            closeModal();
  					openModal("Thank you for submitting your request! Our experts will revert to you in the shortest delay.","Ask the expert");
			    }
			};
			xhr.send(formData);*/
      fetch( ihagRest.resturl + 'formExpert', { 
        method: 'POST',
        body: formData,
        headers: {'X-WP-Nonce': ihagRest.nonce},
        cache: 'no-cache',
      } )
      .then(function (data) {
        if (data.status === 200) { // Vérifier le statut comme un nombre
          document.getElementById('contact_sendAskExpert').disabled = false;
          closeModal();
          openModal("Thank you for submitting your request! Our experts will revert to you in the shortest delay.","Ask the expert");
        }
      })
        
        .catch( function ( err ) {
          console.log( 'Fetch Error :-S', err );
        } );

		});
	}
  if(document.getElementById('more-news')){
    document.getElementById('more-news').addEventListener('click', function(e){
      e.preventDefault();
      var overlay = document.getElementById("overlay");
    	addClass(overlay, 'overlayOpen');
      var cat = "";
      var checkboxCat = document.getElementsByClassName("checkboxCat");
      for (var i = 0, len = checkboxCat.length; i < len; i++) {
        if (checkboxCat[i].checked == true){
          cat += checkboxCat[i].value + ",";
        }
      }
      xhr = new XMLHttpRequest();
      xhr.open('POST', url.ajaxurl, true);//ok
      xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
      xhr.onload = function() {
          if (xhr.status === 200) {
            elem = document.getElementById("archive-news");
            elem.innerHTML += xhr.responseText;
            removeClass(overlay, 'overlayOpen');
          }
      };
      xhr.send('action=morenews&cat='+cat+'&page='+(document.getElementsByClassName('archive-news').length + 1));
    });
  }
  if(document.getElementById('form-profil')) {
    document.getElementById('checkForm').checked = false;
    /* let input = document.getElementById('checkForm').checked = false;
    console.log(input); */
  }

  posScroll = window.pageYOffset;
  if(posScroll > 50){
    addClass(document.getElementById('header'), 'move');
    addClass(document.getElementById('headerMobile'), 'move');
  }
});

$(window).load(function() {
  if($('#listing-events') && $('#listing-events').length){
    if(window.innerWidth > 600){
      $('#listing-events').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        customDirectionNav: $(".custom-navigation a")
      });
      $('#listing-events').flexslider("pause");
    }
  }

  if($('#category-lactose-mobile') && $('#category-lactose-mobile').length){
    if(window.innerWidth <= 800){
      $('#category-lactose-mobile').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        maxItems: (window.innerWidth < 600) ? 1 : 2,
        minItems: 1,
        itemWidth: 280,
      });
    }
  }

  if($('#listing-news-mobile') && $('#listing-news-mobile').length){
    if(window.innerWidth <= 600){
      $('#listing-news-mobile').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        maxItems: 1,
        minItems: 1,
        customDirectionNav: $(".custom-navigation-news-mobile a")
      });
    }
  }

  if($('#listing-events-mobile') && $('#listing-events-mobile').length){
    if(window.innerWidth <= 600){
      $('#listing-events-mobile').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        maxItems: 1,
        minItems: 1,
        customDirectionNav: $(".custom-navigation-events-mobile a")
      });
    }
  }

  if($('#sliderEvent') && $('#sliderEvent').length){
    $('#sliderEvent').flexslider({
      animation: "slide",
      controlNav: false,
      itemWidth: 210,
      itemMargin: 5,
      minItems: 2,
      maxItems: 4
    });
  }

  if($('#carouselEvents') && $('#carouselEvents').length){
    var nbItem = (window.innerWidth < 450) ? 3 : (window.innerWidth > 900) ? 12 : 6;
    $('#carouselEvents').flexslider({
      animation: "slide",
      controlNav: false,
      animationLoop: false,
      slideshow: false,
      customDirectionNav: $(".custom-navigation a"),
      itemMargin: 5,
      itemWidth: 70,
      maxItems:  nbItem,
      minItems:  nbItem,
    });

    if(nbItem == 12){
      $('#carouselEvents').flexslider( $('#carouselEvents .active').data( "value12" ) );
    }
    else if(nbItem == 6){
      $('#carouselEvents').flexslider( $('#carouselEvents .active').data( "value6" ) );
    }
    else if(nbItem == 3){
      $('#carouselEvents').flexslider( $('#carouselEvents .active').data( "value3" ) );
    }

    $('#carouselEvents').flexslider("pause");
  }




  if($('#archive-event.flexslider') && $('#archive-event.flexslider').length){
    $('#archive-event.flexslider').flexslider({
      animation: "slide",
      controlNav: false,
      animationLoop: false,
      slideshow: false,
      itemMargin: 5,
      itemWidth: 210,
      maxItems:  (window.innerWidth < 450) ? 1 : (window.innerWidth < 600) ? 2 : (window.innerWidth > 900) ? 4 : 3,
      minItems:  (window.innerWidth < 450) ? 1 : (window.innerWidth < 600) ? 2 : (window.innerWidth > 900) ? 4 : 3,
    });
    $('#archive-event').flexslider("pause");
  }

  /*if($('#archive-event-pSingle') && $('#archive-event-pSingle').length){
    $('#archive-event-pSingle').flexslider({
      animation: "slide",
      controlNav: false,
      animationLoop: false,
      slideshow: false,
      itemMargin: 5,
      itemWidth: 210,
      maxItems:  (window.innerWidth < 450) ? 1 : (window.innerWidth < 600) ? 2 : (window.innerWidth > 900) ? 4 : 3,
      minItems:  (window.innerWidth < 450) ? 1 : (window.innerWidth < 600) ? 2 : (window.innerWidth > 900) ? 4 : 3,
    });
    $('#archive-event-pSingle').flexslider("pause");
  }*/

  /*if($('#archive-news-flexslider') && $('#archive-news-flexslider').length){
    $('#archive-news-flexslider').flexslider({
      animation: "slide",
      controlNav: false,
      animationLoop: false,
      slideshow: false,
      itemMargin: 5,
      itemWidth: 210,
      maxItems:  (window.innerWidth < 450) ? 1 : (window.innerWidth < 600) ? 2 : (window.innerWidth > 900) ? 4 : 3,
      minItems:  (window.innerWidth < 450) ? 1 : (window.innerWidth < 600) ? 2 : (window.innerWidth > 900) ? 4 : 3,
    });
    $('#archive-news-flexslider').flexslider("pause");
  }*/


});

function checkboxCat(){
  var overlay = document.getElementById("overlay");
  addClass(overlay, 'overlayOpen');
  var cat = "";
  var checkboxCat = document.getElementsByClassName("checkboxCat");
  for (var i = 0, len = checkboxCat.length; i < len; i++) {
    if (checkboxCat[i].checked == true){
      cat += checkboxCat[i].value + ",";
    }
  }
  xhr = new XMLHttpRequest();
  xhr.open('POST', url.ajaxurl, true);//ok
  xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhr.onload = function() {
      if (xhr.status === 200) {
        elem = document.getElementById("archive-news");
        elem.innerHTML = xhr.responseText;
        removeClass(overlay, 'overlayOpen');
      }
  };
  xhr.send('action=morenews&page=1&cat='+cat);
};


/*RANGE*/
$( function() {
  $(".slider-custom").each(function(){
    $( this ).slider({
      range: true,
      min: $( this ).data("min"),
      max: $( this ).data("max"),
      values: $( this ).data("value"),
      step: $( this ).data("step"),
      create: function() {
        $( this ).find(".handle-min>span" ).text( $( this ).slider( "values" )[0] );
        $( this ).find(".handle-max>span" ).text( $( this ).slider( "values" )[1] );
      },
      slide: function( event, ui ) {
        $( this ).find(".handle-min>span" ).text( ui.values[0] );
        $( this ).find(".handle-max>span" ).text( ui.values[1] );
      },
      change : updateConfigurateur,
    });
  });

    $('#application5').on("click",function(){
      addClass(document.getElementById('modalInhaler'), 'openModal');
      overlay = document.getElementById("overlay");
      addClass(overlay, 'overlayOpen');
    }); 

    $("#sendAskInhaler").on("click",function(e){
      e.preventDefault();
      var form = document.getElementById("formInhaler");
      testForm = form.reportValidity();
      if(testForm){
        var formData = new FormData(form);
        /*console.log(formData);
        xhr = new XMLHttpRequest();
        xhr.open('POST', url.ajaxurl, true);//à traiter
        xhr.onload = function() {
          if (xhr.status === 200) {
            console.log(xhr.responseText);
            closeModal();
            openModal("Your request has been successfully submitted","Thank you");
          }
        };
        xhr.send(formData);*/
        fetch( ihagRest.resturl + 'formExpert', { 
          method: 'POST',
          body: formData,
          headers: {'X-WP-Nonce': ihagRest.nonce},
          cache: 'no-cache',
        } )
        .then(function (data) {
          if (data.status === 200) { // Vérifier le statut comme un nombre
            closeModal();
            openModal("Your request has been successfully submitted","Thank you");
          }
        })
          .catch( function ( err ) {
            console.log( 'Fetch Error :-S', err );
          } );
      }
    });

  $(".configurateur-label h4").on("click", function(){
    if(!$(this).hasClass("picto")){
      $(this).parent(".configurateur-label").next(".configurateur-values").toggleClass("displayNone");
      $(this).toggleClass("active")
    }

  });

});

function updateConfigurateur(){
  $("#cont-res-configurateur").hide("blind",[],200);

  var slidersData = [];
  $(".slider-custom").each(function(){
    slidersData.push({"name": $(this).attr("id"), "values" :$(this).slider( "values" ) });
  });
  slidersData.push({"name": "airjetSieveParticleSize", "values" :$("#airjetSieveParticleSize").val() });

  slidersData.push({"name": "application1", "values" :$("#application1").prop( "checked" ) });
  slidersData.push({"name": "application2", "values" :$("#application2").prop( "checked" ) });
  slidersData.push({"name": "application3", "values" :$("#application3").prop( "checked" ) });
  slidersData.push({"name": "application4", "values" :$("#application4").prop( "checked" ) });

  xhr = new XMLHttpRequest();
  xhr.open('POST', url.ajaxurl, true);//ok
  xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhr.onload = function() {
      if (xhr.status === 200) {
        elem = document.getElementById("res-configurateur");
        elem.innerHTML = xhr.responseText;

        showChartConfigurateur();

        $("#cont-res-configurateur").show("blind",[],800);
        $(".productMobile").on("click", function(){
          $("#contProductMobile"+$(this).data("id")).toggle("blind");
          $(this).toggleClass("open");
        });
        if($("#nbResult").data("value") > 0){
          $("#displayNbrResult").text($("#nbResult").data("value")+" products");
        }
        else {
          $("#displayNbrResult").text("no result");
        }
        $("#configurateurButton").on("click", function(){
          var offset = $("#cont-res-configurateur").offset();
          $('html, body').animate({scrollTop : offset.top},800);
        });

        $(".cartLink").on("click", function(e){
          e.preventDefault();
          addCart($(this));
        });

        $("#picto-info-graph").on("click", function(){
          xhr = new XMLHttpRequest();
          xhr.open('POST', url.ajaxurl, true);//ok
          xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
          xhr.onload = function() {
              if (xhr.status === 200) {
                var post = JSON.parse(xhr.responseText);
                openModal(post["content"], post["title"]);
              }
          };
          xhr.send("action=displayInfo&slug="+$(this).data("slug"));
        });

      }
  };
  xhr.send("action=formConfigurateur&slidersData="+JSON.stringify(slidersData));

}
function showChartConfigurateur(){
  datasetsTemp = [];
  $(".lactoseCofigurateur").each(function(){
    datasetsTemp.push( JSON.parse($(this).data('set').replace(/'/g, '"') ));
  });
  var chartjsCol = new Chart(document.getElementById("chartjsConfigurateur"),
  {
    "type":"radar",
    "data":
      {"labels": ["Compressibility","Flowability","Cohesivness","Poured density","PSD (d50)","PSD (span)","SSA"],
        "datasets": datasetsTemp, 
      },
      "options":{
        "elements":{
          "line":{"tension":0,"borderWidth":3},
        },
        "legend":{
          "display": false,
          "position":"bottom"
        },
        "scale": {
           "ticks": {
              "max":5,
              "display": false,
              "beginAtZero":true,
              "maxTicksLimit": 5
           }
        },
        tooltips: {
          enabled: false,
          callbacks: {
            label: function(tooltipItem, chart) {
                return chart.datasets[tooltipItem.datasetIndex].dataLabel[tooltipItem.index];
            }
          }
        }
      }
  });

  datasetsTemp = [];
  dataLabel = '';
  $(".lactoseCofigurateur").each(function(){
    datasetsTemp.push( JSON.parse($(this).data('volume').replace(/'/g, '"') ));
    dataLabel = JSON.parse($(this).data('label').replace(/'/g, '"') );
  });


  var mixedChart = new Chart(document.getElementById("chartjsConfigurateurCourbe"), {
    type: 'line',
    data: {
      labels: dataLabel,
      datasets: datasetsTemp,
    },
    options: {
      tooltips:{enabled:false},
      responsive: true,
      hoverMode: 'index',
      stacked: false,
      scales: {
        xAxes: [{
          display: true,
          scaleLabel: {
            display: true,
            labelString: 'Particle Size (µm)'
          }
        }],
        yAxes: [{
          type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
          display: true,
          position: 'right',
          id: 'y-axis-2',
          scaleLabel: {
            display: true,
            labelString: 'Cumulative volume (%)'
          },
          // grid line settings
          gridLines: {
            drawOnChartArea: false, // only want the grid lines for one axis to show up
          },
        }],
      }
    }
  });
}
$("#submitAskExpert").on("click", function(e){
  e.preventDefault();
  if($("#specTxt").val().length > 0){
    $("#subject").val("Question for the TECHNICAL support expert");
    if($("#subjectLactose").length > 0){
      $("#subject").val($("#subjectLactose").val());
    }
    $("#comments").val($("#specTxt").val());
    addClass(document.getElementById('modalAskExpert'), 'openModal');
  	overlay = document.getElementById("overlay");
  	addClass(overlay, 'overlayOpen');
    $("#specTxt").css("borderColor", "#8ca1c2");
  }
  else{
    $("#specTxt").css("borderColor", "red");
  }


})


/*function imageZoom(imgID, resultID) {
  var img, lens, result, cx, cy;
  img = document.getElementById(imgID);
  result = document.getElementById(resultID);

  lens = document.createElement("DIV");
  lens.setAttribute("class", "img-zoom-lens");

  img.parentElement.insertBefore(lens, img);

  cx = result.offsetWidth / lens.offsetWidth;
  cy = 250 / lens.offsetHeight;

  result.style.backgroundImage = "url('" + img.src + "')";
  result.style.backgroundSize = (img.width * cx) + "px " + (img.height * cy) + "px";

  lens.addEventListener("mousemove", moveLens);
  img.addEventListener("mousemove", moveLens);

  lens.addEventListener("touchmove", moveLens);
  img.addEventListener("touchmove", moveLens);
  function moveLens(e) {
    var pos, x, y;

    e.preventDefault();

    pos = getCursorPos(e);

    x = pos.x - (lens.offsetWidth / 2);
    y = pos.y - (lens.offsetHeight / 2);

    if (x > img.width - lens.offsetWidth) {x = img.width - lens.offsetWidth;}
    if (x < 0) {x = 0;}
    if (y > img.height - lens.offsetHeight) {y = img.height - lens.offsetHeight;}
    if (y < 0) {y = 0;}

    lens.style.left = x + "px";
    lens.style.top = y + "px";

    result.style.backgroundPosition = "-" + (x * cx) + "px -" + (y * cy) + "px";
  }
  function getCursorPos(e) {
    var a, x = 0, y = 0;
    e = e || window.event;

    a = img.getBoundingClientRect();

    x = e.pageX - a.left;
    y = e.pageY - a.top;

    x = x - window.pageXOffset;
    y = y - window.pageYOffset;
    return {x : x, y : y};
  }
}*/

$('.imgZoom').on("click", function(){
  openModal('<p id="zoom'+$(this).attr("id")+'"><img src="'+$(this).data('img-full')+'"></p>', $(this).data('title'));
  $('#zoom'+$(this).attr("id")).zoom();
});
function addCart(elem){
  var cart = getCookie("cart");
  var id_lactose = elem.data("lactose-id");
  if(cart.length == 0){
    cart = [new lactose( id_lactose )];
    setCookie("cart", JSON.stringify(cart), 1)
  }
  else{
    cart = JSON.parse(cart);
    insertNewLactose = true;
    for(var i = 0; i < cart.length; i++){// verif si id_lactose présent dans le panier
      if(cart[i].id_lactose == id_lactose){
        insertNewLactose = false;
        break;
      }
    }
    if(insertNewLactose){
      cart.push( new lactose( id_lactose ) ) ;
      setCookie("cart", JSON.stringify(cart), 1)
    }
  }
  $( ".nbCart" ).removeClass('animate');
  xhr = new XMLHttpRequest();
  xhr.open('POST', url.ajaxurl, true);//ok
  xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhr.onload = function() {
    if (xhr.status === 200) {
      $(".nbCart").text("("+cart.length+")");
      $( ".nbCart" ).addClass('animate');
    }
  };
  xhr.send("action=addToCart&cart="+JSON.stringify(cart));
}

var datasetsTemp = [];
window.addEventListener("load", function() {
  if(document.getElementById("chartjsCol")){
    datasetsTemp = datasets;
    var chartjsCol = new Chart(document.getElementById("chartjsCol"),
      {
        "type":"radar",
        "data":
          {"labels": ["Compressibility","Flowability","Cohesivness","Poured density","PSD (d50)","PSD (span)","Specific Surface Area"],
            "datasets": datasetsTemp,
            "borderDash": 5
          },
          "options":{
            "elements":{
              "line":{"tension":0,"borderWidth":3},
            },
            "legend":{
              "position":"bottom"
            },
            "scale": {
               "ticks": {
                 "max":5,
                  "display": false,
                  "beginAtZero":true,
                  "maxTicksLimit": 5,
               }
            },
            tooltips: {
              enabled: false,
              callbacks: {
                label: function(tooltipItem, chart) {
                    return chart.datasets[tooltipItem.datasetIndex].dataLabel[tooltipItem.index];
                }
              }
            }
          }
      });

      $(".compareLactose").on("click", function(){
        datasetsTemp = [];
        var el = document.getElementsByClassName('compareLactose');
        for(var i = 0; i < el.length; i++){
          if(el[i].checked){
              datasetsTemp.push(datasets[i]);
              $("#modal-post-"+el[i].dataset.id).css("display","inline-block");
          }
          else{
            $("#modal-post-"+el[i].dataset.id).css("display","none");
          }
        }
        chartjsCol.data.datasets = datasetsTemp;
        chartjsCol.update();
      });
    }

    $(".cartLink").on("click", function(e){
      e.preventDefault();
      addCart($(this));
    });

    $(".picto-trash").on("click", function(){
      var cart = getCookie("cart");
      var remove_id = $(this).data("value");
      cart = JSON.parse(cart);
      cartTemp = [];
      for(var i = 0; i < cart.length; i++){// verif si id_lactose présent dans le panier
        if(cart[i].id_lactose != remove_id){
          cartTemp.push(cart[i]);

        }
      }
      $("#getMySample").show();
      if(cartTemp.length == 0){
        $("#getMySample").hide();
      }
      cart = cartTemp;
      setCookie("cart", JSON.stringify(cart), 1)
      xhr = new XMLHttpRequest();
      xhr.open('POST', url.ajaxurl, true);
      xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
      xhr.onload = function() {
          if (xhr.status === 200) {
            $(".nbCart").text("("+cart.length+")");
            $("div[data-lactose="+remove_id+"]").remove();
            updateCart();
          }
      };
      xhr.send("action=addToCart&cart="+JSON.stringify(cart));
    });



    if(document.getElementById("chartProduct")){

      var mixedChart = new Chart(document.getElementById("chartProduct"), {
        type: 'bar',
        data: {
          datasets: [{
            label: 'Cumulative volume %',
            data:dataProductCombinate,
            "type":"line",
            "fill":false,
            "borderColor":"#3caf66",
            "yAxisID": "y-axis-2",
          },{
                label: 'Volume %',
                data:dataProductVolume,
                "borderColor":"#272571",
                "backgroundColor":"#378dcc",
                "yAxisID": "y-axis-1",
              }],
          labels: dataLabel
        },
        options: {
					responsive: true,
					hoverMode: 'index',
					stacked: false,
          tooltips: {
            enabled: false
          },
					scales: {
            xAxes: [{
  						display: true,
  						scaleLabel: {
  							display: true,
  							labelString: 'Particle Size (µm)'
  						},ticks: {
                    // Include a dollar sign in the ticks
                    callback: function(value, index, values) {
                        return (Math.round(value * 100) / 100);
                    },
                    
                    autoSkipPadding:10,
                }
  					}],
						yAxes: [{
							type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
							display: true,
							position: 'left',
							id: 'y-axis-1',
              scaleLabel: {
  							display: true,
  							labelString: 'Volume (%)'
  						},
						}, {
							type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
							display: true,
							position: 'right',
							id: 'y-axis-2',
              scaleLabel: {
  							display: true,
  							labelString: 'Cumulative volume (%)'
  						},
							// grid line settings
							gridLines: {
								drawOnChartArea: false, // only want the grid lines for one axis to show up
							},
						}],
					}
				}
      });
    }
});

function openModalChart(){
	addClass(document.getElementById('modal-chart'), 'openModal');
	overlay = document.getElementById("overlay");
	addClass(overlay, 'overlayOpen');
  var chartjsModal = new Chart(document.getElementById("chartjs-modal"),
    {
      "type":"radar",
      "data":
        {"labels": ["Compressibility","Flowability","Cohesivness","Poured density","PSD (d50)","PSD (span)","Specific Surface Area"],
          "datasets": datasetsTemp
        },
        "options":{
          "tooltips":{"enabled":false},
          "elements":{
            "line":{"tension":0,"borderWidth":3},
          },
          "legend":{
            "position":"bottom"
          },
          "scale": {
             "ticks": {
                "max":5,
                "display": false,
                "beginAtZero":true,
                "maxTicksLimit": 5
             }
          }
        }
    });
}

$(".picto-info").on("click", function(e){
  xhrPicto = new XMLHttpRequest();
  xhrPicto.open('POST', url.ajaxurl+"?info", true);//ok
  xhrPicto.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhrPicto.onload = function() {
      if (xhrPicto.status === 200) {
        console.log(xhrPicto.responseText);
        var retour = JSON.parse(xhrPicto.responseText);
        openModal(retour["content"], retour["title"]);
      }
  };
  xhrPicto.send("action=displayInfo&slug="+$(this).data("slug"));
});

$('#returnTop').click(function(){
    $('html, body').animate({scrollTop : 0},800);
    return false;
});

$(window).scroll(function(){
    if ($(this).scrollTop() > 100) {
        $('#returnTop').show();
    } else {
        $('#returnTop').hide();
    }
});


function openModalConnect(idUser){
  console.log(url);
  if (idUser > 0){//connect
    location.href= url.myAccountUrl;
  }
  else{
    addClass(document.getElementById('modalConnect'), 'openModal');
  	overlay = document.getElementById("overlay");
  	addClass(overlay, 'overlayOpen');
  }
}
/*
function openModalConnect(idUser){
  if (idUser > 0){//connect
    location.href= myAccountUrl;
  }
  else{
    addClass(document.getElementById('modalConnect'), 'openModal');
  	overlay = document.getElementById("overlay");
  	addClass(overlay, 'overlayOpen');
  }
}*/

function lactose(id_lactose, quantity, formulation, tablet, specifics){
  this.id_lactose = id_lactose;
  this.quantity = (typeof quantity !== 'undefined') ? quantity : "";
  this.formulation = (typeof formulation !== 'undefined') ? formulation : "";
  this.tablet = (typeof tablet !== 'undefined') ? tablet : "";
  this.specifics = (typeof specifics !== 'undefined') ? specifics : "";
}

function myCart(idUser){
  if (idUser > 0){//connect
    location.href= url.myCartUrl;
  }
  else{
    addClass(document.getElementById('modalConnect'), 'openModal');
  	overlay = document.getElementById("overlay");
  	addClass(overlay, 'overlayOpen');
  }
}


$("#tabCart select:not(.new)").on("change", function(){
  console.log("select");
  updateCart();
});
$("#tabCart input, #tabCart textarea:not(.new)").on("blur", function(){
  updateCart();
});
$(document).ready(function(){
  if($("#tabCart").length){
    updateCart();
  }
});

$(".add .picto-add").on('click', function(){
  if($("#newLactose").val() != ""){
    if($("#newLactose option[value=" + $("#newLactose").val() + "]").attr('disabled') != 'disabled'){
      $("#newLactose").data('lactose-id', $("#newLactose").val());
      console.log($("#newLactose").data('lactose-id')); 
      addCart($("#newLactose"));
      var cart = getCookie("cart");
      cart = JSON.parse(cart);
      for(var i = 0; i < cart.length; i++){
        var id = cart[i].id_lactose;
        if(id == $("#newLactose").val()){
          cart[i].quantity = $("#select_quantity_new").val();
          cart[i].formulation = $("#select_formulation_new").val();
          cart[i].tablet = $("#select_tablet_new").val();
          cart[i].specifics = $("#specifics_new").val();
          console.log(cart);
        }
        else{
          cart[i].quantity = $("#select_quantity_"+id).val();
          cart[i].formulation = $("#select_formulation_"+id).val();
          cart[i].tablet = $("#select_tablet_"+id).val();
          cart[i].specifics = $("#specifics_"+id).val();
        }
        
      }
      
      
      xhr = new XMLHttpRequest();
      xhr.open('POST', url.ajaxurl, true);//ok
      xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
      xhr.onload = function() {
          if (xhr.status === 200) {
            console.log(xhr.responseText);
            location.reload();
          }
      };
      xhr.send("action=addToCart&cart="+JSON.stringify(cart));
      setCookie("cart", JSON.stringify(cart), 1);
    }
  }
   
});


function updateCart(){
  var cart = getCookie("cart");
  cart = JSON.parse(cart);
  $("#newLactose option").removeAttr('disabled');
  for(var i = 0; i < cart.length; i++){
    var id = cart[i].id_lactose;
    cart[i].quantity = $("#select_quantity_"+id).val();
    cart[i].formulation = $("#select_formulation_"+id).val();
    cart[i].tablet = $("#select_tablet_"+id).val();
    cart[i].specifics = $("#specifics_"+id).val();

    $("#newLactose option[value=" + id + "]").attr('disabled','disabled');

  }
  xhr = new XMLHttpRequest();
  xhr.open('POST', url.ajaxurl, true);//ok
  xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhr.onload = function() {
      if (xhr.status === 200) {
        console.log(xhr.responseText);
      }
  };
  xhr.send("action=addToCart&cart="+JSON.stringify(cart));
  setCookie("cart", JSON.stringify(cart), 1);
}

/*function divClicked() {
    elem = $("#adressDisplay>div");
    var divHtml = elem.html().replace(/<br>/g, '\n');
    var editableText = $("<textarea />");
    editableText.val(divHtml);
    elem.replaceWith(editableText);
    editableText.focus();
    // setup the blur event for this new textarea
    editableText.blur(editableTextBlurred);
}*/

/*function editableTextBlurred() {
    var html = $(this).val().replace(/\n/g, '<br>');
    var viewableText = $("<div>");
    viewableText.html(html);
    $(this).replaceWith(viewableText);
    // setup the click event for this new div
    viewableText.click(divClicked);
}*/

$(document).ready(function() {
    //$("#shipmentAdresse").click(divClicked);

    $("#order.button").on("click", function(e){
      e.preventDefault();

      testForm = document.forms[0].reportValidity();
      if(testForm){
        xhr = new XMLHttpRequest();
        xhr.open('POST', url.ajaxurl, true);//ok
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.onload = function() {
            if (xhr.status === 200) {
              console.log(xhr.responseText);
              openModal("Thank you for your order !<br>We will revert to you in the shortest delay.", "Your order");
              setTimeout(function(){ location.href= url.homeUrl; }, 3000);

            }
        };
        xhr.send("action=newOrder&address="+$("#address").val() );
      }
    });
});


$("#menu-account .active").on("click", function(e){
  e.preventDefault();
  $("#menu-account a:not(.active)").toggleClass("show");
});


$(".all_category").on("click", function(){
  cat = $(this).data("category");

  if($(this).prop( "checked")){
    $(".category_"+cat+":not(.hidden)").prop( "checked", true );
  }
  else{
    $(".category_"+cat).prop( "checked", false );
  }
  nbFileSelected();
});

$("#selectLactose").on("change", function(){
  if($(this).val() > 0){
    $('.listing-product input[type="checkbox"]').prop( "checked", false );
    $('.lactose_'+$(this).val()).prop( "checked", true );
    $('.listing-product .p_all_category input[type="checkbox"]').prop( "checked", true );
  } 
  else{
    $('.listing-product input[type="checkbox"]').prop( "checked", true );
    
  }

  $('.listing-product input[type="checkbox"]').each(function(){
    if($(this).prop( "checked") == true){
      $(this).removeClass("hidden");
    }
    else{
      $(this).addClass("hidden");
    }
  });

  if($(".p_all_category")){
    $('#category_other>p').each(function(){
      $(this).prop( "checked", true );
    });
  }

  $('#category_other>p').each(function(){
    if($(this).find(".file").prop( "checked") == false){
      $(this).addClass('cache');
    }
    else if($(this).find(".file").prop( "checked") == true){
      $(this).removeClass('cache');
    }
  });
  nbFileSelected();
});
$(".listing-product .file").on("click", nbFileSelected);
function nbFileSelected(){

  var buttonValHtml = $("#downloadZipFile").html();
  $("#downloadZipFile").attr("disabled", true);
  $("#downloadZipFile").html('<span class="picto picto-download"></span> Please wait');

  var nb = $('.listing-product input[type="checkbox"]:not(.all_category):checked ').length;
  $("#nbFileSelected>span").text(nb);

  files = [];

  $('.listing-product input[type="checkbox"]:not(.all_category):checked ').each(function(){
    files.push($(this).data("file"));
  });
  if(files.length>0){
    xhr = new XMLHttpRequest();
    xhr.open('POST', url.ajaxurl, true);//ok
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.onload = function() {
        if (xhr.status === 200) {
          $("#downloadZipFile").attr("href", xhr.responseText);
          $("#downloadZipFile").attr("disabled", false);
          $("#downloadZipFile").html('<span class="picto picto-download"></span> Download selected items in zip file');
        }
    };
    xhr.send("action=technicalLibraryZipFile&files="+JSON.stringify(files) );
    $("#downloadZipFile").on('click', function(){
      
      var el = document.getElementById('selectLactose');
      var selectLactose = el.options[el.selectedIndex].innerHTML;
      var data = selectLactose+ " : ";
      $('.listing-product input[type="checkbox"]:not(.all_category):checked ').each(function(){
        data += $(this).parent().text().trim() + " - ";
      });
      
      if($("body.page-template-template-quality-documentation").length){
        xhr = new XMLHttpRequest();
        xhr.open('POST', url.ajaxurl, true);//ok
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.send("action=qualityLibraryUserDownload&data="+JSON.stringify(data) );
      }
      else if($("body.page-template-template-technical-library").length){
        xhr = new XMLHttpRequest();
        xhr.open('POST', url.ajaxurl, true);//ok
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.send("action=technicalLibraryUserDownload&data="+JSON.stringify(data) );
      }
      
    });
  }
  else if(files.length == 0){
    $("#downloadZipFile").html('<span class="picto picto-download"></span> Select a technical documentation');
  }
  
}

nbFileSelected();



$(".faq-cat h3").on("click", function(){
  $("#question_" + $(this).data("id") ).toggle("blind");
});
$(".faq-cat .question").on("click", function(){
  $("#answer_" + $(this).data("id") ).toggle("blind");
});

$(".image-category-wrapper").on("click", function(){
  $(".image-category-wrapper.active").removeClass("active");
  $(".image-category.active").removeClass("active");
  $(this).addClass("active");
  $(this).find(".image-category").addClass("active");

  $(".testMethod.active").hide();
  $(".testMethod.active").removeClass("active");
  $("#testMethod_" + $(this).data("term-id") ).show();
  $("#testMethod_" + $(this).data("term-id") ).addClass("active");

});

$(".question").on('click', function(){
  $('#'+$(this).data("reponse") ).toggle("clip", [], 500); 
  $(this).toggleClass("active");
});

$(".post-risk-title").on('click', function(){
  $(this).toggleClass("active");
  $('.'+$(this).data("repeat") ).toggle("clip", [], 500);
});


if (document.getElementById("formCoa")) {
  
  var form = document.getElementById("formCoa");
  form.addEventListener( 'submit', function ( e ) {
    console.log("submit");
    e.preventDefault();
    e.stopPropagation();
    $("#submitCoA").html('Please wait...');
    $("#submitCoA").attr("disabled", true);
    formdata = new FormData( form );
    
    fetch( ihagRest.resturl + 'formCoa', { 
      method: 'POST',
      body: formdata,
      headers: { 'X-WP-Nonce': ihagRest.nonce },
      cache: 'no-cache',
    } )
      .then( function ( response ) {
        response.json().then( function ( data ) {
          if(data.status == '200'){
              document.getElementById('coaAnswer').innerHTML = "<b>Thanks for submitting your request! The batch concerned by your CoA is not yet available; or includes additional mentions or tests compared to our standard specification. Our quality team will provide you with this CoA in the shortest delay!</b>";
              $("#submitCoA").attr("disabled", false);
              $("#submitCoA").html("send");
          }
          if(data.status == '403'){
            $("#submitCoA").attr("disabled", false);
            $("#submitCoA").html("send");
        }
        } );
      } )
      .catch( function ( err ) {
        console.log( 'Fetch Error :-S', err );
      } );
  } );
}




/*******DSIGN LACTOSE**********/

$("#designSelectLactose").on('change', function(){
  var url = $("a#buttonNext").data('url');
  var id = $(this).val();
  console.log(url+'?id='+id); 
  $("a#buttonNext").attr("href", url+'?id='+id);
});

$(document).ready(function(){
  if($(".Pharmacopoeial").length){
    updateSpecification();
  }
})

$('.Pharmacopoeial').on("click", function(){
  updateSpecification();
});
$('.microbial').on("click", function(){
  updateSpecification();
});
$('.microbial_value').on("blur", function(){
  updateSpecification();
});
$('.particule').on("blur", function(){
  updateSpecification();
});
$('.targetSpecification').on("blur", function(){
  updateSpecification();
});

$("#addAnotherTest").on("click", function(){
  bool = true;
  if($("#anotherTestLabel").val() == ""){
    $("#anotherTestLabel").css("borderColor","red");
    bool = false;
  }
  else{
    $("#anotherTestLabel").css("borderColor","#8ca1c2");
  }
  if($("#anotherTestLimits").val() == ""){
    $("#anotherTestLimits").css("borderColor","red");
    bool = false;
  }
  else{
    $("#anotherTestLimits").css("borderColor","#8ca1c2");
  }
  if(bool){
    if(typeof design.anotherTest !== 'undefined'){
      design.anotherTest.push(
        {
          'label':$("#anotherTestLabel").val(),
          'limits':$("#anotherTestLimits").val(),
          'method':$("#anotherTestMethod").val(),
        }
      );
    }
    else{
      design.anotherTest=[
        {
          'label':$("#anotherTestLabel").val(),
          'limits':$("#anotherTestLimits").val(),
          'method':$("#anotherTestMethod").val(),
        }
      ];
    }
    $('#anotherTest tr:first').after('<tr><td>'+$("#anotherTestLabel").val()+'</td><td>'+$("#anotherTestLimits").val()+'</td><td>'+$("#anotherTestMethod").val()+'</td></tr>');
  }
  updateSpecification();
});

$("#fileSpecification").on('change', function() {
  formData = new FormData();
  formData.append("action","file_specification");
  var fileInputElement = document.getElementById("fileSpecification");
  formData.append("file_specification", fileInputElement.files[0]);
  $.ajax({
      type: 'POST',
      url: url.ajaxurl,//ok
      data: formData,
      contentType:false,
      processData: false,
      success: function (data) {
          design.fileSpecification = JSON.parse(data);
          updateSpecification();
      },
  });
});

function updateSpecification(){

  var Pharmacopoeial = [];
  $('.Pharmacopoeial').each(function(){
    if ( $( this ).prop( "checked" ) ){
      Pharmacopoeial.push({'label':$(this).val(), 'standard':$(this).data('standard')});
    }
  });
  design.pharmacopoeial = Pharmacopoeial;

  var microbial = [];
  $('.microbial').each(function(){
    if ( $( this ).prop( "checked" ) ){
      var standard = $(this).data('standard');
      value = $("#microbial_value_" + $(this).data('i') ).val();
      if(standard && value != ""){
        
        valueStandard = $("#microbial_value_" + $(this).data('i') ).data("value-standard");
        var regex = RegExp("^"+valueStandard+"$");
        standard = regex.test(value);
      }
      microbial.push({
        'label':$(this).val(),
        'standard':standard,
        'valueStandard': $("#microbial_value_" + $(this).data('i') ).data("value-standard"),
        'value':$("#microbial_value_" + $(this).data('i') ).val(),
        'unit':$("#microbial_value_" + $(this).data('i') ).data('unit'),
      });
    }
  });
  design.microbial = microbial;

  var particule = [];
  $('.particule').each(function(){
    value = $(this).val();
    valueStandard = $(this).data("value-standard");
    var regex = RegExp("^"+valueStandard+"$");
    standard = regex.test(value);
    particule.push({'label':$(this).data('label'), 'value':$(this).val(), 'standard':standard});
  });
  design.particule = particule;

  design.targetSpecification = sanitizeJSON($(".targetSpecification").val());

  xhr = new XMLHttpRequest();
  xhr.open('POST', url.ajaxurl, true);//ok
  xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhr.onload = function() {
      if (xhr.status === 200) {
        $("#rightCol").html(xhr.responseText);
      }
  };
  xhr.send("action=design&design="+JSON.stringify(design));
}
function sanitizeJSON(unsanitized){
    return unsanitized;//.replace(/"/g,"\\\"").replace(/'/g,"").replace(/\&/g, "");
}
function rightCol(){
  xhr = new XMLHttpRequest();
  xhr.open('POST', url.ajaxurl, true);//ok
  xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhr.onload = function() {
      if (xhr.status === 200) {
        $("#rightCol").html(xhr.responseText);
      }
  };
  xhr.send("action=rightCol");
}

function updatePackaging(){

  design.packaging = {'id':$(".checkboxPackaging:checked").data("id"),'label':$(".checkboxPackaging:checked").val(),'standard':$(".checkboxPackaging:checked").data("is-standard"), 'packagingOptions':[]};
  $("#" + $(".checkboxPackaging:checked").data("bag") + " .option").each(function(){
    if($(this).hasClass("optionCheckbox")){
      if($(this).children(".checkbox").prop("checked")){
        var elem = $(this).children(".checkbox");
        design.packaging.packagingOptions.push({'id':elem.attr("id"),'value':elem.val(),'valueText':"Specifics needs : " + elem.val()});
      }
    }
    if($(this).hasClass("optionRadio")){
      if($(this).children(".radio").prop("checked")){
        var elem = $(this).children(".radio");
        design.packaging.packagingOptions.push({'id':elem.attr("id"),'value':elem.val(),'valueText':"Specifics needs : " + elem.val()});
      }
    }
    else if($(this).hasClass("textarea")){
      if($(this).children(".textarea") != ""){
        var elem = $(this).children(".textarea");
        design.packaging.packagingOptions.push({'id':elem.attr("id"),'value': elem.val(),'valueText':"Specifics needs : " + elem.val()});
      }
    }
    else if($(this).hasClass("range")){
      elem = $(this).children(".slider");
      design.packaging.packagingOptions.push(
        {'id':elem.attr("id"),'value': $(this).children(".slider").slider( "value" ), 'valueText': elem.data('label') + " : " + $(this).children(".slider").slider( "value" )}
      );
    }
  });

  design.targetPackaging = $('#targetPackaging').val();

  xhr = new XMLHttpRequest();
  xhr.open('POST', url.ajaxurl, true);//ok
  xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhr.onload = function() {
      if (xhr.status === 200) {
        $("#rightCol").html(xhr.responseText);
      }
  };
  xhr.send("action=design&design="+JSON.stringify(design));
}


function updatePalet(){
  design.palet = {};
  design.palet.typePalet = [];
  $(".checkboxTypePallet:checked").each(function(){
    design.palet.typePalet.push(
      {'id':$(this).attr("id"),'value':$(this).val(),'valueText':$(this).val()}
    );
  });

  design.palet.adjustPalet = [];
  $("#adjustPalet .slider").each(function(){
    design.palet.adjustPalet.push(
      {'id':$(this).attr("id"),'value': $(this).slider( "value" ), 'valueText': $(this).data('label') + " : " + $(this).slider( "value" )}
    );
  });

  design.targetPalet = $('#targetPalet').val();

  xhr = new XMLHttpRequest();
  xhr.open('POST', url.ajaxurl, true);//ok
  xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhr.onload = function() {
      if (xhr.status === 200) {
        $("#rightCol").html(xhr.responseText);
      }
  };
  xhr.send("action=design&design="+JSON.stringify(design));
}

function updateDelivery(){
  if(typeof design.delivery === 'undefined'){
    design.delivery = {};
  }

  design.delivery.needQuantity = {'id':$("#needQuantity .slider").attr("id"),'value': $("#needQuantity .slider").slider( "value" ), 'valueText': $("#needQuantity .slider").data('label') + " : " + $("#needQuantity .slider").slider( "value" )};
  design.delivery.needText = $("#excpectedDelivery").val();

  xhr = new XMLHttpRequest();
  xhr.open('POST', url.ajaxurl, true);//ok
  xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhr.onload = function() {
      if (xhr.status === 200) {
        $("#rightCol").html(xhr.responseText);
      }
  };
  xhr.send("action=design&design="+JSON.stringify(design));
}

$("#excpectedDelivery").on("blur",updateDelivery);

$(".checkboxTypePallet").on("click",updatePalet);
$("#targetPalet").on("blur",updatePalet);

$("#targetPackaging").on("blur",updatePackaging);


$(".packaging .textarea>.textarea").on('blur',function(){
  $(".checkboxPackaging").prop("checked",false);
  $("#" + $(this).data("bag")).prop("checked",true);
  updatePackaging();
});
$(".packaging .checkbox").on('click',function(){
  //$(".packaging .checkbox").prop("checked",false);
  //$(this).prop("checked",true);
  $(".checkboxPackaging").prop("checked",false);
  $("#" + $(this).data("bag")).prop("checked",true);
  updatePackaging();
  $(".checkboxPackaging").each(function(){
    if($(this).prop("checked") == false){
      if($("#"+$(this).data("bag")+"_0").length){
        $("#"+$(this).data("bag")+"_0").prop("checked",false);
      }
      if($("#"+$(this).data("bag")+"_1").length){
        $("#"+$(this).data("bag")+"_1").prop("checked",false);
      }
      if($("#"+$(this).data("bag")+"_2").length){
        $("#"+$(this).data("bag")+"_2").prop("checked",false);
      }
      if($("#"+$(this).data("bag")+"_3").length){
        $("#"+$(this).data("bag")+"_3").prop("checked",false);
      }
    }
  });
});

$(".packaging .radio").on('click',function(){
  $(".checkboxPackaging").prop("checked",false);
  $("#" + $(this).data("bag")).prop("checked",true);
  updatePackaging();
});




$(document).ready(function(){
  $(".packaging .slider").each(function(){
    $( this ).slider({
      //range: true,
      min: $( this ).data("min"),
      max: $( this ).data("max"),
      value: $( this ).data("value"),
      step: $( this ).data("step"),
      create: function() {
        $( this ).find(".custom-handle>span" ).text( $( this ).slider( "value" ));
      },
      slide: function( event, ui ) {
        $( this ).find(".custom-handle>span" ).text( ui.value );
      },
      change : function(){
        $(".checkboxPackaging").prop("checked",false);
        $("#" + $(this).data("bag")).prop("checked",true);
        updatePackaging();
      },
    });
  });

  $("#palletization .slider").each(function(){
    $( this ).slider({
      //range: true,
      min: $( this ).data("min"),
      max: $( this ).data("max"),
      value: $( this ).data("value"),
      step: $( this ).data("step"),
      create: function() {
        $( this ).find(".custom-handle>span" ).text( $( this ).slider( "value" ));
      },
      slide: function( event, ui ) {
        $( this ).find(".custom-handle>span" ).text( ui.value );
      },
      change : function(){
        //$(".checkboxPackaging").prop("checked",false);
        //$("#" + $(this).data("bag")).prop("checked",true);
        updatePalet();
      },
    });
  });

  $("#needQuantitySlider").each(function(){
    $( this ).slider({
      //range: true,
      min: $( this ).data("min"),
      max: $( this ).data("max"),
      value: $( this ).data("value"),
      step: $( this ).data("step"),
      create: function() {
        $( this ).find(".custom-handle>span" ).text( $( this ).slider( "value" ));
      },
      slide: function( event, ui ) {
        $( this ).find(".custom-handle>span" ).text( ui.value );
      },
      change : function(){
        updateDelivery();
      },
    });
  });

  if($(".page-template-template-design-packaging").length){
    $(".checkboxPackaging").on("change", function(){
      $(".checkboxPackaging").prop("checked",false);
      $(this).prop("checked",true);
      updatePackaging();
    });
    updatePackaging();
  }

  if($(".page-template-template-design-palletization").length){
    updatePalet();
  }

  if($(".page-template-template-design-delivery").length){
    updateDelivery();
  }

  if($(".page-template-template-design-congratulation").length){
    rightCol();
  }
});


$("#filePackaging").on('change', function() {
  formData = new FormData();
  formData.append("action","filePackaging");
  var fileInputElement = document.getElementById("filePackaging");
  formData.append("filePackaging", fileInputElement.files[0]);
  $.ajax({
      type: 'POST',
      url: url.ajaxurl,//ok
      data: formData,
      contentType:false,
      processData: false,
      success: function (data) {
        console.log(JSON.parse(data));
          design.filePackaging = JSON.parse(data);
          updatePackaging();
      },
  });
});


$("#filePalet").on('change', function() {
  formData = new FormData();
  formData.append("action","filePalet");
  var fileInputElement = document.getElementById("filePalet");
  formData.append("filePalet", fileInputElement.files[0]);
  $.ajax({
      type: 'POST',
      url: url.ajaxurl,//ok
      data: formData,
      contentType:false,
      processData: false,
      success: function (data) {
        console.log(JSON.parse(data));
          design.filePalet = JSON.parse(data);
          updatePalet();
      },
  });
});

$("#nextDelivery").on("click",function(e){

  testForm = document.forms[0].reportValidity();
  if(testForm){
    if(typeof design.delivery === 'undefined'){
      design.delivery = {};
    }
    design.delivery.company = $("#delivery_company").val();
    design.delivery.address = $("#delivery_address").val();
    design.delivery.zipCode = $("#delivery_zipCode").val();
    design.delivery.city = $("#delivery_city").val();
    design.delivery.stateProvince = $("#delivery_stateProvince").val();
    design.delivery.country = $("#delivery_country").val();
    updateDelivery();
    location.href=$(this).data("href");
  }
});

function openDistributor(id){
  xhr = new XMLHttpRequest();
  xhr.open('POST', url.ajaxurl, true);//ok
  xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhr.onload = function() {
      if (xhr.status === 200) {
        openModal(xhr.responseText,"Distributor Information");
        $("#contactDistributorModal").on("click", function(){
          closeModal();
          $("#subject").val("Question from Armor Pharma website ("+$(this).data('name')+")");
          addClass(document.getElementById('modalAskExpert'), 'openModal');
        	overlay = document.getElementById("overlay");
          $("#distributorId").val(id);
        	addClass(overlay, 'overlayOpen');
          console.log($("#distributorId").val());
        });
      }
  };
  xhr.send("action=modalDistributor&distributor="+id);
}
$('.openDistributor').on("click", function(){
  openDistributor($(this).data("id"));
});
styles=[
    {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#e9e9e9"
            },
            {
                "lightness": 17
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#f5f5f5"
            },
            {
                "lightness": 20
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "lightness": 17
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "lightness": 29
            },
            {
                "weight": 0.2
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "lightness": 18
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "lightness": 16
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#f5f5f5"
            },
            {
                "lightness": 21
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#dedede"
            },
            {
                "lightness": 21
            }
        ]
    },
    {
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#ffffff"
            },
            {
                "lightness": 16
            }
        ]
    },
    {
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "saturation": 36
            },
            {
                "color": "#333333"
            },
            {
                "lightness": 40
            }
        ]
    },
    {
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#f2f2f2"
            },
            {
                "lightness": 19
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#fefefe"
            },
            {
                "lightness": 20
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#fefefe"
            },
            {
                "lightness": 17
            },
            {
                "weight": 1.2
            }
        ]
    }
];
/*function new_map( $el ) {
	var $markers = $el.find('.marker');
	var args = {
		zoom		: 16,
    styles 		: styles,
		center		: new google.maps.LatLng(0, 0),
		mapTypeId	: google.maps.MapTypeId.ROADMAP
	};
	var map = new google.maps.Map( $el[0], args);
	map.markers = [];
	$markers.each(function(){
  	add_marker( $(this), map );
	});
	center_map( map );
	return map;
}

function add_marker( $marker, map ) {
	var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );
	var marker = new google.maps.Marker({
		position	: latlng,
		map			: map,
    icon: {
            url: pictoMap,
            size: new google.maps.Size(23, 47),
            origin: new google.maps.Point(0,0),
            anchor: new google.maps.Point(11, 47)
        },
	});
	map.markers.push( marker );
	if( $marker.html() )
	{
		var infowindow = new google.maps.InfoWindow({
			content		: $marker.html()
		});
		google.maps.event.addListener(marker, 'click', function() {
			infowindow.open( map, marker );
		});
	}
}

function center_map( map ) {
	var bounds = new google.maps.LatLngBounds();
	$.each( map.markers, function( i, marker ){
		var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );
		bounds.extend( latlng );
	});
	if( map.markers.length == 1 )
	{
	    map.setCenter( bounds.getCenter() );
	    map.setZoom( 16 );
	}
	else
	{
		map.fitBounds( bounds );
	}
}

var map = null;
$(document).ready(function(){
	$('.acf-map').each(function(){
		map = new_map( $(this) );
	});
});
*/
$(".askContact").on("click", function(){
  $("#subject").val("ARMOR PHARMA, get in touch ("+$(this).text()+")");
  $("#teamEmail").val($(this).data("email"));
  addClass(document.getElementById('modalAskExpert'), 'openModal');
  overlay = document.getElementById("overlay");
  $("#distributorId").val($(this).data("id"));
  addClass(overlay, 'overlayOpen');
});

function askContactExcipure(email, text){
  $("#subject").val("ARMOR PHARMA, get in touch ("+ text +")");
  $("#teamEmail").val(email);
  addClass(document.getElementById('modalAskExpert'), 'openModal');
  overlay = document.getElementById("overlay");
  //$("#distributorId").val($(this).data("id"));
  addClass(overlay, 'overlayOpen');
}

$(document).ready(function(){

  $("#cn-accept-cookie").removeClass("cn-set-cookie cn-button bootstrap");
  $("#cn-accept-cookie").addClass("small");

   for(var i=0; i < document.forms.length; i++){
      if(typeof document.forms[i].elements.check !== 'undefined'){
        document.forms[i].elements.check.checked = false;
        console.log("no-check");
      }
   }

  $("#buttonNewsletter").on("click", function(e){
    e.preventDefault();
    var form = document.forms.namedItem("newsletterInter");
    testForm = form.reportValidity();
    if(testForm){
      addClass(document.getElementById('modal-newsletter'), 'openModal');
    	overlay = document.getElementById("overlay");
    	addClass(overlay, 'overlayOpen');
      $("#emailNewsletterModal").val($("#emailNewsletter").val());
      console.log($("#emailNewsletter").val());
    }
  });

  $("#inscriptionNewsletter").on("submit", function(e){
    e.preventDefault();
    var form = document.forms.inscriptionNewsletter;
      var formData = new FormData(form);
      xhr = new XMLHttpRequest();
      xhr.open('POST', url.ajaxurl, true);//ok
      xhr.onload = function() {
        if (xhr.status === 200) {
          console.log(xhr.responseText);
          closeModal();
          openModal("Thank you for your registration","Newsletters");
        }
      };
      xhr.send(formData);
  });

  $('input[type="tel"]').each(function(){
    $(this).intlTelInput({preferredCountries:["fr"], nationalMode:false});
    console.log("phone");
  });


  $("#voteYes").on("click",function(){
    openModal("Thank you for your vote, we are glad you liked our customization tool","Did you like it ?");
  });

  $("#voteNo").on("click",function(){
    openModal("Do not hesitate to give us your feedback and help us enhancing the user experience","Did you like it ?");
  });


});
