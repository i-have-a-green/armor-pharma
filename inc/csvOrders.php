<?php
class wpgreen_CSVOrders
{
	/**
	* Constructor
	*/
	public function __construct()
    {
        if(isset($_GET['report']) && $_GET['report'] == 'wpgreen_CSVOrders')
        {
        	$this->wpgreen_CSVOrders();
        }
    }
	public function wpgreen_CSVOrders(){
		global $wpdb;
        $csv_fields=array();
        $csv_fields[] = 'Date';
        $csv_fields[] = 'Role';
        $csv_fields[] = 'Pays';
        $csv_fields[] = 'Prénom';
        $csv_fields[] = 'Nom';
        $csv_fields[] = 'Société';
        $csv_fields[] = 'Mail';
        $csv_fields[] = 'Profil';
        $csv_fields[] = 'Téléphone';

        $csv_fields[] = 'product';
        $csv_fields[] = 'Qte';
        
        $output_filename = "orders_".date("Y-m-d H:i:s").'.csv';
        $output_handle = @fopen( 'php://output', 'w' );
        header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
        header( 'Content-Description: File Transfer' );
        header( 'Content-type: text/csv' );
        header( 'Content-Disposition: attachment; filename=' . $output_filename );
        header( 'Expires: 0' );
        header( 'Pragma: public' );
        // Insert header row
        fputcsv( $output_handle, $csv_fields,";" );
		
		$user_query = new WP_User_Query(array('fields' => 'all_with_meta'));

		if ( ! empty( $user_query->get_results() ) ) {
            foreach ( $user_query->get_results() as $user ) {
                $orders = get_user_meta($user->ID, 'orders', true);
                $orders = json_decode($orders);
                if($orders){
                    $orders = array_reverse($orders);
                    foreach ($orders as $order){
                        if($order->cart){
                            foreach ($order->cart as $lactose){
                                $tab_data = array( 
                                    $order->date,
                                    $user->roles[0],
                                    $user->country, 
                                    $user->first_name,
                                    $user->last_name,
                                    $user->company,
                                    $user->user_email,
                                    $user->profil,
                                    $user->phone,
                                    get_the_title($lactose->id_lactose),
                                    $lactose->quantity
                                );
                                fputcsv( $output_handle, $tab_data,";" );
                            }
                        }
                    }
                }
            }
        }
        fclose( $output_handle );
		exit();
	}
}
// Instantiate a singleton of this plugin
new wpgreen_CSVOrders();
