<?php

/**
 * Ajouter le nouveau type d’utilisateurs
 *
 * @see https://codex.wordpress.org/Function_Reference/register_activation_hook
 */
register_activation_hook( __FILE__, 'wpgreen_register_role_on_plugin_activation' );
function wpgreen_register_role_on_plugin_activation() {
    add_role( 'armorPharma', __( 'Membre Armor Pharma', 'armor-pharma' ), array( 'read' => true, 'level_0' => true ) );
    add_role( 'distributor', __( 'Distributeur Armor Pharma', 'armor-pharma' ), array( 'read' => true, 'level_0' => true ) );
}

/**
 * Ajout d’options de lecture pour la page de profil
 *
 * @see https://developer.wordpress.org/plugins/settings/settings-api/
 */
add_action( 'admin_init', 'wpgreen_settings_api_init' );
function wpgreen_settings_api_init() {
	register_setting( 'reading', 'profil-page' );

	add_settings_field(
		'profil-page',
		__( 'Page de profil', 'armor-pharma' ),
		'wp_dropdown_pages',
		'reading',
		'default',
		array(
			'selected' => get_option( 'profil-page' ),
			'name'     => 'profil-page',
		)
	);
}

/**
 * Formulaire de connexion
 *
 * @see https://developer.wordpress.org/reference/functions/wp_login_form/
 */
function wpgreen_login() {
    $out = array( '<div class="w_demo-login-form">' );
    $out[] = '<h2>' . __( 'Espace réservé aux membres adhérents Fivape. Connectez-vous ! ', 'fivape' ) . '</h2>';
    $out[] = wp_login_form( array(
        'echo'     => false,
        'remember' => false,
        //'redirect' => get_permalink( get_option( 'profil-page' ) ),
		'redirect' => get_the_permalink(),
		'label_username' => 'Adresse Email',
    ) );
    $out[] = '</div>';
    return implode( PHP_EOL, $out );
}

/**
 * Fonction utile pour générer tout types de champs
 */
function wpgreen_field( $args ) {
    if ( empty( $args['type'] ) ) {
        $args['type'] = 'text';
    }
    switch ( $args['type'] ) {
        case 'textarea':
            return vsprintf( '<p id="elem_%2$s"><label for="%1$s">%4$s%6$s</label><textarea name="%2$s" id="%1$s" %5$s>%3$s</textarea></p>', array(
                isset( $args['id'] ) ? esc_attr( $args['id'] ) : esc_attr( $args['name'] ),
                esc_attr( $args['name'] ),
                esc_attr( $args['value'] ),
                isset( $args['label'] ) ? esc_html( $args['label'] ) : '',
                isset( $args['required'] ) && $args['required'] ? 'required' : '',
                isset( $args['required'] ) && $args['required'] ? ' <span class="required-field">*</span>' : '',
            ) );
            break;
        case 'radio':
            return vsprintf( '<p class="radio" id="elem_civilite"><label for="Mr">Mr</label><input type="radio" id="Mr" name="civilite" value="Mr" %1$s /> <label for="Mrs">Mrs</label><input type="radio" id="Mrs" name="civilite" value="Mrs" %2$s /></p>', array(
              ($args['value']=="Mr")?"checked":"",
              ($args['value']=="Mrs")?"checked":"",
            ) );
            break;
        case 'select':
            return '<p><label for="profil_register">'.__("Profile","armor-pharma").'</label><select name="fonction" id="profil_register" required>
            <option>
              Responsible for excipients purchases
            </option>
            <option>
              Working on tablets/sachets/capsules formulation
            </option>
            <option>
              Working on Dry Powder Inhalers development
            </option>
            <option>
              Student in pharmaceutical formulation
            </option>
            <option>
              Professor/associate professor at University
            </option>
            <option >
              PhD student
            </option>
            <option >
              other
            </option>
            </select></p>';
        default:
            return vsprintf( '<p id="elem_%2$s"><label for="%1$s">%5$s%7$s</label><input type="%3$s" name="%2$s" id="%1$s" value="%4$s" %6$s %8$s  %9$s></p>', array(
                isset( $args['id'] ) ? esc_attr( $args['id'] ) : esc_attr( $args['name'] ),
                ! empty( $args['name'] ) ? esc_attr( $args['name'] ) : '',
                isset( $args['type'] ) ? esc_attr( $args['type'] ) : 'text',
                ! empty( $args['value'] ) ? esc_attr( $args['value'] ) : '',
                isset( $args['label'] ) ? esc_html( $args['label'] ) : '',
                isset( $args['required'] ) && $args['required'] ? 'required' : '',
                isset( $args['required'] ) && $args['required'] ? ' <span class="required-field">*</span>' : '',
                ! empty( $args['pattern'] ) ? esc_attr( 'pattern='.$args['pattern'] ) : '',
                isset( $args['placeholder'] ) ? 'placeholder="'.$args['placeholder'].'"' : '',
            ) );
    }
}

/**
 * Formulaire qui sert  à la création d’un utilisateur
 */
function wpgreen_form_user( $exist = false ) {

    $user = $exist ? wp_get_current_user() : false; // ais-je besoin d'infos existantes ?
    $action = $exist ? 'edit' : 'create'; // création ou update ?

    $form_wrapper = '<form method="post" action="" class="form" id="form-profil">%s</form>';
	// Cette entrée « action » permet de router admin-post.php vers une fonction précise
    $out = array( '<input type="hidden" name="action" value="' . $action . '-customuser">' );
    $out[] = '<input type="hidden" name="honey" value="">';
	// On créer un nonce qui nous permetrta de vérifier que l’utilisateur est bien à l’origine de l’action
    $out[] = wp_nonce_field( "create-user", "create-user");

    if(isset($_GET['id']) && !empty($_GET['id'])){
      $out[] = wpgreen_field( array( // Nom
          'name'     => 'id',
          'value'    => $_GET['id'],
          'type'     => 'hidden',
      ) );
    }

    $out[] = wpgreen_field( array( // Nom
        'name'     => 'civilite',
        'value'    => $exist ? $user->civilite : '',
        'label'    => "",
        'required' => true,
        'type'     => 'radio',
    ) );

    $out[] = wpgreen_field( array(
        'name'     => 'user_firstname',
        'value'    => $exist ? $user->user_firstname : '',
        'label'    => __( 'First name', 'armor-pharma' ),
        'required' => true,
    ) );

    $out[] = wpgreen_field( array( // Nom
        'name'     => 'user_lastname',
        'value'    => $exist ? $user->user_lastname : '',
        'label'    => __( 'Last name', 'armor-pharma' ),
        'required' => true,
    ) );

	$out[] = wpgreen_field( array( // Nom
        'name'     => 'fonction',
        'value'    => $exist ? $user->fonction : '',
        'label'    => __( 'Profile', 'armor-pharma' ),
        'required' => true,
        'type'     => 'select'
    ) );

	  $out[] = wpgreen_field( array( // Email
        'name'     => 'user_email',
        'value'    => $exist ? $user->user_email : '',
        'type'     => 'email',
        'label'    => __( 'Email', 'armor-pharma' ),
        'required' => true,
    ) );

    $out[] = wpgreen_field( array( // Nom
          'name'     => 'company',
          'value'    => $exist ? $user->company : '',
          'label'    => __( 'Company', 'armor-pharma' ),
          'required' => true,
      ) );

      $out[] = wpgreen_field( array( // Email
          'name'     => 'phone',
          'value'    => $exist ? $user->user_email : '',
          'type'     => 'tel',
          'placeholder'=> '+33&nbsp;0&nbsp;00&nbsp;00&nbsp;00&nbsp;00',
          'label'    => __( 'Phone', 'armor-pharma' ),
          //'pattern'  => "[0-9]{3}-[0-9]{3}-[0-9]{4}",
      ) );

    $out[] = wpgreen_field( array( // Nom
          'name'     => 'country',
          'value'    => $exist ? $user->country : '',
          'label'    => __( 'Country', 'armor-pharma' ),
          'required' => true,
      ) );

    $out[] = wpgreen_field( array( // Mot de passe
        'name'     => 'user_pass',
        'type'     => 'password',
        'label'    => __( 'Password', 'armor-pharma' ),
        'required' => $exist ? false : true,
        'pattern'  => '(?=^.{12,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$',
        'placeholder' => '12 char. - Upper case - Numb.  - !@?;-'
    ) );

    $out[] = '<p class="width100"><hr /></p><p class="width100">'.__("Let's keep in touch ! Please confirm you are happy to receive news about","armor-pharma").'</p>';

    $out[] = '
    <p class="width100 newsletter">
    <input type="checkbox" id="newsletter2" class="checkbox" name="newsletter[]" value="2"/>
      <label for="newsletter2">New product &amp; technical documentation</label>
    <input type="checkbox" id="newsletter3" class="checkbox" name="newsletter[]" value="3"/>
      <label for="newsletter3">Quality &amp; regulatory updates</label>
    <input type="checkbox" id="newsletter4" class="checkbox" name="newsletter[]" value="4"/>
      <label for="newsletter4">Coming events</label>
    </p>';
    
    $p = get_page_by_path( "privacy-policy-cookie-policy" );
    $legal = get_page_by_path( "terms-conditions" );
    $out[] = '<p class="width100"><input type="checkbox" class="checkbox" id="checkForm" name="checkForm" checked="false" required /> 
        <label for="checkForm">Armor Protéines as data controller implements processing of personal data for the purpose of creating and managing your user account. For more information about your rights and the <a href="'.get_the_permalink( $p).'">protection of your data</a><br>
        By clicking on « Create an account », you confirm that you have read and understood the conditions of use of the site specified in our <a href="'.get_the_permalink( $legal).'">legal notices</a>, the <a href="'.get_the_permalink( $p).'">data protection policy</a> and <a href="'.get_privacy_policy_url().'">Cookies Policy</a>."</p></label>';

    $out[] = '<p style="text-align:center" class="width100"><button type="submit" name="submit-create-user" class="button">' . __( 'Create an account', 'fivape' ) . '</button></p>';

    return sprintf( $form_wrapper, implode( PHP_EOL, $out ) );
}

/**
 * Formulaire qui sert à l’update d’un utilisateur
 */
function wpgreen_form_user_update( $exist = true ) {

    $user = $exist ? wp_get_current_user() : false; // ais-je besoin d'infos existantes ?
    $action = $exist ? 'edit' : 'create'; // création ou update ?

    $form_wrapper = '<form method="post" action="" class="form" id="form-profil">%s</form>';
	// Cette entrée « action » permet de router admin-post.php vers une fonction précise
    $out = array( '<input type="hidden" name="action" value="' . $action . '-customuser">' );
    $out[] = '<input type="hidden" name="honey" value="">';
	// On créer un nonce qui nous permetrta de vérifier que l’utilisateur est bien à l’origine de l’action
    $out[] = wp_nonce_field( "update-user", "update-user");

    $out[] = wpgreen_field( array( // Nom
        'name'     => 'civilite',
        'value'    => $exist ? $user->civilite : '',
        'label'    => "",
        'required' => true,
        'type'     => 'radio',
    ) );

    $out[] = wpgreen_field( array(
        'name'     => 'user_firstname',
        'value'    => $exist ? $user->user_firstname : '',
        'label'    => __( 'First name', 'armor-pharma' ),
        'required' => true,
    ) );

    $out[] = wpgreen_field( array( // Nom
        'name'     => 'user_lastname',
        'value'    => $exist ? $user->user_lastname : '',
        'label'    => __( 'Last name', 'armor-pharma' ),
        'required' => true,
    ) );


	  $out[] = wpgreen_field( array( // Email
        'name'     => 'user_email',
        'value'    => $exist ? $user->user_email : '',
        'type'     => 'email',
        'label'    => __( 'Email', 'armor-pharma' ),
        'required' => true,
    ) );

    $out[] = '<p style="text-align:right" class="width100"><button type="submit" name="submit-update-user" class="button"><span class="picto picto-check"></span>' . __( 'Save changes', 'fivape' ) . '</button></p>';

    return sprintf( $form_wrapper, implode( PHP_EOL, $out ) );
}

/**
 * Formulaire qui sert à l’update du password utilisateur
 */
function wpgreen_form_user_update_password( $exist = true ) {

    $user = $exist ? wp_get_current_user() : false; // ais-je besoin d'infos existantes ?
    $action = $exist ? 'edit' : 'create'; // création ou update ?

    $form_wrapper = '<form method="post" action="" class="form" id="form-profil">%s</form>';
	// Cette entrée « action » permet de router admin-post.php vers une fonction précise
    $out = array( '<input type="hidden" name="action" value="' . $action . '-customuserpassword">' );
    $out[] = '<input type="hidden" name="honey" value="">';
	// On créer un nonce qui nous permetrta de vérifier que l’utilisateur est bien à l’origine de l’action
    $out[] = wp_nonce_field( "user-password", "user-password" );


    $out[] = wpgreen_field( array( // Mot de passe
        'name'     => 'user_pass',
        'type'     => 'password',
        'label'    => __( 'Password', 'armor-pharma' ),
        'required' => $exist ? false : true,
        'pattern'  => '(?=^.{12,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$',
        'placeholder' => '12 char. - Upper case - Numb.  - !@?;-'
    ) );

    $out[] = '<ul>
    <li>
    12 characters minimum
    </li>
    <li>
    1 number required
    </li>
    <li>
    1 character required (ex: !,&,%...)
    </li>
    </ul>';

    $out[] = '<p style="text-align:right" class="width100"><button type="submit" name="submit-user-password" class="button"><span class="picto picto-check"></span>' . __( 'Save changes', 'fivape' ) . '</button></p>';

    return sprintf( $form_wrapper, implode( PHP_EOL, $out ) );
}

/**
 * Met à jour un utilisateur
 *
 * @see https://codex.wordpress.org/Plugin_API/Action_Reference/admin_post_(action)
 * @see https://codex.wordpress.org/Function_Reference/check_admin_referer
 * @see https://codex.wordpress.org/Validating_Sanitizing_and_Escaping_User_Data
 * @see https://codex.wordpress.org/Function_Reference/wp_update_user
 */
add_action( 'init', 'wpgreen_update_user' );
function wpgreen_update_user() {

    if(isset($_POST['honey']) && !empty($_POST['honey'])){
        return false;
    }
    if (isset($_POST['submit-update-user']) && wp_verify_nonce($_POST['update-user'], 'update-user')) {

        $nom = implode( ' ', array_filter( array(
            ucfirst( sanitize_text_field( $_POST['user_firstname'] ) ),
            ! empty( $_POST['user_lastname'] ) ? ucfirst( sanitize_text_field( $_POST['user_lastname'] ) ) : false,
        ) ) );

        // Mise à jour des champs du membre
        $infos = array(
            'ID'           => get_current_user_id(),
            'first_name'   => sanitize_text_field( $_POST['user_firstname'] ),
            'last_name'    => sanitize_text_field( $_POST['user_lastname'] ),
            'user_email'   => sanitize_text_field($_POST['user_email']),
            'display_name' => $nom,
        );
        $id = wp_update_user( $infos );

        if ( is_wp_error( $id ) ) {
            return false;
        }

        // Mise à jour des metas
        update_user_meta( $id, 'civilite', sanitize_text_field( $_POST['civilite'] ));

        wp_redirect($_SERVER['HTTP_REFERER']);
        exit;
    }

    return false;
}

/**
 * Met à jour un utilisateur
 *
 * @see https://codex.wordpress.org/Plugin_API/Action_Reference/admin_post_(action)
 * @see https://codex.wordpress.org/Function_Reference/check_admin_referer
 * @see https://codex.wordpress.org/Validating_Sanitizing_and_Escaping_User_Data
 * @see https://codex.wordpress.org/Function_Reference/wp_update_user
 */
add_action( 'init', 'wpgreen_update_userPassword' );
function wpgreen_update_userPassword() {
    if(isset($_POST['honey']) && !empty($_POST['honey'])){
        return false;
    }

    if (isset($_POST['submit-user-password']) && wp_verify_nonce($_POST['user-password'], 'user-password' )) {

        if ( ! empty( $_POST['user_pass'] ) ) {
            wp_set_password( $_POST['user_pass'], get_current_user_id() );
            $user = new WP_User( get_current_user_id() );
            wp_set_auth_cookie( $user->ID );
            wp_set_current_user( $user->ID );
            do_action( 'wp_login', $user->user_login, $user );

            wp_redirect($_SERVER['HTTP_REFERER'].'?change');
            exit;
        }
        
    }
    return false;
}

/**
 * Créer un nouvel utilisateur
 *
 * @see https://developer.wordpress.org/reference/hooks/admin_post_nopriv_action/
 * @see https://codex.wordpress.org/Function_Reference/check_admin_referer
 * @see https://codex.wordpress.org/Validating_Sanitizing_and_Escaping_User_Data
 * @see https://codex.wordpress.org/Function_Reference/wp_insert_user
 * @see https://codex.wordpress.org/Function_Reference/wp_signon
 */
add_action( 'init', 'wpgreen_create_user' );
function wpgreen_create_user( $args, $update = false ) {

    if (isset($_POST['submit-create-user']) && wp_verify_nonce($_POST['create-user'], 'create-user' )) {
        
        if(isset($_POST['honey']) && !empty($_POST['honey'])){
            return false;
        }
        // Je créer son display_name
        $nom = implode( ' ', array_filter( array(
            ucfirst( sanitize_text_field( $_POST['user_firstname'] ) ),
            ! empty( $_POST['user_lastname'] ) ? ucfirst( sanitize_text_field( $_POST['user_lastname'] ) ) : false,
        ) ) );

        // Création de l’utilisateur
        $infos = array(
            'first_name'   => sanitize_text_field( $_POST['user_firstname'] ),
            'last_name'    => sanitize_text_field( $_POST['user_lastname'] ),
            'user_login'   => sanitize_email($_POST['user_email']), // Le login est son email :-)
            'user_email'   => sanitize_email($_POST['user_email']),
            'user_pass'    => $_POST['user_pass'],
            'role'         => 'armorPharma',
            'display_name' => $nom,
        );
        $id = wp_insert_user( $infos );
        if ( is_wp_error( $id ) ) {
            wp_redirect( home_url() , 301 );
            return;
        }

        // Je log automatiquement l’utilisateur
        $creds = array();
        $creds['user_login']    = sanitize_email($_POST['user_email']);
        $creds['user_password'] = sanitize_text_field($_POST['user_pass']);
        $creds['remember']      = true;
        $user = wp_signon( $creds, false );

        // Mise à jour des metas
        update_user_meta( $id, 'civilite', sanitize_text_field( (isset($_POST['civilite']) )?$_POST['civilite']:'Mr' ));
        update_user_meta( $id, 'fonction', sanitize_text_field( $_POST['fonction'] ));
        update_user_meta( $id, 'phone', sanitize_text_field( $_POST['phone'] ));
        update_user_meta( $id, 'country', sanitize_text_field( $_POST['country'] ));
        update_user_meta( $id, 'company', sanitize_text_field( $_POST['company'] ));


        wp_redirect( home_url() , 301 );
        exit;
        
    }
    
}

/**
 * Ne pas afficher l’admin bar pour les simples membres
 *
 * @see https://codex.wordpress.org/Function_Reference/show_admin_bar
 */
add_filter( 'show_admin_bar', 'wpgreen_show_admin_bar' );
function wpgreen_show_admin_bar( $show ) {
	if ( current_user_can( 'armorPharma' ) ) {
		$show = false;
	}
	return $show;
}
