<?php
class wpgreen_CSVDesignAbandon
{
	/**
	* Constructor
	*/
	public function __construct()
    {
        if(isset($_GET['report']) && $_GET['report'] == 'wpgreen_CSVDesignAbandon')
        {
        	$this->wpgreen_CSVDesignAbandon();
        }
    }
	public function wpgreen_CSVDesignAbandon(){
		global $wpdb;
        $csv_fields=array();
        $csv_fields[] = 'Role';
        $csv_fields[] = 'Pays';
        $csv_fields[] = 'Prénom';
        $csv_fields[] = 'Nom';
        $csv_fields[] = 'Société';
        $csv_fields[] = 'Mail';
        $csv_fields[] = 'Profil';
        $csv_fields[] = 'Téléphone';

        $csv_fields[] = 'Customization';
        
        $output_filename = "designAbandon_".date("Y-m-d H:i:s").'.csv';
        $output_handle = @fopen( 'php://output', 'w' );
        header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
        header( 'Content-Description: File Transfer' );
        header( 'Content-type: text/csv' );
        header( 'Content-Disposition: attachment; filename=' . $output_filename );
        header( 'Expires: 0' );
        header( 'Pragma: public' );
        // Insert header row
        fputcsv( $output_handle, $csv_fields,";" );
		
		$user_query = new WP_User_Query(array('fields' => 'all_with_meta'));

		if ( ! empty( $user_query->get_results() ) ) {
            foreach ( $user_query->get_results() as $user ) {
                $design = getDesignAbandon($user->ID);
                if($design !== false){
                    $tab_data = array( 
                        $user->roles[0],
                        $user->country, 
                        $user->first_name,
                        $user->last_name,
                        $user->company,
                        $user->user_email,
                        $user->profil,
                        $user->phone,
                        $design    
                    );
                    fputcsv( $output_handle, $tab_data,";" );
                }
            }
        }
        fclose( $output_handle );
		exit();
	}
}
// Instantiate a singleton of this plugin
new wpgreen_CSVDesignAbandon();


function getDesignAbandon($userId){
    $design = get_user_meta($userId, 'design', true);
    if(empty($design)){
        return false;
    }

    $design = json_decode(stripslashes($design));

    global $post;
    $post = get_post($design->lactose->id);
    setup_postdata( $post );
    return get_the_title();
    wp_reset_postdata();

}