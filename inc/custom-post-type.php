<?php
/* joints Custom Post Type Example
This page walks you through creating
a custom post type and taxonomies. You
can edit this one or copy the following code
to create another one.

I put this in a separate file so as to
keep it organized. I find it easier to edit
and change things if they are concentrated
in their own file.

*/

// adding the function to the Wordpress init
add_action( 'init', 'custom_post_type');
// let's create the function for the custom type
function custom_post_type() {
  register_post_type( 'lactose', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	// let's now add all the options for this post type
		array('labels' 			=> array(
				'name' 				=> __('Caractéristiques produit', 'wpgreen'), /* This is the Title of the Group */
				'singular_name' 	=> __('Caractéristique produit', 'wpgreen'), /* This is the individual type */
			), /* end of arrays */
			'menu_position' 	=> 8, /* this is what order you want it to appear in on the left hand side menu */
			'menu_icon' 		=> 'dashicons-category', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
			'hierarchical' 		=> true,
			'public'             => true,
			'has_archive' 		 => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
      'rewrite' => array( 'slug' => 'lactose' ),
      'supports'           => array( 'title', 'editor', 'thumbnail', 'comments', "revisions")
	 	) /* end of options */
	);
  register_taxonomy(
		'category-lactose',
    'lactose',
		array(
			'label' => __( 'Catégorie' ),
			'hierarchical' => true,
		)
	);

  register_taxonomy( 
		'process-lactose',
    'lactose',
		array(
			'label' => __( 'Process' ),
			'hierarchical' => true,
      //'public' => false
		)
	);

  register_post_type( 'event', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	// let's now add all the options for this post type
		array('labels' 			=> array(
				'name' 				=> __('Evenements', 'wpgreen'), /* This is the Title of the Group */
				'singular_name' 	=> __('Evenement', 'wpgreen'), /* This is the individual type */
			), /* end of arrays */
			'menu_position' 	=> 7, /* this is what order you want it to appear in on the left hand side menu */
			'menu_icon' 		=> 'dashicons-tickets', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
			'hierarchical' 		=> true,
			'public'             => true,
			'has_archive' 		 => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
      'rewrite' => array( 'slug' => 'event' ),
      'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments' )
	 	) /* end of options */
	);

  register_post_type( 'info', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	// let's now add all the options for this post type
		array('labels' 			=> array(
				'name' 				=> __('Infos', 'wpgreen'), /* This is the Title of the Group */
				'singular_name' 	=> __('Info', 'wpgreen'), /* This is the individual type */
			), /* end of arrays */
			'menu_position' 	=> 62, /* this is what order you want it to appear in on the left hand side menu */
			'menu_icon' 		=> 'dashicons-info', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
			'hierarchical' 		=> false,
			'public'             => false,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
      'exclude_from_search' => true,
      'supports'           => array( 'title', 'editor')
	 	) /* end of options */
	);


  register_post_type( 'library', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	// let's now add all the options for this post type
		array('labels' 			=> array(
				'name' 				=> __('Documentation produit', 'wpgreen'), /* This is the Title of the Group */
				'singular_name' 	=> __('Documentation produit', 'wpgreen'), /* This is the individual type */
			), /* end of arrays */
			'menu_position' 	=> 9, /* this is what order you want it to appear in on the left hand side menu */
			'menu_icon' 		=> 'dashicons-book-alt', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
			'hierarchical' 		=> true,
			'public'             => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
      'exclude_from_search' => true,
      'supports'           => array( 'title')
	 	) /* end of options */
	);
  register_taxonomy(
		'category-library',
    'library',
		array(
			'label' => __( 'Catégorie' ),
			'hierarchical' => true,
      'public' => true
		)
	);

  register_post_type( 'faq', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	// let's now add all the options for this post type
		array('labels' 			=> array(
				'name' 				=> __('FAQ technique', 'wpgreen'), /* This is the Title of the Group */
				'singular_name' 	=> __('FAQ technique', 'wpgreen'), /* This is the individual type */
			), /* end of arrays */
			'menu_position' 	=> 12, /* this is what order you want it to appear in on the left hand side menu */
			'menu_icon' 		=> 'dashicons-editor-help', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
      'exclude_from_search' => true,
      'supports'           => array( 'title','editor')
	 	) /* end of options */
	);
  register_taxonomy(
		'category-faq',
    'faq',
		array(
			'label' => __( 'Catégorie' ),
			'hierarchical' => true,
      'public' => true
		)
	);

  register_post_type( 'ask_expert', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	// let's now add all the options for this post type
		array('labels' 			=> array(
				'name' 				=> __('Ask Expert', 'wpgreen'), /* This is the Title of the Group */
				'singular_name' 	=> __('Ask Expert', 'wpgreen'), /* This is the individual type */
			), /* end of arrays */
      'menu_position' 	=> 23, /* this is what order you want it to appear in on the left hand side menu */
			//'menu_icon' 		=> 'dashicons-editor-help', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
      'public' => false,
      'exclude_from_search' => true,
      'supports'           => array( 'title','editor')
	 	) /* end of options */
	);

  register_post_type( 'quality', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	// let's now add all the options for this post type
		array('labels' 			=> array(
				'name' 				=> __('Qualité documentation', 'wpgreen'), /* This is the Title of the Group */
				'singular_name' 	=> __('Qualité documentation', 'wpgreen'), /* This is the individual type */
			), /* end of arrays */
			'menu_position' 	=> 14, /* this is what order you want it to appear in on the left hand side menu */
			'menu_icon' 		=> 'dashicons-media-document', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
			'hierarchical' 		=> true,
			'public'             => false,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
      'exclude_from_search' => true,
      'supports'           => array( 'title')
	 	) /* end of options */
	);
  register_taxonomy(
		'category-quality',
    'quality',
		array(
			'label' => __( 'Catégorie' ),
			'hierarchical' => true,
      'public' => true
		)
	);

  register_post_type( 'risk', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
    // let's now add all the options for this post type
    array('labels' 			=> array(
        'name' 				=> __('Qualité - analyse du risque', 'wpgreen'), /* This is the Title of the Group */
        'singular_name' 	=> __('Qualité - analyse du risque', 'wpgreen'), /* This is the individual type */
      ), /* end of arrays */
      'menu_position' 	=> 15, /* this is what order you want it to appear in on the left hand side menu */
      'menu_icon' 		=> 'dashicons-editor-help', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
      'hierarchical' 		=> true,
      'public'             => false,
      'show_ui'            => true,
      'show_in_menu'       => true,
      'query_var'          => true,
      'exclude_from_search' => true,
      'supports'           => array( 'title')
    ) /* end of options */
  );

  register_taxonomy(
		'category-risk',
    'risk',
		array(
			'label' => __( 'Catégorie' ),
			'hierarchical' => true,
      'public' => false
		)
	);

  register_post_type( 'industry-link', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
    // let's now add all the options for this post type
    array('labels' 			=> array(
        'name' 				=> __('Qualité - liens', 'wpgreen'), /* This is the Title of the Group */
        'singular_name' 	=> __('Qualité - lien', 'wpgreen'), /* This is the individual type */
      ), /* end of arrays */
      'menu_position' 	=> 16, /* this is what order you want it to appear in on the left hand side menu */
      'menu_icon' 		=> 'dashicons-share', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
      'hierarchical' 		=> true,
      'public'             => false,
      'show_ui'            => true,
      'show_in_menu'       => true,
      'query_var'          => true,
      'exclude_from_search' => true,
      'supports'           => array( 'title')
    ) /* end of options */
  );

  register_post_type( 'bag', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
    // let's now add all the options for this post type
    array('labels' 			=> array(
        'name' 				=> __('Packagings', 'wpgreen'), /* This is the Title of the Group */
        'singular_name' 	=> __('Packaging', 'wpgreen'), /* This is the individual type */
      ), /* end of arrays */
      'menu_position' 	=> 10, /* this is what order you want it to appear in on the left hand side menu */
      'menu_icon' 		=> 'dashicons-products', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
      'hierarchical' 		=> true,
      'public'             => false,
      'show_ui'            => true,
      'show_in_menu'       => true,
      'query_var'          => true,
      'exclude_from_search' => true,
      'supports'           => array( 'title','thumbnail')
    ) /* end of options */
  );

  register_post_type( 'design', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
    // let's now add all the options for this post type
    array('labels' 			=> array(
        'name' 				=> __('Formulaire design', 'wpgreen'), /* This is the Title of the Group */
        'singular_name' 	=> __('Formulaire design', 'wpgreen'), /* This is the individual type */
      ), /* end of arrays */
      'menu_position' 	=> 17, /* this is what order you want it to appear in on the left hand side menu */
      'menu_icon' 		=> 'dashicons-art', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
      'hierarchical' 		=> true,
      'public'             => false,
      'show_ui'            => true,
      'show_in_menu'       => true,
      'query_var'          => true,
      'exclude_from_search' => true,
      'supports'           => array( 'title','editor')
    ) /* end of options */
  );

  register_post_type( 'distributor', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
    // let's now add all the options for this post type
    array('labels' 			=> array(
        'name' 				=> __('Liste distributeurs', 'wpgreen'), /* This is the Title of the Group */
        'singular_name' 	=> __('Liste distributeurs', 'wpgreen'), /* This is the individual type */
      ), /* end of arrays */
      'menu_position' 	=> 17, /* this is what order you want it to appear in on the left hand side menu */
      'menu_icon' 		=> 'dashicons-location-alt', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
      'hierarchical' 		=> true,
      'public'             => false,
      'show_ui'            => true,
      'show_in_menu'       => true,
      'query_var'          => true,
      'exclude_from_search' => true,
      'supports'           => array( 'title')
    ) /* end of options */
  );
  register_taxonomy(
		'category-distributor',
    'distributor',
		array(
			'label' => __( 'Catégorie' ),
			'hierarchical' => true,
      'public' => true
		)
	);

  register_post_type( 'doc-distributor', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
    // let's now add all the options for this post type
    array('labels' 			=> array(
        'name' 				=> __('Réseau - Doc', 'wpgreen'), /* This is the Title of the Group */
        'singular_name' 	=> __('Réseau - Doc', 'wpgreen'), /* This is the individual type */
      ), /* end of arrays */
      'menu_position' 	=> 18, /* this is what order you want it to appear in on the left hand side menu */
      'menu_icon' 		=> 'dashicons-paperclip', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
      'hierarchical' 		=> true,
      'public'             => false,
      'show_ui'            => true,
      'show_in_menu'       => true,
      'query_var'          => true,
      'exclude_from_search' => true,
      'supports'           => array( 'title')
    ) /* end of options */
  );
  register_taxonomy(
		'category-doc-distributor',
    'doc-distributor',
		array(
			'label' => __( 'Catégorie' ),
			'hierarchical' => true,
      'public' => false
		)
	);

  register_post_type( 'news-distributor', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
    // let's now add all the options for this post type
    array('labels' 			=> array(
        'name' 				=> __('Réseau - actus', 'wpgreen'), /* This is the Title of the Group */
        'singular_name' 	=> __('Réseau - actus', 'wpgreen'), /* This is the individual type */
      ), /* end of arrays */
      'menu_position' 	=> 19, /* this is what order you want it to appear in on the left hand side menu */
      'menu_icon' 		=> 'dashicons-format-aside', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
      'hierarchical' 		=> true,
      'public'             => true,
      'show_ui'            => true,
      'show_in_menu'       => true,
      'query_var'          => true,
      'exclude_from_search' => true,
      'supports'           => array( 'title', 'editor', 'thumbnail'),
      'rewrite' => array('slug'=>'news-distributor'),
      'capability_type' => 'post',
      'has_archive' => true,
    ) /* end of options */
  );

}
