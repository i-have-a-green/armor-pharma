<?php
class wpgreen_CSVDesign
{
	/**
	* Constructor
	*/
	public function __construct()
    {
        if(isset($_GET['report']) && $_GET['report'] == 'wpgreen_csvDesign')
        {
        	$this->wpgreen_csvDesign();
        }
    }
	public function wpgreen_csvDesign(){
		global $wpdb;
        $csv_fields=array();
        $csv_fields[] = 'Date';
        $csv_fields[] = 'Role';
        $csv_fields[] = 'Pays';
        $csv_fields[] = 'Prénom';
        $csv_fields[] = 'Nom';
        $csv_fields[] = 'Société';
        $csv_fields[] = 'Mail';
        $csv_fields[] = 'Profil';
        $csv_fields[] = 'Téléphone';

        $csv_fields[] = 'Customization';
        
        $output_filename = "design_".date("Y-m-d H:i:s").'.csv';
        $output_handle = @fopen( 'php://output', 'w' );
        header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
        header( 'Content-Description: File Transfer' );
        header( 'Content-type: text/csv' );
        header( 'Content-Disposition: attachment; filename=' . $output_filename );
        header( 'Expires: 0' );
        header( 'Pragma: public' );
        // Insert header row
        fputcsv( $output_handle, $csv_fields,";" );
		
		$user_query = new WP_User_Query(array('fields' => 'all_with_meta'));

		if ( ! empty( $user_query->get_results() ) ) {
            foreach ( $user_query->get_results() as $user ) {
                $orders = get_user_meta($user->ID, 'orders', true); 
                $orders = json_decode($orders);
                if($orders){
                    $orders = array_reverse($orders);
                    foreach ($orders as $order){
                        $tab_data = array( 
                            $order->date,
                            $user->roles[0],
                            $user->country, 
                            $user->first_name,
                            $user->last_name,
                            $user->company,
                            $user->user_email,
                            $user->profil,
                            $user->phone,
                            getDesign($order->cart)
                        );
                        fputcsv( $output_handle, $tab_data,";" );
                    }
                }
            }
        }
        fclose( $output_handle );
		exit();
	}
}
// Instantiate a singleton of this plugin
new wpgreen_CSVDesign();


function getDesign($design){

    $body = '';
    if(isset($design->pharmacopoeial)){
        $body .= "Pharmacopoeial conformance : \r\n";
        foreach ($design->pharmacopoeial as $val) {
            $body .= $val->label."\r\n";
        }
    }
    
    if(isset($design->microbial)){
        $body .= "Microbial limits : \r\n";
        foreach ($design->microbial as $val) {
            $body .= $val->label." : ".$val->value." ".$val->unit."\r\n";
        }
    }
    
    if(isset($design->anotherTest)){
        $body .= "Another test : \r\n";
        foreach ($design->anotherTest as $val) {
            $body .= $val->label." : ".$val->limits." ".$val->method."\r\n";
        }
    }
    
    if(isset($design->particule)){
        $body .= "Particule size distribution (Air jet sieve) : \r\n";
        foreach ($design->particule as $val) {
            $body .= $val->label." ".$val->value."\r\n";
        }
    }
    
    if(isset($design->fileSpecification)){
        $body .= "Specification file : \r\n";
        $body .= '<a href="'.$design->fileSpecification->link.'" target="_blank">'.$design->fileSpecification->file.'</a>';
        $body .= "\r\n";
    }
    if(isset($design->targetSpecification) && !empty($design->targetSpecification)){
        $body .= "Specification target : \r\n";
        $body .= $design->targetSpecification;
        $body .= "\r\n";
    }
    
    $body .= "\r\n\r\n Packaging : \r\n\r\n";
    
    if(isset($design->packaging->packagingOptions)){
        $body .= $design->packaging->label."\r\n";
        foreach ($design->packaging->packagingOptions as $val){
            $body .= $val->valueText."\r\n";
        }
    }
    if(isset($design->fileSpecification)){
        $body .= "Packaging file : \r\n";
        $body .= '<a href="'.$design->filePackaging->link.'" target="_blank">'.$design->filePackaging->file.'</a>';
        $body .= "\r\n";
    }
    
    $body .= "\r\n\r\n Palletization : \r\n\r\n";
    
    if(isset($design->palet->typePalet)){
        $body .= "Adjust my palet \r\n";
        foreach ($design->palet->typePalet as $val){
            $body .= $val->valueText."\r\n";
        }
    }
    if(isset($design->palet->adjustPalet)){
        $body .= "The type of pallet \r\n";
        foreach ($design->palet->adjustPalet as $val){
            $body .= $val->valueText."\r\n";
        }
    }
    
    if(isset($design->filePalet)){
        $body .= "Palletization file : \r\n";
        $body .= '<a href="'.$design->filePalet->link.'" target="_blank">'.$design->filePalet->file.'</a>';
        $body .= "\r\n";
    }
    $body .= @$design->targetPalet."\r\n";
    $body .= "Adjust my palet\r\n";
    $body .= @$design->delivery->needQuantity->valueText."\r\n";
    $body .= @$design->delivery->needText."\r\n";
    if(isset($design->delivery->company)){
        $body .= $design->delivery->company."\r\n";
        $body .= $design->delivery->address."\r\n";
        $body .= $design->delivery->zipCode.' ';
        $body .= $design->delivery->city."\r\n";
        $body .= $design->delivery->stateProvince."\r\n";
        $body .= $design->delivery->country."\r\n";
    }
    return $body;
}