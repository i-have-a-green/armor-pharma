<?php
// Numeric Page Navi (built into the theme by default)
function wpgreen_page_navi($before = '', $after = '') {
	global $wpdb, $wp_query;
	$request = $wp_query->request;
	$posts_per_page = intval(get_query_var('posts_per_page'));
	$paged = intval(get_query_var('paged'));
	$numposts = $wp_query->found_posts;
	$max_page = $wp_query->max_num_pages;
	if ( $numposts <= $posts_per_page ) { return; }
	if(empty($paged) || $paged == 0) {
		$paged = 1;
	}
	$pages_to_show = 7;
	$pages_to_show_minus_1 = $pages_to_show-1;
	$half_page_start = floor($pages_to_show_minus_1/2);
	$half_page_end = ceil($pages_to_show_minus_1/2);
	$start_page = $paged - $half_page_start;
	if($start_page <= 0) {
		$start_page = 1;
	}
	$end_page = $paged + $half_page_end;
	if(($end_page - $start_page) != $pages_to_show_minus_1) {
		$end_page = $start_page + $pages_to_show_minus_1;
	}
	if($end_page > $max_page) {
		$start_page = $max_page - $pages_to_show_minus_1;
		$end_page = $max_page;
	}
	if($start_page <= 0) {
		$start_page = 1;
	}
	echo $before.'<nav class="page-navigation"><ul class="pagination">'."";
	if ($start_page >= 2 && $pages_to_show < $max_page) {
		$first_page_text = __( 'First', 'wpgreen' );
		echo '<li><a href="'.get_pagenum_link().'" title="'.$first_page_text.'">'.$first_page_text.'</a></li>';
	}
	echo '<li>';
	previous_posts_link( __('Previous', 'wpgreen') );
	echo '</li>';
	for($i = $start_page; $i  <= $end_page; $i++) {
		if($i == $paged) {
			echo '<li class="current"> '.$i.' </li>';
		} else {
			echo '<li><a href="'.get_pagenum_link($i).'">'.$i.'</a></li>';
		}
	}
	echo '<li>';
	next_posts_link( __('Next', 'wpgreen'), 0 );
	echo '</li>';
	if ($end_page < $max_page) {
		$last_page_text = __( 'Last', 'wpgreen' );
		echo '<li><a href="'.get_pagenum_link($max_page).'" title="'.$last_page_text.'">'.$last_page_text.'</a></li>';
	}
	echo '</ul></nav>'.$after."";
} /* End page navi */



function the_breadcrumb() {
    $sep = ' > ';
    if (!is_front_page()) {

	// Start the breadcrumb with a link to your homepage
        echo '<div class="breadcrumbs">';
        echo '<a href="';
        echo get_option('home');
        echo '">';
        bloginfo('name');
        echo '</a>' . $sep;

	// Check if the current page is a category, an archive or a single page. If so show the category or archive name.
        if (is_category() || is_single() ){
            if ( get_post_type( get_the_ID() ) == 'post' || get_post_type( get_the_ID() ) == 'event' ) {
              $p = get_page_by_path( "events-news" );
              echo '<a href="'.get_the_permalink($p).'">'.$p->post_title.'</a>';
            }
            elseif(is_singular("lactose")) {
              $p = get_page_by_path( "lactose-portfolio" );
              echo '<a href="'.get_the_permalink($p).'">'.$p->post_title.'</a>';?>
              <script>
              window.addEventListener("load", function() {
                el = document.getElementsByClassName('menu-lactose-portfolio');
                addClass(el[0], "active");
                addClass(el[1], "active");
              });
              </script>
              <?php echo $sep;
              $id = get_primary_taxonomy_id(get_the_id(),'category-lactose');
              $category = get_term( $id, 'category-lactose');
              $category_link = get_category_link( $id );
              $category_display = $category->name;
              echo '<a href="'.$category_link.'">'.$category_display.'</a>';
            }
            else{
              the_category('title_li=');
            }

        } elseif (is_archive() || is_single()){
            if(is_tax("category-lactose")){
              $p = get_page_by_path( "lactose-portfolio" );
              echo '<a href="'.get_the_permalink($p).'">'.$p->post_title.'</a>';?>
              <script>
              window.addEventListener("load", function() {
                el = document.getElementsByClassName('menu-lactose-portfolio');
                addClass(el[0], "active");
                addClass(el[1], "active");
              });
              </script>
              <?php echo $sep;
              echo single_cat_title( '', false );
            } elseif ( is_day() ) {
                printf( __( '%s', 'text_domain' ), get_the_date() );
            } elseif ( is_month() ) {
                printf( __( '%s', 'text_domain' ), get_the_date( _x( 'F Y', 'monthly archives date format', 'text_domain' ) ) );
            } elseif ( is_year() ) {
                printf( __( '%s', 'text_domain' ), get_the_date( _x( 'Y', 'yearly archives date format', 'text_domain' ) ) );
            } else {
              $p = get_page_by_path( "my-details" );
              echo '<a href="'.get_the_permalink($p).'">'.$p->post_title.'</a>';
              echo $sep;
              _e("Distributor area","armor-pharma");
                //_e( 'Blog Archives', 'text_domain' );
            }
        }

	// If the current page is a single post, show its title with the separator
        if (is_single()) {
            echo $sep;
            the_title();
        }

	// If the current page is a static page, show its title.
        if (is_page()) {
            if($postID = wp_get_post_parent_id(get_the_id()) ){
                echo '<a href="'.get_the_permalink($postID).'">'.get_the_title($postID).'</a>';
                echo $sep;
            }
            the_title();
        }

	// if you have a static page assigned to be you posts list page. It will find the title of the static page and display it. i.e Home >> Blog
        if (is_home()){
            global $post;
            $page_for_posts_id = get_option('page_for_posts');
            if ( $page_for_posts_id ) {
                $post = get_page($page_for_posts_id);
                setup_postdata($post);
                the_title();
                rewind_posts();
            }
        }
        echo '</div>';
    }
}
