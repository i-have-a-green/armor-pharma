<?php
// SIDEBARS AND WIDGETIZED AREAS
function wpgreen_register_sidebars() {
	  register_sidebar(array(
		'id' => 'language',
		'name' => __('language', 'wpgreen'),
		'description' => __('Menu language', 'wpgreen'),
		'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget' => '</li>',
		'before_title' => '',
		'after_title' => '',
	));

} // don't remove this bracket!
