<?php
define('EVENT_WPGREEN_EMAIL_TO', get_option( 'admin_email') );
define('EVENT_WPGREEN_EMAIL_FROM', __('no-reply@', 'wpgreen') . str_replace('www.', '', $_SERVER['SERVER_NAME'] ) );
define('EVENT_WPGREEN_SUBJECT', __('Nouveau message depuis votre site web','wpgreen') );

add_action( 'init', 'event_custom_post_type');
function event_custom_post_type() {
	register_post_type( 'wpgreen_event', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		array('labels' 			=> array(
				'name' 				=> __('Formulaire Events', 'wpgreen'), /* This is the Title of the Group */
				'singular_name' 	=> __('Formulaire event', 'wpgreen'), /* This is the individual type */
			), /* end of arrays */
			'menu_position' 	=> 20, /* this is what order you want it to appear in on the left hand side menu */
			'menu_icon' 		=> 'dashicons-email-alt', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
			'hierarchical' 		=> false,
			'public'             => false,
			'publicly_queryable' => false,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => false,
			'supports' 			=> array( 'title', 'editor')
	 	) /* end of options */
	);
}


/*
* traitement du post du form de Event
* enregistrement des values dans le custom post type
*/
/*add_action( 'wp_ajax_formmeeting', 'wpgreen_formmeeting' );
add_action( 'wp_ajax_nopriv_formmeeting', 'wpgreen_formmeeting' );
function wpgreen_formmeeting(){
	if (wp_verify_nonce($_POST['nonceformmeeting'], 'nonceformmeeting')) {


		global $wpdb;

		$subject = __("Request for the meeting","armor-pharma").' - '.sanitize_text_field($_POST['event']);

		$body = sanitize_text_field($_POST['genre']).' '.sanitize_text_field($_POST['firstname']) .' '.sanitize_text_field($_POST['lastname'])."\r\n";
		$body .= 'Country : '.sanitize_text_field($_POST['country'])."\r\n";
		$body .= 'Phone : '.sanitize_text_field($_POST['phone'])."\r\n";
		$body .= 'Profil : '.sanitize_text_field($_POST['profil'])."\r\n";
		$body .= 'Company : '.sanitize_text_field($_POST['company'])."\r\n";
    $body .= 'Email : '.sanitize_text_field($_POST['email'])."\r\n";

    $body .= 'Subject : '.sanitize_text_field($_POST['subject'])."\r\n";
    $body .= 'Who would you like to meet ? : '.sanitize_text_field($_POST['meeting_who'])."\r\n";
    $body .= 'Comments : '.sanitize_textarea_field($_POST['meeting_comments'])."\r\n";
		$body .= 'Date : '.sanitize_text_field($_POST['meeting_date'])."\r\n";


    $headers[] = 'From: '.get_bloginfo('name').' <'. get_option( 'admin_email') .'>';
		wp_mail( get_option( 'admin_email'), $subject, $body, $headers);
        $post['post_type']   = 'wpgreen_event';
        $post['post_status'] = 'publish';
        $post['post_title'] = sanitize_text_field($_POST['genre']).' '.sanitize_text_field($_POST['firstname']) .' '.sanitize_text_field($_POST['lastname']);
		    $post['post_content'] = nl2br($body);
		wp_insert_post( $post, true );

    $body = __('<b>Thank you for contacting ARMOR PHARMA !</b>
<p>
  Your request has been successfully submitted and we will revert to you in the shortest delay
</p>
<p>
Kind regards<br />
ARMOR PHARMA’s team
</p>','wpgreen');
		$headers[] = 'From: '.get_bloginfo('name').' <'. WPGREEN_EMAIL_FROM .'>';
		wp_mail( sanitize_email($_POST['email']), __("Request for the meeting","armor-pharma").' - '.sanitize_text_field($_POST['event']), $body, $headers);
	}
	die();
}
*/