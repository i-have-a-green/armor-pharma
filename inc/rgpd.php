<?php



add_action('admin_menu', 'ihag_user_sub_menu');
function ihag_user_sub_menu() {
    add_submenu_page(
        'tools.php',
        'RGPD',
        'Clean RGPD',
        'manage_options',
        'RGPD',
        'RGPD',
    );
    global $submenu;
}


function RGPD(){
    ?>
    <div class="wrap">
        <h1>Clean RGPD </h1>
        <a href="?rgpd=ask_expert" class="button">Delete ask expert > 3 years</a>
        <a href="?rgpd=delete_design" class="button">Delete design & order > 3 years</a>
        <a href="?rgpd=delete_user" class="button">Delete user > 3 years</a>
    <?php
}

add_action('init', function() {
    if(isset($_GET['rgpd']) && $_GET['rgpd'] == 'ask_expert'){
        delete_ask_expert();
    }
    else if(isset($_GET['rgpd']) && $_GET['rgpd'] == 'delete_design'){
        delete_order();
    }
    else if(isset($_GET['rgpd']) && $_GET['rgpd'] == 'delete_user'){
        delete_user();
    }
    
});

function delete_ask_expert(){
    $args = array(
		'post_type'         => 'ask_expert',
        'posts_per_page' => -1,
        'date_query' => array(
            array(
                'column' => 'post_date',
                'before' => '3 years ago',
            ),
        ),
	);
	$posts= get_posts($args);
	foreach ( $posts as $ask ){
		wp_delete_post( $ask->ID);
        //echo substr($ask->post_date,0,4).' - '.get_the_title($ask->ID).' -> '.$ask->post_date.'<br>';
	}

    error_log('delete_ask_expert OK');
    return true;
}

function delete_order(){//design and orders
    $threeYears = strtotime("-3 years");
    $user_query = new WP_User_Query(array('fields' => 'all_with_meta'));
    if ( ! empty( $user_query->get_results() ) ) {
        foreach ( $user_query->get_results() as $user ) {
            $orders = get_user_meta($user->ID, 'orders', true);
            $orders = json_decode($orders);
            if($orders){
                $orders = array_reverse($orders);
                foreach ($orders as $order){
                    $d = substr($order->date,0,10);
                    $d = strtotime($d);
                    if($d < $threeYears){
                        delete_user_meta($user->ID, 'orders');
                    }
                }
            }
        }
    }

    error_log('delete_order OK');
}

function delete_user(){
    $blogusers = get_users(
        [
            'role__not_in'  => ['administrator','distributor'],
        ]
    );
    
    // Compte à ne pas supprimmer
    $email_to_save = array(
        'arsim.zhubi@armor-pharma.com',
        'cecile.lemee@armor-pharma.com', 
        'marie.charbaut@armor-pharma.com', 
        'jan.dijksterhuis@armor-pharma.com',
        'mohamad@aromachemicals.online',
        'alexander.frueh@heel.com',
        'kazim@hcp.com.pk',
    );
    
    // Array of WP_User objects.
    foreach ($blogusers as $user) {

        // date du jour -3 ans 
        $limit_time = strtotime("-3 year", time());
        // date de la dernière connexion
        $last_connection = (int)get_user_meta($user->ID, 'last_login')[0];
        // date d'inscription
        $inscription_date = get_userdata( $user->ID )->user_registered;
        // email de l'utilisateur
        $email_user = get_userdata( $user->ID )->user_email;

        // Si l'email de l'utilisateur n'est pas dans le tableau d'adresse mail
        if (!in_array($email_user, $email_to_save)) {

            // S'il y a une date de dernière connexion sinon compare avec la date de création
            if ($last_connection != 0) {
    
                // si la dernière connection est plus ancienne que 3 ans 
                if ($last_connection < $limit_time) {
                    $user_data = wp_update_user(array( 
                        'ID' => $user->ID, 
                        'user_nicename' => '********',
                        'user_email' => '********',
                        'display_name' => '********',
                        'nickname' => '********',
                        'first_name' => '********',
                        'last_name' => '********',
                        'user_nicename' => '********',
                    ) );
                }
            }
            else if (date("Y-m-d h:i:s", $limit_time) > $inscription_date) {
                $user_data = wp_update_user(array( 
                        'ID' => $user->ID, 
                        'user_nicename' => '********',
                        'user_email' => '********',
                        'display_name' => '********',
                        'nickname' => '********',
                        'first_name' => '********',
                        'last_name' => '********',
                        'user_nicename' => '********',
                    )
                );
            }
        }
    }

    error_log('delete_user OK');
}

register_activation_hook( __FILE__, 'activation_rgpd' );

function activation_rgpd() {
    wp_schedule_event( time(), 'monthly', 'monthly_rgpd' );
}

add_action( 'monthly_rgpd', 'do_rgpd' );
function do_rgpd() {
    delete_ask_expert();
    delete_order();
    delete_user();
}