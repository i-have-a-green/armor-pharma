<?php
add_image_size('wpgreen-full', 600, 600, true);
add_image_size('wpgreen-logo', 200, 100, false);
add_image_size('wpgreen-400', 400, 400, false);
add_image_size('wpgreen-250', 250, 250, false);
add_image_size('wpgreen-200', 200, 200, false);
add_image_size('wpgreen-75-90', 75, 90, false);
add_image_size('flag', 35, 25, false);
