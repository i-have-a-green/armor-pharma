<?php
function site_scripts() {
  	global $wp_styles;

    

    //remove jquery on the footer
    wp_deregister_script( 'jquery' );

    // Adding scripts file in the footer
    wp_enqueue_script( 'modernizr', get_stylesheet_directory_uri() . '/assets/js/modernizr.js', false, '', true );
    wp_enqueue_script( 'jquery', get_stylesheet_directory_uri() . '/assets/js/jquery.min.js', false, array('modernizr'), true );
    wp_enqueue_script( 'input-phone', get_stylesheet_directory_uri() . '/assets/js/intlTelInput-jquery.min.js', false, array('jquery'), true );
    wp_enqueue_script( 'flexslider', get_stylesheet_directory_uri() . '/assets/js/jquery.flexslider.js', false, array('jquery'), true );
    /*if(get_page_template_slug() == 'template-contact.php' || get_page_template_slug() == 'template-about-us.php'){
      wp_enqueue_script( 'googleMaps', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAcsaZSk-QezAqacx-tUhSaHiTI7W3RnK4', false, array('jquery'), true );
      wp_localize_script('googleMaps', 'pictoMap', get_stylesheet_directory_uri() . '/assets/css/images/pictoMap.png' );
    }*/
    wp_enqueue_script( 'script', get_stylesheet_directory_uri() . '/assets/js/script.js', false, array('jquery','flexslider'), true );
    //wp_localize_script('script', 'ajaxurl', admin_url( 'admin-ajax.php' ) );
    //wp_localize_script('script', 'ajaxurl', home_url( 'admin-ajax.php' ) );
    wp_localize_script('script', 'url', array(  
        'myAccountUrl' => get_the_permalink( get_page_by_path("my-details")),
        'myCartUrl' => get_the_permalink( get_page_by_path("my-cart")),
        'homeUrl' => home_url() ,
        'ajaxurl' => admin_url( 'admin-ajax.php' )
      ));
    wp_localize_script(
      'script',
      'ihagRest',
      array(
        'root'    => esc_url_raw( rest_url() ),
        'nonce'   => wp_create_nonce( 'wp_rest' ),
        'resturl' => site_url() . '/wp-json/ihag/', 
      ) 
    );




	if(get_page_template_slug() == 'template-contact.php' ){
		wp_enqueue_script( 'googleCaptcha', 'https://www.google.com/recaptcha/api.js');
		//wp_localize_script('script', 'contactTitle', __('Formulaire de contact', 'wpgreen') );
		//wp_localize_script('script', 'contactText', __('Votre message a été envoyé,<br>il sera traité rapidement.', 'wpgreen') );
	}
  //if(get_page_template_slug() == 'template-portfolio-lactose.php' || is_tax("category-lactose")){
    wp_enqueue_script( 'jquery-ui', get_stylesheet_directory_uri() . '/assets/js/jquery-ui.min.js', false, array('jquery'), true );
    wp_enqueue_script( 'jquery-ui-touch', '//cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js', false, array('jquery-ui'), true );
    wp_enqueue_style( 'jquery-ui', get_stylesheet_directory_uri() . '/assets/css/jquery-ui.min.css', '', '', 'all' );
  //}
  if(is_tax("category-lactose") || is_singular( "lactose" ) || get_page_template_slug() == 'template-portfolio-lactose.php'){
		wp_enqueue_script( 'chartJS', 'https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js',false, array('jquery'), true );
    wp_enqueue_script( 'zoom', get_stylesheet_directory_uri() . '/assets/js/jquery.zoom.min.js',false, array('jquery'), true );

	}


	//stylesheet
  wp_enqueue_style( 'flexslider', get_stylesheet_directory_uri() . '/assets/css/flexslider.css', '', '', 'all' );
  wp_enqueue_style( 'global-site-min', get_stylesheet_directory_uri() . '/assets/css/global.min.css', array('flexslider'), '', 'all' );
  wp_enqueue_style( 'input-phone', get_stylesheet_directory_uri() . '/assets/css/intlTelInput.min.css', array('global-site-min'), '', 'all' );
  wp_enqueue_style( 'tarteaucitron', get_stylesheet_directory_uri() . '/assets/css/tarteaucitron.css', '', '', 'all' );
  
}
add_action('wp_enqueue_scripts', 'site_scripts', 999);
