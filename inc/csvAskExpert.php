<?php
class wpgreen_CSVAsk
{
	/**
	* Constructor
	*/
	public function __construct()
    {
        if(isset($_GET['report']) && $_GET['report'] == 'wpgreen_CSVAsk')
        {
        	$this->wpgreen_CSVAsk();
        }
    }
	public function wpgreen_CSVAsk(){
		global $wpdb;
        $csv_fields=array();
        $csv_fields[] = 'Ask';
        $csv_fields[] = 'Name';
        $csv_fields[] = 'Pays';
        $csv_fields[] = 'Téléphone';
        $csv_fields[] = 'Profil';
        $csv_fields[] = 'Société';
        $csv_fields[] = 'Mail';
        $csv_fields[] = 'Subject';
        $csv_fields[] = 'Comments';
        
        
        $output_filename = "askExpert_".date("Y-m-d H:i:s").'.csv';
        $output_handle = @fopen( 'php://output', 'w' );
        header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
        header( 'Content-Description: File Transfer' );
        header( 'Content-type: text/csv' );
        header( 'Content-Disposition: attachment; filename=' . $output_filename );
        header( 'Expires: 0' );
        header( 'Pragma: public' );
        // Insert header row
        fputcsv( $output_handle, $csv_fields,";" );
		
		$args = array( 'posts_per_page' => -1, 'post_type' => 'ask_expert' );
        $myposts = get_posts( $args );
        foreach ( $myposts as $post ) {
            $tab_data = array();
            $content = explode ( "\r\n" , $post->post_content );
            $tab_data[] = $post->post_title;
            $tab_data[] = $content[0];
            $tab_data[] = str_replace("country : ","", $content[1]);
            $tab_data[] = str_replace("phone : ","", $content[2]);
            $tab_data[] = str_replace("profil : ","", $content[3]);
            $tab_data[] = str_replace("company : ","", $content[4]);
            $tab_data[] = str_replace("email : ","", $content[5]);
            $tab_data[] = str_replace("subject : ","", $content[6]);
            $tab_data[] = str_replace("comments : ","", $content[7]);
            $comment = '';
            for($i=8;$i < sizeof($content);$i++){
                $comment .= $content[$i];
            }
            $tab_data[] = $comment;
            fputcsv( $output_handle, $tab_data,";" );
            /*if(preg_match("#Question related to the#",get_the_title($post->ID))){
                $tab_data = array();
                $content = explode ( "\r\n" , $post->post_content );
                $tab_data[] = "Question related to a product";
                $tab_data[] = $content[0];
                $tab_data[] = str_replace("country : ","", $content[1]);
                $tab_data[] = str_replace("phone : ","", $content[2]);
                $tab_data[] = str_replace("profil : ","", $content[3]);
                $tab_data[] = str_replace("company : ","", $content[4]);
                $tab_data[] = str_replace("email : ","", $content[5]);
                $tab_data[] = str_replace("subject : ","", $content[6]);
                $tab_data[] = str_replace("comments : ","", $content[7]);
                $comment = '';
                for($i=8;$i < sizeof($content);$i++){
                    $comment .= $content[$i];
                }
                $tab_data[] = $comment;
                fputcsv( $output_handle, $tab_data );
            }
            elseif(preg_match("#Question for the technical support#",get_the_title($post->ID))){
                $tab_data = array();
                $content = explode ( "\r\n" , $post->post_content );
                $tab_data[] = "Question for the technical support";
                $tab_data[] = $content[0];
                $tab_data[] = str_replace("country : ","", $content[1]);
                $tab_data[] = str_replace("phone : ","", $content[2]);
                $tab_data[] = str_replace("profil : ","", $content[3]);
                $tab_data[] = str_replace("company : ","", $content[4]);
                $tab_data[] = str_replace("email : ","", $content[5]);
                $tab_data[] = str_replace("subject : ","", $content[6]);
                $tab_data[] = str_replace("comments : ","", $content[7]);
                $comment = '';
                for($i=8;$i < sizeof($content);$i++){
                    $comment .= $content[$i];
                }
                $tab_data[] = $comment;
                fputcsv( $output_handle, $tab_data );
            }
            elseif(preg_match("#Question for the TECHNICAL support expert#",get_the_title($post->ID))){
                $tab_data = array();
                $content = explode ( "\r\n" , $post->post_content );
                $tab_data[] = "Question for the TECHNICAL support expert";
                $tab_data[] = $content[0];
                $tab_data[] = str_replace("country : ","", $content[1]);
                $tab_data[] = str_replace("phone : ","", $content[2]);
                $tab_data[] = str_replace("profil : ","", $content[3]);
                $tab_data[] = str_replace("company : ","", $content[4]);
                $tab_data[] = str_replace("email : ","", $content[5]);
                $tab_data[] = str_replace("subject : ","", $content[6]);
                $tab_data[] = str_replace("comments : ","", $content[7]);
                $comment = '';
                for($i=8;$i < sizeof($content);$i++){
                    $comment .= $content[$i];
                }
                $tab_data[] = $comment;
                fputcsv( $output_handle, $tab_data );
            }
            elseif(preg_match("#Help me finding the suitable lactose#",get_the_title($post->ID))){
                $tab_data = array();
                $content = explode ( "\r\n" , $post->post_content );
                $tab_data[] = "Help me finding the suitable lactose";
                $tab_data[] = $content[0];
                $tab_data[] = str_replace("country : ","", $content[1]);
                $tab_data[] = str_replace("phone : ","", $content[2]);
                $tab_data[] = str_replace("profil : ","", $content[3]);
                $tab_data[] = str_replace("company : ","", $content[4]);
                $tab_data[] = str_replace("email : ","", $content[5]);
                $tab_data[] = str_replace("subject : ","", $content[6]);
                $tab_data[] = str_replace("comments : ","", $content[7]);
                $comment = '';
                for($i=8;$i < sizeof($content);$i++){
                    $comment .= $content[$i];
                }
                $tab_data[] = $comment;
                fputcsv( $output_handle, $tab_data );
            }
            elseif(preg_match("#ARMOR PHARMA, get in touch#",get_the_title($post->ID))){
                $tab_data = array();
                $content = explode ( "\r\n" , $post->post_content );
                $tab_data[] = "ARMOR PHARMA, get in touch";
                $tab_data[] = $content[0];
                $tab_data[] = str_replace("country : ","", $content[1]);
                $tab_data[] = str_replace("phone : ","", $content[2]);
                $tab_data[] = str_replace("profil : ","", $content[3]);
                $tab_data[] = str_replace("company : ","", $content[4]);
                $tab_data[] = str_replace("email : ","", $content[5]);
                $tab_data[] = str_replace("subject : ","", $content[6]);
                $tab_data[] = str_replace("comments : ","", $content[7]);
                $comment = '';
                for($i=8;$i < sizeof($content);$i++){
                    $comment .= $content[$i];
                }
                $tab_data[] = $comment;
                fputcsv( $output_handle, $tab_data );
            }
            elseif(preg_match("#Question from Armor Pharma website#",get_the_title($post->ID))){
                $tab_data = array();
                $content = explode ( "\r\n" , $post->post_content );
                $tab_data[] = "Question from Armor Pharma website";
                $tab_data[] = $content[0];
                $tab_data[] = str_replace("country : ","", $content[1]);
                $tab_data[] = str_replace("phone : ","", $content[2]);
                $tab_data[] = str_replace("profil : ","", $content[3]);
                $tab_data[] = str_replace("company : ","", $content[4]);
                $tab_data[] = str_replace("email : ","", $content[5]);
                $tab_data[] = str_replace("subject : ","", $content[6]);
                $tab_data[] = str_replace("comments : ","", $content[7]);
                $comment = '';
                for($i=8;$i < sizeof($content);$i++){
                    $comment .= $content[$i];
                }
                $tab_data[] = $comment;
                fputcsv( $output_handle, $tab_data );
            }*/
        }
        fclose( $output_handle );
		exit();
	}
}
// Instantiate a singleton of this plugin
new wpgreen_CSVAsk();
