<?php
class wpgreen_CSVOrderAbandon
{
	/**
	* Constructor
	*/
	public function __construct()
    {
        if(isset($_GET['report']) && $_GET['report'] == 'wpgreen_CSVOrderAbandon')
        {
        	$this->wpgreen_CSVOrderAbandon();
        }
    }
	public function wpgreen_CSVOrderAbandon(){
		global $wpdb;
        $csv_fields=array();
        $csv_fields[] = 'Role';
        $csv_fields[] = 'Pays';
        $csv_fields[] = 'Prénom';
        $csv_fields[] = 'Nom';
        $csv_fields[] = 'Société';
        $csv_fields[] = 'Mail';
        $csv_fields[] = 'Profil';
        $csv_fields[] = 'Téléphone';

        $csv_fields[] = 'product';
        
        $output_filename = "orderAbandon_".date("Y-m-d H:i:s").'.csv';
        $output_handle = @fopen( 'php://output', 'w' );
        header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
        header( 'Content-Description: File Transfer' );
        header( 'Content-type: text/csv' );
        header( 'Content-Disposition: attachment; filename=' . $output_filename );
        header( 'Expires: 0' );
        header( 'Pragma: public' );
        // Insert header row
        fputcsv( $output_handle, $csv_fields,";" );
		
		$user_query = new WP_User_Query(array('fields' => 'all_with_meta'));

		if ( ! empty( $user_query->get_results() ) ) {
            foreach ( $user_query->get_results() as $user ) {
                $cart = get_user_meta($user->ID, 'cart', true);
                $cart = json_decode($cart);
                if($cart){
                    foreach ($cart as $lactose){
                        $tab_data = array( 
                            $user->roles[0],
                            $user->country, 
                            $user->first_name,
                            $user->last_name,
                            $user->company,
                            $user->user_email,
                            $user->profil,
                            $user->phone,
                            get_the_title($lactose->id_lactose)
                        );
                        fputcsv( $output_handle, $tab_data,";" );
                    }
                }
            }
        }
        fclose( $output_handle );
		exit();
	}
}
// Instantiate a singleton of this plugin
new wpgreen_CSVOrderAbandon();
