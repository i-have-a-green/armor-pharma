<?php
class wpgreen_CSVTechnicalDocumentation
{
	/**
	* Constructor
	*/
	public function __construct()
    {
        if(isset($_GET['report']) && $_GET['report'] == 'wpgreen_CSVTechnicalDocumentation')
        {
        	$this->wpgreen_CSVTechnicalDocumentation();
        }
    }
	public function wpgreen_CSVTechnicalDocumentation(){
		global $wpdb;
        $csv_fields=array();
        $csv_fields[] = 'Role';
        $csv_fields[] = 'Pays';
        $csv_fields[] = 'Prénom';
        $csv_fields[] = 'Nom';
        $csv_fields[] = 'Société';
        $csv_fields[] = 'Mail';
        $csv_fields[] = 'Profil';
        $csv_fields[] = 'Téléphone';

        $csv_fields[] = 'TechnicalDocumentation';
        
        $output_filename = "orderTechnicalDocumentation_".date("Y-m-d H:i:s").'.csv';
        $output_handle = @fopen( 'php://output', 'w' );
        header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
        header( 'Content-Description: File Transfer' );
        header( 'Content-type: text/csv' );
        header( 'Content-Disposition: attachment; filename=' . $output_filename );
        header( 'Expires: 0' );
        header( 'Pragma: public' );
        // Insert header row
        fputcsv( $output_handle, $csv_fields,";" );
		
		$user_query = new WP_User_Query(array('fields' => 'all_with_meta'));

		if ( ! empty( $user_query->get_results() ) ) {
            foreach ( $user_query->get_results() as $user ) {
                $technicalDocumentation = get_user_meta($user->ID, 'technicalDocumentation', true);
                if($technicalDocumentation){
                    $tab_data = array( 
                        $user->roles[0],
                        $user->country, 
                        $user->first_name,
                        $user->last_name,
                        $user->company,
                        $user->user_email,
                        $user->profil,
                        $user->phone,
                        $technicalDocumentation
                    );
                    fputcsv( $output_handle, $tab_data,";" );
                }
            }
        }
        fclose( $output_handle );
		exit();
	}
}
// Instantiate a singleton of this plugin
new wpgreen_CSVTechnicalDocumentation();
