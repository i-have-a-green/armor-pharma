<?php
/*
Template Name: Design congratulation
*/

$user = get_current_user_id();
if($user == 0){
  wp_redirect( home_url(), 302 );
}
get_header();
$design = get_user_meta(get_current_user_id(), 'design', true);
$JSONdesign = $design;
$design = json_decode(stripslashes($design));
?>
<script>var design = <?php echo $JSONdesign;?>;</script>
<?php

get_header();
if (have_posts()) :  while ( have_posts() ) : the_post();
?>
<div id="content-header">
  <div class="wrapper">
    <?php the_breadcrumb();?>
  </div>
</div>
<div class="text-center">
  <h1 class="degrade"><?php _e("Design the lactose <b>you</b> like","armor-pharma");?></h1><br />
  <p class="degrade" id="titreProduct">
    <?php _e("How would you like your","armor-pharma");?> « <?php echo $design->lactose->name;?> »
  </p>
</div>
<?php
set_query_var( 'item', 6 );
get_template_part( 'template-parts/arianne', 'design' );?>
<div class="wrapper" id="contentDesign">
  <main>
    <div class="help">
      <a href="<?php echo get_the_permalink(get_page_by_path( "contact" ) );?>">
        <span><?php _e("For more complicated requests (several product, global sourcing...) Contact us directly!","armor-pharma");?></span>
      </a>
      <a class="picto-info" data-slug="help-design">
        <img src="<?php echo get_stylesheet_directory_uri() . '/assets/css/images/help.png';?>">
      </a>
    </div>
    <a id="" href="<?php echo get_the_permalink(get_page_by_path( 'design-the-lactose-you-like/validation/' ) );?>" class="button small next"><?php _e("NEXT","armor-pharma");?></a>
    <a href="<?php echo get_the_permalink(get_page_by_path( 'design-the-lactose-you-like/delivery' ) );?>" class="button small back"><?php _e("BACK","armor-pharma");?></a>
    <div id="congratulation" class="text-center">
      <h2 class="degrade semiCercle small"><?php the_title();?></h2><br />
      <img src="<?php echo get_stylesheet_directory_uri() . '/assets/css/images/summary.png';?>" class="pictoSummary">
      <p>
        <b><?php _e("You've completed all the items.","armor-pharma");?></b>
      </p>
      <p>
        <?php _e("Please verify your summary on the right, you can modifiy every step.","armor-pharma");?>
      </p>
    </div>
  </main>
  <div id="rightCol">


  </div>
</div>
<?php
endwhile; endif;
get_footer(); ?>
