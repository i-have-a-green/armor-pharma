<?php get_header(); ?>

  <div id="content-header">
    <div class="wrapper">
      <?php the_breadcrumb();?>
    </div>
  </div>
	<main id="main" role="main" class="wrapper">
		<article>
			<header class="wrapper" id="header-page">
				<h1 class="page-title degrade"><?php the_title();?></h1>
			</header>
			<section class="entry-content wrapper" itemprop="articleBody">
        <?php if (have_posts()) : while (have_posts()) : the_post();?>
          <article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article">
          	<header class="article-header">
          		<h2>
          			<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
          				<?php the_title(); ?>
          			</a>
          		</h2>
          	</header> <!-- end article header -->
          	<section class="entry-content" itemprop="articleBody">
          		<?php the_excerpt(); ?>
              <hr />
          	</section>
          </article> <!-- end article -->
        <?php endwhile; endif; ?>
			</section>
		</article>
	</main>

<?php get_footer(); ?>
