<?php
get_header();
/*
Template Name: EventNews
*/
?>
<?php if (have_posts()) : while (have_posts()) : the_post();?>
    <div id="content-header">
      <div class="wrapper">
        <?php the_breadcrumb();?>
      </div>
    </div>
    <div id="content-body" class="wrapper">
      
      <section id="news">
        <h1 class="degrade"><?php _e("News & Articles", 'armar-pharma');?></h1>
        <div id="header-news">
          <div>
              <?php the_field('content_news');?>
          </div>
          <div>
          <!--  <h2 class="degrade"><?php _e("Register your email & be informed", "armor-pharma");?></h2>
            <form class="form" name="newsletterInter">
            <input type="hidden" name="honey" value="">
              <input type="email" required name="emailNewsletter" id="emailNewsletter" placeholder="<?php _e("Your email", "armor-pharma");?>" />
              <button type="submit" id="buttonNewsletter">OK</button>
            </form>-->
          </div>
          <div id="follow-linkedin">
            <div>
              <a href="<?php echo get_theme_mod( 'wpgreen_share_linkedin' );?>" target="_blank">Follow<br />us on</a>
              <img src="<?php echo get_stylesheet_directory_uri() . '/assets/css/images/linkedin.png';?>" id="picto-linkedin" />
            </div>
          </div>
        </div>

        <div id="news-cat">
          <?php _e("CATEGORY", "armor-pharma");?> :
          <?php $terms = get_terms('category', array('hide_empty' => true));?>
          <form class="form">
          <?php foreach ($terms as $term): ?>
            <input type="checkbox" id="cat_<?php echo $term->term_id;?>" onclick="checkboxCat()"  value="<?php echo $term->term_id;?>" class="checkboxCat" >
            <label for="cat_<?php echo $term->term_id;?>" onclick="checkboxCat()"><?php echo $term->name;?></label>
          <?php endforeach;?>
          </form>
        </div>
  		  <div id="archive-news">
            <?php get_archive_news(); ?>
    		</div>
        <!--<div class="text-center">
          <hr class="hrTriangle">
            <a href="#" id="more-news" class="">More news</a>
        </div>-->

      </section>

  	</div>
<?php endwhile; endif; ?>
<?php get_footer(); ?>
