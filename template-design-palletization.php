<?php
/*
Template Name: Design palletization
*/

$user = get_current_user_id();
if($user == 0){
  wp_redirect( home_url(), 302 );
}
get_header();
$design = get_user_meta(get_current_user_id(), 'design', true);
$JSONdesign = $design;
$design = json_decode(stripslashes($design));
?>
<script>var design = <?php echo $JSONdesign;?>;</script>
<?php

get_header();
if (have_posts()) :  while ( have_posts() ) : the_post();
?>
<div id="content-header">
  <div class="wrapper">
    <?php the_breadcrumb();?>
  </div>
</div>
<div class="text-center">
  <h1 class="degrade"><?php _e("Design the lactose <b>you</b> like","armor-pharma");?></h1><br />
  <p class="degrade" id="titreProduct">
    <?php _e("How would you like your","armor-pharma");?> « <?php echo $design->lactose->name;?> »
  </p>
</div>
<?php
set_query_var( 'item', 4 );
get_template_part( 'template-parts/arianne', 'design' );?>
<div class="wrapper" id="contentDesign">
  <main>
    <div class="help">
      <a href="<?php echo get_the_permalink(get_page_by_path( "contact" ) );?>">
        <span><?php _e("For more complicated requests (several product, global sourcing...) Contact us directly!","armor-pharma");?></span>
      </a>
      <a class="picto-info" data-slug="help-design">
        <img src="<?php echo get_stylesheet_directory_uri() . '/assets/css/images/help.png';?>">
      </a>
    </div>
    <a href="<?php echo get_the_permalink(get_page_by_path( 'design-the-lactose-you-like/delivery' ) );?>" class="button small next" data-url="<?php echo get_the_permalink(get_page_by_path( 'design-the-lactose-you-like/registration' ) );?>"><?php _e("NEXT","armor-pharma");?></a>
    <a href="<?php echo get_the_permalink(get_page_by_path( 'design-the-lactose-you-like/packaging/' ) );?>" class="button small back" data-url="<?php echo get_the_permalink(get_page_by_path( 'design-the-lactose-you-like/my-customize-specification' ) );?>"><?php _e("BACK","armor-pharma");?></a>
    <h2 class="degrade semiCercle small"><?php the_title();?></h2><br />
    <div id="palletization" class="formDesign">
      <h3><?php _e("Select the type of pallet you would like :","armor-pharma");?></h3>
      <p>
        <?php _e("All pallets are filmed, have a protection cap on the top. For bags & cartons, a carton box protects the 2 first raws.","armor-pharma");?>
      </p>
      <div id="checkboxTypePallet">
      
        <?php
        $checked = "checked";
        if(isset($design->palet)):
          $checked = "";
          foreach ($design->palet->typePalet as $val){
            if($val->id == "checkboxTypePallet1"){
              $checked = "checked";
            }
          }
        endif;?>
        <input type="radio" <?php echo $checked;?> class="checkboxTypePallet optionRadio" name="checkboxTypePallet[]" id="checkboxTypePallet1" value="<?php _e("100 x 120 cm - Wooden (NIMP15 heat treated)","armor-pharma");?>" />
        <label for="checkboxTypePallet1" class="labelRadio">
          <?php _e("100 x 120 cm - Wooden (NIMP15 heat treated)","armor-pharma");?> <?php info('wooden');?>
        </label>
      
        <?php
        $checked = "";
        if(isset($design->palet)):
          $checked = "";
          foreach ($design->palet->typePalet as $val){
            if($val->id == "checkboxTypePallet3"){
              $checked = "checked";
            }
          }
        endif;?>
        <input type="radio" <?php echo $checked;?> class="checkboxTypePallet optionRadio" name="checkboxTypePallet[]" id="checkboxTypePallet3" value="<?php _e("80 x 120 cm - Wooden (NIMP15 heat treated)","armor-pharma");?>" />
        <label for="checkboxTypePallet3" class="labelRadio">
          <?php _e("80 x 120 cm - Wooden (NIMP15 heat treated)","armor-pharma");?> <?php info('wooden');?>
        </label>
      
        <?php
        $checked = "";
        if(isset($design->palet)):
          foreach ($design->palet->typePalet as $val){
            if($val->id == "checkboxTypePallet2"){
              $checked = "checked";
            }
          }
        endif;?>
        <input type="radio" <?php echo $checked;?> class="checkboxTypePallet optionRadio" name="checkboxTypePallet[]" id="checkboxTypePallet2" value="<?php _e("100 x 120 cm - Plastic","armor-pharma");?>" />
        <label for="checkboxTypePallet2" class="labelRadio">
          <?php _e("100 x 120 cm - Plastic","armor-pharma");?>
        </label>

        <?php
        $checked = "";
        if(isset($design->palet)):
          foreach ($design->palet->typePalet as $val){
            if($val->id == "checkboxTypePallet4"){
              $checked = "checked";
            }
          }
        endif;?>
        <input type="radio" <?php echo $checked;?> class="checkboxTypePallet optionRadio" name="checkboxTypePallet[]" id="checkboxTypePallet4" value="<?php _e("80 x 120 cm - Plastic","armor-pharma");?>" />
        <label for="checkboxTypePallet4" class="labelRadio">
          <?php _e("80 x 120 cm - Plastic","armor-pharma");?> 
        </label>
      </div>

      <div id="adjustPalet" class="formDesign">
        <h3><?php _e("Adjust my palet :","armor-pharma");?></h3>
        <div class="adjustPalet">
          <?php
          $value = 185;
           if(isset($design->palet)):
            foreach ($design->palet->adjustPalet as $val){
              if($val->id == "adjustPalet1"){
                $value = $val->value;
              }
            }
          endif;?>
          <div>
            <?php _e("Adjust maximum pallet height (cm)","armor-pharma");?>
          </div>
          <div>
            0
          </div>
          <div id="adjustPalet1"
            data-label="<?php _e("Adjust maximum pallet height (cm)","armor-pharma");?>"
            class="slider"
            data-min="0"
            data-max="185"
            data-value="<?php echo $value;?>"
            data-step="1">
            <div class="ui-slider-handle custom-handle"><span></span></div>
          </div>
          <div>
            185
          </div>
        </div>
        <!--<div class="adjustPalet">
          <?php
          $value = 0;
           if(isset($design->palet)):
            foreach ($design->palet->adjustPalet as $val){
              if($val->id == "adjustPalet2"){
                $value = $val->value;
              }
            }
          endif;?>
          <div>
            <?php _e("Lactose quantity per pallet (kg)will be adjusted automatically ","armor-pharma");?>
          </div>
          <div>
            0
          </div>
          <div id="adjustPalet2"
            data-label="<?php _e("Lactose quantity per pallet (kg) will be adjusted automatically","armor-pharma");?>"
            class="slider"
            data-min="0"
            data-max="1240"
            data-value="<?php echo $value;?>"
            data-step="40">
            <div class="ui-slider-handle custom-handle"><span></span></div>
          </div>
          <div>
            1240
          </div>
        </div>-->
        <div id="paletHelp">
          <h3><?php _e("Add your pallet labelling specific requirements :","armor-pharma");?></h3>
          <p>
            <?php _e("Upload your pallet label :","armor-pharma");?>
             <input type="file" id="filePalet" name="filePalet" />
          </p>
          <p class="form">
            <?php _e("or What should figure on it :","armor-pharma");?>
            <input type="text" class="targetPalet"
              value="<?php echo isset($design->targetPalet)?$design->targetPalet:'';?>"
              id="targetPalet">
          </p>
        </div>
      </div>
    </div>
  </main>
  <div id="rightCol">


  </div>
</div>
<?php
endwhile; endif;
get_footer(); ?>
