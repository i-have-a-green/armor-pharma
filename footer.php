  </div><!-- end .wrapper -->
    </div><!-- end #content -->
		<div id="footer">
      <hr>
			<footer role="contentinfo" class="wrapper" itemscope="itemscope" itemtype="http://schema.org/WPFooter">
        <div id="logo-footer">
          <?php
          $custom_logo_id = get_theme_mod( 'custom_logo' );
          if ( has_custom_logo() ) :
            echo wp_get_attachment_image( $custom_logo_id , 'wpgreen-logo' );
          else:
            echo get_bloginfo( 'name' );
          endif;
          $i = 0;
            $j=1;
          ?>
        </div>
        <nav id="nav-footer" role="navigation" aria-label="<?php esc_attr_e( 'Navigation secondaire', 'wpgreen' ); ?>"
				itemscope="itemscope" itemtype="http://schema.org/SiteNavigationElement">
					<?php wpgreen_footer_links('footer-links');?>
				</nav>
        <div id="logo-footer-savencia">
          <a href="http://www.savencia-fromagedairy.com/" target="_blank" class="attachment-wpgreen-logo size-wpgreen-logo">
            <img src="<?php echo get_stylesheet_directory_uri() . '/assets/css/images/savencia.png';?>" />
          </a>
        </div>
        <div id="logo-footer-armor">
          <a href="https://armor-dairy.com/" target="_blank">
            <img src="<?php echo get_stylesheet_directory_uri() . '/assets/css/images/armor.png';?>" />
          </a>
        </div>
			</footer> <!-- end .footer -->
      <div class="wrapper" id="under-footer">
        <div>
          <a href="<?php $p = get_page_by_path( "terms-conditions" ); echo get_the_permalink( $p);?>"><?php _e("Terms & conditions","armor-pharma");?></a> -
          <a href="<?php $p = get_page_by_path( "privacy-policy-cookie-policy" ); echo get_the_permalink( $p);?>"><?php _e("Privacy Policy","armor-pharma");?></a> -
          <a href="<?php echo get_privacy_policy_url();?>"><?php _e("Cookie Policy","armor-pharma");?></a>
        </div>
      </div>
		</div>

		<!-- MODAL -->
		<div class="modal" id="modal">
			<div class="modal-header">
				<h2 id="modal-title"></h2>
			</div>
			<div id="modal-content">
			</div>
			<div class="modal-footer">
				<p>
					<button class="button" onclick="closeModal()">
						&times; <?php _e('Close','wpgreen');?>
					</button>
				</p>
			</div>
		</div>

    <div class="modal" id="modalConnect">
			<div class="modal-header">
				<h2 id="modal-title"><?php _e("Connect","armor-pharma");?></h2>
			</div>
			<div class="modal-content">
        <?php get_template_part( 'template-parts/form', 'connect' );?>
			</div>
			<div class="modal-footer">
				<p>
					<button class="button" onclick="closeModal()">
						&times; <?php _e('Close','wpgreen');?>
					</button>
				</p>
			</div>
		</div>

		<div class="overlay" id="overlay"></div>
    <div id="returnTop">
      <span class="picto picto-top"></span>
      <?php _e("TOP","armor-pharma");?>
    </div>
    <?php
    if(isset($_GET['login']) && ($_GET['login']=='failed' || $_GET['login']=='1' )){
      echo '<script>document.addEventListener("DOMContentLoaded", function(event) {
        openModalConnect(0);});</script>';
    }

    ?>
    <div class="modal" id="modalcreate_user">
			<div class="modal-header">
				<h2 id="modal-title"><?php _e("Thank you!","armor-pharma");?></h2>
			</div>
			<div class="modal-content">
        <?php _e("You are now logged in. Your account will give you full acess to ARMOR PHARMA's website content.","armor-pharma");?>
			</div>
			<div class="modal-footer">
				<p>
					<button class="button" onclick="closeModal()">
						&times; <?php _e('Close','wpgreen');?>
					</button>
				</p>
			</div>
		</div>

	<?php	wp_footer();
  if(isset($_GET['create_user']) && $_GET['create_user']=='create_user' ){
    echo '<script>document.addEventListener("DOMContentLoaded", function(event) {
      document.getElementById("modalcreate_user").classList.add("openModal");
      document.getElementById("overlay").classList.add("overlayOpen");
      });</script>';
  }
  ?>
	</body>
</html> <!-- end page -->