<?php
add_filter('script_loader_src', 'agnostic_script_loader_src', 20,2);
function agnostic_script_loader_src($src, $handle) {
    return preg_replace('/^(http|https):/', '', $src);
}

add_filter('style_loader_src', 'agnostic_style_loader_src', 20,2);
function agnostic_style_loader_src($src, $handle) {
    return preg_replace('/^(http|https):/', '', $src);
}
require_once(get_stylesheet_directory() . '/inc/member.php');

// Theme support options
require_once(get_stylesheet_directory() . '/inc/theme-support.php');

// Register sidebars/widget areas
require_once(get_stylesheet_directory() . '/inc/sidebar.php');

// Register scripts and stylesheets
require_once(get_stylesheet_directory() . '/inc/enqueue-scripts.php');

// Register custom menus and menu walkers
require_once(get_stylesheet_directory() . '/inc/menu.php');

// Replace 'older/newer' post links with numbered navigation
require_once(get_stylesheet_directory() . '/inc/page-navi.php');

// ajouter des formats d'image
require_once(get_stylesheet_directory() . '/inc/add_image_size.php');

// Ajouter des options dans l'admin
//require_once(get_stylesheet_directory().'/inc/options.php');

// Use this as a template for custom post types
require_once(get_stylesheet_directory() . '/inc/custom-post-type.php');

// Customize the WordPress login menu
require_once(get_stylesheet_directory() . '/inc/login.php');

require_once(get_stylesheet_directory() . '/inc/acf.php');

//contact
require_once(get_stylesheet_directory() . '/inc/contact.php');
require_once(get_stylesheet_directory() . '/inc/event.php');

require_once(get_stylesheet_directory() . '/inc/csvUser.php');
require_once(get_stylesheet_directory() . '/inc/csvDesign.php');
require_once(get_stylesheet_directory() . '/inc/csvDesignAbandon.php');
require_once(get_stylesheet_directory() . '/inc/csvOrders.php');
require_once(get_stylesheet_directory() . '/inc/csvOrderAbandon.php');
require_once(get_stylesheet_directory() . '/inc/csvAskExpert.php');
require_once(get_stylesheet_directory() . '/inc/csvTechnicalDocumentation.php');
require_once(get_stylesheet_directory() . '/inc/csvQualityDocumentation.php');

require_once(get_stylesheet_directory() . '/inc/rgpd.php');



/************* CUSTOMIZE ADMIN *******************/

function wpreen_show_admin_bar() {
	return false;
}
add_filter('show_admin_bar' , 'wpreen_show_admin_bar');

// Custom Backend Footer
function wpgreen_custom_admin_footer()
{
  _e('<span id="footer-thankyou">Developed by <a href="https://www.wpgreen.co" target="_blank">WPgreen</a></span>.', 'wpgreen');
}
// adding it to the admin area
add_filter('admin_footer_text', 'wpgreen_custom_admin_footer');

//cache les erreurs de connexion
add_filter('login_errors', function () {
  return __("Mauvais identifiants ? <a href='" . site_url() . "/wp-login.php?action=lostpassword'>Mot de passe oublié ?</a>", "wpgreen");
});

//define('DISALLOW_FILE_EDIT', true);


add_filter('get_search_form', 'my_search_form');
function my_search_form($html)
{
  $html = '<form role="search" method="get" class="search-form form" action="">
					  <input type="search" class="search-field" placeholder="Search &hellip;" value="" name="s" />
				    <input type="submit" class="search-submit" value="&nbsp;" />
			      <input type="hidden" name="lang" value="en" />
          </form>';
  return $html;
}


function wpgreen_get_the_category()
{
  $category = get_the_category();
  $useCatLink = true;
  // If post has a category assigned.
  if ($category) {
    $category_display = '';
    $category_link = '';
    if (class_exists('WPSEO_Primary_Term')) {
  		// Show the post's 'Primary' category, if this Yoast feature is available, & one is set
      $wpseo_primary_term = new WPSEO_Primary_Term('category', get_the_id());
      $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
      $term = get_term($wpseo_primary_term);
      if (is_wp_error($term)) {
  			// Default to first category (not Yoast) if an error is returned
        $category_display = $category[0]->name;
        $category_link = get_category_link($category[0]->term_id);
      } else {
  			// Yoast Primary category
        $category_display = $term->name;
        $category_link = get_category_link($term->term_id);
      }
    } else {
  		// Default, display the first category in WP's list of assigned categories
      $category_display = $category[0]->name;
      $category_link = get_category_link($category[0]->term_id);
    }
  	// Display category
    if (!empty($category_display)) {
      if ($useCatLink == true && !empty($category_link)) {
  		//echo '<span class="post-category">';
  		//echo '<a href="'.$category_link.'">'.htmlspecialchars($category_display).'</a>';
  		//echo '</span>';
        echo '<span class="post-category">' . htmlspecialchars($category_display) . '</span>';
      } else {
        echo '<span class="post-category">' . htmlspecialchars($category_display) . '</span>';
      }
    }
  }
}

function wpgreen_get_the_category_id()
{
  $category = get_the_category();
  $useCatLink = true;
  // If post has a category assigned.
  if ($category) {
    $category_display = '';
    $category_link = '';
    if (class_exists('WPSEO_Primary_Term')) {
  		// Show the post's 'Primary' category, if this Yoast feature is available, & one is set
      $wpseo_primary_term = new WPSEO_Primary_Term('category', get_the_id());
      $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
      $term = get_term($wpseo_primary_term);
      if (is_wp_error($term)) {
        return $category[0]->term_id;
      } else {
        return $term->term_id;
      }
    } else {
      return $category[0]->term_id;
    }
  }
  return false;
}

/*add_filter( 'embed_oembed_html', 'wpse_embed_oembed_html', 99, 4 );
function wpse_embed_oembed_html( $cache, $url, $attr, $post_ID ) {
    return '<div class="embed-container">' . $cache . '</div>';
}*/

function wpgreen_excerpt($length)
{
  if (get_post_type(get_the_ID()) == 'event') {
    return 15;
  }
  return 60;
}
add_filter('excerpt_length', 'wpgreen_excerpt');

function get_excerpt($limit, $source = null)
{
  global $post;
  if ($source == "content" ? ($excerpt = get_the_content()) : ($excerpt = get_the_excerpt()));
  $excerpt = preg_replace(" (\[.*?\])", '', $excerpt);
  $excerpt = strip_shortcodes($excerpt);
  $excerpt = strip_tags($excerpt);
  $excerpt = substr($excerpt, 0, $limit);
  $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
  $excerpt = trim(preg_replace('/\s+/', ' ', $excerpt));
  $excerpt = $excerpt . '...';
  return $excerpt;
}

add_action('wp_ajax_morenews', 'get_archive_news_ajax');
add_action('wp_ajax_nopriv_morenews', 'get_archive_news_ajax');
function get_archive_news_ajax()
{
  get_archive_news(sanitize_text_field($_POST['page']));
  die();
}
function get_archive_news($paged = 1)
{
  global $post;
  $args = array(
    'paged' => $paged,
    'page' => $paged,
    'posts_per_page' => 5,
    'post_type' => 'post',
    'status' => 'publish'
  );
  if (isset($_POST['cat']) && !empty($_POST['cat'])) {
    $args['cat'] = substr($_POST['cat'], 0, -1);
  }

  $posts = get_posts($args);
  if ($posts) {
    echo '<div class="archive-news">';
    foreach ($posts as $post) {
      setup_postdata($post);
      get_template_part('template-parts/loop', 'archive');
    }
    echo '</div>';
    wp_reset_postdata();
  }
}

function configateurValeur($name, $label, $labelMin, $labelMax, $dataMin, $dataMax, $dataValueMin, $dataValueMax, $step = 1)
{
  ?>
  <div class="configurateur-value">
    <div class="configurateur-value-label">
      <?php echo $label; ?>
    </div>
    <div>
      <?php echo $labelMin; ?>
    </div>
    <div>
      <!--<div id="<?php echo $name; ?>" class="slider-custom" data-min="<?php echo $dataMin; ?>" data-max="<?php echo $dataMax; ?>" data-value="[<?php echo $dataValueMin; ?>,<?php echo $dataValueMax; ?>]" data-step="<?php echo $step; ?>">-->
      <div id="<?php echo $name; ?>" class="slider-custom" data-min="<?php echo $dataMin; ?>" data-max="<?php echo $dataMax; ?>" data-value="[<?php echo $dataMin; ?>,<?php echo $dataMax; ?>]" data-step="<?php echo $step; ?>">
        <div class="handle-min custom-handle ui-slider-handle"><span></span></div>
        <div class="handle-max custom-handle ui-slider-handle"><span></span></div>
      </div>
    </div>
    <div>
      <?php echo $labelMax; ?>
    </div>
  </div>

  <?php

}
/*function configateurValeur($name, $label, $labelMin, $labelMax, $dataMin, $dataMax, $dataValueMin, $dataValueMax, $step=1){
  ?>
  <div class="configurateur-value">
    <div class="configurateur-value-label">
      <?php echo $label;?>(TEST en %)
    </div>
    <div>
      0
    </div>
    <div>
      <div id="<?php echo $name;?>" class="slider-custom" data-min="0" data-max="100" data-value="[0,100]" data-step="1">
        <div class="handle-min custom-handle ui-slider-handle"><span></span></div>
        <div class="handle-max custom-handle ui-slider-handle"><span></span></div>
      </div>
    </div>
    <div>
      100
    </div>
  </div>

  <?php
}*/

add_action('wp_ajax_formConfigurateur', 'formConfigurateur');
add_action('wp_ajax_nopriv_formConfigurateur', 'formConfigurateur');
function formConfigurateur()
{
  $slidersData = json_decode(stripslashes($_POST["slidersData"]));
  $args = [];
  $verifAirjetSieveParticleSize = false;
  $boolApplication = false;
  foreach ($slidersData as $data) :
    if (!in_array($data->name, array("airjetSieveParticleSize", "psd_airjet", "application1", "application2", "application3", "application4", "application5"))) {
    if (in_array($data->name, array("poured_density", "tapped_density", "hausner_ratio", "carr_s_index"))) {
      $args[] = array(
        'key' => $data->name,
        'value' => array($data->values[0], $data->values[1]),
        'type' => 'DECIMALS',
        'compare' => 'BETWEEN',
      );
    } else if (in_array($data->name, array("psd_10", "psd_50", "psd_90"))) {
      $args[] = array(
        'key' => $data->name,
        'value' => array($data->values[0], $data->values[1]),
        'type' => 'NUMERIC',
        'compare' => 'BETWEEN',
      );
    }
  } else {
    if (!$boolApplication && $data->values == "true") {
      $tabApplication = array(
        'relation' => 'OR',
      );
      $boolApplication = true;
    }
    if ($data->name == "application1" && $data->values == "true") {
      $tabApplication[] = array(
        'key' => 'application',
        'value' => "comprime",
        'compare' => 'LIKE'
      );
    } elseif ($data->name == "application2" && $data->values == "true") {
      $tabApplication[] = array(
        'key' => 'application',
        'value' => "sachet",
        'compare' => 'LIKE'
      );
    } elseif ($data->name == "application3" && $data->values == "true") {
      $tabApplication[] = array(
        'key' => 'application',
        'value' => "effervescent",
        'compare' => 'LIKE'
      );
    } elseif ($data->name == "application4" && $data->values == "true") {
      $tabApplication[] = array(
        'key' => 'application',
        'value' => "gelules",
        'compare' => 'LIKE'
      );
    } elseif ($data->name == "airjetSieveParticleSize" && !empty($data->values)) {
      foreach ($slidersData as $data2) {
        if ($data2->name == "psd_airjet") {
          $specValues = array($data2->values[0], $data2->values[1]);
          $specLabel = $data->values;
          $verifAirjetSieveParticleSize = true;
        }
      }
    }
  }
  endforeach;
  global $wpdb;
  $posts = get_posts(array(
    'posts_per_page' => -1,
    'post_type' => 'lactose',
    'tax_query' => array(
      array(
        'taxonomy' => 'category-lactose',
        'terms' => array('excipure'),
        'field' => 'slug',
        'operator' => 'NOT IN',
      )
    ),
    'meta_query' => array(
      'relation' => 'AND',
      $args, $tabApplication
    ),
  ));

  if (!$posts) {
    _e("No result", "armor-pharma");
    die();
  }
  $first = true;
  global $post;
  foreach ($posts as $post) :
    $bool = true;
  setup_postdata($post);
  if ($verifAirjetSieveParticleSize) {
    $bool = false;
    $specs = get_field("specification");
    foreach ($specs as $spec) {
      if ($spec["label"] == $specLabel) {
        if ($specValues[0] <= $spec["value"] && $specValues[1] >= $spec["value"]) {
          $bool = true;
        }
      }
    }
  }
  if ($bool) :
    $pDesign = get_the_permalink(get_page_by_path("design-the-lactose-you-like/my-customize-specification"));
  if ($first) : $first = false;
  ?>
      <input type="hidden" id="nbResult" data-value="<?php echo sizeof($posts); ?>" />
      <table>
        <tr>
          <th>
            <?php _e("Products", "armor-pharma"); ?>
          </th>
          <th>
            <?php _e("Process", "armor-pharma"); ?>
          </th>
          <th>
            <?php _e("X10*", "armor-pharma"); ?>
          </th>
          <th>
            <?php _e("X50*", "armor-pharma"); ?>
          </th>
          <th>
            <?php _e("X90*", "armor-pharma"); ?>
          </th>
          <th>
            <?php _e("Poured density", "armor-pharma"); ?>
          </th>
          <th>
            <?php _e("Tapped density", "armor-pharma"); ?>
          </th>
          <th>
            <?php _e("Hausner ratio", "armor-pharma"); ?>
          </th>
          <th>
            <?php _e("Get free&nbsp;sample", "armor-pharma"); ?>
          </th>
          <th>
            <?php _e("Download spec", "armor-pharma"); ?>
          </th>
          <th>
            <?php _e("Customize it", "armor-pharma"); ?>
          </th>
          <th>
            <?php _e("View product", "armor-pharma"); ?>
          </th>
        </tr>
      <?php
      endif;
      $process = get_the_terms($post->ID, 'process-lactose');

      $dataLabel = [];
      $dataProductVolume = [];
      $dataProductCombinate = [];

      $volume = 0;
      $dataProductCombinate = "";
      if (have_rows('chart_simplifie')) :
        while (have_rows('chart_simplifie')) : the_row();
      $dataLabel[] = get_sub_field("particle_diameter");
      $dataProductVolume[] = get_sub_field("volume");
      $volume += get_sub_field("volume");
      $dataProductCombinate .= get_sub_field("volume") . ",";
        endwhile;
      endif;

      $line = "{'label': '" . get_the_lactose() . "',
					'backgroundColor': '" . get_field('color') . "',
					'borderColor': '" . get_field('color') . "',
					'data': [" . substr($dataProductCombinate, 0, -1) . "],
					'fill': false}";
      ?>
    <tr class="lactoseCofigurateur" data-volume="<?php echo $line; ?>" data-label="<?php echo str_replace('"', "'", json_encode($dataLabel)); ?>" data-set="{'label':'<?php echo get_the_lactose(); ?>',
      'data':[<?php the_field('graphe_compressibility'); ?>,
        <?php the_field('graphe_flowability'); ?>,
        <?php the_field('graphe_cohesivness'); ?>,
        <?php the_field('graphe_poured_density'); ?>,
        <?php the_field('graphe_psd_50'); ?>,
        <?php the_field('graphe_psd_span'); ?>,
        <?php the_field('graphe_specific_surface_area'); ?>],
      'dataLabel':[<?php the_field('carr_s_index'); ?>,
        <?php the_field('hausner_ratio'); ?>,
        <?php the_field('tapped_density'); ?>,
        <?php the_field('poured_density'); ?>,
        <?php the_field('psd_50'); ?>,
        <?php the_field('psd_airjet'); ?>,
        <?php the_field('specific_surface_area'); ?>],
      'fill':true,
      'backgroundColor':'<?php the_field('color'); ?>00',
      'borderColor':'<?php the_field('color'); ?>',
      'pointBackgroundColor':'<?php the_field('color'); ?>',
      'pointBorderColor':'#fff',
      'pointHoverBackgroundColor':'#fff',
      'pointHoverBorderColor':'<?php the_field('color'); ?>'}">
      <td>
        <?php echo get_the_lactose(); ?>
        <?php //the_field("application");?>
      </td>
      <td>
        <?php echo $process[0]->name; ?>
      </td>
      <td>
        <?php the_field("psd_10"); ?>
      </td>
      <td>
        <?php the_field("psd_50"); ?>
      </td>
      <td>
        <?php the_field("psd_90"); ?>
      </td>
      <td>
        <?php the_field("poured_density"); ?>
      </td>
      <td>
        <?php the_field("tapped_density"); ?>
      </td>
      <td>
        <?php the_field("hausner_ratio"); ?>
      </td>
      <td>
        <a href="#"  data-lactose-id="<?php the_id(); ?>" class="cartLink"><span class="picto picto-cart"></span></a>
      </td>
      <td>
        <?php if (!empty(get_field("spec"))) : ?>
          <a href="<?php the_field("spec"); ?>" download><span class="picto picto-download"></span></a>
        <?php endif; ?>
      </td>
      <td>
        <a href="<?php echo add_query_arg('id', get_the_id(), $pDesign); ?>" class="button small customized"><?php _e("GET IT CUSTOMIZED", "armor-pharma"); ?></a>
      </td>
      <td>
        <a href="<?php the_permalink(); ?>" class="button small"><?php _e("View Product", "armor-pharma"); ?></a>
      </td>
    </tr>
    <?php
    endif;
    endforeach;
    wp_reset_postdata();
    echo '</table>';
    echo '<em>*Laser diffraction, Malvern wet.</em>';
    echo '<div id="graphConfig"><canvas id="chartjsConfigurateurCourbe" class="chartjs" width="500" height="500"></canvas>';
    echo '<canvas id="chartjsConfigurateur" class="chartjs" width="500" height="500"></canvas></div>';
    echo '<div id="resultMobile"><h2>' . __("Products", "armor-pharma") . '</h2>';
    foreach ($posts as $post) :
      setup_postdata($post);
    $process = get_the_terms($post->ID, 'process-lactose');
    ?>
    <div>
      <h3 class="productMobile" data-id="<?php echo get_the_id(); ?>"><?php echo get_the_lactose(); ?></h3>
      <div id="contProductMobile<?php echo get_the_id(); ?>" class="contProductMobile">
        <div>
          <b><?php _e("Process", "armor-pharma"); ?></b> : <?php echo $process[0]->name; ?>
        </div>
        <div>
          <b><?php _e("X10*", "armor-pharma"); ?></b> : <?php the_field("psd_10"); ?>
        </div>
        <div>
          <b><?php _e("X50*", "armor-pharma"); ?></b> : <?php the_field("psd_50"); ?>
        </div>
        <div>
          <b><?php _e("X90*", "armor-pharma"); ?></b> : <?php the_field("psd_90"); ?>
        </div>
        <div>
          <b><?php _e("Poured density", "armor-pharma"); ?></b> : <?php the_field("poured_density"); ?>
        </div>
        <div>
          <b><?php _e("Tapped density", "armor-pharma"); ?></b> : <?php the_field("tapped_density"); ?>
        </div>
        <div>
          <b><?php _e("Hausner ratio", "armor-pharma"); ?></b> : <?php the_field("hausner_ratio"); ?>
        </div>
        <div>
          <b><?php _e("Get freesample", "armor-pharma"); ?></b> : <a href="#"  data-lactose-id="<?php the_id(); ?>" class="cartLink"><span class="picto picto-cart"></span></a>
        </div>
        <div>
          <b><?php _e("Download spec", "armor-pharma"); ?></b> : <?php if (!empty(get_field("spec"))) : ?>
            <a href="<?php the_field("spec"); ?>" download><span class="picto picto-download"></span></a>
          <?php endif; ?>
        </div>
        <div class="text-center">
          <a href="<?php the_permalink(); ?>" class="button small customized"><?php _e("GET IT CUSTOMIZED", "armor-pharma"); ?></a>
        </div>
        <div class="text-center">
          <a href="<?php the_permalink(); ?>" class="buton small"><?php _e("View Product", "armor-pharma"); ?></a>
        </div>
      </div>
    </div>
    <?php
    endforeach;
    wp_reset_postdata();
    echo '<em>*Laser diffraction, Malvern wet.</em></div>';
    echo '<div class="text-center"><span class="picto picto-info" id="picto-info-graph" data-slug="enlarge"></span></div>';
    die();
  }

  function info($slug)
  {
    echo '<span class="picto picto-info" data-slug="' . $slug . '"></span>';
  }

  add_action('wp_ajax_displayInfo', 'displayInfo');
  add_action('wp_ajax_nopriv_displayInfo', 'displayInfo');
  function displayInfo()
  {
    $post = get_page_by_path( sanitize_text_field($_POST["slug"]), OBJECT, 'info');
    echo json_encode(array("title" => $post->post_title, "content" => $post->post_content));
    die();
  }


  add_action('wp_login_failed', 'my_front_end_login_fail');  // hook failed login
  function my_front_end_login_fail($username)
  {
    $referrer = $_SERVER['HTTP_REFERER'];  // where did the post submission come from?
   // if there's a valid referrer, and it's not the default log-in screen
    if (!empty($referrer) && !strstr($referrer, 'wp-login') && !strstr($referrer, 'wp-admin')) {
      if (substr_count($referrer, '?')) {
        wp_redirect($referrer . '&login=failed');  // let's append some information (login=failed) to the URL for the theme to use
      } else {
        wp_redirect($referrer . '?login=failed');  // let's append some information (login=failed) to the URL for the theme to use
      }
      exit;
    }
  }


  add_action('wp_ajax_addToCart', 'addToCart');
  add_action('wp_ajax_nopriv_addToCart', 'addToCart');
  function addToCart()
  {
    $user = get_current_user_id();
    if ($user > 0) {
      update_user_meta(get_current_user_id(), 'cart', sanitize_text_field($_POST['cart']));
      echo sanitize_text_field($_POST['cart']);
    }
    die();
  }

  function nbLactoseInCart()
  {
    $userId = get_current_user_id();
    $cart = [];
    if ($userId > 0) {
      if (isset($_COOKIE['cart'])) {
        update_user_meta($userId, 'cart', $_COOKIE['cart']);
        $cart = json_decode(stripslashes($_COOKIE['cart']));
      } else {
        $metaCart = get_user_meta($userId, 'cart', true);
        if ($metaCart !== false) {
          setcookie("cart", $metaCart, false, "/");
          $cart = json_decode(stripslashes($metaCart));
        }
      }
    } else if (isset($_COOKIE['cart'])) {
      $cart = json_decode(stripslashes($_COOKIE['cart']));
    }
    //$nb = sizeof($cart);
    if (is_array($cart)) {
      return '(' . sizeof($cart) . ')';
    }
    else if(!empty($cart)) {
      return '(' . $cart . ')';
    }
    return '';
  }

  add_action('wp_ajax_newOrder', 'newOrder');
  add_action('wp_ajax_nopriv_newOrder', 'newOrder');
  function newOrder()
  {
    $user = get_current_user_id();
    if ($user > 0) {
      $cart = get_user_meta($user, 'cart', true);
      $orders = get_user_meta($user, 'orders', true);
      $cart = json_decode(stripslashes($cart));
      $orders = (array)json_decode(stripslashes($orders));
      $nbOrders = sizeof($orders);

      if ($nbOrders == 0) {
        $orders = array(array(
          "adress" => sanitize_text_field($_POST['address']),
          "cart" => $cart,
          "date" => date("Y-m-d H:i")
        ));
      } else {
        $orders[] = array(
          "adress" => sanitize_text_field($_POST['address']),
          "cart" => $cart,
          "date" => date("Y-m-d H:i")
        );
      }

      $user = wp_get_current_user();

      $subject = __("My ARMOR PHARMA lactose sample request", "armor-parma");
      $body = __("Shipment address", "armor-pharma") . " : " . sanitize_text_field($_POST['address']) . "\r\n";
      $body .= $user->civilite . ' ' . $user->user_firstname . " " . $user->user_lastname . "\r\n";
      $body .= 'country : ' . $user->country . "\r\n";
      $body .= 'phone : ' . $user->phone . "\r\n";
      $body .= 'profil : ' . $user->profil . "\r\n";
      $body .= 'company : ' . $user->company . "\r\n";
      $body .= 'email : ' . $user->user_email . "\r\n\r\n";

      $metaCart = get_user_meta(get_current_user_id(), 'cart', true);
      $metaCart = json_decode($metaCart);
      foreach ($metaCart as $lactose) :
        $body .= get_the_title($lactose->id_lactose) . ' - ' . $lactose->quantity . ' - ' . $lactose->formulation . ' - ' . $lactose->tablet . ' - ' . $lactose->specifics . "\r\n\r\n";
      endforeach;



      $headers[] = 'From: ' . get_bloginfo('name') . ' <' . __('no-reply@', 'wpgreen') . str_replace('www.', '', $_SERVER['SERVER_NAME']) . '>';
		//wp_mail( get_option( 'admin_email'), $subject, $body, $headers);
      wp_mail("samples@armor-pharma.com", $subject, $body, $headers);

      $body = __('<b>Thank you for contacting ARMOR PHARMA !</b>
    <p>
      Your request has been successfully submitted and we will revert to you in the shortest delay
    </p><p>Kink regards<br />
    ARMOR PHARMA’s team
    </p>', 'wpgreen');
      wp_mail($user->user_email, __("My ARMOR PHARMA lactose sample request", "armor-parma"), $body, $headers);

      update_user_meta(get_current_user_id(), 'orders', json_encode($orders));
      delete_user_meta(get_current_user_id(), 'cart');
      setcookie("cart", '', time(), "/");
    }
    die();
  }

  
  add_action('wp_ajax_technicalLibraryUserDownload', 'technicalLibraryUserDownload');
  add_action('wp_ajax_nopriv_technicalLibraryUserDownload', 'technicalLibraryUserDownload');
  function technicalLibraryUserDownload()
  {
    $user = get_current_user_id();
    if ($user > 0) {
      update_user_meta(get_current_user_id(), 'technicalDocumentation', $_POST["data"]);
    }
  }

  add_action('wp_ajax_qualityLibraryUserDownload', 'qualityLibraryUserDownload');
  add_action('wp_ajax_nopriv_qualityLibraryUserDownload', 'qualityLibraryUserDownload');
  function qualityLibraryUserDownload()
  {
    $user = get_current_user_id();
    if ($user > 0) {
      update_user_meta(get_current_user_id(), 'qualityDocumentation', $_POST["data"]);
    }
  }

  add_action('wp_ajax_technicalLibraryZipFile', 'technicalLibraryZipFile');
  add_action('wp_ajax_nopriv_technicalLibraryZipFile', 'technicalLibraryZipFile');
  function technicalLibraryZipFile()
  {
    $user = get_current_user_id();
    if ($user > 0) {
      $files = json_decode(stripslashes($_POST["files"]));
      $current_user = wp_get_current_user();
      $upload_dir = wp_upload_dir();
      $user_dirname = $upload_dir['basedir'] . '/zip/' . sanitize_file_name($current_user->display_name);

      if (!file_exists($user_dirname)) {
        wp_mkdir_p($user_dirname);
      }

      $zip = new ZipArchive;
      if ($zip->open($user_dirname . '/armor-pharma_' . date('m-d-Y_hia') . '.zip', ZipArchive::CREATE) === true) {
        foreach ($files as $idFile) {
          $fichier = get_attached_file($idFile);
          $zip->addFile($fichier, sanitize_file_name(basename($fichier)));
        }
        $zip->close();
        echo $upload_dir['baseurl'] . '/zip/' . sanitize_file_name($current_user->display_name) . '/armor-pharma_' . date('m-d-Y_hia') . '.zip';
      }
    }

    die();
  }


  /*add_action('wp_ajax_formExpert', 'formExpert');
  add_action('wp_ajax_nopriv_formExpert', 'formExpert');*/
  add_action(
    'rest_api_init',
    function() {
          // Enregistrement de la route REST
      register_rest_route(
        'ihag',
        'formExpert',
        array(
          'methods'               => 'POST',
          'callback'              => 'formExpert',
          'permission_callback' => '__return_true',
          'args'                  => array(),
        )
      );
      }
  );
  function formExpert(WP_REST_Request $request)
  {
    $params = $request->get_params();
    $upload_file_text = "";
    if(isset($params['honey']) && !empty($params['honey'])){
      return new WP_REST_Response( array( 'status' => 401, 'message' => 'honey') );
    }

    if(check_nonce()){
      $attachments = array(); 
      if (isset($_FILES['specFile'])) {
        $maintenant = date("d-m-Y_H:i:s");
        $upload_dir = wp_upload_dir();
        $uploaddirimg = $upload_dir['basedir'] . '/form/';
        mkdir($uploaddirimg, 0755);
        $uploadfile = $uploaddirimg . $maintenant . '-' . basename($_FILES['specFile']['name']);
        if (move_uploaded_file($_FILES['specFile']['tmp_name'], $uploadfile)) {
          array_push($attachments, $uploadfile);
          $upload_file_text = "Fichier : " . $upload_dir['baseurl'] . '/img-form/' . $maintenant . '-' . basename($_FILES['specFile']['name']);
        }
      }


      $subject = sanitize_text_field($params['subject']);
      $body = sanitize_text_field($params['genre']) . ' ' . sanitize_text_field($params['firstname']) . ' ' . sanitize_text_field($params['lastname']) . "\r\n";
      $body .= 'country : ' . sanitize_text_field($params['country']) . "\r\n";
      $body .= 'phone : ' . sanitize_text_field($params['phone']) . "\r\n";
      $body .= 'profil : ' . sanitize_text_field($params['profil']) . "\r\n";
      $body .= 'company : ' . sanitize_text_field($params['company']) . "\r\n";
      $body .= 'email : ' . sanitize_text_field($params['email']) . "\r\n";

      if(isset($params["exipure_by"])){
        foreach($params["exipure_by"] as $by){
          $body .= 'contact by : ' . $by . "\r\n";
        }
      }

      if(isset($params["date"])){
         $body .= 'date : ' . $params["date"] . "\r\n";
      }
      if(isset($params["authorize"])){
        $body .= 'authorize : ' . $params["authorize"] . "\r\n";
      }
      
      
      $body .= 'subject : ' . sanitize_text_field($params['subject']) . "\r\n";
      $body .= 'comments : ' . sanitize_textarea_field($params['comments']) . "\r\n";

      $body .= $upload_file_text;
      $headers[] = 'From: ' . get_bloginfo('name') . ' <' . __('no-reply@', 'wpgreen') . str_replace('www.', '', $_SERVER['SERVER_NAME']) . '>';
      wp_mail(get_option('admin_email'), $subject, $body, $headers, $attachments);

      if (isset($params["distributorId"]) && !empty($params["distributorId"])) {
        wp_mail(get_field("email", $params["distributorId"]), $subject.' ('.get_field("email", $params["distributorId"]).')', $body, $headers);
      }
      if (isset($params["teamEmail"]) && !empty($params["teamEmail"])) {
        wp_mail(sanitize_email($params["teamEmail"]), $subject.' ('.sanitize_email($params["teamEmail"]).')', $body, $headers);
      }

      $post['post_type'] = 'ask_expert';
      $post['post_status'] = 'publish';
      $post['post_title'] = sanitize_text_field($params['firstname']) . ' ' . sanitize_text_field($params['lastname']) . ' (' . sanitize_text_field($params['subject']) . ')';
      $post['post_content'] = $body;
      wp_insert_post($post, true);

      $body = __('<b>Thank you for contacting ARMOR PHARMA !</b>
<p>
  Your request has been successfully submitted and we will revert to you in the shortest delay
</p>
<p>
Kind regards<br />
ARMOR PHARMA’s team
</p>', 'wpgreen');
      $headers[] = 'From: ' . get_bloginfo('name') . ' <' . WPGREEN_EMAIL_FROM . '>';
      $wp_mail = wp_mail(sanitize_email($params['email']), sanitize_text_field($params['subject']), $body, $headers);
      return new WP_REST_Response( array( 'status' => 200, 'message' => '') );
    }

    return new WP_REST_Response( array( 'status' => 401, 'message' => '') );

  }

  add_action('init', 'wpgreen2_init');
  function wpgreen2_init()
  {
    $GLOBALS['wp_rewrite']->use_verbose_page_rules = true;
  }

  add_filter('page_rewrite_rules', 'wpgreen_collect_page_rewrite_rules');
  function wpgreen_collect_page_rewrite_rules($page_rewrite_rules)
  {
    $GLOBALS['wpgreen_page_rewrite_rules'] = $page_rewrite_rules;
    return array();
  }

  add_filter('rewrite_rules_array', 'wpgreen_prepend_page_rewrite_rules');
  function wpgreen_prepend_page_rewrite_rules($rewrite_rules)
  {
    return $GLOBALS['wpgreen_page_rewrite_rules'] + $rewrite_rules;
  }


  add_action(
    'rest_api_init',
    function() {
          // Enregistrement de la route REST
      register_rest_route(
        'ihag',
        'formCoa',
        array(
          'methods'               => 'POST',
          'callback'              => 'formCoa',
          'permission_callback'   => function () {
                     return is_user_logged_in();
                  },
          'args'                  => array(),
        )
      );
      }
  );
  function formCOA(WP_REST_Request $request)
  {
    $params = $request->get_params();
    if(isset($params['honey']) && !empty($params['honey'])){
      return new WP_REST_Response( array( 'status' => 403, '' ) );
    }
    if(check_nonce()){
      $upload_dir = wp_upload_dir();
      $coaDir = $upload_dir['basedir'] . '/coa/';
      $file = $coaDir . $params['product'] . '/CoA-' . $params['number'] . '-' . str_replace("/", "", $params['date']) . '.pdf';
      if (is_file($file)) {
        echo '<a target="_blank" href="' . $upload_dir['baseurl'] . '/coa/' . $params['product'] . '/CoA-' . $params['number'] . '-' . str_replace('/', '', $params['date']) . '.pdf' . '" class="button" download="CoA.pdf">Download</a>';
        $coa = get_user_meta(get_current_user_id(), 'coa', true);
        $coa = json_decode(stripslashes($coa));
        $nbCoa = sizeof($coa);
        $arrayCoa = array(
          "label" => $params['product'] . ' - ' . $params['number'] . ' ' . $params['date'],
          "url" => $upload_dir['baseurl'] . '/coa/' . $params['product'] . '/CoA-' . $params['number'] . '-' . str_replace('/', '', $params['date']) . '.pdf',
          "date" => date("Y-m-d H:i")
        );
        if ($nbCoa == 0) {
          $coa = array($arrayCoa);
        } else {
          $dejaPresent = false;
          foreach ($coa as $val) {
            if ($val->url == $upload_dir['baseurl'] . '/coa/' . $params['product'] . '/CoA-' . $params['number'] . '-' . str_replace('/', '', $params['date']) . '.pdf') {
              $dejaPresent = true;
            }
          }
          if (!$dejaPresent) {
            $coa[] = $arrayCoa;
          }
        }
        update_user_meta(get_current_user_id(), 'coa', json_encode($coa));
        return new WP_REST_Response( array( 'status' => 200, '' ) );
      } 
    }
    return new WP_REST_Response( array( 'status' => 403, '' ) );
  }

  if (!function_exists('get_primary_taxonomy_id')) {
    function get_primary_taxonomy_id($post_id, $taxonomy)
    {
      $prm_term = '';
      if (class_exists('WPSEO_Primary_Term')) {
        $wpseo_primary_term = new WPSEO_Primary_Term($taxonomy, $post_id);
        $prm_term = $wpseo_primary_term->get_primary_term();
      }
      if (!is_object($wpseo_primary_term) && empty($prm_term)) {
        $term = wp_get_post_terms($post_id, $taxonomy);
        if (isset($term) && !empty($term)) {
          return wp_get_post_terms($post_id, $taxonomy)[0]->term_id;
        } else {
          return '';
        }
      }
      return $wpseo_primary_term->get_primary_term();
    }
  }

  function get_the_lactose()
  {
    global $post;

    $category = get_primary_taxonomy_id(get_the_id(), 'category-lactose');
    $category = get_term($category);

    return strtoupper($category->name) . ' ' . get_the_title();
  }


  add_action('wp_ajax_design', 'design');
  add_action('wp_ajax_nopriv_design', 'design');
  function design()
  {
    $user = get_current_user_id();
    if ($user > 0) {
      update_user_meta(get_current_user_id(), 'design', sanitize_text_field($_POST['design']));
      rightCol();
    }
    die();
  }

  add_action('wp_ajax_rightCol', 'rightCol');
  add_action('wp_ajax_nopriv_rightCol', 'rightCol');
  function rightCol()
  {
    get_template_part('template-parts/design', 'right');
    die();
  }

  add_action('wp_ajax_file_specification', 'file_specification');
  add_action('wp_ajax_nopriv_file_specification', 'file_specification');
  function file_specification()
  {

    $user = get_current_user_id();
    if ($user > 0) {
      $maintenant = date("d-m-Y_H:i:s");
      $upload_dir = wp_upload_dir();
      $uploaddirimg = $upload_dir['basedir'] . '/fileSpecification/';
      if (!file_exists($uploaddirimg)) {
        wp_mkdir_p($uploaddirimg);
      }
      $uploadfile = $uploaddirimg . $maintenant . '-' . basename($_FILES['file_specification']['name']);
      $upload_file_text = "";
      if (move_uploaded_file($_FILES['file_specification']['tmp_name'], $uploadfile)) {
        $upload_file_text = $upload_dir['baseurl'] . '/fileSpecification/' . $maintenant . '-' . basename($_FILES['file_specification']['name']);
      }
      echo '{"file":"' . basename($_FILES['file_specification']['name']) . '","link":"' . $upload_file_text . '"}';
    }
    die();
  }


  add_action('wp_ajax_filePackaging', 'filePackaging');
  add_action('wp_ajax_nopriv_filePackaging', 'filePackaging');
  function filePackaging()
  {

    $user = get_current_user_id();
    if ($user > 0) {
      $maintenant = date("d-m-Y_H:i:s");
      $upload_dir = wp_upload_dir();
      $uploaddirimg = $upload_dir['basedir'] . '/filePackaging/';
      if (!file_exists($uploaddirimg)) {
        wp_mkdir_p($uploaddirimg);
      }
      $uploadfile = $uploaddirimg . $maintenant . '-' . basename($_FILES['filePackaging']['name']);
      $upload_file_text = "";
      if (move_uploaded_file($_FILES['filePackaging']['tmp_name'], $uploadfile)) {
        $upload_file_text = $upload_dir['baseurl'] . '/filePackaging/' . $maintenant . '-' . basename($_FILES['filePackaging']['name']);
      }
      echo '{"file":"' . basename($_FILES['filePackaging']['name']) . '","link":"' . $upload_file_text . '"}';
    }
    die();
  }


  add_action('wp_ajax_filePalet', 'filePalet');
  add_action('wp_ajax_nopriv_filePalet', 'filePalet');
  function filePalet()
  {

    $user = get_current_user_id();
    if ($user > 0) {
      $maintenant = date("d-m-Y_H:i:s");
      $upload_dir = wp_upload_dir();
      $uploaddirimg = $upload_dir['basedir'] . '/filePalet/';
      if (!file_exists($uploaddirimg)) {
        wp_mkdir_p($uploaddirimg);
      }
      $uploadfile = $uploaddirimg . $maintenant . '-' . basename($_FILES['filePalet']['name']);
      $upload_file_text = "";
      if (move_uploaded_file($_FILES['filePalet']['tmp_name'], $uploadfile)) {
        $upload_file_text = $upload_dir['baseurl'] . '/filePalet/' . $maintenant . '-' . basename($_FILES['filePalet']['name']);
      }
      echo '{"file":"' . basename($_FILES['filePalet']['name']) . '","link":"' . $upload_file_text . '"}';
    }
    die();
  }

  function wpgreen_youtube($html, $url, $args)
  {
    if (strstr($html, 'youtube.com/embed/')) {
      $html = str_replace('?feature=oembed', '?feature=oembed&fs=0&controls=0&rel=0&showinfo=0', $html);
    }
    return '<div class="embed-container">' . $html . '</div>';
  }
  add_filter('oembed_result', 'wpgreen_youtube', 10, 3);


  add_action('wp_ajax_modalDistributor', 'modalDistributor');
  add_action('wp_ajax_nopriv_modalDistributor', 'modalDistributor');
  function modalDistributor()
  {
    $id = (int)sanitize_text_field($_POST["distributor"]);
    ?>
  <div id="modalDistributor">
    <h3><?php the_field('Distributor', $id); ?></h3>
    <div class="grid">
      <div>
        <img src="<?php echo get_stylesheet_directory_uri() . '/assets/css/images/distributorContact.png'; ?>" />
      </div>
      <div>
        <?php the_field("contact", $id); ?>
      </div>

      <div><img src="<?php echo get_stylesheet_directory_uri() . '/assets/css/images/distributorWeb.png'; ?>" /></div>
      <div>
        <a href="<?php the_field("website", $id); ?>" target="_blank"><?php the_field("website", $id); ?></a>
      </div>

      <div><img src="<?php echo get_stylesheet_directory_uri() . '/assets/css/images/distributorPhone.png'; ?>" /></div>
      <div>
        Tel : <?php the_field("tel_fixe", $id); ?><br />
        Mobile : <?php the_field("tel_mob", $id); ?>
      </div>

      <div><img src="<?php echo get_stylesheet_directory_uri() . '/assets/css/images/distributorMap.png'; ?>" /></div>
      <div>
        <?php the_field("address", $id); ?>
      </div>
    </div>
    <div class="grid">
      <div>
        <button id="contactDistributorModal" class="button" data-name="<?php the_field("contact", $id); ?>" data-id="<?php echo $id; ?>"><?php _e("GET IN TOUCH", "armor-pharma"); ?></button>
      </div>
      <div>
        <?php $location = get_field('coordonnees', $id); ?>
        <a class="button" target="_blank" id="mapDistributorModal" href="https://www.google.com/maps/place/<?php echo $location['lat'];?>+<?php echo $location['lng'];?>/@<?php echo $location['lng'];?>,<?php echo $location['lat'];?>z"><?php _e("GET DIRECTION", "armor-pharma"); ?></a>
      </div>
    </div>
  </div>
  <?php
  die();
}
add_action('wp_ajax_inscriptionNewsletter', 'inscriptionNewsletter');
add_action('wp_ajax_nopriv_inscriptionNewsletter', 'inscriptionNewsletter');
function inscriptionNewsletter()
{
  if (wp_verify_nonce($_POST['nonceformNewsletter'], 'nonceformNewsletter')) {
    if (isset($_POST["newsletter"])) {
      if (!empty($_POST['newsletter'])) {
        $infos = array(
          'first_name' => sanitize_text_field($_POST['email']),
          'last_name' => sanitize_text_field($_POST['email']),
          'user_login' => sanitize_text_field($_POST['email']), // Le login est son email :-)
          'user_email' => sanitize_text_field($_POST['email']),
          'user_pass' => null,
          'display_name' => sanitize_text_field($_POST['email']),
        );
        $id = wp_insert_user($infos);
        $list_ids = [];
        foreach ($_POST['newsletter'] as $val) {
          $list_ids[] = sanitize_text_field($val);
        }
        try {
          \MailPoet\API\API::MP('v1')->subscribeToLists(sanitize_text_field($_POST['email']), $list_ids);
        } catch (Exception $exception) {
          //echo $exception->getMessage();
        }
      }
    }
  }
  wp_die();
}

/*add_filter("login_redirect", "wpgreen_subscriber_login_redirect", 10, 3);
function wpgreen_subscriber_login_redirect($redirect_to, $request, $user) {
  if(is_array($user->roles)){
      if(in_array('administrator', $user->roles)){
        return site_url('/wp-admin/');
      }
  }
  return home_url();
}*/
function wpgreen_lost_password_redirect()
{
  wp_redirect(home_url());
  exit;
}
add_action('after_password_reset', 'wpgreen_lost_password_redirect');


function mailpoetUserList($idUser, $idList)
{
  try {
    $subscriber = \MailPoet\API\API::MP('v1')->getSubscriber($idUser);
    foreach ($subscriber["subscriptions"] as $subscription) {
      if ($subscription["segment_id"] == $idList) {
        return $subscription["status"];
      }
    }
  } catch (Exception $exception) {
    return '';
    //return $exception->getMessage();
  }
  return '';
}

// Last connection from User
add_action( 'wp_login', 'last_time_login_ihag', 10, 2 );

function last_time_login_ihag( $user_login, $user ) {
    update_user_meta( $user->ID, 'last_login', time() );
}

function check_nonce() {
	global $wp_rest_auth_cookie;
	/*
	 * Is cookie authentication being used? (If we get an auth
	 * error, but we're still logged in, another authentication
	 * must have been used.)
	 */
	if ( true !== $wp_rest_auth_cookie && is_user_logged_in() ) {
		return false;
	}
	// Is there a nonce?
	$nonce = null;
	if ( isset( $_REQUEST['_wp_rest_nonce'] ) ) {
		$nonce = $_REQUEST['_wp_rest_nonce'];
	} elseif ( isset( $_SERVER['HTTP_X_WP_NONCE'] ) ) {
		$nonce = $_SERVER['HTTP_X_WP_NONCE'];
	}
	if ( null === $nonce ) {
		// No nonce at all, so act as if it's an unauthenticated request.
		wp_set_current_user( 0 );
		return false;
	}
	// Check the nonce.
	return wp_verify_nonce( $nonce, 'wp_rest' );
}
