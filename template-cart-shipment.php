<?php
/*
Template Name: Cart Shipment
*/
get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post();?>
  <div id="content-header">
    <div class="wrapper">
      <?php the_breadcrumb();?>
    </div>
  </div>
	<main id="main" role="main" class="wrapper">
		<article>
			<header class="wrapper" id="header-page">
				<h1 class="page-title degrade"><?php the_title();?></h1>
			</header>
			<div id="contentCartShipment">
			    <div>
            <h2><?php _e("Shipment address","armor-pharma");?></h2>
          </div>
          <div>
            <h2><?php
            $metaCart = get_user_meta(get_current_user_id(), 'cart', true);
            $metaCart = json_decode($metaCart);
            echo sizeof($metaCart);
            ?>  <?php _e("products","armor-pharma");?></h2>
          </div>
          <div id="shipmentAdresse">
            <!--<span class="picto picto-update">Modify</span>-->
            <h3><?php echo get_user_meta(get_current_user_id(), 'company', true);?></h3>

            <?php
              $orders = json_decode(get_user_meta(get_current_user_id(), 'orders', true));
              $address = "";
              if(!empty($orders) && sizeof($orders) > 0):
                end($orders);
                $address = $orders[key($orders)]->adress;
              endif;
            ?>

            <div id="adressDisplay">
              <form name="FormAddress" id="FormAddress" class="form">
              <input type="hidden" name="honey" value="">
                <label for="address"><?php _e("Your address","armor-pharma");?></label><br />
                <textarea id="address" required><?php echo $address;?></textarea>
              </form>
            </div>
            <div id="phone">
              <b><?php _e("Phone number","armor-pharma");?> : </b><?php echo get_user_meta(get_current_user_id(), 'phone', true);?>
            </div>
          </div>
          <div id="products">
            <?php
            if(empty($metaCart) && sizeof($metaCart) == 0):?>
              <p class="text-center" id="bagEmpty">
                <?php _e("Your bag is empty. Don't hesitate to get free sample or order your check_upload_mimes","armor-pharma");?><br />
                <a href="<?php echo home_url();?>" class="center">
                  <?php _e("Back Home","armor-pharma");?>
                </a>
              </p>
            <?php
            else:
              foreach ($metaCart as $lactose):
                $post = get_post( $lactose->id_lactose );
                setup_postdata( $post );?> 
                <div class='product'>
                  <h4><?php echo get_the_lactose();?></h4>
                  <div class="quality">
                    <?php echo $lactose->quantity; ?>Kg
                  </div>
                  <hr>
                  <a href="<?php echo get_the_permalink(get_page_by_path("my-cart"));?>" class="linkUpdate">
                    <span class="picto picto-update"></span><?php _e("Modify","armor-pharma");?>
                  </a>
                </div>
              <?php
              endforeach;
              wp_reset_postdata();
            endif;?>
          </div>
        </div>
			</div>
      <div class="wrapper" id="footerButtonCart">
          <button id="order" class="button"><span class="picto picto-cart"></span><?php _e("Order","armor-pharma");?></button>
      </div>

		</article>
	</main>
<?php endwhile; endif; ?>
<?php get_footer(); ?>
