<?php
/*
Template Name: Design delivery
*/

$user = get_current_user_id();
if($user == 0){
  wp_redirect( home_url(), 302 );
}
get_header();
$design = get_user_meta(get_current_user_id(), 'design', true);
$JSONdesign = $design;
$design = json_decode(stripslashes($design));
?>
<script>var design = <?php echo $JSONdesign;?>;</script>
<?php

get_header();
if (have_posts()) :  while ( have_posts() ) : the_post();
?>
<div id="content-header">
  <div class="wrapper">
    <?php the_breadcrumb();?>
  </div>
</div>
<div class="text-center">
  <h1 class="degrade"><?php _e("Design the lactose <b>you</b> like","armor-pharma");?></h1><br />
  <p class="degrade" id="titreProduct">
    <?php _e("How would you like your","armor-pharma");?> « <?php echo $design->lactose->name;?> »
  </p>
</div>
<?php
set_query_var( 'item', 5 );
get_template_part( 'template-parts/arianne', 'design' );?>
<div class="wrapper" id="contentDesign">
  <main>
    <div class="help">
      <a href="<?php echo get_the_permalink(get_page_by_path( "contact" ) );?>">
        <span><?php _e("For more complicated requests (several product, global sourcing...) Contact us directly!","armor-pharma");?></span>
      </a>
      <a class="picto-info" data-slug="help-design">
        <img src="<?php echo get_stylesheet_directory_uri() . '/assets/css/images/help.png';?>">
      </a>
    </div>
    <a id="nextDelivery" data-href="<?php echo get_the_permalink(get_page_by_path( 'design-the-lactose-you-like/congratulation' ) );?>" class="button small next"><?php _e("NEXT","armor-pharma");?></a>
    <a href="<?php echo get_the_permalink(get_page_by_path( 'design-the-lactose-you-like/packaging/' ) );?>" class="button small back"><?php _e("BACK","armor-pharma");?></a>
    <h2 class="degrade semiCercle small"><?php the_title();?></h2><br />
    <div id="delivery" class="formDesign">
      <h3><?php _e("Select the needed quantity (kg)","armor-pharma");?></h3>
      <div id="needQuantity">
        <?php $value = 100;?>
        <?php if(isset($design->delivery->needQuantity->value) && !empty($design->delivery->needQuantity->value) ){
          $value = $design->delivery->needQuantity->value;
        }
        ?>
        <div>
          100
        </div>
        <div id="needQuantitySlider"
          data-label="<?php _e("Need quantity (Kg)","armor-pharma");?>"
          class="slider"
          data-min="100"
          data-max="200000"
          data-value="<?php echo $value;?>"
          data-step="20">
          <div class="ui-slider-handle custom-handle"><span></span></div>
        </div>
        <div>
          200 000
        </div>
      </div>
      <div>
        <p>
          <b><?php _e("If you would like to get a quotation for your customized lactose, please precise your need :","armor-pharma");?></b>
        </p>

        <?php
        $value = "";
        if(isset($design->delivery->needText) && !empty($design->delivery->needText) ){
          $value = $design->delivery->needText;
        }
        ?>
        <textarea id="excpectedDelivery" placeholder="(excpected delivery date(s), if several deliveries or needed...)"><?php echo $value;?></textarea>
      </div>
      <form id="form">
        <div id="PlaceDelivery" class="form formDesign">
          <h3><?php _e("Place of Delivery:","armor-pharma");?></h3>
          <p>
            <label for="delivery_company"><?php _e("Company","armor-pharma");?></label>
            <input type="text" id="delivery_company" value="<?php echo (isset($design->delivery->company))?$design->delivery->company:'';?>" />
          </p>
          <p>

          </p>

          <p>
            <label for="delivery_address"><?php _e("Address","armor-pharma");?></label>
            <input type="text" id="delivery_address" value="<?php echo (isset($design->delivery->address))?$design->delivery->address:'';?>" />
          </p>

          <p>
            <label for="delivery_zipCode"><?php _e("ZIP code","armor-pharma");?>*</label>
            <input type="text" id="delivery_zipCode" value="<?php echo (isset($design->delivery->zipCode))?$design->delivery->zipCode:'';?>" required />
          </p>

          <p>
            <label for="delivery_city"><?php _e("City","armor-pharma");?>*</label>
            <input type="text" id="delivery_city" value="<?php echo (isset($design->delivery->city))?$design->delivery->city:'';?>" required />
          </p>

          <p>
            <label for="delivery_stateProvince"><?php _e("State/Province","armor-pharma");?></label>
            <input type="text" id="delivery_stateProvince" value="<?php echo (isset($design->delivery->stateProvince))?$design->delivery->stateProvince:'';?>" />
          </p>

          <p >
            <label for="delivery_country"><?php _e("Country","armor-pharma");?>*</label>
            <input type="text" id="delivery_country" value="<?php echo (isset($design->delivery->country))?$design->delivery->country:'';?>" required />
          </p>

          <p class="blocP">
            <input type="checkbox" required id="check" />
            <label for="check"><?php _e("By submitting your data through this form, you confirm that you are above the age of 18, that you have read and understood the Privacy Policy, and that you agree to the collection, use and processing of your Personal Information by ARMOR PHARMA in accordance with said Policy","armor-pharma");?></label>
          </p>
          <div id="mandatoryField">
            <?php _e("Mandatory field","armor-pharma");?>*
          </div>
        </div>
      </form>
    </div>
  </main>
  <div id="rightCol">


  </div>
</div>
<?php
endwhile; endif;
get_footer(); ?>
