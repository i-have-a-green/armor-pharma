<?php
/*
Template Name: Contact
*/
get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post();?>
  <div id="content-header">
    <div class="wrapper">
      <?php the_breadcrumb();?>
    </div>
  </div>
	<main id="main" role="main" class="wrapper">
		<article>
			<header class="wrapper" id="header-page">
				<h1 class="page-title degrade"><?php _e("Get in touch...","armor-pharma");?></h1><br />
        <span class="degrade"><?php _e("...With Armor Pharma team","armor-pharma");?></span>
			</header>
      <?php if(isset($_GET['askContactExcipure'])): ?>
        <script>
          document.addEventListener('DOMContentLoaded', function () {
            askContactExcipure("<?php echo $_GET['email'];?>", "<?php echo $_GET['prenom'];?>");
          });
          
        </script>  
      <?php endif;?>
      <div id="team">
        <?php if( have_rows('contacts') ):
          while ( have_rows('contacts') ) : the_row();
            ?>
            <div class="team">
              <div class="teamPicture">
                <?php
                $image = get_sub_field('image');
                $size = 'medium'; // (thumbnail, medium, large, full or custom size)
                if( $image ) {
                  echo wp_get_attachment_image( $image, $size );
                }
                ?>
                <img src="<?php echo get_stylesheet_directory_uri() . '/assets/css/images/bg-contact.png';?>" class="mask" />
              </div>
              <div class="name">
                <div>
                  <?php the_sub_field("firstname");?> <?php the_sub_field("lastname");?>
                </div>
              </div>
              <hr>
              <div class="function">
                <div><?php the_sub_field("function");?></div>
              </div>
              <p class="tel">
                <?php the_sub_field("tel");?>
              </p>
              <p>
                <button class="button small askContact" data-email="<?php the_sub_field("email");?>"><?php _e("Contact","armor-pharma");?> <?php the_sub_field("firstname");?></button>
              </p>

            </div>
            <?php
          endwhile;
        endif;?>
      </div>
      <?php
        $user = wp_get_current_user();
        $current_user_exists = $user->exists();
        $genre = ($current_user_exists)?$user->civilite:'';
        $firstname = ($current_user_exists)?$user->user_firstname:'';
        $lastname = ($current_user_exists)?$user->user_lastname:'';
        $country = ($current_user_exists)?$user->country:'';
        $phone = ($current_user_exists)?$user->phone:'';
        $profil = ($current_user_exists)?$user->profil:'';
        $company = ($current_user_exists)?$user->company:'';
        $email = ($current_user_exists)?$user->user_email:'';
      ?>
      <div class="wrapper" id="formContact">
        <form id="form-contact" name="form-contact" action="<?php the_permalink();?>" method="post" enctype="multipart/form-data">
          <input type="hidden" name="action" value="formExpert">
          <input type="hidden" name="honey" value="">
          <?php wp_nonce_field('nonceformContact', 'nonceformContact'); ?>
          <div class="form">
            <div class="colLeft">

              <div class="label">
                <label for="contact_Mr" class="notDisplayBlock">Mr</label><input type="radio" <?php echo ($genre == "Mr")?'checked':'';?> id="contact_Mr" name="genre" value="Mr" />
                <label for="contact_Mrs" class="notDisplayBlock">Mrs</label><input type="radio" <?php echo ($genre == "Mrs")?'checked':'';?> id="contact_Mrs" name="genre" value="Mrs" />
              </div>
              <div class="grid2">
                <div>
                  <label for="contact_firstname"><?php _e("First name","armor-pharma");?>*</label>
                  <input type="text" value="<?php echo $firstname;?>" name="firstname" id="contact_firstname" required />
                </div>
                <div>
                  <label for="contact_lastname"><?php _e("Last name","armor-pharma");?>*</label>
                  <input type="text" value="<?php echo $lastname;?>" name="lastname" id="contact_lastname" required />
                </div>
              </div>
              <div class="grid2">
                <div>
                  <label for="contact_country"><?php _e("Country","armor-pharma");?>*</label>
                  <input type="text" value="<?php echo $country;?>" name="country" id="contact_country" required />
                </div>
                <div>
                  <label for="contact_phone"><?php _e("Phone","armor-pharma");?></label>
                  <input type="tel"  name="phone" value="<?php echo $phone;?>" id="contact_phone"  />
                </div>
              </div>
              <div>
                <label id="contact_profil">
                  <?php _e("Profile","armor-pharma");?>*
                </label>
                <select name="profil" id="contact_profil" required>
                  <option <?php echo ($profil == "Responsible for excipients purchases")?'selected':'';?>>
                    Responsible for excipients purchases
                  </option>
                  <option <?php echo ($profil == "Working on tablets/sachets/capsules formulation")?'selected':'';?>>
                    Working on tablets/sachets/capsules formulation
                  </option>
                  <option <?php echo ($profil == "Working on Dry Powder Inhalers development")?'selected':'';?>>
                    Working on Dry Powder Inhalers development
                  </option>
                  <option <?php echo ($profil == "Student in pharmaceutical formulation")?'selected':'';?>>
                    Student in pharmaceutical formulation
                  </option>
                  <option <?php echo ($profil == "Professor/associate professor at University")?'selected':'';?>>
                    Professor/associate professor at University
                  </option>
                  <option <?php echo ($profil == "PhD student")?'selected':'';?>>
                    PhD student
                  </option>
                  <option <?php echo ($profil == "other")?'selected':'';?>>
                    other
                  </option>
                </select>
              </div>
              <div class="grid2">
                <div>
                  <label for="contact_company"><?php _e("Company","armor-pharma");?>*</label>
                  <input type="text" name="company" value="<?php echo $company;?>" id="contact_company" required />
                </div>
                <div>
                  <label for="contact_email"><?php _e("Email","armor-pharma");?>*</label>
                  <input type="email" name="email" value="<?php echo $email;?>" id="contact_email" required />
                </div>
              </div>
            </div>
            <div class="colRight">
              <div>
                <label for="contact_subject"><?php _e("Subject","armor-pharma");?></label>
                <input type="text" name="subject" id="contact_subject" required />
              </div>
              <div>
                <label for="contact_comments"><?php _e("Comments","armor-pharma");?></label>
                <textarea name="comments"  id="contact_comments" required ></textarea>
              </div>
            </div>
            <div class="colLeft"></div>
            <div class="colRight">
              <div>
                <input type="checkbox" name="check" id="contact_check" required />
                <label for="contact_check">
                  Armor Protéines as data controller implements data processing for the purpose of managing requests made via the contact form. The data is intended for the authorized department of Armor Protéines and is based on the legitimate interest of Armor Pharma to offer a high quality customer relationship.<br>
For more information about your rights and the protection of your data <a href="<?php $p = get_page_by_path( "terms-conditions" ); echo get_the_permalink( $p);?>">click here</a><br>
By clicking on « send », you confirm that you have read and understood our <a href="<?php $p = get_page_by_path( "privacy-policy-cookie-policy" ); echo get_the_permalink( $p);?>"><?php _e("Privacy Policy","armor-pharma");?></a> - & <a href="<?php echo get_privacy_policy_url();?>"><?php _e("Cookie Policy","armor-pharma");?></a></a>.
</label>
              </div>
            </div>
            <div class="col2 text-center">
              <button class="button" id="contact_sendAskExpert"><?php _e("SEND","armor-pharma");?></button>
            </div>
          </div>
        </form>
      </div>
      <?php get_template_part( 'template-parts/distributor', '' );?>
		</article>
	</main>

  
<?php endwhile; endif; ?>
<?php get_footer(); ?>
