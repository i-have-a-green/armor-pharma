<?php
get_header();
/*
Template Name: Quality Documentation
*/
?>
<?php if (have_posts()) : while (have_posts()) : the_post();?>
  <div id="content-header">
    <div class="wrapper">
      <?php the_breadcrumb();?>
    </div>
  </div>
	<main id="main" role="main" class="wrapper">
		<article>
			<header class="wrapper" id="header-page">
				<h1 class="page-title degrade"><?php the_title();?></h1>
			</header>
      <?php get_template_part( 'template-parts/header', 'quality' );?>

			<section class="entry-content wrapper" itemprop="articleBody">
        <!--<h2 id="Certificates">
          <?php _e("Certificates, statements, MSDS, flow charts...","armor-pharma");?>
        </h2>-->
        <div id="contSelectLactose">
          <div>
            <strong><?php _e("Select your product and download full related technical documentation","armor-pharma");?></strong>
          </div>
          <div class="form">
            <select id="selectLactose">
              <!--<option value="0"><?php _e("View all","armor-pharma");?></option>-->
              <?php
              $posts = get_posts(array(
                "posts_per_page"=>-1,
                "post_type"=>"lactose",
                'tax_query'      => array(
                    array(
                        'taxonomy' => 'category-lactose',
                        'terms' => array('excipure'),
                        'field' => 'slug',
                        'operator' => 'NOT IN',
                    )
                ),
                'status'      => 'publish'
              ));

              foreach ($posts as $post) :   setup_postdata( $post );
                echo '<option value="'.$post->ID.'"">'.get_the_lactose().'</option>';
              endforeach;
              wp_reset_postdata();
              ?>
            </select>
          </div>
        </div>
        <p>
          <?php if(get_current_user_id() == 0):?>
          <em><?php _e("Select needed documents to download all files at once (zip) or click on title to display document. You need to be register to get full access !","armor-pharma");?></em>
        <?php endif;?>
        </p>
        <div id="files">
        <?php
        $categorys = get_terms( 'category-quality' );
        foreach ($categorys as $category) :
          $args = array(
            "posts_per_page"=> -1,
          	'post_type' => 'quality',
          	'tax_query' => array(
              array(
          			'taxonomy' => 'category-quality',
          			'field'    => 'id',
          			'terms'    => $category->term_id,
          		)
          	),
          );


          $query = new WP_Query($args);
          if ( $query->have_posts() ) :
            echo '<div class="listing-product form" id="category_'.$category->slug.'"><h2>'.$category->name.'</h2>';
            echo '<hr class="hrTriangle" /><p class="p_all_category">
            <input type="checkbox" class="all_category" id="all_category_'.$category->term_id.'" data-category="'.$category->term_id.'" checked />
            <label for="all_category_'.$category->term_id.'"><b>'.__("Select all documents","armor-pharma").'</b></label>
            </p><br />';
          	while ( $query->have_posts() ) :
          		$query->the_post();
              echo '<p>
                <input type="checkbox" id="ckeckbox_'.get_the_id().'" class="file ';
                $lactoses = get_field("lactose");
                if(is_array($lactoses)){
                  foreach( $lactoses as $lactose){
                    echo " lactose_".$lactose;
                  }
                }
                else{
                  echo " lactose_".$lactoses;
                }
              echo ' category_'.$category->term_id.'" data-file="'.get_field('fichier').'" /><label for="ckeckbox_'.get_the_id().'">'.get_the_title().'</label>
              </p>';
          	endwhile;
          	echo '</div>';
          	wp_reset_postdata();
          endif;
        endforeach;
        ?>
        </div>
        <div id="nbFileSelected" class="text-center">
          <span>0</span> <?php _e("Files selected","armor-pharma");?>
        </div>
        <div class="text-center">
            <?php if(get_current_user_id() > 0):?>
            <a href="#" download id="downloadZipFile" class="button"><span class="picto picto-download"></span> <?php _e("Download selected items in zip file","armor-pharma");?></a>
            <?php else:?>
            <button onclick="openModalConnect(0)" id="downloadZipFile" class="button">
              <span class="picto picto-lock"></span>
              <span class="picto picto-download"></span>
              <?php _e("Download selected items in zip file","armor-pharma");?>
            </button>
          <?php endif;?>
        </div>

			</section>
		</article>
	</main>
  <?php //get_template_part( 'template-parts/faq', '' );?>
  <div id="askExpert" class="wrapper">
    <hr>
    <div class="form">
      <label for="specTxt">
        <?php _e("Can't find what you're looking for ...", "arma-pharma");?>
        <input type="text" id="specTxt" name="specTxt" placeholder="<?php _e("Type your question", "armor-pharma");?>" />
        <button class="button" id="submitAskExpert"><?php _e("Ask the expert","armar-pharma");?></button>
      </label>
    </div>
  </div>
  <!-- MODAL -->
  <div class="modal" id="modalAskExpert">
    <form id="form-expert" name="form-expert" action="<?php the_permalink();?>" method="post" enctype="multipart/form-data">
      <input type="hidden" name="action" value="formExpert">
      <input type="hidden" name="honey" value="">
      <?php wp_nonce_field('nonceformExpert', 'nonceformExpert'); ?>
      <div class="modal-content form">
        <div>
          <label for="Expert_Mr" class="notDisplayBlock">Mr</label><input type="radio" id="Expert_Mr" name="genre" value="Mr" />
          <label for="Expert_Mrs" class="notDisplayBlock">Mrs</label><input type="radio" id="Expert_Mrs" name="genre" value="Mrs" />
        </div>
        <div class="grid2">
          <div>
            <label for="firstname"><?php _e("First name","armor-pharma");?></label>
            <input type="text" name="firstname" id="firstname" required />
          </div>
          <div>
            <label for="lastname"><?php _e("Last name","armor-pharma");?></label>
            <input type="text" name="lastname" id="lastname" required />
          </div>
        </div>
        <div class="grid2">
          <div>
            <label for="Expert_country"><?php _e("Country","armor-pharma");?></label>
            <input type="text" name="country" id="Expert_country" required />
          </div>
          <div>
            <label for="Expert_phone"><?php _e("Phone","armor-pharma");?></label>
            <input type="tel" name="phone" id="Expert_phone"  />
          </div>
        </div>
        <div>
          <label id="profil">
            <?php _e("Profile","armor-pharma");?>
          </label>
          <select name="profil" id="profil" required>
            <option>
              profil 1
            </option>
            <option>
              profil 2
            </option>
            <option>
              profil 3
            </option>
          </select>
        </div>
        <div class="grid2">
          <div>
            <label for="Expert_company"><?php _e("Company","armor-pharma");?></label>
            <input type="text" name="company" id="Expert_company" required />
          </div>
          <div>
            <label for="email"><?php _e("Email","armor-pharma");?></label>
            <input type="email" name="email" id="email" required />
          </div>
        </div>
        <div class="border"></div>
        <div>
          <label for="subject"><?php _e("Subject","armor-pharma");?></label>
          <input type="text" readonly name="subject" id="subject" required />
        </div>
        <div>
          <label for="comments"><?php _e("Comments","armor-pharma");?></label>
          <textarea name="comments" id="comments" required ></textarea>
        </div>
        <div>
          <input type="checkbox" name="check" id="check" required />
          <label for="check">
            <?php _e("By submitting your data through this form, you confirm that you are above the age of 18, that you have read and understood the Privacy Policy, and that you agree to the collection, use and processing of your Personal Information by ARMOR PHARMA in accordance with said Policy","armor-pharma");?>
          </label>
        </div>
        <div class="text-center">
          <button class="button" id="sendAskExpert"><?php _e("SEND","armor-pharma");?></button>
        </div>
      </div>
    </form>
  </div>

<?php endwhile; endif; ?>
<?php get_footer(); ?>
<script>
$(document).ready(function(){
  var idLactose = <?php $obj = get_page_by_path('200-mesh', OBJECT, 'lactose');echo $obj->ID;?>;
  $("#selectLactose").val(idLactose);
  $('.listing-product input[type="checkbox"]:not(.all_category)').prop( "checked", false );
  $('.lactose_'+idLactose).prop( "checked", true );

  $('.listing-product input[type="checkbox"]').each(function(){
    if($(this).prop( "checked") == true){
      $(this).removeClass("hidden");
    }
    else{
      $(this).addClass("hidden");
    }
  });

  $('#category_other>p').each(function(){
    if($(this).find(".file").hasClass( "hidden")){
      $(this).addClass('cache');
    }
    else{
      $(this).removeClass('cache');
    }
  });

  nbFileSelected();
});
</script>
