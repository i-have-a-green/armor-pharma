<?php
get_header();
/*
Template Name: Quality link
*/
?>
<?php if (have_posts()) : while (have_posts()) : the_post();?>
  <div id="content-header">
    <div class="wrapper">
      <?php the_breadcrumb();?>
    </div>
  </div>
	<main id="main" role="main" class="">
		<article>
			<header class="wrapper" id="header-page">
				<h1 class="page-title degrade">
          <?php //the_title();?>
          <?php $p = get_page_by_path("quality"); echo $p->post_title; ?>
        </h1>
			</header>
      <div class="wrapper">
        <?php get_template_part( 'template-parts/header', 'quality' );?>
      </div>
			<section class="entry-content" itemprop="articleBody">
        <div class="wrapper">
            <?php the_content();?>
        </div>
        <div class="wrapper" id="links">
          <?php
          $posts = get_posts(array(
            'post_type'        => 'industry-link',
            'post_status'      => 'publish',
          ));

          foreach ( $posts as $post ) :
            setup_postdata( $post );
            ?>
              <div class="link">
                <h2 class="semiCercle small degrade"><?php the_title();?></h2>
                <?php
                if( have_rows('links') ):
                  echo '<ul>';
                  while ( have_rows('links') ) : the_row();
                    echo '<li>
                    <a href="'.get_sub_field('link').'" target="_blank">'.get_sub_field('label').'</a>
                    </li>';
                  endwhile;
                  echo '</ul>';
                endif;
                ?>
              </div>
            <?php
          endforeach;
          wp_reset_postdata();
          ?>
        </div>
			</section>
		</article>
	</main>
  <?php //get_template_part( 'template-parts/faq', '' );?>
  <div id="askExpert" class="wrapper">
    <hr>
    <div class="form">
      <label for="specTxt">
        <?php _e("Can't find what you're looking for ...", "arma-pharma");?>
        <input type="text" id="specTxt" name="specTxt" placeholder="<?php _e("Type your question", "armor-pharma");?>" />
        <buton class="button" id="submitAskExpert"><?php _e("Ask the expert","armar-pharma");?></buton>
      </label>
    </div>
  </div>
  <!-- MODAL -->
  <div class="modal" id="modalAskExpert">
    <form id="form-expert" name="form-expert" action="<?php the_permalink();?>" method="post" enctype="multipart/form-data">
      <input type="hidden" name="action" value="formExpert">
      <input type="hidden" name="honey" value="">
      <?php wp_nonce_field('nonceformExpert', 'nonceformExpert'); ?>
      <div class="modal-content form">
        <div>
          <label for="Expert_Mr" class="notDisplayBlock">Mr</label><input type="radio" id="Expert_Mr" name="genre" value="Mr" />
          <label for="Expert_Mrs" class="notDisplayBlock">Mrs</label><input type="radio" id="Expert_Mrs" name="genre" value="Mrs" />
        </div>
        <div class="grid2">
          <div>
            <label for="firstname"><?php _e("First name","armor-pharma");?></label>
            <input type="text" name="firstname" id="firstname" required />
          </div>
          <div>
            <label for="lastname"><?php _e("Last name","armor-pharma");?></label>
            <input type="text" name="lastname" id="lastname" required />
          </div>
        </div>
        <div class="grid2">
          <div>
            <label for="Expert_country"><?php _e("Country","armor-pharma");?></label>
            <input type="text" name="country" id="Expert_country" required />
          </div>
          <div>
            <label for="Expert_phone"><?php _e("Phone","armor-pharma");?></label>
            <input type="tel" name="phone" id="Expert_phone"  />
          </div>
        </div>
        <div>
          <label id="profil">
            <?php _e("Profile","armor-pharma");?>
          </label>
          <select name="profil" id="profil" required>
            <option>
              profil 1
            </option>
            <option>
              profil 2
            </option>
            <option>
              profil 3
            </option>
          </select>
        </div>
        <div class="grid2">
          <div>
            <label for="Expert_company"><?php _e("Company","armor-pharma");?></label>
            <input type="text" name="company" id="Expert_company" required />
          </div>
          <div>
            <label for="email"><?php _e("Email","armor-pharma");?></label>
            <input type="email" name="email" id="email" required />
          </div>
        </div>
        <div class="border"></div>
        <div>
          <label for="subject"><?php _e("Subject","armor-pharma");?></label>
          <input type="text" readonly name="subject" id="subject" required />
        </div>
        <div>
          <label for="comments"><?php _e("Comments","armor-pharma");?></label>
          <textarea name="comments" id="comments" required ></textarea>
        </div>
        <div>
          <input type="checkbox" name="check" id="check" required />
          <label for="check">
            <?php _e("By submitting your data through this form, you confirm that you are above the age of 18, that you have read and understood the Privacy Policy, and that you agree to the collection, use and processing of your Personal Information by ARMOR PHARMA in accordance with said Policy","armor-pharma");?>
          </label>
        </div>
        <div class="text-center">
          <button class="button" id="sendAskExpert"><?php _e("SEND","armor-pharma");?></button>
        </div>
      </div>
    </form>
  </div>

<?php endwhile; endif; ?>
<?php get_footer(); ?>
