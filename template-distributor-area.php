<?php
get_header();
/*
Template Name: Distributor Area
*/
?>
<?php if (have_posts()) : while (have_posts()) : the_post();?>
  <div id="content-header">
    <div class="wrapper">
      <?php the_breadcrumb();?>
    </div>
  </div>
	<main id="main" role="main" class="wrapper">
		<article>
			<header class="wrapper" id="header-page">
				<h1 class="page-title degrade"><?php the_title();?></h1>
			</header>
      <div id="header-quality">
        <div id="header-quality-content">
          <div>
            <?php the_content();?>
          </div>
        </div>
        <ul id="menu-menu-quality" class="main-menu">
          <li id="menu-item-574" class="menu-item menu-documentation current_page_item active"><a href="#"><?php _e("Documentation","armor-pharma");?></a></li>
          <li id="menu-item-574" class="menu-item menu-news current_page_item active"><a href="#"><?php _e("News","armor-pharma");?></a></li>
        </ul>
      </div>


			<section class="entry-content wrapper" itemprop="articleBody">
        <div id="">
          <div>
            <strong><?php _e("Get here all documentations you might need !","armor-pharma");?></strong>
          </div>
        </div>
        <div id="files">
        <?php
        $categorys = get_terms( 'category-doc-distributor' );
        foreach ($categorys as $category) :
          $args = array(
            "posts_per_page"=> -1,
          	'post_type' => 'doc-distributor',
          	'tax_query' => array(
              array(
          			'taxonomy' => 'category-doc-distributor',
          			'field'    => 'id',
          			'terms'    => $category->term_id,
          		)
          	),
          );


          $query = new WP_Query($args);
          if ( $query->have_posts() ) :
            echo '<div class="listing-product form"><h2>'.$category->name.'</h2>';
            echo '<hr class="hrTriangle" />';
          	while ( $query->have_posts() ) :
          		$query->the_post();
              echo '<p><a href="'.get_field("fichier").'" download="'.basename(str_replace(" ","-",get_the_title())).'.pdf">'.get_the_title().'</a></p>';
          	endwhile;
          	echo '</div>';
          	wp_reset_postdata();
          endif;
        endforeach;
        ?>
        </div>
			</section>
		</article>
	</main>
<?php endwhile; endif; ?>
<?php get_footer(); ?>
