<?php
/*
Template Name: Design packaging
*/

$user = get_current_user_id();
if($user == 0){
  wp_redirect( home_url(), 302 );
}
get_header();
$design = get_user_meta(get_current_user_id(), 'design', true);
$JSONdesign = $design;
$design = json_decode(stripslashes($design));
?>
<script>var design = <?php echo $JSONdesign;?>; </script>
<?php

get_header();
if (have_posts()) :  while ( have_posts() ) : the_post();
?>
<div id="content-header">
  <div class="wrapper">
    <?php the_breadcrumb();?>
  </div>
</div>
<div class="text-center">
  <h1 class="degrade"><?php _e("Design the lactose <b>you</b> like","armor-pharma");?></h1><br />
  <p class="degrade" id="titreProduct">
    <?php _e("How would you like your","armor-pharma");?> « <?php echo $design->lactose->name;?> »
  </p>
</div>
<?php
set_query_var( 'item', 3 );
get_template_part( 'template-parts/arianne', 'design' );?>
<div class="wrapper" id="contentDesign">
  <main>
    <div class="help">
      <a href="<?php echo get_the_permalink(get_page_by_path( "contact" ) );?>">
        <span><?php _e("For more complicated requests (several product, global sourcing...) Contact us directly!","armor-pharma");?></span>
      </a>
      <a class="picto-info" data-slug="help-design">
        <img src="<?php echo get_stylesheet_directory_uri() . '/assets/css/images/help.png';?>">
      </a>
    </div>
    <a href="<?php echo get_the_permalink(get_page_by_path( 'design-the-lactose-you-like/palletization' ) );?>" class="button small next" data-url="<?php echo get_the_permalink(get_page_by_path( 'design-the-lactose-you-like/registration' ) );?>"><?php _e("NEXT","armor-pharma");?></a>
    <a href="<?php echo get_the_permalink(get_page_by_path( 'design-the-lactose-you-like/my-customize-specification/' ) );?>" class="button small back" data-url="<?php echo get_the_permalink(get_page_by_path( 'design-the-lactose-you-like/my-customize-specification' ) );?>"><?php _e("BACK","armor-pharma");?></a>
    <h2 class="degrade semiCercle small"><?php the_title();?></h2><br />
    <div id="packagings" class="formDesign">
      <?php
        $myposts = get_posts(array('posts_per_page' => -1,"post_type" => "bag"));
        $idBagDefault = @get_field('packaging', $design->lactose->id)->ID;
        foreach ( $myposts as $post ) : setup_postdata( $post );?>
          <?php
          $checked = '';
          if(isset($design->packaging->id)){
            if($design->packaging->id == $post->ID){
              $checked = 'checked';
            }
          }
          elseif ($idBagDefault == $post->ID){
            $checked = 'checked';
          }
          ?>

          <div class="packaging" id="<?php echo $post->post_name;?>">
            <div class="checkbox">
              <input type="checkbox"
                data-standard="<?php echo ($idBagDefault == $post->ID)?'true':'false';?>"
                id="checkbox<?php echo $post->ID;?>" <?php echo $checked;?>
                data-id="<?php echo $post->ID;?>"
                data-bag="<?php echo $post->post_name;?>"
                data-is-standard="<?php echo ($idBagDefault == $post->ID)?'true':'false';?>"
                value="<?php the_title();?>" class="checkboxPackaging">
              <label for="checkbox<?php echo $post->ID;?>">
                <?php the_title();?>
              </label>
            </div>
            <div class="thumbnail">
      				<?php the_post_thumbnail( 'wpgreen-200'); ?>
      			</div>
            <div class="text">
              <?php the_field("content");?>
            </div>
            <?php if( have_rows('options') ): $i=0; $range=0; while ( have_rows('options') ) : the_row();?>
              <?php $checked = "";?>
              <?php if(get_sub_field('type') == "checkbox"):?>
                <?php if(isset($design->packaging->packagingOptions)){
                  if($design->packaging->id == $post->ID){
                    foreach ($design->packaging->packagingOptions as $option) {
                      if($option->id == $post->post_name.'_'.$i){
                        $checked = "checked";
                      }
                    }
                  }
                }?>
                <div class="option optionCheckbox">
                  <input type="checkbox"
                    id="<?php echo $post->post_name.'_'.$i;?>" <?php echo $checked;?>
                    data-bag="checkbox<?php echo $post->ID;?>"
                    value="<?php the_sub_field("label");?>" class="checkbox">
                  <label for="<?php echo $post->post_name.'_'.$i;?>">
                    <?php the_sub_field("label");?>
                  </label>
                  <?php if(get_sub_field("info")):info(get_sub_field('info')->post_name);endif;?>
                </div>
              <?php elseif(get_sub_field('type') == "radio"):?>
                <?php if(isset($design->packaging->packagingOptions)){
                  if($design->packaging->id == $post->ID){
                    foreach ($design->packaging->packagingOptions as $option) {
                      if($option->id == $post->post_name.'_'.$i){
                        $checked = "checked";
                      }
                    }
                  }
                }?>
                <div class="option optionRadio">
                  <input type="radio"
                    id="<?php echo $post->post_name.'_'.$i;?>" <?php echo $checked;?>
                    data-bag="checkbox<?php echo $post->ID;?>"
                    name="<?php echo $post->post_name.'_radio';?>"
                    value="<?php the_sub_field("label");?>" class="radio">
                  <label class="labelRadio" for="<?php echo $post->post_name.'_'.$i;?>">
                    <?php the_sub_field("label");?>
                  </label>
                  <?php if(get_sub_field("info")):info(get_sub_field('info')->post_name);endif;?>
                </div>
              <?php elseif(get_sub_field('type') == "range"):?>
                  <?php $val = get_sub_field("maxi");
                  if(isset($design->packaging->packagingOptions)){
                    if($design->packaging->id == $post->ID){
                      foreach ($design->packaging->packagingOptions as $option) {
                        if($option->id == $post->post_name.'_'.$i){
                          $val = $option->value;
                        }
                      }
                    }
                  }
                  ?>
                <div class="option range" id="range_<?php echo $range;?>">
                  <div>
                    <?php the_sub_field("label");?> <?php if(get_sub_field("info")):info(get_sub_field('info')->post_name);endif;?>
                  </div>
                  <div>
                    <?php the_sub_field("mini");?>
                  </div>
                  <div id="<?php echo $post->post_name.'_'.$i;?>"
                    data-label="<?php the_sub_field("label");?>"
                    data-bag="checkbox<?php echo $post->ID;?>"
                    class="slider"
                    data-min="<?php the_sub_field("mini");?>"
                    data-max="<?php the_sub_field("maxi");?>"
                    data-value="<?php echo $val;?>"
                    data-step="<?php the_sub_field("pas");?>
                    ">
                    <div class="ui-slider-handle custom-handle"><span></span></div>
                  </div>
                  <div>
                    <?php the_sub_field("maxi");?>
                  </div>
                </div>
              <?php else:?>
                <?php
                $val = "";
                if(isset($design->packaging->packagingOptions)){
                  if($design->packaging->id == $post->ID){
                    foreach ($design->packaging->packagingOptions as $option) {
                      if($option->id == $post->post_name.'_'.$i){
                        $val = $option->value;
                      }
                    }
                  }
                }
                ?>
                <div class="option textarea">
                  <?php the_sub_field("label");?> <?php if(get_sub_field("info")):info(get_sub_field('info')->post_name);endif;?><br />
                  <textarea
                    id="<?php echo $post->post_name.'_'.$i;?>" <?php echo $checked;?>
                    data-label="<?php the_sub_field("label");?>"
                    data-bag="checkbox<?php echo $post->ID;?>"
                    class="textarea"><?php echo $val;?></textarea>
                </div>
              <?php endif;?>
            <?php $i++;$range++; endwhile; endif;?>
          </div>
        <?php endforeach; ?>
    </div>
    <div id="packagingHelp">
      <h3><?php _e("Add extra labelling requirements :","armor-pharma");?></h3>
      <p>
        <a href="https://ipec.org/sites/default/files/files/20170515_GDP%20Guide%202017_FINAL.pdf" target="_blank"><?php _e("Our standard label is based on IPEC Good Distribution Practices Guide","armor-pharma");?></a><br />
        <?php _e("Upload your label :","armor-pharma");?>
         <input type="file" id="filePackaging" name="filePackaging" />
      </p>
      <p class="form">
        <?php _e("or Precise what should figure on it :","armor-pharma");?>
        <input type="text" class="targetPackaging"
          value="<?php echo isset($design->targetPackaging)?$design->targetPackaging:'';?>"
          id="targetPackaging">
      </p>
    </div>
  </main>
  <div id="rightCol">


  </div>
</div>
<?php
endwhile; endif;
get_footer(); ?>
